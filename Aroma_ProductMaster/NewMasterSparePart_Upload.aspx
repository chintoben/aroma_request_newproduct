﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="NewMasterSparePart_Upload.aspx.vb" Inherits="Aroma_ProductMaster.NewMasterSparePart_Upload" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>

    <table>
   
    <tr>
        <td class="style13">
            <asp:Label ID="Label19" runat="server" Text="บริษัท : " Font-Size="Medium" 
                Width="100px" Font-Bold="True"></asp:Label>
            </td>
        <td class="style14">

            <asp:CheckBox ID="chkKVN" runat="server" 
                Text="บจ. เค.วี.เอ็น.อิมปอร์ต เอ็กซ์ปอร์ต  (1991)" />
              
        </td>
        <td class="style2">

            &nbsp;&nbsp;

            <asp:CheckBox ID="chkLION" runat="server" 
                Text="บจ. ไลอ้อน ทรี-สตาร์" Checked="True" Width="200px" />
        </td>
        <td>

            <asp:Label ID="Label89" runat="server" Text="อื่นๆ ระบุ :" Font-Size="Medium"></asp:Label>

            </td>
        <td class="style2">

            <asp:TextBox ID="txtCompany" runat="server" Width="200px" Height="30px"></asp:TextBox>
              
        </td>
    </tr>

   
    <tr>
        <td class="style13">
            <asp:Label ID="Label29" runat="server" Text="ชื่อผู้ขอ : " Font-Size="Medium" 
                Width="100px" Font-Bold="True"></asp:Label>
            </td>
        <td class="style14">

            <asp:DropDownList ID="drpRequester" runat="server" Width="300px" Height="30px">
            </asp:DropDownList>
              
        </td>
        <td class="style2">

            &nbsp;</td>
        <td>
            &nbsp;</td>
        <td class="style2">

            &nbsp;</td>
    </tr>

 
    <tr>
        <td class="style13">
            &nbsp;</td>
        <td class="style14">

            &nbsp;</td>
        <td class="style2">

            &nbsp;</td>
        <td>
            &nbsp;</td>
        <td class="style2">

            &nbsp;</td>
    </tr>


    </table>

    
     <table>
            
                
                
                <tr>
                    <td class="style2">
                        &nbsp;</td>
                    <td class="style2" colspan="2">

                                              

                              <li><a href="Template/Template Import Item.xlsx" target="_blank">   Download Template สำหรับ Upload Item</a></li>
                    </td>
                    <td class="style2">

                                              

                              &nbsp;</td>
                </tr>
                
                
                
                <tr>
                    <td class="style2">
                        &nbsp;</td>
                    <td>
                        <asp:Label ID="Label51" runat="server" Font-Bold="True" Font-Size="Medium" 
                            ForeColor="Red" Text="*"></asp:Label>
                        <asp:Label ID="Label41" runat="server" Font-Bold="True" Text="เลือกไฟล์ (Excel)"></asp:Label>
                    </td>
                    <td class="style1">
                       <input id="Attach1" style="WIDTH: 100%; " type="file" name="Attach1" 
                            runat="server"   ></td>
                    <td class="style1">

                                              <%--<asp:Button ID="btnSubmit" runat="server" Text="อัพโหลด"  
                              class="btn btn-success"  Height="30px" Width="82px" />--%>

                                <asp:Button ID="btnSubmit" runat="server" Text="Upload" class="btn btn-primary" 
                Font-Bold="True" Font-Size="Small" 
                 Height="30px" Width="100px"/>

                                </td>
                </tr>
                
                
                <tr>
                    <td class="style2">
                        <asp:TextBox ID="TextBox1" runat="server" BorderColor="White" BorderWidth="0px" 
                            Width="50px"></asp:TextBox></td>
                    <td class="style2"></td>
                    <td class="style3">
                        <p class="help-block">
												
                                                    <asp:Label ID="Label3" runat="server" Text="some help text here." 
                                                        Font-Bold="False"></asp:Label>

                                                    <br />

                                                      <asp:Label ID="lblMissItem" runat="server" Text=""></asp:Label>

                                                    
												</p>

                                                  <%--<asp:listbox id="lstMissItem" style="Z-INDEX: 103; POSITION: absolute; TOP: 484px; left: 33px;"
				                                    runat="server" >		
			                                    </asp:listbox>--%>
            			
                        	                    <asp:datagrid id="Gridview2" style="Z-INDEX: 101; POSITION: absolute; TOP: 300px; LEFT: 282px; right: 543px;"
				                                    runat="server" CellPadding="4" ForeColor="#333333" GridLines="None" >
                                                    <AlternatingItemStyle BackColor="White" />
                                                    <EditItemStyle BackColor="#2461BF" />
                                                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                    <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                    <ItemStyle BackColor="#EFF3FB" />
                                                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                                    <SelectedItemStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                                </asp:datagrid>
			
			                                    
                              <asp:Table id="Table2" 
                                                    runat="server"
				                                    GridLines="Both"></asp:Table>
                    
                    </td>
                    <td class="style3">
                        &nbsp;</td>
                </tr>
                
                
             
                
                <tr>
                    <td class="style2">
                        &nbsp;</td>
                    <td class="style2" colspan="3">
            <asp:Label ID="Label90" runat="server" Text="* กรุณากด Refresh เพื่อดูข้อมูล" 
                            Font-Size="Small" Font-Bold="False" ForeColor="#006600"></asp:Label>
                    </td>
                </tr>
                        
                </table>


            <script src="https://code.jquery.com/jquery-2.1.1.min.js" type="text/javascript"></script>
            <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2-rc.1/css/select2.min.css" rel="stylesheet" />
            <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2-rc.1/js/select2.min.js" type="text/javascript"></script>
            <script type="text/javascript">

                $(document).ready(function () {
                    $("#drpRequester").select2({
                    });
                });
                
            </script>  

    </div>
    </form>
</body>
</html>
