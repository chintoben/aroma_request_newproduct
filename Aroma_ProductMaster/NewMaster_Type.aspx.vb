﻿Public Class NewMaster_Type
    Inherits System.Web.UI.Page
    Dim username As String


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        username = Request.QueryString("usr")
    End Sub

    Protected Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Server.Transfer("NewMasterMachine.aspx?&usr=" & username & "")
    End Sub

    Protected Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Server.Transfer("NewMasterRawMat.aspx?&usr=" & username & "")
    End Sub

    Protected Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Server.Transfer("NewMasterSparePart.aspx?&usr=" & username & "")
    End Sub
End Class