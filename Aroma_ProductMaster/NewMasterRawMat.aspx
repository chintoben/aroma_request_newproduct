﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="NewMasterRawMat.aspx.vb" Inherits="Aroma_ProductMaster.NewMasterRawMat" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
 <title>Product Master</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="Your description">
<meta name="keywords" content="Your keywords">
<meta name="author" content="Your name">
<link rel="icon" href="images/favicon.ico" type="image/x-icon">
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />

<link rel="stylesheet" href="css/bootstrap.css" type="text/css" media="screen">
<link rel="stylesheet" href="css/bootstrap-responsive.css" type="text/css" media="screen">    
<link rel="stylesheet" href="css/camera.css" type="text/css" media="screen">
<link rel="stylesheet" href="css/style.css" type="text/css" media="screen">

<script type="text/javascript" src="js/jquery.js"></script>  
<script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="js/superfish.js"></script>

<script type="text/javascript" src="js/jquery.ui.totop.js"></script>

<script type="text/javascript" src="js/camera.js"></script>
<script type="text/javascript" src="js/jquery.mobile.customized.min.js"></script>
<script>
    $(document).ready(function () {
        // camera
        $('#camera_wrap').camera({
            //thumbnails: true
            autoAdvance: true,
            mobileAutoAdvance: true,
            //fx					: 'simpleFade',
            height: '45%',
            hover: false,
            loader: 'none',
            navigation: false,
            navigationHover: true,
            mobileNavHover: true,
            playPause: false,
            pauseOnClick: false,
            pagination: true,
            time: 7000,
            transPeriod: 1000,
            minHeight: '200px'
        });

    }); //
    $(window).load(function () {
        //

    }); //
</script>		
    <style type="text/css">
        .style1
        {
            height: 49px;
        }
        .style2
        {
            width: 268435456px;
        }
        .style3
        {
            height: 22px;
        }
        .style4
        {
            width: 6px;
        }
        .style5
        {
            width: 112px;
        }
        .style8
        {
            height: 25px;
        }
        .style10
        {
            width: 6px;
            height: 44px;
        }
        .style11
        {
            height: 44px;
        }
        .style12
        {
            width: 112px;
            height: 42px;
        }
        .style13
        {
            height: 27px;
        }
        .style14
        {
            height: 29px;
        }
        .style15
        {
            height: 45px;
        }
        .style16
        {
            height: 42px;
        }
        .style17
        {
            height: 21px;
        }
    </style>
<!--[if lt IE 8]>
		<div style='text-align:center'><a href="http://www.microsoft.com/windows/internet-explorer/default.aspx?ocid=ie6_countdown_bannercode"><img src="http://www.theie6countdown.com/images/upgrade.jpg"border="0"alt=""/></a></div>  
	<![endif]-->    

<!--[if lt IE 9]>
  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>      
  <link rel="stylesheet" href="css/ie.css" type="text/css" media="screen">
<![endif]-->
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <div id="main">
<div class="top1_wrapper">
<div class="top1">
<div class="container">
<div class="row">
<div class="span12">
<div class="top1_inner clearfix">
	
<div class="top2 clearfix">
<header><div class="logo_wrapper"><a href="index.html" class="logo">
        <img src="images/aroma/logotrans.png" alt="" >
        <%--<img src="images/aroma/Lion.png" alt="" >--%>
        </a></div>
        
  </header>	
<div class="top3 clearfix">
<div class="phone1">
	<div class="txt1">Request New Master</div>
	<div class="txt2">Product</div>
</div>

<div class="search-form-wrapper clearfix">
    <%--<form id="search-form" action="search.php" method="GET" accept-charset="utf-8" class="navbar-form" >
	<input type="text" name="s" value='Search' onBlur="if(this.value=='') this.value='Search'" onFocus="if(this.value =='Search' ) this.value=''">
	<a href="#" onClick="document.getElementById('search-form').submit()"></a>
</form>	--%>
</div>
</div>

</div>

<div class="menu_wrapper">
<div class="navbar navbar_">
	<div class="navbar-inner navbar-inner_">
		<a class="btn btn-navbar btn-navbar_" data-toggle="collapse" data-target=".nav-collapse_">
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</a>
		<div class="nav-collapse nav-collapse_ collapse">
		<ul class="nav sf-menu clearfix">
            <li class="active"><a href="Main.aspx?usr=<%=Request.QueryString("usr") %>">Home</a></li>
            <li><a href="NewMaster_Type.aspx?usr=<%=Request.QueryString("usr") %>">Request New Master</a></li>
            <li><a href="NewMasterAll.aspx?usr=<%=Request.QueryString("usr") %>">Request New Master All</a></li>
            <li><a href="ProductMaster.aspx?usr=<%=Request.QueryString("usr") %>">Product Master</a></li>           
            <li><a href="Maintain_ProductType.aspx?usr=<%=Request.QueryString("usr") %>" target="_blank">จัดการสินค้า</a></li>
           
            <li><a href="Login.aspx">Sign Out</a></li>										
            </ul>
		</div>
	</div>
</div>	
</div>

<%--<div id="slider_wrapper">
<div id="slider" class="clearfix">
<div id="camera_wrap">
	<div data-src="images/slide01.jpg">
		<div class="camera_caption fadeIn">
			<div class="txt1">keep your home clean</div>								
		</div>     
	</div>
	<div data-src="images/slide02.jpg">
		<div class="camera_caption fadeIn">
			<div class="txt1">keep your home clean</div>								
		</div>     
	</div>
	<div data-src="images/slide03.jpg">
		<div class="camera_caption fadeIn">
			<div class="txt1">keep your home clean</div>								
		</div>     
	</div>
	<div data-src="images/slide04.jpg">
		<div class="camera_caption fadeIn">
			<div class="txt1">keep your home clean</div>								
		</div>     
	</div>
	<div data-src="images/slide05.jpg">
		<div class="camera_caption fadeIn">
			<div class="txt1">keep your home clean</div>								
		</div>     
	</div>
</div>	
</div>	
</div>--%>


</div>	
</div>	
</div>	
</div>	
</div>	
</div>
<div id="inner">

<div id="content">
<div class="container">
<div class="row">
<div class="span12">


<%--<div class="breadcrumbs1"><a href="Main.aspx?usr=<%=Request.QueryString("usr") %>">Home Page</a><span></span>Request New Master</div>--%>
<div class="breadcrumbs1">
<a href="Main.aspx?usr=<%=Request.QueryString("usr") %>">Home Page</a>
<span></span>

<a href="NewMaster_Type.aspx?usr=<%=Request.QueryString("usr") %>">Request New Master</a>
<span></span>
Raw Material
</div>

<div class="row">
<div class="span8">

<h2>ตั้งรหัสสินค้าใหม่ : Raw Material</h2>

</div>	

</div>


            <div class="banners">
            <div class="row">

   <table bgcolor="#CCCCCC">
   
   
 

   
    <tr>
        <td class="style13">

            <asp:Label ID="Label127" runat="server" Text="Doc No. " Font-Size="Medium" 
                Width="220px" Font-Bold="True"></asp:Label>
            </td>
        <td class="style14">

      
            <asp:Label ID="lblRequestID" runat="server" Text="ID" Font-Size="Medium" 
                Width="200px" Font-Bold="True" ForeColor="#0000CC"></asp:Label>
            </td>
        <td colspan="2" class="style14">

           
            &nbsp;</td>
        <td class="style2">


            &nbsp;</td>
        <td class="style2">

            &nbsp;</td>
    </tr>

   

   
   
    <tr>
        <td class="style13">

            <asp:Label ID="Label126" runat="server" Text="สถานะ : " Font-Size="Medium" 
                Width="220px" Font-Bold="True"></asp:Label>
            </td>
        <td class="style14">

      
            <asp:Label ID="lblStatus" runat="server" Text="status" Font-Size="Medium" 
                Width="300px" Font-Bold="True" ForeColor="#009900"></asp:Label>
            </td>
        <td colspan="2" class="style14">

           
            &nbsp;</td>
        <td class="style2">


            &nbsp;</td>
        <td class="style2">

            &nbsp;</td>
    </tr>

   

   
   
    <tr>
        <td class="style13">

            <asp:Label ID="Label1" runat="server" Text="บริษัท : " Font-Size="Medium" 
                Width="220px" Font-Bold="True"></asp:Label>
            </td>
        <td class="style14">

      
            <asp:CheckBox ID="chkKVN" runat="server" 
                Text="บจก. เค.วี.เอ็น.อิมปอร์ต เอ็กซ์ปอร์ต  (1991)" Width="250px" 
                Checked="True" />
              
        </td>
        <td colspan="2" class="style14">

           
            <asp:CheckBox ID="chkLION" runat="server" 
                Text="บจก. ไลอ้อน ทรี-สตาร์" Width="129px" />

        </td>
        <td class="style2">


            <asp:Label ID="Label54" runat="server" Text="อื่นๆ ระบุ :" Font-Size="Medium" 
                Width="70px"></asp:Label>

        </td>
        <td class="style2">

            <asp:TextBox ID="txtOther" runat="server" Width="150px" Height="30px"></asp:TextBox>
              
        </td>
    </tr>

   

   
    <tr>
        <td class="style13">

            <asp:Label ID="Label2" runat="server" Text="ชื่อผู้ขอ : " Font-Size="Medium" 
                Width="100px" Font-Bold="True"></asp:Label>
            </td>
        <td class="style14">

      
            <asp:DropDownList ID="drpRequester" runat="server" Width="250px" 
                AutoPostBack="True" Font-Size="Medium" Height="35px">
            </asp:DropDownList>
              
        </td>
        <td class="style14">

           
            <asp:Label ID="Label3" runat="server" Text="แผนกผู้ขอ" Font-Size="Medium" 
                Width="80px"></asp:Label>
              
        </td>
        <td class="style14">

  

            <asp:TextBox ID="txtDep" runat="server" Height="30px" Width="250px"></asp:TextBox>
              
        </td>
        <td class="style2">


            <asp:Label ID="Label5" runat="server" Text="วันที่ :" Font-Size="Medium" 
                Width="50px" Font-Bold="False" Height="20px"></asp:Label>
        </td>
        <td class="style2">

             <asp:TextBox ID="txtRequestDate" runat="server" class="form-control" 
                 placeholder="yyyy-MM-DD" ClientIDMode="Static" 
                            Width="150px" Height="30px" ></asp:TextBox>
              
        </td>
    </tr>

   

   
    <tr>
        <td class="style13">
            &nbsp;</td>
        <td colspan="3" class="style14">

             &nbsp;</td>
        <td colspan="2" class="style2">

            &nbsp;</td>
    </tr>

   </table>
   <table>
   

    <tr>
        <td class="style8">
            <br />
            <asp:Label ID="Label6" runat="server" 
                Text="วัตถุประสงค์ที่ขอตั้งรหัสสินค้าใหม่ : " Font-Size="Medium" 
                Width="220px" Font-Bold="True"></asp:Label>
        </td>
        <td>

         

            <asp:RadioButtonList ID="rdoObjective1" runat="server" RepeatColumns="3" 
                Width="700px" Font-Names="tahoma">
                  <asp:ListItem Value="สินค้าสำเร็จรูป" Selected="True">สินค้าสำเร็จรูป</asp:ListItem>
                  <asp:ListItem Value="วัตถุดิบ/บรรจุภัณฑ์ (โรงงาน)">วัตถุดิบ/บรรจุภัณฑ์ (โรงงาน)</asp:ListItem>           
                  <asp:ListItem Value="วัตถุดิบ/บรรจุภัณฑ์ (หน้าร้าน)">วัตถุดิบ/บรรจุภัณฑ์ (หน้าร้าน)</asp:ListItem>
            </asp:RadioButtonList>

        </td>
    </tr>

    <tr>
        <td class="style8">
            &nbsp;</td>
        <td align ="rigth">

           

            <asp:RadioButtonList ID="rdoObjective" runat="server" RepeatColumns="3" 
                Width="700px" Font-Names="tahoma">
           <%--       <asp:ListItem Value="aaa">สินค้าผลิต</asp:ListItem>--%>
                    <asp:ListItem Value="สินค้าซื้อมาขายไป" Selected="True">สินค้าซื้อมาขายไป</asp:ListItem>  
                 <asp:ListItem Value="สินค้าผลิต">สินค้าผลิต</asp:ListItem>
                  <asp:ListItem Value="สินค้าระหว่างกลุ่มอโรม่า">สินค้าระหว่างกลุ่มอโรม่า</asp:ListItem>
                  <asp:ListItem Value="สินค้าชุด/Promotion">สินค้าชุด/Promotion</asp:ListItem>
                  <asp:ListItem Value="อื่นๆ">อื่นๆ</asp:ListItem>
             
            </asp:RadioButtonList>

        </td>
    </tr>
   
    <tr>
        <td class="style8">
            &nbsp;</td>
        <td align ="rigth">
            <asp:Label ID="Label9" runat="server" Text="อื่นๆ ระบุ :"></asp:Label>
            <asp:TextBox ID="txtObject" runat="server" Width="200px" Height="30px"></asp:TextBox>
        </td>
    </tr>
   
    <tr>
        <td class="style8">
            &nbsp;</td>
        <td align ="rigth">
            &nbsp;</td>
    </tr>
   </table>

   <table>

    <tr>
        <td class="style1">
            <asp:Label ID="Label43" runat="server" Text="ประเภทสินค้า : " 
                Font-Size="Medium" Font-Bold="True" Width="220px"></asp:Label>
            <br />
            <br />
            <br />
        </td>
        <td align ="rigth" width="100%" class="style1">


            <asp:RadioButtonList ID="rdoItem_Type" runat="server" RepeatColumns="3" 
                Width="800px" Font-Names="tahoma">
                 <asp:ListItem Value="วัตถุดิบ (ใช้ผลิตสินค้า)" Selected="True">วัตถุดิบ (ใช้ผลิตสินค้า)</asp:ListItem>
                  <asp:ListItem Value="สินค้าสำเร็จรูป (เพื่อจำหน่าย)">สินค้าสำเร็จรูป (เพื่อจำหน่าย)</asp:ListItem>
                  <asp:ListItem Value="บรรจุภัณฑ์(หีบ/ห่อ)">บรรจุภัณฑ์(หีบ/ห่อ)</asp:ListItem>
                  <asp:ListItem Value="สินค้าตัวอย่าง">สินค้าตัวอย่าง</asp:ListItem>
                  <asp:ListItem Value="วัสดุสิ้นเปลือง">วัสดุสิ้นเปลือง</asp:ListItem>
                   <asp:ListItem Value="สินค้าเพื่อส่งเสริมการขาย">สินค้าเพื่อส่งเสริมการขาย</asp:ListItem>
            </asp:RadioButtonList>


        </td>
    </tr>

    <tr>
        <td class="style8">
           
            &nbsp;</td>
        <td align ="rigth">
          
            &nbsp;</td>
    </tr>
       </table>


    <table>
    
 
    <tr>
        <td class="style8">
           
            <asp:Label ID="Label102" runat="server" Text="กลุ่มสินค้าที่ซื้อ (Product Category) : " 
                Font-Size="Medium" Font-Bold="True" Width="220px"></asp:Label>
                </td>


        <td align="rigth" >
          
            <asp:RadioButtonList ID="rdoItem_ProductCat" runat="server" RepeatColumns="6" 
                Width="800px" Font-Names="tahoma" Font-Size="Small">
                 <asp:ListItem Value="Coffee" Selected="True">Coffee</asp:ListItem>
                 <asp:ListItem Value="Fruit Concentrated">Fruit Concentrated</asp:ListItem>
                 <asp:ListItem Value="Barista Tools">Barista Tools</asp:ListItem>
                 <asp:ListItem Value="Cocoa">Cocoa</asp:ListItem>
                 <asp:ListItem Value="Sauce">Sauce</asp:ListItem>
                 <asp:ListItem Value="Hario">Hario</asp:ListItem>
                 <asp:ListItem Value="Condiment">Condiment</asp:ListItem>
                 <asp:ListItem Value="Syrup">Syrup</asp:ListItem>
                 <asp:ListItem Value="Supply Use">Supply Use</asp:ListItem>
                 <asp:ListItem Value="Tea">Tea</asp:ListItem>
                 <asp:ListItem Value="Topping">Topping</asp:ListItem>
                 <asp:ListItem Value="ส่งเสริมการขาย">ส่งเสริมการขาย</asp:ListItem>
                 <asp:ListItem Value="Powder Mixed">Powder Mixed</asp:ListItem>
                 <asp:ListItem Value="Bakery">Bakery</asp:ListItem>
                 <asp:ListItem Value="เครื่องชง/เครื่องฯ">เครื่องชง/เครื่องฯ</asp:ListItem>
                 <asp:ListItem Value="ชุดชงพร้อมผงผสม">ชุดชงพร้อมผงผสม</asp:ListItem>
                 <asp:ListItem Value="อาหาร">อาหาร</asp:ListItem>
                 <asp:ListItem Value="อะไหล่">อะไหล่</asp:ListItem>
            </asp:RadioButtonList>


        </td>
    </tr>

    <tr>
        <td class="style22">
            &nbsp;</td>
        <td class="style23">
            &nbsp;</td>
    </tr>

    </table>
    
    <table>
    <tr>
        <td colspan="10" class="style12">
            <asp:Label ID="Label19" runat="server" Text="รายละเอียดสินค้าในระบบ (กรณีมีสินค้าหลายรายการกรุณาแนบเอกสารประกอบ) " 
                Font-Size="Large" Font-Bold="True" Width="800px"></asp:Label></td>
    </tr>
    <tr>
        <td class="style8">
            <asp:Label ID="Label30" runat="server" Text="รหัสสินค้า :" 
                Font-Bold="False" Font-Size="Medium"></asp:Label>
            </td>
        <td colspan="9">
            <asp:TextBox ID="txtItem" runat="server" Width="200px" Height="30px"></asp:TextBox>
            <asp:Label ID="Label121" runat="server" Text="(บัญชีระบุ รหัสสินค้า)" 
                Font-Bold="False"></asp:Label>
        </td>
    </tr>
   
   
  
   
  
    <tr>
        <td class="style8">
            <asp:Label ID="Label20" runat="server" Text="ชื่อสินค้าในระบบ" 
                Font-Bold="False" Font-Size="Medium"></asp:Label>
            </td>
        <td colspan="9">
            <asp:TextBox ID="txtItemDescription" runat="server" Width="100%" Height="30px"></asp:TextBox>
        </td>
    </tr>
   
   
    
    <tr>
        <td class="style3">
            <asp:Label ID="Label103" runat="server" Text="ขนาดบรรจุ" 
                Font-Bold="False" Font-Size="Medium"></asp:Label>
            </td>
        <td class="style3" colspan="3">
            <asp:TextBox ID="txtPaking" runat="server" Width="200px" 
                Height="30px"></asp:TextBox>
        </td>
        <td class="style3" colspan="3">
            </td>
        <td align =rigth class="style3" colspan="3"> 
            </td>
    </tr>
  
    
   
    
    <tr>
        <td class="style3">
            &nbsp;</td>
        <td class="style3" colspan="9">


          <%--  <asp:RadioButtonList ID="rdoUnit" runat="server" RepeatColumns="12" 
                Width="800px" Font-Names="tahoma">
                 <asp:ListItem Selected="True">ซอง</asp:ListItem>
                  <asp:ListItem>ชิ้น</asp:ListItem>
                  <asp:ListItem>ขวด</asp:ListItem>
                   <asp:ListItem>ใบ</asp:ListItem>
                  <asp:ListItem>เครื่อง</asp:ListItem>
                  <asp:ListItem>เมตร</asp:ListItem>
                 <asp:ListItem>กิโลกรัม</asp:ListItem>
                 <asp:ListItem>แถว</asp:ListItem>
                 <asp:ListItem>แพ็ค</asp:ListItem>
                 <asp:ListItem>กล่อง</asp:ListItem>
                 <asp:ListItem>หีบ</asp:ListItem>
            </asp:RadioButtonList>--%>

       

        </td>
    </tr>
  
    
  
    
    <tr>
        <td class="style17">
            <asp:Label ID="Label106" runat="server" Text="หน่วยสินค้า" 
                Font-Bold="False" Font-Size="Medium"></asp:Label>
            </td>
        <td class="style17">


            <asp:CheckBox ID="chkUnit1" runat="server" 
                Text="กรัม" Width="100px" 
                Checked="True" />
              
        </td>
        <td class="style17">


            <asp:CheckBox ID="chkUnit2" runat="server" 
                Text="ซอง" Width="100px" />
              
        </td>
        <td class="style17" colspan="2">


            <asp:CheckBox ID="chkUnit3" runat="server" 
                Text="ชิ้น" Width="100px" />
              
        </td>
        <td class="style17">


            <asp:CheckBox ID="chkUnit4" runat="server" 
                Text="ขวด" Width="100px" />
              
        </td>
        <td class="style17" colspan="2">


            <asp:CheckBox ID="chkUnit5" runat="server" 
                Text="ใบ" Width="100px" />
              
        </td>
        <td class="style17">


            <asp:CheckBox ID="chkUnit6" runat="server" 
                Text="เครื่อง" Width="100px" />
              
        </td>
        <td class="style17">


            <asp:CheckBox ID="chkUnit13" runat="server" 
                Text="ดวง" Width="100px" />
              
        </td>
        <td class="style17">


        </td>
    </tr>
  
    
  
    
    <tr>
        <td class="style3">
            &nbsp;</td>
        <td class="style3">


            <asp:CheckBox ID="chkUnit7" runat="server" 
                Text="เมตร" Width="100px" />
              
        </td>
        <td class="style3">


            <asp:CheckBox ID="chkUnit8" runat="server" 
                Text="กก." Width="100px" />
              
        </td>
        <td class="style3" colspan="2">


            <asp:CheckBox ID="chkUnit9" runat="server" 
                Text="แถว" Width="100px" />
              
        </td>
        <td class="style3">


            <asp:CheckBox ID="chkUnit10" runat="server" 
                Text="แพ็ค" Width="100px" />
              
        </td>
        <td class="style3" colspan="2">


            <asp:CheckBox ID="chkUnit11" runat="server" 
                Text="กล่อง" Width="100px" />
              
        </td>
        <td class="style3">


            <asp:CheckBox ID="chkUnit12" runat="server" 
                Text="หีบ" Width="100px" />
              
        </td>
        <td class="style17">


            <asp:CheckBox ID="chkUnit14" runat="server" 
                Text="ชุด" Width="100px" />
              
        </td>
        <td class="style3">


            &nbsp;</td>
    </tr>
  
    
  
    <tr>
        <td class="style8">
            <asp:Label ID="Label21" runat="server" Text="หน่วยนับเล็กสุด  : " 
                Font-Bold="False" Font-Size="Medium"></asp:Label>
            </td>
        <td class="style18" colspan="3">

            <asp:DropDownList ID="txtUnitsmall" runat="server" Width="200px" Height="35px">
            </asp:DropDownList>
        </td>
        <td class="style4" colspan="3">
            <asp:Label ID="Label104" runat="server" Text="หน่วยนับหลัก  : " 
                Font-Bold="False" Width="100px" Font-Size="Medium"></asp:Label>
            </td>
        <td align ="rigth" colspan="3"> 

            <asp:DropDownList ID="txtUnitbig" runat="server" Width="200px" Height="35px" 
                Font-Size="Medium">
            </asp:DropDownList>
            </td>
    </tr>
    <tr>
        <td class="style11">
            <asp:Label ID="Label105" runat="server" Text="Barcode (Pcs)  : " 
                Font-Bold="False" Font-Size="Medium"></asp:Label>
            </td>
        <td class="style11" colspan="3">

            <asp:TextBox ID="txtbarcodepcs" runat="server" Width="200px" 
                Height="30px"></asp:TextBox>
        </td>
        <td class="style11" colspan="3">

            <asp:Label ID="Label107" runat="server" Text="Barcode (Case)  : " 
                Font-Bold="False" Font-Size="Medium" Width="140px"></asp:Label>
            </td>
        <td class="style11" colspan="3">

            <asp:TextBox ID="txtbarcodecase" runat="server" Width="200px" 
                Height="30px"></asp:TextBox>
        </td>
    </tr>
  
    
    <tr>
        <td class="style8">
            <asp:Label ID="Label33" runat="server" Text="* Product Type  : " 
                Font-Bold="False" Height="40px" Font-Italic="False" Font-Size="Medium"></asp:Label>
            </td>
        <td class="style18" colspan="3">
            <asp:DropDownList ID="drpProductType" runat="server" Width="400px" 
                AutoPostBack="True" Height="35px" Font-Size="Medium">
            </asp:DropDownList>
        </td>
        <td class="style4" colspan="3">
            &nbsp;</td>
        <td align =rigth colspan="3"> 
            &nbsp;</td>
    </tr>
  
    
  
    
    <tr>
        <td class="style8">
            <asp:Label ID="Label34" runat="server" Text="* Product Category  : " 
                Font-Bold="False" Height="40px" Font-Size="Medium" Width="220px"></asp:Label>
            </td>
        <td class="style18" colspan="3">
            <asp:DropDownList ID="drpProductCat" runat="server" Width="400px" 
                AutoPostBack="True" Height="35px" Font-Size="Medium">
            </asp:DropDownList>
        </td>
        <td class="style4" colspan="3">
            &nbsp;</td>
        <td align =rigth colspan="3"> 
            &nbsp;</td>
    </tr>
  
    
  
    
    <tr>
        <td class="style8">
            <asp:Label ID="Label35" runat="server" 
                Text=" * Product Group : " Font-Bold="False" Width="200px" 
                Height="40px" Font-Size="Medium"></asp:Label>
            </td>
        <td class="style18" colspan="3">
            <asp:DropDownList ID="drpProductGroup" runat="server" Width="400px" 
                Height="35px" Font-Size="Medium">
            </asp:DropDownList>
        </td>
        <td class="style4" colspan="3">
            &nbsp;</td>
        <td align =rigth colspan="3"> 
            &nbsp;</td>
    </tr>
  
    
  
   
    <tr>
        <td class="style8">
            <asp:Label ID="Label128" runat="server" 
                Text="Brand : " Font-Bold="False" Width="200px" 
                Height="40px" Font-Size="Medium"></asp:Label>
            </td>
        <td class="style18" colspan="3">
            <asp:DropDownList ID="drpBrand" runat="server" Width="400px" 
                Height="35px" Font-Size="Medium">
            </asp:DropDownList>
        </td>
        <td class="style4" colspan="3">
            &nbsp;</td>
        <td align =rigth colspan="3"> 
            &nbsp;</td>
    </tr>
  
    
  
   
    <tr>
        <td class="style8">
            &nbsp;</td>
        <td class="style18" colspan="3">
            &nbsp;</td>
        <td class="style4" colspan="3">
            &nbsp;</td>
        <td align =rigth colspan="3"> 
            &nbsp;</td>
    </tr>
  
    
  
   
    </table>

   
      
        <asp:Panel ID="Panel_Purchase" runat="server">
         <table>
         <tr>
        <td>
            <asp:Label ID="Label7" runat="server" Text="ข้อมูลการซื้อ" 
                Font-Size="Large" Font-Bold="True"></asp:Label>
            </td>
        <td >
            &nbsp;</td>
        <td>
            &nbsp;</td>
        <td > 
            &nbsp;</td>
        <td > 
            &nbsp;</td>
        <td >

            &nbsp;</td>
        <td >
            &nbsp;</td>
        <td>
            &nbsp;</td>
        <td>

            &nbsp;</td>
        <td>
            &nbsp;</td>
        <td>
            &nbsp;</td>
        <td>
            &nbsp;</td>
    </tr>



    <tr>
        <td>
            <asp:Label ID="Label114" runat="server" Text="ชื่อ Vendor" 
                Font-Size="Medium" Font-Bold="False" Width="150px"></asp:Label>
            </td>
        <td colspan="11" >

               <asp:TextBox ID="txtvendor" runat="server" Width="100%" 
                Height="30px" Font-Size="Medium"></asp:TextBox>

            </td>
    </tr>



    <tr>
        <td>
            <asp:Label ID="Label115" runat="server" Text="ชื่อเรียกสินค้า Vendor" 
                Font-Size="Medium" Font-Bold="False" Width="150px"></asp:Label>
            </td>
        <td colspan="11" >

            <asp:TextBox ID="txtvendor_item" runat="server" Width="100%" 
                Height="30px" Font-Size="Medium"></asp:TextBox>
        </td>
    </tr>


    <tr>
        <td>
            <asp:Label ID="Label47" runat="server" Text="ราคาซื้อ ปลีก (ไม่รวมVAT)" 
                Font-Size="Medium" Font-Bold="False" Width="220px"></asp:Label>
            </td>
        <td >
            <asp:TextBox ID="txtPurchasePrice_retail" runat="server" Width="100px" 
                Height="30px">0</asp:TextBox>
        </td>
        <td>
            <asp:Label ID="Label76" runat="server" Text="บาท/" 
                Font-Size="Small" Font-Bold="False" Width="30px"></asp:Label>
            </td>
        <td > 
            &nbsp;</td>
        <td > 
            <asp:Label ID="Label53" runat="server" Text="ส่วนลด" 
                Font-Size="Medium" Font-Bold="False" Width="100px"></asp:Label>
            </td>
        <td >
            <asp:TextBox ID="txtPurchasePrice_dis" runat="server" Width="100px" 
                Height="30px">0</asp:TextBox>
            </td>
        <td >
            &nbsp;</td>
        <td>
            <asp:Label ID="Label108" runat="server" Text="หน่วยนับ" 
                Font-Size="Medium" Font-Bold="False" Width="100px"></asp:Label>
        </td>
        <td>

            <asp:DropDownList ID="txtPurchasePrice_unit" runat="server" Width="200px" 
                Height="35px" Font-Size="Medium">
            </asp:DropDownList>
        </td>
        <td>
            &nbsp;</td>
        <td>
            &nbsp;</td>
        <td>
            &nbsp;</td>
    </tr>
      <tr>
        <td>
            <asp:Label ID="Label80" runat="server" Text="ราคาซื้อ ส่ง (ไม่รวมVAT)" 
                Font-Size="Medium" Font-Bold="False"></asp:Label>
            </td>
        <td >
            <asp:TextBox ID="txtPurchasePrice_wholesale" runat="server" Width="100px" 
                Height="30px">0</asp:TextBox>
        </td>
        <td>
            <asp:Label ID="Label77" runat="server" Text="บาท/" 
                Font-Size="Small" Font-Bold="False" Width="30px"></asp:Label>
            </td>
        <td > 
            &nbsp;</td>
        <td > 
            <asp:Label ID="Label70" runat="server" Text="ส่วนลด" 
                Font-Size="Medium" Font-Bold="False" Width="70px" Height="20px"></asp:Label>
            </td>
        <td >
            <asp:TextBox ID="txtPurchasePrice_wholesale_dis" runat="server" Width="100px" 
                Height="30px">0</asp:TextBox>
            </td>
        <td >
            &nbsp;</td>
        <td>
            <asp:Label ID="Label109" runat="server" Text="หน่วยนับ" 
                Font-Size="Medium" Font-Bold="False" Width="100px"></asp:Label>
        </td>
        <td>

            <asp:DropDownList ID="txtPurchasePrice_wholesale_unit" runat="server" 
                Width="200px" Height="35px" Font-Size="Medium">
            </asp:DropDownList>
        </td>
        <td>
            &nbsp;</td>
        <td>
            &nbsp;</td>
        <td>
            &nbsp;</td>
    </tr>
         <tr>
        <td>
            &nbsp;</td>
        <td >
            &nbsp;</td>
        <td>
            &nbsp;</td>
        <td > 
            &nbsp;</td>
        <td > 
            &nbsp;</td>
        <td >
            &nbsp;</td>
        <td >
            &nbsp;</td>
        <td>
            &nbsp;</td>
        <td>

            &nbsp;</td>
        <td>
            &nbsp;</td>
        <td>
            &nbsp;</td>
        <td>
            &nbsp;</td>
    </tr>
     
    </table>

        </asp:Panel>

   
  

     <table>
   
     <tr>
        <td>
            <asp:Label ID="Label4" runat="server" Text="ข้อมูลการขาย" 
                Font-Size="Large" Font-Bold="True"></asp:Label>
            </td>
        <td >
            &nbsp;</td>
        <td>
            &nbsp;</td>
        <td > 
            &nbsp;</td>
        <td > 
            &nbsp;</td>
        <td >

            &nbsp;</td>
        <td >
            &nbsp;</td>
        <td>
            &nbsp;</td>
        <td>

            &nbsp;</td>
        <td>
            &nbsp;</td>
        <td class="style4">
            &nbsp;</td>
        <td>
            &nbsp;</td>
    </tr>
   
     <tr>
        <td>
            <asp:Label ID="Label112" runat="server" Text="กลุ่มลูกค้า" 
                Font-Size="Medium" Font-Bold="False" Width="70px"></asp:Label>
            </td>
        <td colspan="11" >

            <asp:TextBox ID="drpCustomerGroup" runat="server" Width="100%" 
                Height="30px" Font-Size="Medium">Aroma Shop, Chao Doi, Corporate Account, Online, Hario Cafe</asp:TextBox>
            </td>
    </tr>
   
     <tr>
        <td>
            <asp:Label ID="Label113" runat="server" Text="รหัสลูกค้า" 
                Font-Size="Medium" Font-Bold="False" Width="120px"></asp:Label>
            </td>
        <td colspan="11" >

            <asp:TextBox ID="drpCustomer" runat="server" Width="100%" 
                Height="30px"></asp:TextBox>
            </td>
    </tr>
     <tr>
        <td>
            <asp:Label ID="Label81" runat="server" Text="ราคาขาย ปลีก (ไม่รวมVAT)" 
                Font-Size="Medium" Font-Bold="False" Width="220px"></asp:Label>
            </td>
        <td >
            <asp:TextBox ID="txtPrice_retail" runat="server" Width="100px" Height="30px">0</asp:TextBox>
        </td>
        <td>
            <asp:Label ID="Label78" runat="server" Text="บาท/" 
                Font-Size="Small" Font-Bold="False" Width="30px"></asp:Label>
            </td>
        <td > 
            &nbsp;</td>
        <td > 
            <asp:Label ID="Label71" runat="server" Text="ส่วนลด" 
                Font-Size="Medium" Font-Bold="False" Width="100px"></asp:Label>
            </td>
        <td >
            <asp:TextBox ID="txtPrice_dis" runat="server" Width="100px" Height="30px">0</asp:TextBox>
            </td>
        <td >
            &nbsp;</td>
        <td>
            <asp:Label ID="Label110" runat="server" Text="หน่วยนับ" 
                Font-Size="Medium" Font-Bold="False" Width="100px"></asp:Label>
        </td>
        <td>

            <asp:DropDownList ID="txtPrice_unit" runat="server" Width="200px" Height="35px" 
                Font-Size="Medium">
            </asp:DropDownList>
        </td>
        <td>
            &nbsp;</td>
        <td class="style4">
            &nbsp;</td>
        <td>
            &nbsp;</td>
    </tr>
      <tr>
        <td>
            <asp:Label ID="Label82" runat="server" Text="ราคาขาย ส่ง (ไม่รวมVAT)" 
                Font-Size="Medium" Font-Bold="False"></asp:Label>
            </td>
        <td >
            <asp:TextBox ID="txtPrice_wholesale" runat="server" Width="100px" Height="30px">0</asp:TextBox>
        </td>
        <td>
            <asp:Label ID="Label79" runat="server" Text="บาท/" 
                Font-Size="Small" Font-Bold="False" Width="30px"></asp:Label>
            </td>
        <td > 
            &nbsp;</td>
        <td > 
            <asp:Label ID="Label72" runat="server" Text="ส่วนลด" 
                Font-Size="Medium" Font-Bold="False" Width="100px"></asp:Label>
            </td>
        <td >
            <asp:TextBox ID="txtPrice_wholesale_dis" runat="server" Width="100px" 
                Height="30px">0</asp:TextBox>
            </td>
        <td >
            &nbsp;</td>
        <td>
            <asp:Label ID="Label111" runat="server" Text="หน่วยนับ" 
                Font-Size="Medium" Font-Bold="False" Width="100px"></asp:Label>
        </td>
        <td>

            <asp:DropDownList ID="txtPrice_wholesale_unit" runat="server" Width="200px" 
                Height="35px" Font-Size="Medium">
            </asp:DropDownList>
        </td>
        <td>
            &nbsp;</td>
        <td class="style4">
            &nbsp;</td>
        <td>
            &nbsp;</td>
    </tr>
      <tr>
        <td>
            &nbsp;</td>
        <td >
            &nbsp;</td>
        <td>
            &nbsp;</td>
        <td > 
            &nbsp;</td>
        <td > 
            &nbsp;</td>
        <td >
            &nbsp;</td>
        <td >
            &nbsp;</td>
        <td>
            &nbsp;</td>
        <td>

            &nbsp;</td>
        <td>
            &nbsp;</td>
        <td class="style4">
            &nbsp;</td>
        <td>
            &nbsp;</td>
    </tr>
     </table>


                <asp:Panel ID="PanelLF" runat="server">

                
      <table>
   
     <tr>
        <td colspan="3" class="style11">
            <asp:Label ID="Label8" runat="server" Text="ข้อมูลสำคัญสินค้า" 
                Font-Size="Large" Font-Bold="True"></asp:Label>
            &nbsp;<asp:Label ID="Label122" runat="server" Text="(กรอกข้อมูลทุกช่อง)" 
                Font-Size="Small" Font-Bold="True" ForeColor="#FF3300"></asp:Label>
            </td>
        <td colspan="5" class="style11">
            &nbsp;</td>
        <td class="style11">
            </td>
        <td class="style10">
            </td>
        <td class="style11">
            </td>
    </tr>
     <tr>
        <td>
            <asp:Label ID="Label12" runat="server" Text="สินค้านำเข้าคลัง LFDC" 
                Font-Size="Medium" Font-Bold="False" Width="200px"></asp:Label>
            </td>
        <td >


            <asp:RadioButtonList ID="rdoLFDC" runat="server" RepeatColumns="2" 
                Width="200px">
                <asp:ListItem Selected="True">No</asp:ListItem>
                <asp:ListItem>Yes</asp:ListItem>
            </asp:RadioButtonList>


        </td>
        <td colspan="2" > 
            <asp:Label ID="Label14" runat="server" Text="หน่วยนับเล็ก" 
                Font-Size="Medium" Font-Bold="False" Width="100px"></asp:Label>
            </td>
        <td class="style5" >

            <asp:DropDownList ID="txtLF_unitsmall" runat="server" Height="35px" 
                Width="200px" Font-Size="Medium">
            </asp:DropDownList>
            </td>
        <td >
            &nbsp;</td>
        <td>
            <asp:Label ID="Label15" runat="server" Text="หน่วยนับใหญ่" 
                Font-Size="Medium" Font-Bold="False" Width="100px"></asp:Label>
        </td>
        <td>

            <asp:DropDownList ID="txtLF_unitbig" runat="server" Height="35px" Width="200px" 
                Font-Size="Medium">
            </asp:DropDownList>
        </td>
        <td>
            &nbsp;</td>
        <td class="style4">
            &nbsp;</td>
        <td>
            &nbsp;</td>
    </tr>
     <tr>
        <td>
            <asp:Label ID="Label116" runat="server" Text="Storage Category" 
                Font-Size="Medium" Font-Bold="False" Width="200px"></asp:Label>
            </td>
        <td >

            <asp:DropDownList ID="txtStorageCat" runat="server" Height="35px" Width="200px" 
                Font-Size="Medium">
            </asp:DropDownList>
            </td>
        <td colspan="2" > 
            <asp:Label ID="Label117" runat="server" Text="Primary Group" 
                Font-Size="Medium" Font-Bold="False" Width="120px"></asp:Label>
            </td>
        <td class="style5" >

            <asp:DropDownList ID="txtPrimaryGroup" runat="server" Height="35px" 
                Width="200px" Font-Size="Medium">
            </asp:DropDownList>
            </td>
        <td >
            &nbsp;</td>
        <td>
            <asp:Label ID="Label118" runat="server" Text="Product Category" 
                Font-Size="Medium" Font-Bold="False" Width="130px"></asp:Label>
            </td>
        <td>

            <asp:DropDownList ID="txtProduct_cat" runat="server" Height="35px" 
                Width="200px" Font-Size="Medium">
            </asp:DropDownList>
            </td>
        <td>
            &nbsp;</td>
        <td class="style4">
            &nbsp;</td>
        <td>
            &nbsp;</td>
    </tr>
     
     </table>

      <table>

    <tr>
        <td>
            <asp:Label ID="Label89" runat="server" Text="ขนาดบรรจุภัณฑ์ (PCS) กว้าง" 
                Font-Size="Medium" Font-Bold="False" Width="200px"></asp:Label>
            </td>
        <td >
            <asp:TextBox ID="txtPcs_wide" runat="server" Width="100px" Height="30px"></asp:TextBox>
        </td>
        <td>
            <asp:Label ID="Label90" runat="server" Text="X ยาว" 
                Font-Size="Small" Font-Bold="False" Width="50px"></asp:Label>
            </td>
        <td > 
            <asp:TextBox ID="txtPcs_long" runat="server" Width="100px" Height="30px"></asp:TextBox>
            </td>
        <td >
            <asp:Label ID="Label91" runat="server" Text="X สูง" 
                Font-Size="Small" Font-Bold="False" Width="50px"></asp:Label>
            </td>
        <td class="style21" >
            <asp:TextBox ID="txtPcs_hight" runat="server" Width="100px" Height="30px"></asp:TextBox>
            </td>
        <td class="style21" >
            <asp:Label ID="Label92" runat="server" Text="(CM) น้ำหนัก" 
                Font-Size="Small" Font-Bold="False" Width="100px"></asp:Label>
            </td>
        <td >
            <asp:TextBox ID="txtPcs_weight" runat="server" Width="100px" Height="30px"></asp:TextBox>
            </td>
        <td >
            <asp:Label ID="Label93" runat="server" Text="Kg." 
                Font-Size="Small" Font-Bold="False" Width="50px"></asp:Label>
            </td>

        <td class="style21" >
            <asp:Label ID="Label10" runat="server" Text=" Gross Weight" 
                Font-Size="Small" Font-Bold="False" Width="100px"></asp:Label>
            </td>
        <td >
            <asp:TextBox ID="txtPcsGross_weight" runat="server" Width="100px" Height="30px"></asp:TextBox>
        </td>
        <td >
            <asp:Label ID="Label11" runat="server" Text="Kg." 
                Font-Size="Small" Font-Bold="False" Width="50px"></asp:Label>
            </td>
    </tr>

    
    <tr>
        <td>
            <asp:Label ID="Label94" runat="server" Text="ขนาดบรรจุภัณฑ์ (CASE) กว้าง" 
                Font-Size="Medium" Font-Bold="False" Width="220px"></asp:Label>
            </td>
        <td >
            <asp:TextBox ID="txtCase_wide" runat="server" Width="100px" Height="30px"></asp:TextBox>
        </td>
        <td>
            <asp:Label ID="Label98" runat="server" Text="X ยาว" 
                Font-Size="Small" Font-Bold="False" Width="50px"></asp:Label>
            </td>
        <td > 
            <asp:TextBox ID="txtCase_long" runat="server" Width="100px" Height="30px"></asp:TextBox>
        </td>
        <td >
            <asp:Label ID="Label97" runat="server" Text="X สูง" 
                Font-Size="Small" Font-Bold="False" Width="50px"></asp:Label>
            </td>
        <td class="style21" >
            <asp:TextBox ID="txtCase_hight" runat="server" Width="100px" Height="30px"></asp:TextBox>
        </td>
        <td class="style21" >
            <asp:Label ID="Label95" runat="server" Text="(CM) น้ำหนัก" 
                Font-Size="Small" Font-Bold="False" Width="100px"></asp:Label>
            </td>
        <td >
            <asp:TextBox ID="txtCase_weight" runat="server" Width="100px" Height="30px"></asp:TextBox>
        </td>
        <td >
            <asp:Label ID="Label96" runat="server" Text="Kg." 
                Font-Size="Small" Font-Bold="False"></asp:Label>
         </td>
         <td class="style21" >
            <asp:Label ID="Label13" runat="server" Text=" Gross Weight" 
                Font-Size="Small" Font-Bold="False" Width="100px"></asp:Label>
            </td>
        <td >
            <asp:TextBox ID="txtCaseGross_weight" runat="server" Width="100px" Height="30px"></asp:TextBox>
        </td>
        <td >
            <asp:Label ID="Label16" runat="server" Text="Kg." 
                Font-Size="Small" Font-Bold="False" Width="50px"></asp:Label>
            </td>
    </tr>

    
  
    <tr>
        <td>
            &nbsp;</td>
        <td >
            &nbsp;</td>
        <td>
            &nbsp;</td>
        <td > 
            &nbsp;</td>
        <td >
            &nbsp;</td>
        <td class="style21" >
            &nbsp;</td>
        <td class="style21" >
            &nbsp;</td>
        <td >
            &nbsp;</td>
        <td >
            &nbsp;</td>
    </tr>

    
  
     </table>

                </asp:Panel>

                 <asp:Panel ID="PanelFac" runat="server">

                 <table>

       <tr>
        <td class="style15" colspan="3">
            <asp:Label ID="Label48" runat="server" Text="ข้อมูล PRODUCTION (โรงงาน)" 
                Font-Size="Large" Font-Bold="True"></asp:Label>&nbsp;<asp:Label 
                ID="Label124" runat="server" Text="(กรอกข้อมูลทุกช่อง)" 
                Font-Size="Small" Font-Bold="True" ForeColor="#FF3300"></asp:Label>
            </td>
        <td class="style15" colspan="3">
            &nbsp;</td>
    </tr>

    
    <tr>
        <td>
            <asp:Label ID="Label123" runat="server" Text="ประเภทรหัสสินค้า" 
                Font-Size="Medium" Font-Bold="False" Width="220px"></asp:Label>
            </td>
        <td >
            <asp:RadioButtonList ID="rdoProductType" runat="server" RepeatColumns="3" 
                Width="220px">
                <asp:ListItem Selected="True">Lot</asp:ListItem>
                <asp:ListItem>Item</asp:ListItem>
                <asp:ListItem>Serial</asp:ListItem>
            </asp:RadioButtonList>
        </td>
        <td>
            &nbsp;</td>
        <td > 
            &nbsp;</td>
        <td >
            &nbsp;</td>
        <td >
            &nbsp;</td>
    </tr>

    
    <tr>
        <td class="style16">
            <asp:Label ID="Label49" runat="server" Text="PRODUCT CODE (สินค้าผลิต)" 
                Font-Size="Medium" Font-Bold="False" Width="220px"></asp:Label>
            </td>
        <td class="style16" >
            <asp:TextBox ID="txtProduct_code" runat="server" Width="200px" Height="30px"></asp:TextBox>
        </td>
        <td class="style16">
            <asp:Label ID="Label83" runat="server" Text="อายุของสินค้า(วัน)" 
                Font-Size="Medium" Font-Bold="False" Width="150px"></asp:Label>
            </td>
        <td class="style16" > 
            <asp:TextBox ID="txtProduct_age" runat="server" Width="120px" Height="30px"></asp:TextBox>
            </td>
        <td class="style16" >
            <asp:Label ID="Label119" runat="server" Text="คลังสินค้าหลัก" 
                Font-Size="Medium" Font-Bold="False" Width="120px"></asp:Label>
            </td>
        <td class="style16" >
            <asp:TextBox ID="txtProduct_store" runat="server" Width="120px" Height="30px"></asp:TextBox>
        </td>
    </tr>

    
    <tr>
        <td class="style20">
            <asp:Label ID="Label125" runat="server" Text="สินค้าภาษีสรรพามิต" 
                Font-Size="Medium" Font-Bold="False" Width="220px"></asp:Label>
            </td>
        <td class="style20" >
            <asp:RadioButtonList ID="rdoProduct_excise" runat="server" RepeatColumns="2" 
                Width="200px">
                <asp:ListItem Selected="True">No</asp:ListItem>
                <asp:ListItem>Yes</asp:ListItem>
            </asp:RadioButtonList>
        </td>
        <td class="style20">
            <asp:Label ID="Label120" runat="server" Text="ประเภทภาษี" 
                Font-Size="Medium" Font-Bold="True" Width="100px" Height="16px"></asp:Label>
            </td>
        <td class="style20" colspan="3" > 
            <asp:RadioButtonList ID="rdoProduct_tax" runat="server" RepeatColumns="3" 
                Width="380px">
                <asp:ListItem>ภส.07-01</asp:ListItem>
                <asp:ListItem>ภส.07-02</asp:ListItem>
                <asp:ListItem>ภส.07-03</asp:ListItem>
            </asp:RadioButtonList>
            </td>
    </tr>

    
    <tr>
        <td>
            <asp:Label ID="Label86" runat="server" Text="ผังบัญชีซื้อ (โรงงาน)" 
                Font-Size="Medium" Font-Bold="True" Width="150px"></asp:Label>
            </td>
        <td colspan="5" >
            <asp:RadioButtonList ID="rdoProduct_Acct" runat="server" RepeatColumns="3" 
                Width="900px">
                <asp:ListItem Selected="True">วัตถุดิบ (ใช้ผลิตสินค้า) (รหัสบัญชี : 51010101)</asp:ListItem>
                <asp:ListItem>ส่วนผสม (ใช้ผลิตสินค้า) (รหัสบัญชี : 51010105)</asp:ListItem>
                <asp:ListItem>บรรจุภันฑ์ (หีบ/ห่อ) (รหัสบัญชี : 51010104)</asp:ListItem>
            </asp:RadioButtonList>
        </td>
    </tr>

    
    <tr>
        <td>
            &nbsp;</td>
        <td >
            &nbsp;</td>
        <td>
            &nbsp;</td>
        <td > 
            &nbsp;</td>
        <td >
            &nbsp;</td>
        <td >
            &nbsp;</td>
    </tr>

   
    </table>

                </asp:Panel>

<%--<asp:Panel ID="Panel_Attach" runat="server">--%>

<hr />

                 <table>


    
   
                     <tr>
                         <td class="style15">
                             <asp:Label ID="Label129" runat="server" Font-Bold="True" Font-Size="Medium" 
                                 Text="แนบไฟล์" Width="220px"></asp:Label>
                         </td>
                         <td class="style15">
                             <asp:FileUpload ID="FileUpload1" runat="server" CssClass="sample" 
                                 Font-Size="Medium" style="font-family: Tahoma" />
                         </td>
                         <td class="style15">
                             <asp:Button ID="btnUpload" runat="server" class="btn btn-primary" 
                                 Text="Upload" />
                         </td>
                     </tr>
                     <tr>
                         <td class="style15" colspan="3">

                         <asp:DataGrid ID="DataGrid1" runat="server" AllowSorting="True" 
                                AutoGenerateColumns="False" CssClass="style98" 
                                DataKeyField="Att_ID" Font-Size="Small" Width="731px" CellPadding="4" 
                                 ForeColor="#333333" GridLines="None">
                                <AlternatingItemStyle BackColor="White" ForeColor="#284775" />
                                <Columns>
                                    <asp:BoundColumn DataField="Att_ID" HeaderText="Id "></asp:BoundColumn>
                                    <asp:BoundColumn DataField="Att_Filename" HeaderText="File Name "></asp:BoundColumn>
                                    <asp:ButtonColumn CommandName="Edit" Text="Download"></asp:ButtonColumn>
                                    <asp:ButtonColumn CommandName="Delete" Text="Delete"></asp:ButtonColumn>
                                </Columns>
                                <EditItemStyle BackColor="#999999" />
                                <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                <HeaderStyle BackColor="#5D7B9D" BorderColor="Black" Font-Bold="True" 
                                    Font-Names="Tahoma" ForeColor="White" Wrap="False" />
                                <ItemStyle BackColor="#F7F6F3" ForeColor="#333333" />
                                <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                                <SelectedItemStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                </asp:DataGrid>

                             &nbsp;</td>
                     </tr>

    
   
    </table>

<%--                </asp:Panel>--%>

     <table>
    <tr>
        <td colspan="5">
            <asp:Button ID="btnSave" runat="server" Text="Save" class="btn btn-primary" 
                BackColor="#003399" BorderColor="#0066FF" Font-Bold="False" Font-Size="Medium" 
                ForeColor="White" Width="100px"/>
  &nbsp;<asp:Button ID="btnSubmit" runat="server" Text="Submit" class="btn btn-primary" 
                BackColor="#003399" BorderColor="#0066FF" Font-Bold="False" Font-Size="Medium" 
                ForeColor="White" Width="100px"/>
        &nbsp;<asp:Button ID="btnApprove" runat="server" Text="Approve" class="btn btn-primary" 
                BackColor="#003399" BorderColor="#0066FF" Font-Bold="False" Font-Size="Medium" 
                ForeColor="White" Width="100px"/>
        &nbsp;<asp:Button ID="btnCancel" runat="server" Text="Cancel" class="btn btn-primary" 
                BackColor="#003399" BorderColor="#0066FF" Font-Bold="False" Font-Size="Medium" 
                ForeColor="White" Width="100px"/>
        &nbsp;<asp:Button ID="btnApproveSCM" runat="server" Text="SCM Approve" class="btn btn-primary" 
                BackColor="#003399" BorderColor="#0066FF" Font-Bold="False" Font-Size="Medium" 
                ForeColor="White" Width="150px"/>
        &nbsp;<asp:Button ID="btnApproveAcct" runat="server" Text="Account Approve" class="btn btn-primary" 
                BackColor="#003399" BorderColor="#0066FF" Font-Bold="False" Font-Size="Medium" 
                ForeColor="White" Width="180px"/>
        &nbsp;<asp:Button ID="btnPrint" runat="server" Text="Print Form" class="btn btn-primary" 
                BackColor="#003399" BorderColor="#0066FF" Font-Bold="True" Font-Size="Medium" 
                ForeColor="White" Width="140px"/>
        </td>
        <td >
            &nbsp;</td>
    </tr>
       </table>
            </div>	
            </div>


</div>	
</div>	
</div>	
</div>

<div class="bot1">
<div class="container">
<div class="row">
<div class="span12">
<div class="bot1_inner">
<div class="row">
<div class="span4">
	
<div class="logo2_wrapper"><a href="index.html" class="logo2"><img src="images/aroma/logotrans.png" alt=""></a></div>

<footer><div class="copyright">Copyright   © 2018. All rights reserved. All rights reserved.reserved.br>ng" alt=""></a></div>

<footer><div class="copyright">Copyright   © 2018. All rights reserved. All rights reserved.reserved.br><a href="#">Aroma Group</a></div></footer>


</div>
<div class="span4">
	
<div class="bot1_title">Contact Us</div>

สำนักงานใหญ่ อโรม่า กรุ๊ป<br>
บริษัท เค.วี.เอ็น.อิมปอร์ต เอกซ์ปอร์ต (1991) จำกัด<br>
เลขที่ 43 ชั้น 2 ซอยนาคนิวาส 6 ถนนนาคนิวาส แขวงลาดพร้าว <br>
เขตลาดพร้าว กรุงเทพฯ 10230<br>
โทรศัพท์ : 02 159-8999<br>
โทรสาร : 02 538-8144, 02 539-5597

<%--E-mail: <a href="#">mail@demosite.com</a>--%>


</div>
<div class="span4">
	
<%--<div class="bot1_title">Follow Us:</div>	

<div class="social_wrapper">
		<ul class="social clearfix">
	    <li><a href="#"><img src="images/social_ic1.png"></a></li>
	    <li><a href="#"><img src="images/social_ic2.png"></a></li>
	    <li><a href="#"><img src="images/social_ic3.png"></a></li>	    
	    <li><a href="#"><img src="images/social_ic4.png"></a></li>
		</ul>
	</div>
--%>

</div>	

</div>	
</div>
</div>	
</div>	
</div>	
</div>

</div>	
</div>
<script type="text/javascript" src="js/bootstrap.js"></script>



 <%--date--%>
               <script src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.7.min.js"></script>
                    <script src="http://ajax.aspnetcdn.com/ajax/jquery.ui/1.8.16/jquery-ui.min.js"></script>
                    <link type="text/css" rel="Stylesheet" href="http://ajax.microsoft.com/ajax/jquery.ui/1.8.16/themes/redmond/jquery-ui.css" />

                    <script type="text/javascript">
                        //ประกาศตัวแปรเพื่อรับใหม่
                        $jq = $.noConflict();
                        $jq().ready(

                 function () {
                     $jq('#<%=txtRequestDate.ClientID%>').datepicker(
                     {

                         dateFormat: 'yy-mm-dd',

                     }
                     );

                  
                 });     
            </script>
              <script src="https://code.jquery.com/jquery-2.1.1.min.js" type="text/javascript"></script>
            <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2-rc.1/css/select2.min.css" rel="stylesheet" />
            <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2-rc.1/js/select2.min.js" type="text/javascript"></script>
            <script type="text/javascript">
                //             
                //                $(document).ready(function () {
                //                    $("#drpProductType").select2({
                //                    });
                //                });

                //                $(document).ready(function () {
                //                    $("#drpProductCat").select2({
                //                    });
                //                });

                //                $(document).ready(function () {
                //                    $("#drpProductGroup").select2({
                //                    });
                //                });

                //                $(document).ready(function () {
                //                    $("#drpGroup").select2({
                //                    });
                //                });

                $(document).ready(function () {
                    $("#drpRequester").select2({
                    });
                });
                
            </script>  

    </div>
    </form>
</body>
</html>
