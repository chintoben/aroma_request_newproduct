﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Configuration

Public Class DBProc
    Inherits System.Web.UI.Page

    Public Function InsertAttatchment(ByVal Employee_id As String, ByVal RequestId As String, ByVal sFilename As String, ByVal Attatch As Object) As String
        Dim Attachment As New Attachment
        Attachment.InsertAttatchment(Employee_id, RequestId, sFilename, Attatch)
    End Function

    Public Function InsertAttatchment_OT(ByVal Employee_id As String, ByVal OTID As String, ByVal sFilename As String, ByVal Attatch As Object) As String
        Dim Attachment As New Attachment
        Attachment.InsertAttatchment_OT(Employee_id, OTID, sFilename, Attatch)
    End Function

    Public Function InsertAttatchmentIndex(ByVal IndexID As String, ByVal sFilename As String, ByVal Attatch As Object) As String
        Dim Attachment As New Attachment
        Attachment.InsertAttatchmentIndex(IndexID, sFilename, Attatch)
    End Function

    Public Function GetContentType(ByRef FileName As String) As String
        'get the file ext
        Dim Ext As String = System.IO.Path.GetExtension(FileName)


        Dim Reg As Microsoft.Win32.RegistryKey = _
          Microsoft.Win32.Registry.ClassesRoot.OpenSubKey(Ext)


        Return Reg.GetValue("Content Type")
    End Function


End Class
