﻿Imports System.Data.SqlClient
Imports System.Drawing
Imports System.Data
Imports System.IO
Imports System.Configuration
Imports System.Collections
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports ICSharpCode.SharpZipLib.Zip
Imports System.Data.SqlClient.SqlConnection

Imports System.Data.OleDb
Imports System.Web
Imports System.Net
Imports System.Net.Mail
Imports System.Net.Mail.MailMessage
Imports System.Web.Mail.SmtpMail
Imports System.Net.Mail.SmtpClient
Imports System.Web.Mail.MailFormat

Public Class NewMasterRawMat
    Inherits System.Web.UI.Page
    Dim db_Mac5 As New Connect_Mac5
    Dim db_customer As New Connect_Mac5
    Dim db_Enpro As New Connect_Enpro
    Private dp As New DBProc
    Dim db_HR As New Connect_HR
    Dim DB_Product As New Connect_product

    Dim dt, dt2, dt3, dt4, dt5, dtt As New DataTable
    Dim sql, sql2, sql3, CallID, Type, item, usr, sql4, sql5 As String
    Dim RequestId As String
    Private sms As New PKMsg("")
    'Private con As New SqlConnection("data source=10.0.24.20;initial catalog=DB_Service;persist security info=false;User ID=sa;Password=Quax_005;Connect Timeout=0;Max Pool Size=500;Enlist=true")
    Dim DocID As String
    Dim objConn As New SqlConnection
    Dim objCmd As SqlCommand
    Dim strConnString, strSQL, api As String


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        usr = Request.QueryString("usr")
        RequestId = Request.QueryString("id")

        'strConnString = "Server=10.0.24.20;Uid=sa;PASSWORD=Quax_005;database=DB_Service;Max Pool Size=400;Connect Timeout=600;"
        strConnString = "Server=10.0.24.20;Uid=armth;PASSWORD=Kb5r#Ge9Z3M*mQ;database=DB_RequestNewProduct;Max Pool Size=400;Connect Timeout=600;"
        objConn = New SqlConnection(strConnString)
        objConn.Open()


        If usr = "" Then
            Server.Transfer("login.aspx")
        End If

        If Not IsPostBack Then

            If RequestId <> "" Then

                'check ข้อมูลเก่า
                sql3 = " select * "
                sql3 += " from  [VW_ProductRequest_RawMat]"
                sql3 += " where  RequestId = '" & RequestId & "'"
                dt3 = DB_Product.GetDataTable(sql3)
                If dt3.Rows.Count > 0 Then

                    lblRequestID.Text = RequestId
                    lblStatus.Text = "" & dt3.Rows(0)("stat")

                    If dt3.Rows(0)("stat") = "Waiting Account Approve" Then

                        'check สิทธิ์
                        sql4 = " select * "
                        sql4 += " from  [TB_UserApprove]"
                        sql4 += " where   [Stauts_approve] = 'Waiting Account Approve' and  [EmployeeID]  = '" & usr & "'"
                        dt4 = DB_Product.GetDataTable(sql4)
                        If dt4.Rows.Count > 0 Then
                            btnSubmit.Visible = False
                            btnApproveSCM.Visible = False
                        Else
                            btnSave.Visible = False
                            btnSubmit.Visible = False
                            btnApproveAcct.Visible = False
                            btnApproveSCM.Visible = False
                        End If

                    ElseIf dt3.Rows(0)("stat") = "Waiting Supply Chain Approve" Then

                        'check สิทธิ์
                        sql5 = " select * "
                        sql5 += " from  [TB_UserApprove]"
                        sql5 += " where  [Stauts_approve] = 'Waiting Supply Chain Approve' and  [EmployeeID]  = '" & usr & "'"
                        dt5 = DB_Product.GetDataTable(sql5)
                        If dt5.Rows.Count > 0 Then
                            btnSubmit.Visible = False
                            btnApproveAcct.Visible = False
                            btnApprove.Visible = False
                        Else
                            btnSave.Visible = False
                            btnSubmit.Visible = False
                            btnApproveAcct.Visible = False
                            btnApproveSCM.Visible = False
                        End If

                    ElseIf dt3.Rows(0)("stat") = "Waiting Manager Approve" Then

                        'check สิทธิ์
                        sql5 = " select * "
                        sql5 += " from  [TB_UserApprove]"
                        sql5 += " where  [Stauts_approve] = 'Waiting Manager Approve' and  [EmployeeID]  = '" & usr & "'"
                        dt5 = DB_Product.GetDataTable(sql5)
                        If dt5.Rows.Count > 0 Then
                            btnSubmit.Visible = False
                            btnApproveAcct.Visible = False
                            btnApproveSCM.Visible = False
                        Else
                            btnSave.Visible = False
                            btnSubmit.Visible = False
                            btnApproveAcct.Visible = False
                            btnApproveSCM.Visible = False
                            btnApprove.Visible = False
                        End If

                    ElseIf dt3.Rows(0)("stat") = "Complete" Then
                        btnSave.Visible = False
                        btnSubmit.Visible = False
                        btnApproveAcct.Visible = False
                        btnApproveSCM.Visible = False
                        btnCancel.Visible = False
                        btnApprove.Visible = False

                    ElseIf dt3.Rows(0)("stat") = "Cancel" Then
                        btnSave.Visible = False
                        btnSubmit.Visible = False
                        btnApproveAcct.Visible = False
                        btnApproveSCM.Visible = False
                        btnCancel.Visible = False
                        btnApprove.Visible = False

                    ElseIf dt3.Rows(0)("stat") = "Draft" Then
                        btnApproveAcct.Visible = False
                        btnApproveSCM.Visible = False
                        btnCancel.Visible = False
                        btnApprove.Visible = False
                    End If

                End If


                LoadData(RequestId)
                att_Read()
            Else
                'new
                drpRequester.SelectedValue = usr
                sql = " select * "
                sql += " from VW_Employee "
                sql += " where Employee_id = '" & usr & "' "
                dt = db_HR.GetDataTable(sql)
                If dt.Rows.Count > 0 Then
                    txtDep.Text = dt.Rows(0)("department_name")
                    drpRequester.SelectedValue = dt.Rows(0)("fullname")
                End If
                txtRequestDate.Text = DateTime.Now.ToString("yyyy-MM-dd")

                'check ข้อมูลเก่า
                sql2 = " select * "
                sql2 += " from  [VW_ProductRequest_RawMat]"
                sql2 += " where  stat = 'draft' and CreateBy = '" & usr & "'"
                dt2 = DB_Product.GetDataTable(sql2)
                If dt2.Rows.Count > 0 Then
                    RequestId = dt2.Rows(0)("RequestId")
                    lblRequestID.Text = RequestId
                    LoadData(RequestId)
                    att_Read()
                    lblStatus.Text = "Draf"
                End If

                btnApproveAcct.Visible = False
                btnApproveSCM.Visible = False
                btnCancel.Visible = False
                btnApprove.Visible = False
            End If

        End If

    End Sub


    Protected Sub drpRequester_Init(sender As Object, e As EventArgs) Handles drpRequester.Init
        Dim sqlCon As New SqlConnection(db_HR.sqlCon)
        Dim sqlCmd As New SqlCommand
        Dim Rs As SqlDataReader
        Dim sql As String

        sql = "select [employee_id] as id,[fullname] from [TB_Employee] order by employee_id asc"

        If sqlCon.State = ConnectionState.Closed Then
            sqlCon = New SqlConnection(db_HR.sqlCon)
            sqlCon.Open()
        End If
        With sqlCmd
            .Connection = sqlCon
            .CommandType = CommandType.Text
            .CommandText = sql
            Rs = .ExecuteReader
        End With
        drpRequester.Items.Clear()
        drpRequester.Items.Add(New ListItem("", 0))
        While Rs.Read
            drpRequester.Items.Add(New ListItem(Rs.GetString(0) + " : " + Rs.GetString(1), Rs.GetString(0)))
        End While
    End Sub

    Protected Sub drpRequester_SelectedIndexChanged(sender As Object, e As EventArgs) Handles drpRequester.SelectedIndexChanged
        sql = " select * "
        sql += " from VW_Employee "
        sql += " where Employee_id = '" & drpRequester.SelectedValue & "' "
        dt = db_HR.GetDataTable(sql)
        If dt.Rows.Count > 0 Then
            txtDep.Text = dt.Rows(0)("department_name")
        End If
    End Sub

    Protected Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click

        sql = " select * "
        sql += " from [VW_ProductRequest_RawMat] "
        'sql += " where Item = '" & txtItem.Text & "' "
        sql += " where RequestId = '" & lblRequestID.Text & "' "
        dt = DB_Product.GetDataTable(sql)
        If dt.Rows.Count >= 1 Then
            UpdateData()
        Else

            GenID()
            lblRequestID.Text = RequestId
            InsertData(RequestId)
        End If

        'LoadData()

        sms.Msg = "บันทึกข้อมูลเรียบร้อยแล้วค่ะ"
        Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "window.alert('" & sms.Msg & "');", True)
        Exit Sub
    End Sub

    Sub InsertData(RequestId)

        Dim kvn, lion As String
        Dim Unit1, Unit2, Unit3, Unit4, Unit5, Unit6, Unit7, Unit8, Unit9, Unit10, Unit11, Unit12, Unit13, Unit14 As String

        If chkKVN.Checked = True Then
            kvn = "Yes"
        Else
            kvn = "No"
        End If

        If chkLION.Checked = True Then
            lion = "Yes"
        Else
            lion = "No"
        End If

        If chkUnit1.Checked = True Then
            Unit1 = "Y"
        Else
            Unit1 = "N"
        End If

        If chkUnit2.Checked = True Then
            Unit2 = "Y"
        Else
            Unit2 = "N"
        End If

        If chkUnit3.Checked = True Then
            Unit3 = "Y"
        Else
            Unit3 = "N"
        End If

        If chkUnit4.Checked = True Then
            Unit4 = "Y"
        Else
            Unit4 = "N"
        End If

        If chkUnit5.Checked = True Then
            Unit5 = "Y"
        Else
            Unit5 = "N"
        End If

        If chkUnit6.Checked = True Then
            Unit6 = "Y"
        Else
            Unit6 = "N"
        End If

        If chkUnit7.Checked = True Then
            Unit7 = "Y"
        Else
            Unit7 = "N"
        End If

        If chkUnit8.Checked = True Then
            Unit8 = "Y"
        Else
            Unit8 = "N"
        End If

        If chkUnit9.Checked = True Then
            Unit9 = "Y"
        Else
            Unit9 = "N"
        End If

        If chkUnit10.Checked = True Then
            Unit10 = "Y"
        Else
            Unit10 = "N"
        End If

        If chkUnit11.Checked = True Then
            Unit11 = "Y"
        Else
            Unit11 = "N"
        End If

        If chkUnit12.Checked = True Then
            Unit12 = "Y"
        Else
            Unit12 = "N"
        End If

        If chkUnit13.Checked = True Then
            Unit13 = "Y"
        Else
            Unit13 = "N"
        End If

        If chkUnit14.Checked = True Then
            Unit14 = "Y"
        Else
            Unit14 = "N"
        End If

        Dim query As String = "INSERT INTO TB_ProductRequest_RawMat ("
        query &= "RequestId,Company_KVN,Company_LION,Requester,RequestDate,objective1,objective,item_type,item_productcat"
        query &= ",ProductType ,ProductCategory,ProductGroup,Brand,Item,ItemDescription,Packing_size"
        query &= ",Unit,Unit1,Unit2,Unit3,Unit4,Unit5 ,Unit6,Unit7 ,Unit8,Unit9,Unit10,Unit11,Unit12,Unit13,Unit14,Unit_small,Unit_big,barcode_pcs,barcode_case,vendor,vendor_item"
        query &= ",PurchasePrice_retail,PurchasePrice_retail_dis,PurchasePrice_retail_unit ,PurchasePrice_wholesale,PurchasePrice_wholesale_dis,PurchasePrice_wholesale_unit"
        query &= ",customerGroup,customerCode"
        query &= ",Price_retail,Price_retail_dis,Price_retail_unit,Price_wholesale,Price_wholesale_dis,Price_wholesale_unit"
        query &= ",LFDC,LF_unitSmall,LF_unitBig,StorageCat ,Primary_Group,Product_Category"
        query &= ",pcs_wide,pcs_long ,pcs_high,pcs_weight,pcs_gross_weight,case_wide,case_long,case_high,case_weight,case_gross_weight"
        query &= ",product_type,product_code,product_age,product_store ,product_exciseTax,product_tax ,product_Acct"
        'query &= ",SCMBy,SCMDate,AcctBy,AcctDate"
        query &= ",Remark,Stat,CreateBy ,CreateDate"
        query &= ")"

        query += " VALUES( "
        query &= "@RequestId,@Company_KVN,@Company_LION,@Requester,@RequestDate,@objective1,@objective,@item_type,@item_productcat"
        query &= ",@ProductType ,@ProductCategory,@ProductGroup,@Brand,@Item,@ItemDescription,@Packing_size"
        query &= ",@Unit,@Unit1,@Unit2,@Unit3,@Unit4,@Unit5 ,@Unit6,@Unit7 ,@Unit8,@Unit9,@Unit10,@Unit11,@Unit12,@Unit13,@Unit14,@Unit_small,@Unit_big,@barcode_pcs,@barcode_case,@vendor,@vendor_item"
        query &= ",@PurchasePrice_retail,@PurchasePrice_retail_dis,@PurchasePrice_retail_unit ,@PurchasePrice_wholesale,@PurchasePrice_wholesale_dis,@PurchasePrice_wholesale_unit"
        query &= ",@customerGroup,@customerCode"
        query &= ",@Price_retail,@Price_retail_dis,@Price_retail_unit,@Price_wholesale,@Price_wholesale_dis,@Price_wholesale_unit"
        query &= ",@LFDC,@LF_unitSmall,@LF_unitBig,@StorageCat ,@Primary_Group,@Product_Category"
        query &= ",@pcs_wide,@pcs_long ,@pcs_high,@pcs_weight,@pcs_gross_weight,@case_wide,@case_long,@case_high,@case_weight,@case_gross_weight"
        query &= ",@product_type,@product_code,@product_age,@product_store ,@product_exciseTax,@product_tax ,@product_Acct"
        'query &= ",@SCMBy,@SCMDate,@AcctBy,@AcctDate"
        query &= ",@Remark,@Stat,@CreateBy ,@CreateDate"
        query &= ")"

        Dim constr As String = ConfigurationManager.ConnectionStrings("ProductConnectionString").ConnectionString

        Using con As SqlConnection = New SqlConnection(constr)
            Using cmd As SqlCommand = New SqlCommand(query)
                cmd.Parameters.AddWithValue("@RequestId", lblRequestID.Text)
                cmd.Parameters.AddWithValue("@Company_KVN", kvn)
                cmd.Parameters.AddWithValue("@Company_LION", lion)
                cmd.Parameters.AddWithValue("@Requester", drpRequester.SelectedValue)
                cmd.Parameters.AddWithValue("@RequestDate", DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss"))
                cmd.Parameters.AddWithValue("@objective1", rdoObjective1.SelectedValue)
                cmd.Parameters.AddWithValue("@objective", rdoObjective.SelectedValue)
                cmd.Parameters.AddWithValue("@item_type", rdoItem_Type.SelectedValue)
                cmd.Parameters.AddWithValue("@item_productcat", rdoItem_ProductCat.SelectedValue)
                cmd.Parameters.AddWithValue("@ProductType", drpProductType.SelectedValue)
                cmd.Parameters.AddWithValue("@ProductCategory", drpProductCat.SelectedValue)
                cmd.Parameters.AddWithValue("@ProductGroup", drpProductGroup.SelectedValue)
                cmd.Parameters.AddWithValue("@brand", drpBrand.SelectedValue)
                cmd.Parameters.AddWithValue("@Item", txtItem.Text)
                cmd.Parameters.AddWithValue("@ItemDescription", txtItemDescription.Text)
                cmd.Parameters.AddWithValue("@Packing_size", txtPaking.Text)
                cmd.Parameters.AddWithValue("@Unit", "")

                cmd.Parameters.AddWithValue("@Unit1", Unit1)
                cmd.Parameters.AddWithValue("@Unit2", Unit2)
                cmd.Parameters.AddWithValue("@Unit3", Unit3)
                cmd.Parameters.AddWithValue("@Unit4", Unit4)
                cmd.Parameters.AddWithValue("@Unit5", Unit5)
                cmd.Parameters.AddWithValue("@Unit6", Unit6)
                cmd.Parameters.AddWithValue("@Unit7", Unit7)
                cmd.Parameters.AddWithValue("@Unit8", Unit8)
                cmd.Parameters.AddWithValue("@Unit9", Unit9)
                cmd.Parameters.AddWithValue("@Unit10", Unit10)
                cmd.Parameters.AddWithValue("@Unit11", Unit11)
                cmd.Parameters.AddWithValue("@Unit12", Unit12)
                cmd.Parameters.AddWithValue("@Unit13", Unit13)
                cmd.Parameters.AddWithValue("@Unit14", Unit14)

                cmd.Parameters.AddWithValue("@Unit_small", txtUnitsmall.SelectedValue)
                cmd.Parameters.AddWithValue("@Unit_big", txtUnitbig.SelectedValue)
                cmd.Parameters.AddWithValue("@barcode_pcs", txtbarcodepcs.Text)
                cmd.Parameters.AddWithValue("@barcode_case", txtbarcodecase.Text)

                cmd.Parameters.AddWithValue("@vendor", txtvendor.Text)
                cmd.Parameters.AddWithValue("@vendor_item", txtvendor_item.Text)
                cmd.Parameters.AddWithValue("@PurchasePrice_retail", txtPurchasePrice_retail.Text)
                cmd.Parameters.AddWithValue("@PurchasePrice_retail_dis", txtPurchasePrice_dis.Text)
                cmd.Parameters.AddWithValue("@PurchasePrice_retail_unit", txtPurchasePrice_unit.SelectedValue)
                cmd.Parameters.AddWithValue("@PurchasePrice_wholesale", txtPurchasePrice_wholesale.Text)
                cmd.Parameters.AddWithValue("@PurchasePrice_wholesale_dis", txtPurchasePrice_wholesale_dis.Text)
                cmd.Parameters.AddWithValue("@PurchasePrice_wholesale_unit", txtPurchasePrice_wholesale_unit.SelectedValue)

                cmd.Parameters.AddWithValue("@customerGroup", drpCustomerGroup.Text)
                cmd.Parameters.AddWithValue("@customerCode", drpCustomer.Text)
                cmd.Parameters.AddWithValue("@Price_retail", txtPrice_retail.Text)
                cmd.Parameters.AddWithValue("@Price_retail_dis", txtPrice_dis.Text)
                cmd.Parameters.AddWithValue("@Price_retail_unit", txtPrice_unit.SelectedValue)
                cmd.Parameters.AddWithValue("@Price_wholesale", txtPrice_wholesale.Text)
                cmd.Parameters.AddWithValue("@Price_wholesale_dis", txtPrice_wholesale_dis.Text)
                cmd.Parameters.AddWithValue("@Price_wholesale_unit", txtPrice_wholesale_unit.SelectedValue)

                cmd.Parameters.AddWithValue("@LFDC", rdoLFDC.SelectedValue)
                cmd.Parameters.AddWithValue("@LF_unitSmall", txtLF_unitsmall.SelectedValue)
                cmd.Parameters.AddWithValue("@LF_unitBig", txtLF_unitbig.SelectedValue)
                cmd.Parameters.AddWithValue("@StorageCat", txtStorageCat.SelectedValue)
                cmd.Parameters.AddWithValue("@Primary_Group", txtPrimaryGroup.SelectedValue)
                cmd.Parameters.AddWithValue("@Product_Category", txtProduct_cat.SelectedValue)

                cmd.Parameters.AddWithValue("@pcs_wide", txtPcs_wide.Text)
                cmd.Parameters.AddWithValue("@pcs_long", txtPcs_long.Text)
                cmd.Parameters.AddWithValue("@pcs_high", txtPcs_hight.Text)
                cmd.Parameters.AddWithValue("@pcs_weight", txtPcs_weight.Text)
                cmd.Parameters.AddWithValue("@pcs_gross_weight", txtPcsGross_weight.Text)
                cmd.Parameters.AddWithValue("@case_wide", txtCase_wide.Text)
                cmd.Parameters.AddWithValue("@case_long", txtCase_long.Text)
                cmd.Parameters.AddWithValue("@case_high", txtCase_hight.Text)
                cmd.Parameters.AddWithValue("@case_weight", txtCase_weight.Text)
                cmd.Parameters.AddWithValue("@case_gross_weight", txtCaseGross_weight.Text)

                cmd.Parameters.AddWithValue("@product_type", rdoProductType.SelectedValue)
                cmd.Parameters.AddWithValue("@product_code", txtProduct_code.Text)
                cmd.Parameters.AddWithValue("@product_age", txtProduct_age.Text)
                cmd.Parameters.AddWithValue("@product_store", txtProduct_store.Text)
                cmd.Parameters.AddWithValue("@product_exciseTax", rdoProduct_excise.SelectedValue)
                cmd.Parameters.AddWithValue("@product_tax", rdoProduct_tax.SelectedValue)
                cmd.Parameters.AddWithValue("@product_Acct", rdoProduct_Acct.SelectedValue)

                cmd.Parameters.AddWithValue("@Remark", "")
                cmd.Parameters.AddWithValue("@Stat", "Draft")

                cmd.Parameters.AddWithValue("@CreateBy", usr)
                cmd.Parameters.AddWithValue("@CreateDate", DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss"))

                cmd.Connection = con
                con.Open()
                cmd.ExecuteNonQuery()
                con.Close()
            End Using
        End Using

    End Sub


    Sub LoadData(RequestId)
        sql = " select * "
        sql += " from  [VW_ProductRequest_RawMat]"
        sql += " where requestid = '" & lblRequestID.Text & "' "
        dt = DB_Product.GetDataTable(sql)
        If dt.Rows.Count > 0 Then

            If dt.Rows(0)("Company_KVN") = "Yes" Then
                chkKVN.Checked = True
            Else
                chkKVN.Checked = False
            End If

            If dt.Rows(0)("Company_LION") = "Yes" Then
                chkLION.Checked = True
            Else
                chkLION.Checked = False
            End If

            drpRequester.SelectedValue = dt.Rows(0)("Requester")
            txtDep.Text = dt.Rows(0)("RequestDep")
            txtRequestDate.Text = dt.Rows(0)("RequestDate")
            txtDep.Text = dt.Rows(0)("RequesterDepartment")

            'cmd.Parameters.AddWithValue("@objective", rdoObjective.SelectedValue)
            'cmd.Parameters.AddWithValue("@item_type", rdoObjective.SelectedValue)

            '''''''''''''' พี่เบ็น 28/04/2021''''''''''

            If dt.Rows(0)("objective1") = "สินค้าสำเร็จรูป" Then
                rdoObjective1.Items.FindByValue("สินค้าสำเร็จรูป").Selected = True
            ElseIf dt.Rows(0)("objective1") = "วัตถุดิบ/บรรจุภัณฑ์ (โรงงาน)" Then
                rdoObjective1.Items.FindByValue("วัตถุดิบ/บรรจุภัณฑ์ (โรงงาน)").Selected = True
            ElseIf dt.Rows(0)("objective1") = "วัตถุดิบ/บรรจุภัณฑ์ (หน้าร้าน)" Then
                rdoObjective1.Items.FindByValue("วัตถุดิบ/บรรจุภัณฑ์ (หน้าร้าน)").Selected = True
            End If

            If dt.Rows(0)("objective") = "สินค้าผลิต" Then
                rdoObjective.Items.FindByValue("สินค้าผลิต").Selected = True
            ElseIf dt.Rows(0)("objective") = "สินค้าซื้อมาขายไป" Then
                rdoObjective.Items.FindByValue("สินค้าซื้อมาขายไป").Selected = True
            ElseIf dt.Rows(0)("objective") = "สินค้าระหว่างกลุ่มอโรม่า" Then
                rdoObjective.Items.FindByValue("สินค้าระหว่างกลุ่มอโรม่า").Selected = True
            ElseIf dt.Rows(0)("objective") = "สินค้าชุด/Promotion" Then
                rdoObjective.Items.FindByValue("สินค้าชุด/Promotion").Selected = True
            ElseIf dt.Rows(0)("objective") = "อื่นๆ" Then
                rdoObjective.Items.FindByValue("อื่นๆ").Selected = True
            End If

            ''''''''''''' พี่เบ็น ''''''''''

            If dt.Rows(0)("item_type") = "วัตถุดิบ (ใช้ผลิตสินค้า)" Then
                rdoItem_Type.Items.FindByValue("วัตถุดิบ (ใช้ผลิตสินค้า)").Selected = True
            ElseIf dt.Rows(0)("item_type") = "สินค้าสำเร็จรูป (เพื่อจำหน่าย)" Then
                rdoItem_Type.Items.FindByValue("สินค้าสำเร็จรูป (เพื่อจำหน่าย)").Selected = True
            ElseIf dt.Rows(0)("item_type") = "บรรจุภัณฑ์(หีบ/ห่อ)" Then
                rdoItem_Type.Items.FindByValue("บรรจุภัณฑ์(หีบ/ห่อ)").Selected = True
            ElseIf dt.Rows(0)("item_type") = "สินค้าตัวอย่าง" Then
                rdoItem_Type.Items.FindByValue("สินค้าตัวอย่าง").Selected = True
            ElseIf dt.Rows(0)("item_type") = "วัสดุสิ้นเปลือง" Then
                rdoItem_Type.Items.FindByValue("วัสดุสิ้นเปลือง").Selected = True
            ElseIf dt.Rows(0)("item_type") = "สินค้าเพื่อส่งเสริมการขาย" Then
                rdoItem_Type.Items.FindByValue("สินค้าเพื่อส่งเสริมการขาย").Selected = True
            End If

            '<asp:ListItem Value="Coffee" Selected="True">Coffee</asp:ListItem>
            '<asp:ListItem Value="Fruit Concentrated">Fruit Concentrated</asp:ListItem>
            '<asp:ListItem Value="Barista Tools">Barista Tools</asp:ListItem>
            '<asp:ListItem Value="Cocoa">Cocoa</asp:ListItem>
            '<asp:ListItem Value="Sauce">Sauce</asp:ListItem>
            '<asp:ListItem Value="Hario">Hario</asp:ListItem>
            '<asp:ListItem Value="Condiment">Condiment</asp:ListItem>
            '<asp:ListItem Value="Syrup">Syrup</asp:ListItem>
            '<asp:ListItem Value="Supply Use">Supply Use</asp:ListItem>
            '<asp:ListItem Value="Tea">Tea</asp:ListItem>
            '<asp:ListItem Value="Topping">Topping</asp:ListItem>
            '<asp:ListItem Value="ส่งเสริมการขาย">ส่งเสริมการขาย</asp:ListItem>
            '<asp:ListItem Value="Powder Mixed">Powder Mixed</asp:ListItem>
            '<asp:ListItem Value="Bakery">Bakery</asp:ListItem>
            '<asp:ListItem Value="เครื่องชง/เครื่องฯ">เครื่องชง/เครื่องฯ</asp:ListItem>
            '<asp:ListItem Value="ชุดชงพร้อมผงผสม">ชุดชงพร้อมผงผสม</asp:ListItem>
            '<asp:ListItem Value="อาหาร">อาหาร</asp:ListItem>
            '<asp:ListItem Value="อะไหล่">อะไหล่</asp:ListItem>


            If dt.Rows(0)("item_productcat") = "Coffee" Then
                rdoItem_ProductCat.Items.FindByValue("Coffee").Selected = True
            ElseIf dt.Rows(0)("item_productcat") = "Fruit Concentrated" Then
                rdoItem_ProductCat.Items.FindByValue("Fruit Concentrated").Selected = True
            ElseIf dt.Rows(0)("item_productcat") = "Barista Tools" Then
                rdoItem_ProductCat.Items.FindByValue("Barista Tools").Selected = True
            ElseIf dt.Rows(0)("item_productcat") = "Cocoa" Then
                rdoItem_ProductCat.Items.FindByValue("Cocoa").Selected = True
            ElseIf dt.Rows(0)("item_productcat") = "Sauce" Then
                rdoItem_ProductCat.Items.FindByValue("Sauce").Selected = True
            ElseIf dt.Rows(0)("item_productcat") = "Hario" Then
                rdoItem_ProductCat.Items.FindByValue("Hario").Selected = True
            ElseIf dt.Rows(0)("item_productcat") = "Condiment" Then
                rdoItem_ProductCat.Items.FindByValue("Condiment").Selected = True
            ElseIf dt.Rows(0)("item_productcat") = "Syrup" Then
                rdoItem_ProductCat.Items.FindByValue("Syrup").Selected = True
            ElseIf dt.Rows(0)("item_productcat") = "Supply Use" Then
                rdoItem_ProductCat.Items.FindByValue("Supply Use").Selected = True
            ElseIf dt.Rows(0)("item_productcat") = "Tea" Then
                rdoItem_ProductCat.Items.FindByValue("Tea").Selected = True
            ElseIf dt.Rows(0)("item_productcat") = "Topping" Then
                rdoItem_ProductCat.Items.FindByValue("Topping").Selected = True
            ElseIf dt.Rows(0)("item_productcat") = "ส่งเสริมการขาย" Then
                rdoItem_ProductCat.Items.FindByValue("ส่งเสริมการขาย").Selected = True
            ElseIf dt.Rows(0)("item_productcat") = "Powder Mixed" Then
                rdoItem_ProductCat.Items.FindByValue("Powder Mixed").Selected = True
            ElseIf dt.Rows(0)("item_productcat") = "Bakery" Then
                rdoItem_ProductCat.Items.FindByValue("Bakery").Selected = True
            ElseIf dt.Rows(0)("item_productcat") = "เครื่องชง/เครื่องฯ" Then
                rdoItem_ProductCat.Items.FindByValue("เครื่องชง/เครื่องฯ").Selected = True
            ElseIf dt.Rows(0)("item_productcat") = "เครื่องชง/เครื่องฯ" Then
                rdoItem_ProductCat.Items.FindByValue("เครื่องชง/เครื่องฯ").Selected = True
            ElseIf dt.Rows(0)("item_productcat") = "อาหาร" Then
                rdoItem_ProductCat.Items.FindByValue("อาหาร").Selected = True
            ElseIf dt.Rows(0)("item_productcat") = "อะไหล่" Then
                rdoItem_ProductCat.Items.FindByValue("อะไหล่").Selected = True
            End If

            drpProductType.SelectedValue = dt.Rows(0)("ProductType")
            drpProductCat.SelectedValue = dt.Rows(0)("ProductCategory")
            drpProductGroup.SelectedValue = dt.Rows(0)("ProductGroup")
            drpBrand.SelectedValue = dt.Rows(0)("Brand")

            txtItem.Text = dt.Rows(0)("Item")
            txtItemDescription.Text = dt.Rows(0)("ItemDescription")
            txtPaking.Text = dt.Rows(0)("Packing_size")

            'cmd.Parameters.AddWithValue("@Unit", rdoUnit.SelectedValue)
            '<asp:ListItem>ซอง</asp:ListItem>
            '   <asp:ListItem>ชิ้น</asp:ListItem>
            '   <asp:ListItem>ขวด</asp:ListItem>
            '    <asp:ListItem>ใบ</asp:ListItem>
            '   <asp:ListItem>เครื่อง</asp:ListItem>
            '   <asp:ListItem>เมตร</asp:ListItem>
            '  <asp:ListItem>กิโลกรัม</asp:ListItem>
            '  <asp:ListItem>แถว</asp:ListItem>
            '  <asp:ListItem>แพ็ค</asp:ListItem>
            '  <asp:ListItem>กล่อง</asp:ListItem>
            '  <asp:ListItem>หีบ</asp:ListItem>


            If dt.Rows(0)("Unit1") = "Y" Then
                chkUnit1.Checked = True
            Else
                chkUnit1.Checked = False
            End If

            If dt.Rows(0)("Unit2") = "Y" Then
                chkUnit2.Checked = True
            Else
                chkUnit2.Checked = False
            End If

            If dt.Rows(0)("Unit3") = "Y" Then
                chkUnit3.Checked = True
            Else
                chkUnit3.Checked = False
            End If

            If dt.Rows(0)("Unit4") = "Y" Then
                chkUnit4.Checked = True
            Else
                chkUnit4.Checked = False
            End If

            If dt.Rows(0)("Unit5") = "Y" Then
                chkUnit5.Checked = True
            Else
                chkUnit5.Checked = False
            End If

            If dt.Rows(0)("Unit6") = "Y" Then
                chkUnit6.Checked = True
            Else
                chkUnit6.Checked = False
            End If

            If dt.Rows(0)("Unit7") = "Y" Then
                chkUnit7.Checked = True
            Else
                chkUnit7.Checked = False
            End If

            If dt.Rows(0)("Unit8") = "Y" Then
                chkUnit8.Checked = True
            Else
                chkUnit8.Checked = False
            End If

            If dt.Rows(0)("Unit9") = "Y" Then
                chkUnit9.Checked = True
            Else
                chkUnit9.Checked = False
            End If

            If dt.Rows(0)("Unit10") = "Y" Then
                chkUnit10.Checked = True
            Else
                chkUnit10.Checked = False
            End If

            If dt.Rows(0)("Unit11") = "Y" Then
                chkUnit11.Checked = True
            Else
                chkUnit11.Checked = False
            End If

            If dt.Rows(0)("Unit12") = "Y" Then
                chkUnit12.Checked = True
            Else
                chkUnit12.Checked = False
            End If

            'cmd.Parameters.AddWithValue("@Unit_small", txtUnitsmall.Text)
            'cmd.Parameters.AddWithValue("@Unit_big", txtUnitbig.Text)
            'cmd.Parameters.AddWithValue("@barcode_pcs", txtbarcodepcs.Text)
            'cmd.Parameters.AddWithValue("@barcode_case", txtbarcodecase.Text)
            txtUnitsmall.SelectedValue = dt.Rows(0)("Unit_small")
            txtUnitbig.SelectedValue = dt.Rows(0)("Unit_big")
            txtbarcodepcs.Text = dt.Rows(0)("barcode_pcs")
            txtbarcodecase.Text = dt.Rows(0)("barcode_case")


            'cmd.Parameters.AddWithValue("@vendor", txtvendor.Text)
            'cmd.Parameters.AddWithValue("@vendor_item", txtvendor_item.Text)
            'cmd.Parameters.AddWithValue("@PurchasePrice_retail", txtPurchasePrice_retail.Text)
            'cmd.Parameters.AddWithValue("@PurchasePrice_retail_dis", txtPurchasePrice_dis.Text)
            'cmd.Parameters.AddWithValue("@PurchasePrice_retail_unit", txtPurchasePrice_unit.SelectedValue)

            'cmd.Parameters.AddWithValue("@PurchasePrice_wholesale", txtPurchasePrice_wholesale.Text)
            'cmd.Parameters.AddWithValue("@PurchasePrice_wholesale_dis", txtPurchasePrice_wholesale_dis.Text)
            'cmd.Parameters.AddWithValue("@PurchasePrice_wholesale_unit", txtPurchasePrice_wholesale_unit.SelectedValue)

            txtvendor.Text = dt.Rows(0)("vendor")
            txtvendor_item.Text = dt.Rows(0)("vendor_item")
            txtPurchasePrice_retail.Text = dt.Rows(0)("PurchasePrice_retail")
            txtPurchasePrice_dis.Text = dt.Rows(0)("PurchasePrice_retail_dis")
            txtPurchasePrice_unit.SelectedValue = dt.Rows(0)("PurchasePrice_retail_unit")

            txtPurchasePrice_wholesale.Text = dt.Rows(0)("PurchasePrice_wholesale")
            txtPurchasePrice_wholesale_dis.Text = dt.Rows(0)("PurchasePrice_wholesale_dis")
            txtPurchasePrice_wholesale_unit.SelectedValue = dt.Rows(0)("PurchasePrice_wholesale_unit")

            'cmd.Parameters.AddWithValue("@customerGroup", drpCustomerGroup.Text)
            'cmd.Parameters.AddWithValue("@customerCode", drpCustomer.Text)

            'cmd.Parameters.AddWithValue("@Price_retail", txtPrice_wholesale.Text)
            'cmd.Parameters.AddWithValue("@Price_retail_dis", txtPrice_wholesale_dis.Text)
            'cmd.Parameters.AddWithValue("@Price_retail_unit", txtPrice_unit.SelectedValue)

            'cmd.Parameters.AddWithValue("@Price_wholesale", txtPrice_wholesale.Text)
            'cmd.Parameters.AddWithValue("@Price_wholesale_dis", txtPrice_wholesale_dis.Text)
            'cmd.Parameters.AddWithValue("@Price_wholesale_unit", txtPrice_wholesale_unit.SelectedValue)

            drpCustomerGroup.Text = dt.Rows(0)("customerGroup")
            drpCustomer.Text = dt.Rows(0)("customerCode")

            txtPrice_retail.Text = dt.Rows(0)("Price_retail")
            txtPrice_dis.Text = dt.Rows(0)("Price_retail_dis")
            txtPrice_unit.SelectedValue = dt.Rows(0)("Price_retail_unit")

            txtPrice_wholesale.Text = dt.Rows(0)("Price_wholesale")
            txtPrice_wholesale_dis.Text = dt.Rows(0)("Price_wholesale_dis")
            txtPrice_wholesale_unit.SelectedValue = dt.Rows(0)("Price_wholesale_unit")


            'cmd.Parameters.AddWithValue("@LFDC", LFDC)
            If dt.Rows(0)("LFDC") = "Yes" Then
                rdoLFDC.Items.FindByValue("Yes").Selected = True
            ElseIf dt.Rows(0)("LFDC") = "No" Then
                rdoLFDC.Items.FindByValue("No").Selected = True
            End If

            'cmd.Parameters.AddWithValue("@LF_unitSmall", txtLF_unitsmall.Text)
            'cmd.Parameters.AddWithValue("@LF_unitBig", txtLF_unitbig.Text)
            'cmd.Parameters.AddWithValue("@StorageCat", txtStorageCat.Text)
            'cmd.Parameters.AddWithValue("@Primary_Group", txtPrimaryGroup.Text)
            'cmd.Parameters.AddWithValue("@Product_Category", txtProduct_cat.Text)
            txtLF_unitsmall.SelectedValue = dt.Rows(0)("LF_unitSmall")
            txtLF_unitbig.SelectedValue = dt.Rows(0)("LF_unitBig")
            txtStorageCat.SelectedValue = dt.Rows(0)("StorageCat")
            txtPrimaryGroup.SelectedValue = dt.Rows(0)("Primary_Group")
            txtProduct_cat.SelectedValue = dt.Rows(0)("Product_Category")

            'cmd.Parameters.AddWithValue("@pcs_wide", txtPcs_wide.Text)
            'cmd.Parameters.AddWithValue("@pcs_long", txtPcs_long.Text)
            'cmd.Parameters.AddWithValue("@pcs_high", txtPcs_wide.Text)
            'cmd.Parameters.AddWithValue("@pcs_weight", txtPcs_wide.Text)
            'cmd.Parameters.AddWithValue("@case_wide", txtCase_wide.Text)
            'cmd.Parameters.AddWithValue("@case_long", txtCase_long.Text)
            'cmd.Parameters.AddWithValue("@case_high", txtCase_hight.Text)
            'cmd.Parameters.AddWithValue("@case_weight", txtCase_weight.Text)
            txtPcs_wide.Text = dt.Rows(0)("pcs_wide")
            txtPcs_long.Text = dt.Rows(0)("pcs_long")
            txtPcs_hight.Text = dt.Rows(0)("pcs_high")
            txtPcs_weight.Text = dt.Rows(0)("pcs_weight")
            txtPcsGross_weight.Text = dt.Rows(0)("pcs_gross_weight")
            txtCase_wide.Text = dt.Rows(0)("case_wide")
            txtCase_long.Text = dt.Rows(0)("case_long")
            txtCase_hight.Text = dt.Rows(0)("case_high")
            txtCase_weight.Text = dt.Rows(0)("case_weight")
            txtCaseGross_weight.Text = dt.Rows(0)("case_gross_weight")

            'cmd.Parameters.AddWithValue("@product_type", rdoProductType.SelectedValue)
            If dt.Rows(0)("product_type") = "Lot" Then
                rdoProductType.Items.FindByValue("Lot").Selected = True
            ElseIf dt.Rows(0)("product_type") = "Item" Then
                rdoProductType.Items.FindByValue("Item").Selected = True
            ElseIf dt.Rows(0)("product_type") = "Serial" Then
                rdoProductType.Items.FindByValue("Serial").Selected = True
            End If


            'cmd.Parameters.AddWithValue("@product_code", txtProduct_code.Text)
            'cmd.Parameters.AddWithValue("@product_age", txtProduct_age.Text)
            'cmd.Parameters.AddWithValue("@product_store", txtProduct_store.Text)
            txtProduct_code.Text = dt.Rows(0)("product_code")
            txtProduct_age.Text = dt.Rows(0)("product_age")
            txtProduct_store.Text = dt.Rows(0)("product_store")


            'cmd.Parameters.AddWithValue("@product_exciseTax", rdoProduct_excise.SelectedValue)
            If dt.Rows(0)("product_exciseTax") = "No" Then
                rdoProduct_excise.Items.FindByValue("No").Selected = True
            ElseIf dt.Rows(0)("product_exciseTax") = "Yes" Then
                rdoProduct_excise.Items.FindByValue("Yes").Selected = True

            End If

            'cmd.Parameters.AddWithValue("@product_tax", rdoProduct_tax.SelectedValue)
            If dt.Rows(0)("product_tax") = "ภส.07-01" Then
                rdoProduct_tax.Items.FindByValue("ภส.07-01").Selected = True
            ElseIf dt.Rows(0)("product_tax") = "ภส.07-02" Then
                rdoProduct_tax.Items.FindByValue("ภส.07-02").Selected = True
            ElseIf dt.Rows(0)("product_tax") = "ภส.07-03" Then
                rdoProduct_tax.Items.FindByValue("ภส.07-03").Selected = True
            End If

            'cmd.Parameters.AddWithValue("@product_Acct", rdoProduct_Acct.SelectedValue)
            If dt.Rows(0)("product_Acct") = "วัตถุดิบ (ใช้ผลิตสินค้า) (รหัสบัญชี : 51010101)" Then
                rdoProduct_Acct.Items.FindByValue("วัตถุดิบ (ใช้ผลิตสินค้า) (รหัสบัญชี : 51010101)").Selected = True
            ElseIf dt.Rows(0)("product_Acct") = "บรรจุภันฑ์ (หีบ/ห่อ) (รหัสบัญชี : 51010104)" Then
                rdoProduct_Acct.Items.FindByValue("บรรจุภันฑ์ (หีบ/ห่อ) (รหัสบัญชี : 51010104)").Selected = True
            Else
                rdoProduct_Acct.Items.FindByValue("ส่วนผสม (ใช้ผลิตสินค้า) (รหัสบัญชี : 51010105)").Selected = True
            End If


        End If
    End Sub


    Sub UpdateData()

        Dim x As Integer
        Dim db_Mac5 As New Connect_Mac5
        Dim Cn As New SqlConnection(DB_Product.sqlCon)
        Dim kvn, lion, lfdc As String
        Dim objective, objective1, item_type, product_type, product_exciseTax, product_tax, product_Acct, itemproductcat As String
        Dim Unit1, Unit2, Unit3, Unit4, Unit5, Unit6, Unit7, Unit8, Unit9, Unit10, Unit11, Unit12, Unit13, Unit14 As String

        Dim sSql As String = "UPDATE [TB_ProductRequest_RawMat] SET  "
        sSql += " Company_KVN=@Company_KVN,Company_LION=@Company_LION ,Requester=@Requester,RequestDate=@RequestDate,objective1=@objective1,objective=@objective,item_type=@item_type"
        sSql += " ,item_productcat=@item_productcat"
        sSql += " ,ProductType=@ProductType, ProductCategory=@ProductCategory, ProductGroup=@ProductGroup, brand=@brand ,Item=@Item ,ItemDescription=@ItemDescription ,Packing_size=@Packing_size"
        sSql += " ,Unit=@Unit ,Unit1=@Unit1,Unit2=@Unit2,Unit3=@Unit3,Unit4=@Unit4,Unit5=@Unit5,Unit6=@Unit6,Unit7=@Unit7,Unit8=@Unit8,Unit9=@Unit9,Unit10=@Unit10,Unit11=@Unit11,Unit12=@Unit12,Unit13=@Unit13,Unit14=@Unit14 "
        sSql += " ,Unit_small=@Unit_small,Unit_big=@Unit_big ,barcode_pcs=@barcode_pcs ,barcode_case=@barcode_case ,vendor=@vendor,vendor_item=@vendor_item "
        sSql += " ,PurchasePrice_retail=@PurchasePrice_retail ,PurchasePrice_retail_dis=@PurchasePrice_retail_dis ,PurchasePrice_retail_unit=@PurchasePrice_retail_unit"
        sSql += " ,PurchasePrice_wholesale=@PurchasePrice_wholesale ,PurchasePrice_wholesale_dis=@PurchasePrice_wholesale_dis ,PurchasePrice_wholesale_unit=@PurchasePrice_wholesale_unit "
        sSql += " ,customerGroup=@customerGroup ,customerCode=@customerCode "
        sSql += " ,Price_retail=@Price_retail,Price_retail_dis=@Price_retail_dis ,Price_retail_unit=@Price_retail_unit  "
        sSql += " ,Price_wholesale=@Price_wholesale ,Price_wholesale_dis=@Price_wholesale_dis ,Price_wholesale_unit=@Price_wholesale_unit "
        sSql += " ,LFDC=@LFDC,LF_unitSmall=@LF_unitSmall,LF_unitBig=@LF_unitBig,StorageCat=@StorageCat"
        sSql += " ,Primary_Group=@Primary_Group,Product_Category=@Product_Category"
        sSql += " ,pcs_wide=@pcs_wide,pcs_long=@pcs_long,pcs_high=@pcs_high,pcs_weight=@pcs_weight,pcs_gross_weight=@pcs_gross_weight"
        sSql += " ,case_wide=@case_wide,case_long=@case_long,case_high=@case_high,case_weight=@case_weight,case_gross_weight=@case_gross_weight"
        sSql += " ,product_type=@product_type,product_code=@product_code,product_age=@product_age,product_store=@product_store"
        sSql += " ,product_exciseTax=@product_exciseTax,product_tax=@product_tax,product_Acct=@product_Acct"
        sSql += " ,Remark=@Remark ,ModifyBy=@ModifyBy ,ModifyDate=@ModifyDate "

        sSql += " Where requestid ='" & lblRequestID.Text & "' "

        Dim command As SqlCommand = New SqlCommand(sSql, Cn)
        Try

            If chkKVN.Checked = True Then
                kvn = "Yes"
            Else
                kvn = "No"
            End If

            If chkLION.Checked = True Then
                lion = "Yes"
            Else
                lion = "No"
            End If

            If rdoLFDC.Items.FindByValue("Yes").Selected = True Then
                lfdc = "Yes"
            Else
                lfdc = "No"
            End If

            '<asp:ListItem Value="สินค้าสำเร็จรูป" Selected="True">สินค้าสำเร็จรูป</asp:ListItem>
            '    <asp:ListItem Value="วัตถุดิบ/บรรจุภัณฑ์ (โรงงาน)">วัตถุดิบ/บรรจุภัณฑ์ (โรงงาน)</asp:ListItem>           
            '    <asp:ListItem Value="วัตถุดิบ/บรรจุภัณฑ์ (หน้าร้าน)">วัตถุดิบ/บรรจุภัณฑ์ (หน้าร้าน)</asp:ListItem>



            If rdoObjective1.Items.FindByValue("สินค้าสำเร็จรูป").Selected = True Then
                objective1 = "สินค้าสำเร็จรูป"
            ElseIf rdoObjective1.Items.FindByValue("วัตถุดิบ/บรรจุภัณฑ์ (โรงงาน)").Selected = True Then
                objective1 = "วัตถุดิบ/บรรจุภัณฑ์ (โรงงาน)"
            ElseIf rdoObjective1.Items.FindByValue("วัตถุดิบ/บรรจุภัณฑ์ (หน้าร้าน)").Selected = True Then
                objective1 = "วัตถุดิบ/บรรจุภัณฑ์ (หน้าร้าน)"
            End If


            If rdoObjective.Items.FindByValue("สินค้าผลิต").Selected = True Then
                objective = "สินค้าผลิต"
            ElseIf rdoObjective.Items.FindByValue("สินค้าซื้อมาขายไป").Selected = True Then
                objective = "สินค้าซื้อมาขายไป"
            ElseIf rdoObjective.Items.FindByValue("สินค้าระหว่างกลุ่มอโรม่า").Selected = True Then
                objective = "สินค้าระหว่างกลุ่มอโรม่า"
            ElseIf rdoObjective.Items.FindByValue("สินค้าชุด/Promotion").Selected = True Then
                objective = "สินค้าชุด/Promotion"
            ElseIf rdoObjective.Items.FindByValue("อื่นๆ").Selected = True Then
                objective = "อื่นๆ"
            End If


            command.Parameters.Add("@Company_KVN", Data.SqlDbType.VarChar).Value = kvn
            command.Parameters.Add("@Company_LION", Data.SqlDbType.VarChar).Value = lion
            command.Parameters.Add("@Requester", Data.SqlDbType.VarChar).Value = drpRequester.SelectedValue
            command.Parameters.Add("@RequestDate", Data.SqlDbType.VarChar).Value = txtRequestDate.Text
            command.Parameters.Add("@objective1", Data.SqlDbType.VarChar).Value = objective1
            command.Parameters.Add("@objective", Data.SqlDbType.VarChar).Value = objective


            If rdoItem_Type.Items.FindByValue("วัตถุดิบ (ใช้ผลิตสินค้า)").Selected = True Then
                item_type = "วัตถุดิบ (ใช้ผลิตสินค้า)"
            ElseIf rdoItem_Type.Items.FindByValue("สินค้าสำเร็จรูป (เพื่อจำหน่าย)").Selected = True Then
                item_type = "สินค้าสำเร็จรูป (เพื่อจำหน่าย)"
            ElseIf rdoItem_Type.Items.FindByValue("บรรจุภัณฑ์(หีบ/ห่อ)").Selected = True Then
                item_type = "บรรจุภัณฑ์(หีบ/ห่อ)"
            ElseIf rdoItem_Type.Items.FindByValue("สินค้าตัวอย่าง").Selected = True Then
                item_type = "สินค้าตัวอย่าง"
            ElseIf rdoItem_Type.Items.FindByValue("วัสดุสิ้นเปลือง").Selected = True Then
                item_type = "วัสดุสิ้นเปลือง"
            ElseIf rdoItem_Type.Items.FindByValue("สินค้าเพื่อส่งเสริมการขาย").Selected = True Then
                item_type = "สินค้าเพื่อส่งเสริมการขาย"
            End If

            If rdoItem_ProductCat.Items.FindByValue("Coffee").Selected = True Then
                itemproductcat = "Coffee"
            ElseIf rdoItem_ProductCat.Items.FindByValue("Fruit Concentrated").Selected = True Then
                itemproductcat = "Fruit Concentrated"
            ElseIf rdoItem_ProductCat.Items.FindByValue("Barista Tools").Selected = True Then
                itemproductcat = "Barista Tools"
            ElseIf rdoItem_ProductCat.Items.FindByValue("Cocoa").Selected = True Then
                itemproductcat = "Cocoa"
            ElseIf rdoItem_ProductCat.Items.FindByValue("Sauce").Selected = True Then
                itemproductcat = "Sauce"
            ElseIf rdoItem_ProductCat.Items.FindByValue("Hario").Selected = True Then
                itemproductcat = "Hario"
            ElseIf rdoItem_ProductCat.Items.FindByValue("Condiment").Selected = True Then
                itemproductcat = "Condiment"
            ElseIf rdoItem_ProductCat.Items.FindByValue("Syrup").Selected = True Then
                itemproductcat = "Syrup"
            ElseIf rdoItem_ProductCat.Items.FindByValue("Supply Use").Selected = True Then
                itemproductcat = "Supply Use"
            ElseIf rdoItem_ProductCat.Items.FindByValue("Tea").Selected = True Then
                itemproductcat = "Tea"
            ElseIf rdoItem_ProductCat.Items.FindByValue("Topping").Selected = True Then
                itemproductcat = "Topping"
            ElseIf rdoItem_ProductCat.Items.FindByValue("ส่งเสริมการขาย").Selected = True Then
                itemproductcat = "ส่งเสริมการขาย"
            ElseIf rdoItem_ProductCat.Items.FindByValue("Powder Mixed").Selected = True Then
                itemproductcat = "Powder Mixed"
            ElseIf rdoItem_ProductCat.Items.FindByValue("SBakery").Selected = True Then
                itemproductcat = "Bakery"
            ElseIf rdoItem_ProductCat.Items.FindByValue("เครื่องชง/เครื่องฯ").Selected = True Then
                itemproductcat = "เครื่องชง/เครื่องฯ"
            ElseIf rdoItem_ProductCat.Items.FindByValue("ชุดชงพร้อมผงผสม").Selected = True Then
                itemproductcat = "ชุดชงพร้อมผงผสม"
            ElseIf rdoItem_ProductCat.Items.FindByValue("อาหาร").Selected = True Then
                itemproductcat = "อาหาร"
            ElseIf rdoItem_ProductCat.Items.FindByValue("อะไหล่").Selected = True Then
                itemproductcat = "อะไหล่"
            End If


            command.Parameters.Add("@item_type", Data.SqlDbType.VarChar).Value = item_type
            command.Parameters.Add("@item_productcat", Data.SqlDbType.VarChar).Value = itemproductcat
            command.Parameters.Add("@ProductType", Data.SqlDbType.VarChar).Value = drpProductType.SelectedValue
            command.Parameters.Add("@ProductCategory", Data.SqlDbType.VarChar).Value = drpProductCat.SelectedValue
            command.Parameters.Add("@ProductGroup", Data.SqlDbType.VarChar).Value = drpProductGroup.SelectedValue
            command.Parameters.Add("@brand", Data.SqlDbType.VarChar).Value = drpBrand.SelectedValue
            command.Parameters.Add("@Item", Data.SqlDbType.VarChar).Value = txtItem.Text
            command.Parameters.Add("@ItemDescription", Data.SqlDbType.VarChar).Value = txtItemDescription.Text
            command.Parameters.Add("@Packing_size", Data.SqlDbType.VarChar).Value = txtPaking.Text


            'If rdoUnit.Items.FindByValue("ซอง").Selected = True Then
            '    unit = "Cocoa"
            'ElseIf rdoUnit.Items.FindByValue("ชิ้น").Selected = True Then
            '    unit = "ชิ้น"
            'ElseIf rdoUnit.Items.FindByValue("ขวด").Selected = True Then
            '    unit = "ขวด"
            'ElseIf rdoUnit.Items.FindByValue("ใบ").Selected = True Then
            '    unit = "ใบ"
            'ElseIf rdoUnit.Items.FindByValue("เครื่อง").Selected = True Then
            '    unit = "เครื่อง"
            'ElseIf rdoUnit.Items.FindByValue("เมตร").Selected = True Then
            '    unit = "เมตร"
            'ElseIf rdoUnit.Items.FindByValue("กิโลกรัม").Selected = True Then
            '    unit = "กิโลกรัม"
            'ElseIf rdoUnit.Items.FindByValue("แถว").Selected = True Then
            '    unit = "แถว"
            'ElseIf rdoUnit.Items.FindByValue("แพ็ค").Selected = True Then
            '    unit = "แพ็ค"
            'ElseIf rdoUnit.Items.FindByValue("กล่อง").Selected = True Then
            '    unit = "กล่อง"
            'ElseIf rdoUnit.Items.FindByValue("หีบ").Selected = True Then
            '    unit = "หีบ"
            'End If

            If chkUnit1.Checked = True Then
                Unit1 = "Y"
            Else
                Unit1 = "N"
            End If

            If chkUnit2.Checked = True Then
                Unit2 = "Y"
            Else
                Unit2 = "N"
            End If

            If chkUnit3.Checked = True Then
                Unit3 = "Y"
            Else
                Unit3 = "N"
            End If

            If chkUnit4.Checked = True Then
                Unit4 = "Y"
            Else
                Unit4 = "N"
            End If

            If chkUnit5.Checked = True Then
                Unit5 = "Y"
            Else
                Unit5 = "N"
            End If

            If chkUnit6.Checked = True Then
                Unit6 = "Y"
            Else
                Unit6 = "N"
            End If

            If chkUnit7.Checked = True Then
                Unit7 = "Y"
            Else
                Unit7 = "N"
            End If

            If chkUnit8.Checked = True Then
                Unit8 = "Y"
            Else
                Unit8 = "N"
            End If

            If chkUnit9.Checked = True Then
                Unit9 = "Y"
            Else
                Unit9 = "N"
            End If

            If chkUnit10.Checked = True Then
                Unit10 = "Y"
            Else
                Unit10 = "N"
            End If

            If chkUnit11.Checked = True Then
                Unit11 = "Y"
            Else
                Unit11 = "N"
            End If

            If chkUnit12.Checked = True Then
                Unit12 = "Y"
            Else
                Unit12 = "N"
            End If

            If chkUnit13.Checked = True Then
                Unit13 = "Y"
            Else
                Unit13 = "N"
            End If

            If chkUnit14.Checked = True Then
                Unit14 = "Y"
            Else
                Unit14 = "N"
            End If

            command.Parameters.Add("@Unit", Data.SqlDbType.VarChar).Value = ""
            command.Parameters.Add("@Unit1", Data.SqlDbType.VarChar).Value = Unit1
            command.Parameters.Add("@Unit2", Data.SqlDbType.VarChar).Value = Unit2
            command.Parameters.Add("@Unit3", Data.SqlDbType.VarChar).Value = Unit3
            command.Parameters.Add("@Unit4", Data.SqlDbType.VarChar).Value = Unit4
            command.Parameters.Add("@Unit5", Data.SqlDbType.VarChar).Value = Unit5
            command.Parameters.Add("@Unit6", Data.SqlDbType.VarChar).Value = Unit6
            command.Parameters.Add("@Unit7", Data.SqlDbType.VarChar).Value = Unit7
            command.Parameters.Add("@Unit8", Data.SqlDbType.VarChar).Value = Unit8
            command.Parameters.Add("@Unit9", Data.SqlDbType.VarChar).Value = Unit9
            command.Parameters.Add("@Unit10", Data.SqlDbType.VarChar).Value = Unit10
            command.Parameters.Add("@Unit11", Data.SqlDbType.VarChar).Value = Unit11
            command.Parameters.Add("@Unit12", Data.SqlDbType.VarChar).Value = Unit12
            command.Parameters.Add("@Unit13", Data.SqlDbType.VarChar).Value = Unit13
            command.Parameters.Add("@Unit14", Data.SqlDbType.VarChar).Value = Unit14

            command.Parameters.Add("@Unit_small", Data.SqlDbType.VarChar).Value = txtUnitsmall.SelectedValue
            command.Parameters.Add("@Unit_big", Data.SqlDbType.VarChar).Value = txtUnitbig.SelectedValue

            command.Parameters.Add("@barcode_pcs", Data.SqlDbType.VarChar).Value = txtbarcodepcs.Text
            command.Parameters.Add("@barcode_case", Data.SqlDbType.VarChar).Value = txtbarcodecase.Text


            'cmd.Parameters.AddWithValue("@vendor", txtvendor.Text)
            'cmd.Parameters.AddWithValue("@vendor_item", txtvendor_item.Text)
            'cmd.Parameters.AddWithValue("@PurchasePrice_retail", txtPurchasePrice_retail.Text)
            'cmd.Parameters.AddWithValue("@PurchasePrice_retail_dis", txtPurchasePrice_dis.Text)
            'cmd.Parameters.AddWithValue("@PurchasePrice_retail_unit", txtPurchasePrice_unit.SelectedValue)
            'cmd.Parameters.AddWithValue("@PurchasePrice_wholesale", txtPurchasePrice_wholesale.Text)
            'cmd.Parameters.AddWithValue("@PurchasePrice_wholesale_dis", txtPurchasePrice_wholesale_dis.Text)
            'cmd.Parameters.AddWithValue("@PurchasePrice_wholesale_unit", txtPurchasePrice_wholesale_unit.SelectedValue)


            command.Parameters.Add("@vendor", Data.SqlDbType.VarChar).Value = txtvendor.Text
            command.Parameters.Add("@vendor_item", Data.SqlDbType.VarChar).Value = txtvendor_item.Text
            command.Parameters.Add("@PurchasePrice_retail", Data.SqlDbType.VarChar).Value = txtPurchasePrice_retail.Text
            command.Parameters.Add("@PurchasePrice_retail_dis", Data.SqlDbType.VarChar).Value = txtPurchasePrice_dis.Text
            command.Parameters.Add("@PurchasePrice_retail_unit", Data.SqlDbType.VarChar).Value = txtPurchasePrice_unit.SelectedValue

            command.Parameters.Add("@PurchasePrice_wholesale", Data.SqlDbType.VarChar).Value = txtPurchasePrice_wholesale.Text
            command.Parameters.Add("@PurchasePrice_wholesale_dis", Data.SqlDbType.VarChar).Value = txtPurchasePrice_wholesale_dis.Text
            command.Parameters.Add("@PurchasePrice_wholesale_unit", Data.SqlDbType.VarChar).Value = txtPurchasePrice_wholesale_unit.SelectedValue

            'cmd.Parameters.AddWithValue("@customerGroup", drpCustomerGroup.Text)
            'cmd.Parameters.AddWithValue("@customerCode", drpCustomer.Text)
            'cmd.Parameters.AddWithValue("@Price_retail", txtPrice_wholesale.Text)
            'cmd.Parameters.AddWithValue("@Price_retail_dis", txtPrice_wholesale_dis.Text)
            'cmd.Parameters.AddWithValue("@Price_retail_unit", txtPrice_unit.SelectedValue)
            'cmd.Parameters.AddWithValue("@Price_wholesale", txtPrice_wholesale.Text)
            'cmd.Parameters.AddWithValue("@Price_wholesale_dis", txtPrice_wholesale_dis.Text)
            'cmd.Parameters.AddWithValue("@Price_wholesale_unit", txtPrice_wholesale_unit.SelectedValue)

            command.Parameters.Add("@customerGroup", Data.SqlDbType.VarChar).Value = drpCustomerGroup.Text
            command.Parameters.Add("@customerCode", Data.SqlDbType.VarChar).Value = drpCustomer.Text

            command.Parameters.Add("@Price_retail", Data.SqlDbType.VarChar).Value = txtPrice_retail.Text
            command.Parameters.Add("@Price_retail_dis", Data.SqlDbType.VarChar).Value = txtPrice_dis.Text
            command.Parameters.Add("@Price_retail_unit", Data.SqlDbType.VarChar).Value = txtPrice_unit.SelectedValue

            command.Parameters.Add("@Price_wholesale", Data.SqlDbType.VarChar).Value = txtPrice_wholesale.Text
            command.Parameters.Add("@Price_wholesale_dis", Data.SqlDbType.VarChar).Value = txtPrice_wholesale_dis.Text
            command.Parameters.Add("@Price_wholesale_unit", Data.SqlDbType.VarChar).Value = txtPrice_wholesale_unit.SelectedValue

            command.Parameters.Add("@LFDC", Data.SqlDbType.VarChar).Value = lfdc
            command.Parameters.Add("@LF_unitSmall", Data.SqlDbType.VarChar).Value = txtUnitsmall.SelectedValue
            command.Parameters.Add("@LF_unitBig", Data.SqlDbType.VarChar).Value = txtUnitbig.SelectedValue
            command.Parameters.Add("@StorageCat", Data.SqlDbType.VarChar).Value = txtStorageCat.SelectedValue
            command.Parameters.Add("@Primary_Group", Data.SqlDbType.VarChar).Value = txtPrimaryGroup.SelectedValue
            command.Parameters.Add("@Product_Category", Data.SqlDbType.VarChar).Value = txtProduct_cat.SelectedValue

            'Query &= ",pcs_wide,pcs_long ,pcs_high,pcs_weight,case_wide,case_long,case_high,case_weight"
            'Query &= ",product_type,product_code,product_age,product_store ,product_exciseTax,product_tax ,product_Acct"
            ''query &= ",SCMBy,SCMDate,AcctBy,AcctDate"
            'Query &= ",Remark,Stat,CreateBy ,CreateDate"

            command.Parameters.Add("@pcs_wide", Data.SqlDbType.NVarChar).Value = txtPcs_wide.Text
            command.Parameters.Add("@pcs_long", Data.SqlDbType.NVarChar).Value = txtPcs_long.Text
            command.Parameters.Add("@pcs_high", Data.SqlDbType.NVarChar).Value = txtPcs_hight.Text
            command.Parameters.Add("@pcs_weight", Data.SqlDbType.NVarChar).Value = txtPcs_weight.Text
            command.Parameters.Add("@pcs_gross_weight", Data.SqlDbType.NVarChar).Value = txtPcsGross_weight.Text
            command.Parameters.Add("@case_wide", Data.SqlDbType.NVarChar).Value = txtCase_wide.Text
            command.Parameters.Add("@case_long", Data.SqlDbType.NVarChar).Value = txtCase_long.Text
            command.Parameters.Add("@case_high", Data.SqlDbType.NVarChar).Value = txtCase_hight.Text
            command.Parameters.Add("@case_weight", Data.SqlDbType.NVarChar).Value = txtCase_weight.Text
            command.Parameters.Add("@case_gross_weight", Data.SqlDbType.NVarChar).Value = txtCaseGross_weight.Text


            If rdoProductType.Items.FindByValue("Lot").Selected = True Then
                product_type = "Lot"
            ElseIf rdoProductType.Items.FindByValue("Item").Selected = True Then
                product_type = "Item"
            ElseIf rdoProductType.Items.FindByValue("Serial").Selected = True Then
                product_type = "Serial"
            End If


            command.Parameters.Add("@product_type", Data.SqlDbType.VarChar).Value = product_type
            command.Parameters.Add("@product_code", Data.SqlDbType.VarChar).Value = txtProduct_code.Text
            command.Parameters.Add("@product_age", Data.SqlDbType.VarChar).Value = txtProduct_age.Text
            command.Parameters.Add("@product_store", Data.SqlDbType.VarChar).Value = txtProduct_store.Text



            If rdoProduct_excise.Items.FindByValue("No").Selected = True Then
                product_exciseTax = "No"
            ElseIf rdoProduct_excise.Items.FindByValue("Yes").Selected = True Then
                product_exciseTax = "Yes"
            End If


            If rdoProduct_tax.Items.FindByValue("ภส.07-01").Selected = True Then
                product_tax = "ภส.07-01"
            ElseIf rdoProduct_tax.Items.FindByValue("ภส.07-02").Selected = True Then
                product_tax = "ภส.07-02"
            ElseIf rdoProduct_tax.Items.FindByValue("ภส.07-03").Selected = True Then
                product_tax = "ภส.07-03"
            Else
                product_tax = ""
            End If


            If rdoProduct_Acct.Items.FindByValue("วัตถุดิบ (ใช้ผลิตสินค้า) (รหัสบัญชี : 51010101)").Selected = True Then
                product_Acct = "วัตถุดิบ (ใช้ผลิตสินค้า) (รหัสบัญชี : 51010101)"
            ElseIf rdoProduct_Acct.Items.FindByValue("บรรจุภันฑ์ (หีบ/ห่อ) (รหัสบัญชี : 51010104)").Selected = True Then
                product_Acct = "บรรจุภันฑ์ (หีบ/ห่อ) (รหัสบัญชี : 51010104)"
            ElseIf rdoProduct_Acct.Items.FindByValue("ส่วนผสม (ใช้ผลิตสินค้า) (รหัสบัญชี : 51010105)").Selected = True Then
                product_Acct = "ส่วนผสม (ใช้ผลิตสินค้า) (รหัสบัญชี : 51010105)"
            Else
                product_Acct = ""
            End If


            command.Parameters.Add("@product_exciseTax", Data.SqlDbType.VarChar).Value = product_exciseTax
            command.Parameters.Add("@product_tax", Data.SqlDbType.VarChar).Value = product_tax
            command.Parameters.Add("@product_Acct", Data.SqlDbType.VarChar).Value = product_Acct
            command.Parameters.Add("@Remark", Data.SqlDbType.VarChar).Value = ""

            command.Parameters.Add("@ModifyBy", Data.SqlDbType.VarChar).Value = usr
            command.Parameters.Add("@ModifyDate", Data.SqlDbType.VarChar).Value = DateTime.Now.ToString("yyyy-MM-dd")


            command.CommandType = Data.CommandType.Text
            Cn.Open()
            x = command.ExecuteScalar
        Catch x1 As Exception
            Err.Raise(60002, , x1.Message)
        Finally
            Cn.Close()
        End Try

    End Sub




    Protected Sub drpProductType_Init(sender As Object, e As EventArgs) Handles drpProductType.Init
        Dim sqlCon As New SqlConnection(DB_Product.sqlCon)
        Dim sqlCmd As New SqlCommand
        Dim Rs As SqlDataReader
        Dim sql As String


        sql = "SELECT [ProductType] FROM [dbo].[TB_ProductMaster] where ProductType is not null group by [ProductType]  "

        If sqlCon.State = ConnectionState.Closed Then
            sqlCon = New SqlConnection(DB_Product.sqlCon)
            sqlCon.Open()
        End If
        With sqlCmd
            .Connection = sqlCon
            .CommandType = CommandType.Text
            .CommandText = sql
            Rs = .ExecuteReader
        End With
        drpProductType.Items.Clear()
        drpProductType.Items.Add(New ListItem("", 0))
        While Rs.Read
            drpProductType.Items.Add(New ListItem(Rs.GetString(0), Rs.GetString(0)))
        End While
    End Sub

    Protected Sub drpProductType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles drpProductType.SelectedIndexChanged
        Dim sqlCon As New SqlConnection(DB_Product.sqlCon)
        Dim sqlCmd As New SqlCommand
        Dim Rs As SqlDataReader
        Dim sql As String


        sql = "SELECT [ProductCategory] FROM [dbo].[TB_ProductMaster] "

        sql = sql & "where ([ProductCategory] Is Not null)  and ProductType  = '" & drpProductType.SelectedValue & "' "

        sql = sql & "group by [ProductCategory]  "


        If sqlCon.State = ConnectionState.Closed Then
            sqlCon = New SqlConnection(DB_Product.sqlCon)
            sqlCon.Open()
        End If
        With sqlCmd
            .Connection = sqlCon
            .CommandType = CommandType.Text
            .CommandText = sql
            Rs = .ExecuteReader
        End With
        drpProductCat.Items.Clear()
        drpProductCat.Items.Add(New ListItem("", 0))
        While Rs.Read
            drpProductCat.Items.Add(New ListItem(Rs.GetString(0), Rs.GetString(0)))
        End While
    End Sub

    Protected Sub drpProductCat_Init(sender As Object, e As EventArgs) Handles drpProductCat.Init
        Dim sqlCon As New SqlConnection(DB_Product.sqlCon)
        Dim sqlCmd As New SqlCommand
        Dim Rs As SqlDataReader
        Dim sql As String


        sql = "SELECT [ProductCategory] FROM [dbo].[TB_ProductMaster]  where [ProductCategory] is not null  group by [ProductCategory]  "


        If sqlCon.State = ConnectionState.Closed Then
            sqlCon = New SqlConnection(DB_Product.sqlCon)
            sqlCon.Open()
        End If
        With sqlCmd
            .Connection = sqlCon
            .CommandType = CommandType.Text
            .CommandText = sql
            Rs = .ExecuteReader
        End With
        drpProductCat.Items.Clear()
        drpProductCat.Items.Add(New ListItem("", 0))
        While Rs.Read
            drpProductCat.Items.Add(New ListItem(Rs.GetString(0), Rs.GetString(0)))
        End While
    End Sub

    Protected Sub drpProductCat_SelectedIndexChanged(sender As Object, e As EventArgs) Handles drpProductCat.SelectedIndexChanged
        Dim sqlCon As New SqlConnection(DB_Product.sqlCon)
        Dim sqlCmd As New SqlCommand
        Dim Rs As SqlDataReader
        Dim sql As String


        sql = " SELECT [ProductGroup] FROM [dbo].[TB_ProductMaster] "
        sql = sql & " where  ProductCategory  = '" & drpProductCat.SelectedValue & "' "
        sql = sql & " and  ProductType  = '" & drpProductType.SelectedValue & "' "


        If sqlCon.State = ConnectionState.Closed Then
            sqlCon = New SqlConnection(DB_Product.sqlCon)
            sqlCon.Open()
        End If
        With sqlCmd
            .Connection = sqlCon
            .CommandType = CommandType.Text
            .CommandText = sql
            Rs = .ExecuteReader
        End With
        drpProductGroup.Items.Clear()
        drpProductGroup.Items.Add(New ListItem("", 0))
        While Rs.Read
            drpProductGroup.Items.Add(New ListItem(Rs.GetString(0), Rs.GetString(0)))
        End While
    End Sub

    Protected Sub drpProductGroup_Init(sender As Object, e As EventArgs) Handles drpProductGroup.Init
        Dim sqlCon As New SqlConnection(DB_Product.sqlCon)
        Dim sqlCmd As New SqlCommand
        Dim Rs As SqlDataReader
        Dim sql As String


        sql = "SELECT [ProductGroup] FROM [dbo].[TB_ProductMaster]  where [ProductGroup] is not null  group by [ProductGroup]  "


        If sqlCon.State = ConnectionState.Closed Then
            sqlCon = New SqlConnection(DB_Product.sqlCon)
            sqlCon.Open()
        End If
        With sqlCmd
            .Connection = sqlCon
            .CommandType = CommandType.Text
            .CommandText = sql
            Rs = .ExecuteReader
        End With
        drpProductGroup.Items.Clear()
        drpProductGroup.Items.Add(New ListItem("", 0))
        While Rs.Read
            drpProductGroup.Items.Add(New ListItem(Rs.GetString(0), Rs.GetString(0)))
        End While
    End Sub


    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        Dim kvn, lion, company As String

        If chkKVN.Checked = False And chkLION.Checked = False Then
            sms.Msg = "กรุณาระบุบริษัท"
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "window.alert('" & sms.Msg & "');", True)
            Exit Sub
        End If


        '****
        'sql = " select * "
        'sql += " from VW_ProductRequest "
        'sql += " where item = '" & txtItem.Text & "'  "
        'dt = DB_Product.GetDataTable(sql)
        'If dt.Rows.Count > 0 Then
        '    sms.Msg = "Duplicate Item !"
        '    Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "window.alert('" & sms.Msg & "');", True)
        '    Exit Sub
        'End If

        '-----------check ข้อมูลที่ต้องระบุ------------------'
        If txtItemDescription.Text = "" Then
            sms.Msg = "กรุณาระบุชื่อสินค้า"
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "window.alert('" & sms.Msg & "');", True)
            Exit Sub
        End If

        If txtPaking.Text = "" Then
            sms.Msg = "กรุณาระบุขนาดบรรจุ"
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "window.alert('" & sms.Msg & "');", True)
            Exit Sub
        End If

        If txtUnitsmall.SelectedValue = "0" Or txtUnitbig.SelectedValue = "0" Then
            sms.Msg = "กรุณาระบุหน่วยนับ"
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "window.alert('" & sms.Msg & "');", True)
            Exit Sub
        End If

        If drpProductType.SelectedValue = "0" Or drpProductCat.SelectedValue = "0" Or drpProductGroup.SelectedValue = "0" Then
            sms.Msg = "กรุณาระบุข้อมูลกลุ่มสินค้า ให้ครบ"
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "window.alert('" & sms.Msg & "');", True)
            Exit Sub
        End If

        If txtbarcodecase.Text = "" Or txtbarcodepcs.Text = "" Then
            sms.Msg = "กรุณาระบุ Barcode"
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "window.alert('" & sms.Msg & "');", True)
            Exit Sub
        End If

        If rdoLFDC.Items.FindByValue("Yes").Selected = True Then
            If txtPcs_wide.Text = "" Or txtPcs_long.Text = "" Or txtPcs_hight.Text = "" Or txtPcs_weight.Text = "" Or txtCase_wide.Text = "" Or txtCase_long.Text = "" Or txtCase_hight.Text = "" Or txtCase_weight.Text = "" Or txtLF_unitsmall.SelectedValue = "0" Or txtLF_unitbig.SelectedValue = "0" Or txtStorageCat.SelectedValue = "0" Or txtPrimaryGroup.SelectedValue = "0" Or txtProduct_cat.SelectedValue = "0" Then
                sms.Msg = "กรุณาระบุข้อมูลสินค้าเข้าคลัง LFDC ให้ครบถ้วน"
                Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "window.alert('" & sms.Msg & "');", True)
                Exit Sub
            End If
        Else
            '------
        End If

        If rdoObjective.Items.FindByValue("สินค้าผลิต").Selected = True Then
            If txtProduct_code.Text = "" Or txtProduct_age.Text = "" Or txtProduct_store.Text = "" Or txtPcs_weight.Text = "" Or txtCase_wide.Text = "" Or txtCase_long.Text = "" Or txtCase_hight.Text = "" Or txtCase_weight.Text = "" Or txtLF_unitsmall.SelectedValue = "0" Or txtLF_unitbig.SelectedValue = "0" Or txtStorageCat.SelectedValue = "0" Or txtPrimaryGroup.SelectedValue = "0" Or txtProduct_cat.SelectedValue = "0" Then
                sms.Msg = "กรุณาระบุข้อมูลสำคัญสินค้า ให้ครบถ้วน"
                Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "window.alert('" & sms.Msg & "');", True)
                Exit Sub
            End If
        Else
            '------
        End If

        If rdoProduct_excise.Items.FindByValue("Yes").Selected = True Then
            If rdoProduct_tax.Items.FindByValue("ภส.07-01").Selected = False And rdoProduct_tax.Items.FindByValue("ภส.07-02").Selected = False And rdoProduct_tax.Items.FindByValue("ภส.07-03").Selected = False Then
                sms.Msg = "กรุณาระบุประเภทภาษี"
                Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "window.alert('" & sms.Msg & "');", True)
                Exit Sub
            End If
        End If

        If rdoObjective1.Items.FindByValue("วัตถุดิบ/บรรจุภัณฑ์ (โรงงาน)").Selected = True Then
            If rdoProduct_Acct.Items.FindByValue("วัตถุดิบ (ใช้ผลิตสินค้า) (รหัสบัญชี : 51010101)").Selected = False And rdoProduct_Acct.Items.FindByValue("บรรจุภันฑ์ (หีบ/ห่อ) (รหัสบัญชี : 51010104)").Selected = False And rdoProduct_Acct.Items.FindByValue("ส่วนผสม (ใช้ผลิตสินค้า) (รหัสบัญชี : 51010105)").Selected = False Then
                sms.Msg = "กรุณาระบุผังบัญชีซื้อ (โรงงาน)"
                Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "window.alert('" & sms.Msg & "');", True)
                Exit Sub
            End If
        End If


        '-----------end check ข้อมูลที่ต้องระบุ------------------'

        sql = " select * "
        sql += " from [VW_ProductRequest_RawMat] "
        sql += " where RequestId = '" & lblRequestID.Text & "' "
        dt = DB_Product.GetDataTable(sql)
        If dt.Rows.Count >= 1 Then
            UpdateData()
        Else

            GenID()
            lblRequestID.Text = RequestId
            InsertData(RequestId)
        End If


        ''check ส่ง LF หรือไม่ 
        'If rdoLFDC.Items.FindByValue("Yes").Selected = True Then
        '    UpdateStatus_waitSCM()
        '    SentMail_SCM()
        'Else
        '    UpdateStatus_waitAccount()
        '    SentMail_Acct()
        'End If

        UpdateStatus_waitManager()
        SentMail_Manager()

        Server.Transfer("NewMasterAll.aspx?usr=" & usr)

    End Sub


    Sub GenID()

        Dim conn As New Connect_product
        Dim sql As String = "Select * from    TB_ProductRequest_RawMat where len(RequestId) = 8 and substring(cast(year(getdate()) as varchar(50)),3,2) = substring(RequestId,2,2) "
        Dim sql2 As String = "Select Max(Right(RequestId,4)) as RequestId from    TB_ProductRequest_RawMat where len(RequestId) = 8  and substring(cast(year(getdate()) as varchar(50)),3,2) = substring(RequestId,2,2)"

        dt = DB_Product.GetDataTable(sql)
        dt2 = DB_Product.GetDataTable(sql2)

        If dt.Rows.Count <= 0 Then
            DocID = "R" & Now.ToString("yy") & "-" & "0001"
        Else
            Dim newID As Integer = CInt(dt2.Rows(0)("RequestId"))
            newID += 1
            DocID = "R" & Now.ToString("yy") & "-" & newID.ToString("0000")
        End If
        RequestId = DocID
        lblRequestID.Text = RequestId
    End Sub

    Protected Sub btnApproveSCM_Click(sender As Object, e As EventArgs) Handles btnApproveSCM.Click
        UpdateData()
        UpdateStatus_waitacct()
        SentMail_Acct()

        Server.Transfer("NewMasterAll.aspx?usr=" & usr)

    End Sub

    Protected Sub btnApproveAcct_Click(sender As Object, e As EventArgs) Handles btnApproveAcct.Click

        If txtItem.Text = "" Then
            sms.Msg = "กรุณาระบุรหัสสินค้า"
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "window.alert('" & sms.Msg & "');", True)
            Exit Sub
        End If

        UpdateData()
        UpdateStatus_complete()

        If chkKVN.Checked = True Then
            InsertBI_kvn()
            insert_enpro()
        End If

        If chkLION.Checked = True Then
            InsertBI_lion()
            insert_mac5()
        End If

        SentMail_All()

        Server.Transfer("NewMasterAll.aspx?usr=" & usr)

    End Sub

    Sub UpdateStatus_complete()

        Dim x As Integer
        Dim db_Mac5 As New Connect_Mac5
        Dim Cn As New SqlConnection(DB_Product.sqlCon)

        Dim sSql As String = "UPDATE [TB_ProductRequest_RawMat] SET  "
        sSql += " Stat='Complete' ,ModifyBy=@ModifyBy ,ModifyDate=@ModifyDate "
        sSql += " ,AcctBy=@AcctBy , AcctDate=@AcctDate"

        sSql += " Where RequestId ='" & lblRequestID.Text & "' "

        Dim command As SqlCommand = New SqlCommand(sSql, Cn)
        Try
            command.Parameters.Add("@AcctBy", Data.SqlDbType.VarChar).Value = usr
            command.Parameters.Add("@AcctDate", Data.SqlDbType.VarChar).Value = DateTime.Now.ToString("yyyy-MM-dd")
            command.Parameters.Add("@ModifyBy", Data.SqlDbType.VarChar).Value = usr
            command.Parameters.Add("@ModifyDate", Data.SqlDbType.VarChar).Value = DateTime.Now.ToString("yyyy-MM-dd")

            command.CommandType = Data.CommandType.Text
            Cn.Open()
            x = command.ExecuteScalar
        Catch x1 As Exception
            Err.Raise(60002, , x1.Message)
        Finally
            Cn.Close()
        End Try

    End Sub

    Sub UpdateStatus_waitacct()

        Dim x As Integer
        Dim db_Mac5 As New Connect_Mac5
        Dim Cn As New SqlConnection(DB_Product.sqlCon)

        Dim sSql As String = "UPDATE [TB_ProductRequest_RawMat] SET  "
        sSql += " Stat='Waiting Account Approve' ,ModifyBy=@ModifyBy ,ModifyDate=@ModifyDate "
        sSql += " ,SCMBy=@SCMBy , SCMDate=@SCMDate"

        sSql += " Where RequestId ='" & lblRequestID.Text & "' "


        Dim command As SqlCommand = New SqlCommand(sSql, Cn)
        Try
            command.Parameters.Add("@SCMBy", Data.SqlDbType.VarChar).Value = usr
            command.Parameters.Add("@SCMDate", Data.SqlDbType.VarChar).Value = DateTime.Now.ToString("yyyy-MM-dd")
            command.Parameters.Add("@ModifyBy", Data.SqlDbType.VarChar).Value = usr
            command.Parameters.Add("@ModifyDate", Data.SqlDbType.VarChar).Value = DateTime.Now.ToString("yyyy-MM-dd")

            command.CommandType = Data.CommandType.Text
            Cn.Open()
            x = command.ExecuteScalar
        Catch x1 As Exception
            Err.Raise(60002, , x1.Message)
        Finally
            Cn.Close()
        End Try

    End Sub

    Sub UpdateStatus_waitManager()

        Dim x As Integer
        Dim db_Mac5 As New Connect_Mac5
        Dim Cn As New SqlConnection(DB_Product.sqlCon)

        Dim sSql As String = "UPDATE [TB_ProductRequest_RawMat] SET  "
        sSql += " Stat='Waiting Manager Approve' ,ModifyBy=@ModifyBy ,ModifyDate=@ModifyDate "

        sSql += " Where RequestId ='" & lblRequestID.Text & "' "


        Dim command As SqlCommand = New SqlCommand(sSql, Cn)
        Try
            command.Parameters.Add("@ModifyBy", Data.SqlDbType.VarChar).Value = usr
            command.Parameters.Add("@ModifyDate", Data.SqlDbType.VarChar).Value = DateTime.Now.ToString("yyyy-MM-dd")

            command.CommandType = Data.CommandType.Text
            Cn.Open()
            x = command.ExecuteScalar
        Catch x1 As Exception
            Err.Raise(60002, , x1.Message)
        Finally
            Cn.Close()
        End Try

    End Sub

    Sub UpdateStatus_waitSCM()

        Dim x As Integer
        Dim db_Mac5 As New Connect_Mac5
        Dim Cn As New SqlConnection(DB_Product.sqlCon)

        Dim sSql As String = "UPDATE [TB_ProductRequest_RawMat] SET  "
        sSql += " Stat='Waiting Supply Chain Approve' ,ModifyBy=@ModifyBy ,ModifyDate=@ModifyDate "
        sSql += " ,ManagerBy=@ManagerBy,ManagerDate=@ManagerDate "
        sSql += " Where RequestId ='" & lblRequestID.Text & "' "


        Dim command As SqlCommand = New SqlCommand(sSql, Cn)
        Try
            command.Parameters.Add("@ManagerBy", Data.SqlDbType.VarChar).Value = usr
            command.Parameters.Add("@ManagerDate", Data.SqlDbType.VarChar).Value = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")
            command.Parameters.Add("@ModifyBy", Data.SqlDbType.VarChar).Value = usr
            command.Parameters.Add("@ModifyDate", Data.SqlDbType.VarChar).Value = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")

            command.CommandType = Data.CommandType.Text
            Cn.Open()
            x = command.ExecuteScalar
        Catch x1 As Exception
            Err.Raise(60002, , x1.Message)
        Finally
            Cn.Close()
        End Try

    End Sub

    Sub UpdateStatus_waitAccount()

        Dim x As Integer
        Dim db_Mac5 As New Connect_Mac5
        Dim Cn As New SqlConnection(DB_Product.sqlCon)

        Dim sSql As String = "UPDATE [TB_ProductRequest_RawMat] SET  "
        sSql += " Stat='Waiting Account Approve' ,ModifyBy=@ModifyBy ,ModifyDate=@ModifyDate "
        sSql += " ,ManagerBy=@ManagerBy,ManagerDate=@ManagerDate "
        sSql += " Where RequestId ='" & lblRequestID.Text & "' "


        Dim command As SqlCommand = New SqlCommand(sSql, Cn)
        Try
            command.Parameters.Add("@ManagerBy", Data.SqlDbType.VarChar).Value = usr
            command.Parameters.Add("@ManagerDate", Data.SqlDbType.VarChar).Value = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")
            command.Parameters.Add("@ModifyBy", Data.SqlDbType.VarChar).Value = usr
            command.Parameters.Add("@ModifyDate", Data.SqlDbType.VarChar).Value = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")

            command.CommandType = Data.CommandType.Text
            Cn.Open()
            x = command.ExecuteScalar
        Catch x1 As Exception
            Err.Raise(60002, , x1.Message)
        Finally
            Cn.Close()
        End Try

    End Sub

    Sub insert_mac5()
        Dim Cn As New SqlConnection(db_Mac5.sqlCon)
        Dim sqlcon As New OleDbConnection(db_Mac5.cnPAStr)
        Dim sqlcom As New OleDbCommand


        sql = " SELECT STKcode "
        sql += " from    [STK] "
        sql += " where STKcode = '" & txtItem.Text & "'"
        dt = db_Mac5.GetDataTable(sql)
        If dt.Rows.Count <= 0 Then   'check ซ้ำ

            Dim sSql As String = "INSERT INTO [STK] "
            sSql += " (STKcode,STKgroup,STKdescT1,STKdescT2)"
            sSql += " Values(  "
            sSql += "'" & txtItem.Text & "'"
            sSql += ",''"
            sSql += ",'" & txtItemDescription.Text & "'"
            sSql += ",'')"
            If sqlcon.State = ConnectionState.Closed Then
                sqlcon = New OleDbConnection(db_Mac5.cnPAStr)
                sqlcon.Open()
            End If
            With sqlcom
                .Connection = sqlcon
                .CommandType = CommandType.Text
                .CommandText = sSql
                .ExecuteNonQuery()
            End With

        End If


    End Sub


    Sub insert_enpro()
        Dim Cn As New SqlConnection(db_Enpro.sqlCon)
        Dim sqlcon As New OleDbConnection(db_Enpro.cnPAStr)
        Dim sqlcom As New OleDbCommand

        Dim lot As String
        If rdoProductType.Items.FindByValue("Lot").Selected = True Then
            lot = "L"
        ElseIf rdoProductType.Items.FindByValue("Item").Selected = True Then
            lot = "I"
        ElseIf rdoProductType.Items.FindByValue("Serial").Selected = True Then
            lot = "X"
        End If

        sql = " SELECT code "
        sql += " from    [SMITEMMS] "
        sql += " where code = '" & txtItem.Text & "'"
        dt = db_Enpro.GetDataTable(sql)
        If dt.Rows.Count <= 0 Then   'check ซ้ำ

            Dim sSql As String = " "
            sSql += " insert into [SMITEMMS] ("
            sSql += " code, [Code_Secondary], [SMSTORMS_ID], [Type], [F_MRP], [Name1_Th], [Name2_Th], [Name3_Th], [Name1_En], [Name2_En]"
            sSql += " ,[Name3_En],[Descr_Th],[Descr_En],[LeadTime_Pd],[Lot_Size_Pd] ,[LeadTime_Dl_Buffer],[RateWaste_Pd],[Weight],[F_Expire],[Alert_Month]"
            sSql += " ,[Alert_Day],[Expire_Month],[Expire_Day] ,[F_Bin] ,[F_Inspect],[F_Budget],[Inspect_SMSTORMS_ID],[Df_Ltsr],[Ltsr_Charecter],[Df_LtsrFormat]"
            sSql += " ,[Ltsr_running],[F_Unit_MS],[Main_SMUNITMS_ID],[Second_SMUNITMS_ID],[Small_SMUNITMS_ID],[Pur_SMUNITMS_ID],[Sale_SMUNITMS_ID],[F_Parallel],[F_Parallel_Lot],[Par1_SMUNITMS_ID]"
            sSql += " ,[Par2_SMUNITMS_ID],[SMGRP1MS_ID],[SMGRP2MS_ID],[SMGRP3MS_ID],[SMGRP4MS_ID],[SMGRP5MS_ID],[Ref1],[Ref2],[Ref3],[Ref4]"
            sSql += " ,[Ref5],[Remark1],[Remark2],[Remark3],[Remark4] ,[Remark5],[F_Active],[F_KIT],[FileName1],[FileName2]"
            sSql += " ,[FileName3],[FileName4],[FileName5],[FileName6],[F_Pararell_Avg],[F_Tool],[F_Control_Tool],[Tool_Expire_Day],[Tool_Expire_Month]     "
            sSql += " ,[Tool_Alert_Day],[Tool_Alert_Month],[F_bom_cost],[min_qty] ,[max_qty],[F_Lot_FIFO],[F_Cost],[Picture1],[Picture2],[Picture3] "
            sSql += " ,[Picture4],[Picture5],[Picture6],[Df_Serial],[Serial_Charecter],[Df_SerialFormat],[Serial_running],[F_IssuePriority],[Mfg_SMUNITMS_ID],[Seq_Group]"
            sSql += " ,[Activity_Date],[Start_Sale_Date],[Last_Sale_Date],[F_Unit_Control],[Width],[Lenght],[Hight],[MRSTPTHD_id],[F_Auto_Convert_P1_Qty],[Status]     "
            sSql += " ,[F_UseRateWaste],[Temp_SMSTORMS_ID],[SMACCOMS_ID],[LeadTime_Pd_Self],[F_RECMoreThanPO],[Qty_RECMoreThanPO],[Percent_RECMoreThanPO],[User_Code],[Update_Date_Time],[Daily_Capacity]   "
            sSql += " ,[Ref6],[F_UseItemWeightAddUnitWeight],[F_RECMoreThanSTD_P],[Qty_RECMoreThanSTD_P],[Percent_RECMoreThanSTD_P],[F_RECMoreThanPR],[Qty_RECMoreThanPR],[Percent_RECMoreThanPR],[f_Integer_Qty],[F_RECMoreThanPO_P]"
            sSql += " ,[Qty_RECMoreThanPO_P],[Percent_RECMoreThanPO_P],[Barcode_SMUNITMS_ID],[Allocate_SMUNITMS_ID],[F_QC_PO],[F_Issue_Full_Lot],[LastPurchaseDate],[f_CanOpenQtySOOverIV],[SMLOGOMS_ID],[f_lot_fifo_pd]    "
            sSql += " ,[Create_SMUSERMS_ID],[Create_Date_Time],[F_RECMoreThanIP],[Qty_RECMoreThanIP],[Percent_RECMoreThanIP],[F_ExpireDate_Manual],[F_CalBackward_AleartDate],[F_GenDocOnlyMRPbyItem],[SMACCOTG_ID],[app_type]"
            sSql += " ,[acc_match_code],[SMLTTPHD_ID],[SA_SMVATTMS_ID],[PA_SMVATTMS_ID],[JC_SMUNITMS_ID]"
            sSql += " )"
            sSql += " SELECT '" & txtItem.Text & "',[Code_Secondary],1033000054,'" & lot & "','P','" & txtItemDescription.Text & "',[Name2_Th],[Name3_Th],[Name1_En],[Name2_En]"
            sSql += " ,[Name3_En],[Descr_Th],[Descr_En],[LeadTime_Pd],[Lot_Size_Pd] ,[LeadTime_Dl_Buffer],[RateWaste_Pd],[Weight],[F_Expire],[Alert_Month]"
            sSql += " ,[Alert_Day],[Expire_Month],[Expire_Day] ,[F_Bin] ,[F_Inspect],[F_Budget],[Inspect_SMSTORMS_ID],[Df_Ltsr],[Ltsr_Charecter],[Df_LtsrFormat]"
            sSql += " ,[Ltsr_running],[F_Unit_MS],1033000004,[Second_SMUNITMS_ID],1033000062,1033000004,1033000059,'N','N',[Par1_SMUNITMS_ID]"
            sSql += " ,[Par2_SMUNITMS_ID],1033000006,1033000001,[SMGRP3MS_ID],[SMGRP4MS_ID],1076200001,[Ref1],[Ref2],[Ref3],[Ref4]"
            sSql += " ,[Ref5],[Remark1],[Remark2],[Remark3],[Remark4] ,[Remark5],[F_Active],[F_KIT],[FileName1],[FileName2]"
            sSql += " ,[FileName3],[FileName4],[FileName5],[FileName6],[F_Pararell_Avg],[F_Tool],[F_Control_Tool],[Tool_Expire_Day],[Tool_Expire_Month]     "
            sSql += " ,[Tool_Alert_Day],[Tool_Alert_Month],[F_bom_cost],[min_qty] ,[max_qty],'Y','F',[Picture1],[Picture2],[Picture3]"
            sSql += "  ,[Picture4],[Picture5],[Picture6],'N',[Serial_Charecter],[Df_SerialFormat],[Serial_running],[F_IssuePriority],[Mfg_SMUNITMS_ID],[Seq_Group]"
            sSql += " ,[Activity_Date],[Start_Sale_Date],[Last_Sale_Date],[F_Unit_Control],[Width],[Lenght],[Hight],[MRSTPTHD_id],[F_Auto_Convert_P1_Qty],'N'    "
            sSql += " ,[F_UseRateWaste],[Temp_SMSTORMS_ID],[SMACCOMS_ID],[LeadTime_Pd_Self],[F_RECMoreThanPO],[Qty_RECMoreThanPO],[Percent_RECMoreThanPO],'MIS',getdate(),[Daily_Capacity]   "
            sSql += " ,[Ref6],[F_UseItemWeightAddUnitWeight],[F_RECMoreThanSTD_P],[Qty_RECMoreThanSTD_P],[Percent_RECMoreThanSTD_P],[F_RECMoreThanPR],[Qty_RECMoreThanPR],[Percent_RECMoreThanPR],[f_Integer_Qty],[F_RECMoreThanPO_P]"
            sSql += " ,[Qty_RECMoreThanPO_P],[Percent_RECMoreThanPO_P],[Barcode_SMUNITMS_ID],[Allocate_SMUNITMS_ID],[F_QC_PO],[F_Issue_Full_Lot],[LastPurchaseDate],[f_CanOpenQtySOOverIV],[SMLOGOMS_ID],[f_lot_fifo_pd]    "
            sSql += " ,[Create_SMUSERMS_ID],getdate(),[F_RECMoreThanIP],[Qty_RECMoreThanIP],[Percent_RECMoreThanIP],[F_ExpireDate_Manual],[F_CalBackward_AleartDate],[F_GenDocOnlyMRPbyItem],[SMACCOTG_ID],[app_type]"
            sSql += " ,[acc_match_code],[SMLTTPHD_ID],[SA_SMVATTMS_ID],[PA_SMVATTMS_ID],[JC_SMUNITMS_ID]"
            sSql += " FROM [dbo].[SMITEMMS] "
            sSql += "  where  code like 'TEST-01'"


            If sqlcon.State = ConnectionState.Closed Then
                sqlcon = New OleDbConnection(db_Enpro.cnPAStr)
                sqlcon.Open()
            End If
            With sqlcom
                .Connection = sqlcon
                .CommandType = CommandType.Text
                .CommandText = sSql
                .ExecuteNonQuery()
            End With

        End If


    End Sub

    Sub InsertBI_kvn()

        Dim Cn As New SqlConnection(db_Mac5.sqlCon)
        Dim sqlcon As New OleDbConnection(db_Mac5.cnPAStr)
        Dim sqlcom As New OleDbCommand

        sql = " SELECT Item "
        sql += " from    TB_ProductMasterGroup "
        sql += " where Item = '" & txtItem.Text & "' and Company= 'KVN'"
        dt = db_Mac5.GetDataTable(sql)
        If dt.Rows.Count <= 0 Then   'check ซ้ำ

            Dim sSql As String = "INSERT INTO [TB_ProductMasterGroup] "
            sSql += " (Company,ProductType,ProductCategory,ProductGroup,item)"
            sSql += " Values(  "
            sSql += "'KVN'"
            sSql += ",'" & drpProductType.SelectedValue & "'"
            sSql += ",'" & drpProductCat.SelectedValue & "'"
            sSql += ",'" & drpProductGroup.SelectedValue & "'"
            sSql += ",'" & txtItem.Text & "'  )"
            If sqlcon.State = ConnectionState.Closed Then
                sqlcon = New OleDbConnection(db_Mac5.cnPAStr)
                sqlcon.Open()
            End If
            With sqlcom
                .Connection = sqlcon
                .CommandType = CommandType.Text
                .CommandText = sSql
                .ExecuteNonQuery()
            End With


        End If


    End Sub

    Sub InsertBI_lion()

        Dim Cn As New SqlConnection(db_Mac5.sqlCon)
        Dim sqlcon As New OleDbConnection(db_Mac5.cnPAStr)
        Dim sqlcom As New OleDbCommand

        sql = " SELECT Item "
        sql += " from    TB_ProductMasterGroup "
        sql += " where Item = '" & txtItem.Text & "' and Company= 'Lion'"
        dt = db_Mac5.GetDataTable(sql)
        If dt.Rows.Count <= 0 Then   'check ซ้ำ

            Dim sSql As String = "INSERT INTO [TB_ProductMasterGroup] "
            sSql += " (Company,ProductType,ProductCategory,ProductGroup,item)"
            sSql += " Values(  "
            sSql += "'Lion'"
            sSql += ",'" & drpProductType.SelectedValue & "'"
            sSql += ",'" & drpProductCat.SelectedValue & "'"
            sSql += ",'" & drpProductGroup.SelectedValue & "'"
            sSql += ",'" & txtItem.Text & "'  )"
            If sqlcon.State = ConnectionState.Closed Then
                sqlcon = New OleDbConnection(db_Mac5.cnPAStr)
                sqlcon.Open()
            End If
            With sqlcom
                .Connection = sqlcon
                .CommandType = CommandType.Text
                .CommandText = sSql
                .ExecuteNonQuery()
            End With

        End If

    End Sub


    Sub SentMail_SCM()
        '''''http://projectsvbnet.blogspot.com/2013/09/email-vbnet.html
        'LeaveOnline@aromathailandapp.com
        'leaveonline2016
        'mail.yourdomain.com
        'smtp port 25

        Dim i As Integer = 1
        Dim Role As String = ""
        Dim sql As String
        Dim name As String
        Dim strBody As String

        sql = "SELECT fullname  "
        sql = sql & "  FROM [dbo].[TB_Employee] "
        sql = sql & " where Employee_id = '" & drpRequester.SelectedValue & "'"
        dt = db_HR.GetDataTable(sql)
        If dt.Rows.Count > 0 Then
            name = dt.Rows(0)("Fullname")
        End If


        'If Len(txtMyemail.Text) = 0 Or Len(txtMypassword.Text) = 0 Then
        '    MsgBox("ค่าอีเมล์โฮสต์หรือรหัสผ่านโฮสต์ยังไม่ได้ป้อนข้อมูล", MsgBoxStyle.Critical + MsgBoxStyle.ApplicationModal, "ล้มเหลว")
        '    txtMyemail.Focus()
        'Else
        Try
            'If Len(txtTo.Text) = 0 Then
            '    MsgBox("คุณต้องป้อนอีเมล์ที่จะส่งก่อนทำการส่ง", MsgBoxStyle.Critical + MsgBoxStyle.ApplicationModal, "ล้มเหลว")
            'Else
            'โฮสต์ และ เลขพอร์ต ที่คุณจะส่งอีเมล
            'และคุณต้องมีบัญชีของ Gmail ถ้าจะใช้ smtp.gmail.com
            Dim smtp As New SmtpClient("mail.aromathailandapp.com", 25)
            'สร้างตัวแปร eMailmessage เก็บการส่งข้อความอีเมล
            Dim eMailmessage As New System.Net.Mail.MailMessage() 'New MailMessage
            'กำหนดความปลอดภัยในการเข้ารหัสการส่งข้อความให้เป็น True
            smtp.EnableSsl = False
            'ติดตั้งโฮสต์ของ Gmail
            'smtp.Credentials = New System.Net.NetworkCredential("aroma.leave@gmail.com", "aroma1991")
            smtp.Credentials = New System.Net.NetworkCredential("Product@aromathailandapp.com", "product2018")
            'กำค่าส่วนประกอบของอีเมล์ เช่น ข้อความ, หัวข้อ, จากผู้ส่ง และ ผู้รับ
            ' eMailmessage.Subject = "แจ้งยอดการชำระเงิน"

            eMailmessage.Subject = "แจ้งตั้งรหัสสินค้าใหม่กลุ่มวัตถุดิบ Doc No. " & lblRequestID.Text

            strBody = "<html>" & vbCrLf
            strBody = strBody & "<body>" & vbCrLf
            strBody = strBody & "<FONT face=Tahoma>" & vbCrLf

            strBody = strBody & "<table>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td> เรียน  หน่วยงาน Supply Chain" & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>" & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td height=20  valign=middle style=font-family:Georgia, 'Times New Roman', Times, serif; font-size:30px; color:#0033cc;>" & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>" & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf


            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>แจ้งตั้งรหัสสินค้าใหม่ กลุ่มวัตถุดิบ" & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>" & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "</table>" & vbCrLf

            strBody = strBody & "<table>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>Doc No. : " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>" & lblRequestID.Text & " " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>วัตถุประสงค์ที่ขอตั้งรหัสสินค้าใหม่ : " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>" & rdoObjective.SelectedValue & " " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>ประเภทสินค้า : " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>" & rdoItem_Type.SelectedValue & " " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>กลุ่มสินค้าที่ซื้อ (Product Category) : " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>" & rdoItem_ProductCat.SelectedValue & " " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>รหัสสินค้า : " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>รอบัญชีตั้งรหัส" & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>ชื่อสินค้าในระบบ : " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>" & txtItemDescription.Text & " " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>Product Type : " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>" & drpProductType.SelectedValue & " " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>Product Category : " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>" & drpProductCat.SelectedValue & " " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>Product Group : " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>" & drpProductGroup.SelectedValue & " " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>สินค้านำเข้าคลัง LF " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>" & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>" & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>" & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "</table>" & vbCrLf

            strBody = strBody & "<table>" & vbCrLf
            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td> รายละเอียดสินค้าตาม Link ด้านล่าง  " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>https://armapplication.com/Product/login.aspx?id=" & lblRequestID.Text & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td height=20  valign=middle style=font-family:Georgia, 'Times New Roman', Times, serif; font-size:30px; color:#0033cc;>" & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td> จึงเรียนมาเพื่อทราบ" & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>" & name & " " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf
            strBody = strBody & "</table>" & vbCrLf

            strBody = strBody & "</FONT >" & vbCrLf
            strBody = strBody & "</body>" & vbCrLf
            strBody = strBody & "<html>"

            eMailmessage.Body = strBody
            eMailmessage.IsBodyHtml = True

            eMailmessage.From = New MailAddress("Product@aromathailandapp.com", "Product Master")


            eMailmessage.To.Add("phachpornpha@aromathailand.com,nattapolkhu@aromathailand.com,kanitthag@aromathailand.com,kanokphanaroj@aromathailand.com")
            ' eMailmessage.To.Add("Haruthaic@aromathailand.com")
            eMailmessage.CC.Add("nattapolyal@aromathailand.com,noknoilue@aromathailand.com,sudaratche@aromathailand.com") '
            'eMailmessage.To.Add(email)


            smtp.Send(eMailmessage)
            'MsgBox("ส่งอีเมล์สำเร็จ", vbInformation, "รายงานผล")
            '   End If
        Catch ex As Exception
            ' MsgBox("ข้อมูลผิดพลาด กรุณาตรวจสอบข้อมูลอีกครั้ง", MsgBoxStyle.Critical + MsgBoxStyle.ApplicationModal, "การทำงานผิดพลาด")
            sms.Msg = "ข้อมูลผิดพลาด กรุณาตรวจสอบข้อมูลอีกครั้ง"
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "window.alert('" & sms.Msg & "');", True)
        End Try
        ' End If
    End Sub

    Sub SentMail_Acct()
        '''''http://projectsvbnet.blogspot.com/2013/09/email-vbnet.html
        'LeaveOnline@aromathailandapp.com
        'leaveonline2016
        'mail.yourdomain.com
        'smtp port 25

        Dim i As Integer = 1
        Dim Role As String = ""
        Dim sql As String
        Dim name As String
        Dim strBody As String

        sql = "SELECT fullname  "
        sql = sql & "  FROM [dbo].[TB_Employee] "
        sql = sql & " where Employee_id = '" & drpRequester.SelectedValue & "'"
        dt = db_HR.GetDataTable(sql)
        If dt.Rows.Count > 0 Then
            name = dt.Rows(0)("Fullname")
        End If


        'If Len(txtMyemail.Text) = 0 Or Len(txtMypassword.Text) = 0 Then
        '    MsgBox("ค่าอีเมล์โฮสต์หรือรหัสผ่านโฮสต์ยังไม่ได้ป้อนข้อมูล", MsgBoxStyle.Critical + MsgBoxStyle.ApplicationModal, "ล้มเหลว")
        '    txtMyemail.Focus()
        'Else
        Try
            'If Len(txtTo.Text) = 0 Then
            '    MsgBox("คุณต้องป้อนอีเมล์ที่จะส่งก่อนทำการส่ง", MsgBoxStyle.Critical + MsgBoxStyle.ApplicationModal, "ล้มเหลว")
            'Else
            'โฮสต์ และ เลขพอร์ต ที่คุณจะส่งอีเมล
            'และคุณต้องมีบัญชีของ Gmail ถ้าจะใช้ smtp.gmail.com
            Dim smtp As New SmtpClient("mail.aromathailandapp.com", 25)
            'สร้างตัวแปร eMailmessage เก็บการส่งข้อความอีเมล
            Dim eMailmessage As New System.Net.Mail.MailMessage() 'New MailMessage
            'กำหนดความปลอดภัยในการเข้ารหัสการส่งข้อความให้เป็น True
            smtp.EnableSsl = False
            'ติดตั้งโฮสต์ของ Gmail
            'smtp.Credentials = New System.Net.NetworkCredential("aroma.leave@gmail.com", "aroma1991")
            smtp.Credentials = New System.Net.NetworkCredential("Product@aromathailandapp.com", "product2018")
            'กำค่าส่วนประกอบของอีเมล์ เช่น ข้อความ, หัวข้อ, จากผู้ส่ง และ ผู้รับ
            ' eMailmessage.Subject = "แจ้งยอดการชำระเงิน"

            eMailmessage.Subject = "แจ้งตั้งรหัสสินค้าใหม่กลุ่มวัตถุดิบ Doc No. " & lblRequestID.Text

            strBody = "<html>" & vbCrLf
            strBody = strBody & "<body>" & vbCrLf
            strBody = strBody & "<FONT face=Tahoma>" & vbCrLf

            strBody = strBody & "<table>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td> เรียน  หน่วยงานบัญชี" & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>" & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td height=20  valign=middle style=font-family:Georgia, 'Times New Roman', Times, serif; font-size:30px; color:#0033cc;>" & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>" & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf


            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>แจ้งตั้งรหัสสินค้าใหม่ กลุ่มวัตถุดิบ" & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>" & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "</table>" & vbCrLf

            strBody = strBody & "<table>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>Doc No. : " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>" & lblRequestID.Text & " " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>วัตถุประสงค์ที่ขอตั้งรหัสสินค้าใหม่ : " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>" & rdoObjective.SelectedValue & " " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>ประเภทสินค้า : " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>" & rdoItem_Type.SelectedValue & " " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>กลุ่มสินค้าที่ซื้อ (Product Category) : " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>" & rdoItem_ProductCat.SelectedValue & " " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>รหัสสินค้า : " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>รอบัญชีตั้งรหัส " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>ชื่อสินค้าในระบบ : " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>" & txtItemDescription.Text & " " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>Product Type : " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>" & drpProductType.SelectedValue & " " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>Product Category : " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>" & drpProductCat.SelectedValue & " " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>Product Group : " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>" & drpProductGroup.SelectedValue & " " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>" & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>" & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "</table>" & vbCrLf

            strBody = strBody & "<table>" & vbCrLf
            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td> รายละเอียดสินค้าตาม Link ด้านล่าง  " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>https://armapplication.com/Product/login.aspx?id=" & lblRequestID.Text & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td height=20  valign=middle style=font-family:Georgia, 'Times New Roman', Times, serif; font-size:30px; color:#0033cc;>" & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td> จึงเรียนมาเพื่อทราบ" & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>" & name & " " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf
            strBody = strBody & "</table>" & vbCrLf

            strBody = strBody & "</FONT >" & vbCrLf
            strBody = strBody & "</body>" & vbCrLf
            strBody = strBody & "<html>"

            eMailmessage.Body = strBody
            eMailmessage.IsBodyHtml = True

            eMailmessage.From = New MailAddress("Product@aromathailandapp.com", "Product Master")


            eMailmessage.To.Add("rathawit@aromathailand.com,panittas@aromathailand.com,onumau@aromathailand.com,kanokphanaroj@aromathailand.com")
            'eMailmessage.To.Add("Haruthaic@aromathailand.com")
            eMailmessage.CC.Add("nattapolyal@aromathailand.com,noknoilue@aromathailand.com,Haruthaic@aromathailand.com,sudaratche@aromathailand.com") '
            'eMailmessage.To.Add(email)


            smtp.Send(eMailmessage)
            'MsgBox("ส่งอีเมล์สำเร็จ", vbInformation, "รายงานผล")
            '   End If
        Catch ex As Exception
            ' MsgBox("ข้อมูลผิดพลาด กรุณาตรวจสอบข้อมูลอีกครั้ง", MsgBoxStyle.Critical + MsgBoxStyle.ApplicationModal, "การทำงานผิดพลาด")
            sms.Msg = "ข้อมูลผิดพลาด กรุณาตรวจสอบข้อมูลอีกครั้ง"
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "window.alert('" & sms.Msg & "');", True)
        End Try
        ' End If

    End Sub

    Sub SentMail_Manager()
        '''''http://projectsvbnet.blogspot.com/2013/09/email-vbnet.html
        'LeaveOnline@aromathailandapp.com
        'leaveonline2016
        'mail.yourdomain.com
        'smtp port 25

        Dim i As Integer = 1
        Dim Role As String = ""
        Dim sql As String
        Dim name, email As String
        Dim strBody As String

        sql = "SELECT fullname,Email  "
        sql = sql & "  FROM [dbo].[TB_Employee] "
        sql = sql & " where Employee_id = '" & drpRequester.SelectedValue & "'"
        dt = db_HR.GetDataTable(sql)
        If dt.Rows.Count > 0 Then
            name = dt.Rows(0)("Fullname")
            email = dt.Rows(0)("Email")
        End If


        'If Len(txtMyemail.Text) = 0 Or Len(txtMypassword.Text) = 0 Then
        '    MsgBox("ค่าอีเมล์โฮสต์หรือรหัสผ่านโฮสต์ยังไม่ได้ป้อนข้อมูล", MsgBoxStyle.Critical + MsgBoxStyle.ApplicationModal, "ล้มเหลว")
        '    txtMyemail.Focus()
        'Else
        Try
            'If Len(txtTo.Text) = 0 Then
            '    MsgBox("คุณต้องป้อนอีเมล์ที่จะส่งก่อนทำการส่ง", MsgBoxStyle.Critical + MsgBoxStyle.ApplicationModal, "ล้มเหลว")
            'Else
            'โฮสต์ และ เลขพอร์ต ที่คุณจะส่งอีเมล
            'และคุณต้องมีบัญชีของ Gmail ถ้าจะใช้ smtp.gmail.com
            Dim smtp As New SmtpClient("mail.aromathailandapp.com", 25)
            'สร้างตัวแปร eMailmessage เก็บการส่งข้อความอีเมล
            Dim eMailmessage As New System.Net.Mail.MailMessage() 'New MailMessage
            'กำหนดความปลอดภัยในการเข้ารหัสการส่งข้อความให้เป็น True
            smtp.EnableSsl = False
            'ติดตั้งโฮสต์ของ Gmail
            'smtp.Credentials = New System.Net.NetworkCredential("aroma.leave@gmail.com", "aroma1991")
            smtp.Credentials = New System.Net.NetworkCredential("Product@aromathailandapp.com", "product2018")
            'กำค่าส่วนประกอบของอีเมล์ เช่น ข้อความ, หัวข้อ, จากผู้ส่ง และ ผู้รับ
            ' eMailmessage.Subject = "แจ้งยอดการชำระเงิน"

            eMailmessage.Subject = "แจ้งตั้งรหัสสินค้าใหม่กลุ่มวัตถุดิบ Doc No. " & lblRequestID.Text

            strBody = "<html>" & vbCrLf
            strBody = strBody & "<body>" & vbCrLf
            strBody = strBody & "<FONT face=Tahoma>" & vbCrLf

            strBody = strBody & "<table>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td> เรียน ผู้มีอำนาจอนุมัติ" & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>" & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td height=20  valign=middle style=font-family:Georgia, 'Times New Roman', Times, serif; font-size:30px; color:#0033cc;>" & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>" & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf


            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>แจ้งตั้งรหัสสินค้าใหม่ กลุ่มวัตถุดิบ" & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>" & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "</table>" & vbCrLf

            strBody = strBody & "<table>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>Doc No. : " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>" & lblRequestID.Text & " " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>วัตถุประสงค์ที่ขอตั้งรหัสสินค้าใหม่ : " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>" & rdoObjective.SelectedValue & " " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>ประเภทสินค้า : " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>" & rdoItem_Type.SelectedValue & " " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>กลุ่มสินค้าที่ซื้อ (Product Category) : " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>" & rdoItem_ProductCat.SelectedValue & " " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>รหัสสินค้า : " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>รอบัญชีตั้งรหัส " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>ชื่อสินค้าในระบบ : " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>" & txtItemDescription.Text & " " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>Product Type : " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>" & drpProductType.SelectedValue & " " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>Product Category : " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>" & drpProductCat.SelectedValue & " " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>Product Group : " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>" & drpProductGroup.SelectedValue & " " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>" & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>" & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "</table>" & vbCrLf

            strBody = strBody & "<table>" & vbCrLf
            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td> รายละเอียดสินค้าตาม Link ด้านล่าง  " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>https://armapplication.com/Product/login.aspx?id=" & lblRequestID.Text & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td height=20  valign=middle style=font-family:Georgia, 'Times New Roman', Times, serif; font-size:30px; color:#0033cc;>" & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td> จึงเรียนมาเพื่อทราบ" & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>" & name & " " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf
            strBody = strBody & "</table>" & vbCrLf

            strBody = strBody & "</FONT >" & vbCrLf
            strBody = strBody & "</body>" & vbCrLf
            strBody = strBody & "<html>"

            eMailmessage.Body = strBody
            eMailmessage.IsBodyHtml = True

            eMailmessage.From = New MailAddress("Product@aromathailandapp.com", "Product Master")


            'check วัตถุดิบ/บรรจุภัณฑ์ (โรงงาน)
            If rdoObjective1.Items.FindByValue("วัตถุดิบ/บรรจุภัณฑ์ (โรงงาน)").Selected = True Then
                eMailmessage.To.Add("virojs@aromathailand.com,admin_factory@aromathailand.com")
                'eMailmessage.To.Add("Haruthaic@aromathailand.com")
                eMailmessage.CC.Add(email)
            Else
                eMailmessage.To.Add("kanokphanaroj@aromathailand.com,Haruthaic@aromathailand.com,sudaratche@aromathailand.com")
                'kittimapai@aromathailand.com ลบเมล์ ผจก.ของพี่น้ำขิงออก request จาก พี่น้ำขิง จัดการโดยน้ำ 25/05/2021
                'kanokphanaroj@aromathailand.com เพิ่มเมล์ ผจก.ของพี่น้ำขิงออก request จาก พี่น้ำขิง จัดการโดยน้ำ 25/06/2021
                'eMailmessage.To.Add("Haruthaic@aromathailand.com")
                eMailmessage.CC.Add(email)
            End If



            smtp.Send(eMailmessage)
            'MsgBox("ส่งอีเมล์สำเร็จ", vbInformation, "รายงานผล")
            '   End If
        Catch ex As Exception
            ' MsgBox("ข้อมูลผิดพลาด กรุณาตรวจสอบข้อมูลอีกครั้ง", MsgBoxStyle.Critical + MsgBoxStyle.ApplicationModal, "การทำงานผิดพลาด")
            sms.Msg = "ข้อมูลผิดพลาด กรุณาตรวจสอบข้อมูลอีกครั้ง"
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "window.alert('" & sms.Msg & "');", True)
        End Try
        ' End If

    End Sub

    Sub SentMail_All()
        '''''http://projectsvbnet.blogspot.com/2013/09/email-vbnet.html
        'LeaveOnline@aromathailandapp.com
        'leaveonline2016
        'mail.yourdomain.com
        'smtp port 25

        Dim i As Integer = 1
        Dim Role As String = ""
        Dim sql As String
        Dim name As String
        Dim strBody As String

        sql = "SELECT fullname  "
        sql = sql & "  FROM [dbo].[TB_Employee] "
        sql = sql & " where Employee_id = '" & drpRequester.SelectedValue & "'"
        dt = db_HR.GetDataTable(sql)
        If dt.Rows.Count > 0 Then
            name = dt.Rows(0)("Fullname")
        End If


        'If Len(txtMyemail.Text) = 0 Or Len(txtMypassword.Text) = 0 Then
        '    MsgBox("ค่าอีเมล์โฮสต์หรือรหัสผ่านโฮสต์ยังไม่ได้ป้อนข้อมูล", MsgBoxStyle.Critical + MsgBoxStyle.ApplicationModal, "ล้มเหลว")
        '    txtMyemail.Focus()
        'Else
        Try
            'If Len(txtTo.Text) = 0 Then
            '    MsgBox("คุณต้องป้อนอีเมล์ที่จะส่งก่อนทำการส่ง", MsgBoxStyle.Critical + MsgBoxStyle.ApplicationModal, "ล้มเหลว")
            'Else
            'โฮสต์ และ เลขพอร์ต ที่คุณจะส่งอีเมล
            'และคุณต้องมีบัญชีของ Gmail ถ้าจะใช้ smtp.gmail.com
            Dim smtp As New SmtpClient("mail.aromathailandapp.com", 25)
            'สร้างตัวแปร eMailmessage เก็บการส่งข้อความอีเมล
            Dim eMailmessage As New System.Net.Mail.MailMessage() 'New MailMessage
            'กำหนดความปลอดภัยในการเข้ารหัสการส่งข้อความให้เป็น True
            smtp.EnableSsl = False
            'ติดตั้งโฮสต์ของ Gmail
            'smtp.Credentials = New System.Net.NetworkCredential("aroma.leave@gmail.com", "aroma1991")
            smtp.Credentials = New System.Net.NetworkCredential("Product@aromathailandapp.com", "product2018")
            'กำค่าส่วนประกอบของอีเมล์ เช่น ข้อความ, หัวข้อ, จากผู้ส่ง และ ผู้รับ
            ' eMailmessage.Subject = "แจ้งยอดการชำระเงิน"

            eMailmessage.Subject = "แจ้งตั้งรหัสสินค้าใหม่กลุ่มวัตถุดิบ Doc No. " & lblRequestID.Text

            strBody = "<html>" & vbCrLf
            strBody = strBody & "<body>" & vbCrLf
            strBody = strBody & "<FONT face=Tahoma>" & vbCrLf

            strBody = strBody & "<table>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td> เรียน  ทุกท่าน" & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>" & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td height=20  valign=middle style=font-family:Georgia, 'Times New Roman', Times, serif; font-size:30px; color:#0033cc;>" & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>" & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf


            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>แจ้งตั้งรหัสสินค้าใหม่ กลุ่มวัตถุดิบ" & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>" & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "</table>" & vbCrLf

            strBody = strBody & "<table>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>Doc No. : " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>" & lblRequestID.Text & " " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>วัตถุประสงค์ที่ขอตั้งรหัสสินค้าใหม่ : " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>" & rdoObjective.SelectedValue & " " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>ประเภทสินค้า : " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>" & rdoItem_Type.SelectedValue & " " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>กลุ่มสินค้าที่ซื้อ (Product Category) : " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>" & rdoItem_ProductCat.SelectedValue & " " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>รหัสสินค้า : " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>" & txtItem.Text & " " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>ชื่อสินค้าในระบบ : " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>" & txtItemDescription.Text & " " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>Product Type : " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>" & drpProductType.SelectedValue & " " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>Product Category : " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>" & drpProductCat.SelectedValue & " " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>Product Group : " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>" & drpProductGroup.SelectedValue & " " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>สถานะ : " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>ดำเนินการตั้งรหัสสินค้าเรียบร้อย" & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "</table>" & vbCrLf

            strBody = strBody & "<table>" & vbCrLf
            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td> รายละเอียดสินค้าตาม Link ด้านล่าง  " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>https://armapplication.com/Product/login.aspx?id=" & lblRequestID.Text & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td height=20  valign=middle style=font-family:Georgia, 'Times New Roman', Times, serif; font-size:30px; color:#0033cc;>" & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td> จึงเรียนมาเพื่อทราบ" & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>ระบบ Request New Product" & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf
            strBody = strBody & "</table>" & vbCrLf

            strBody = strBody & "</FONT >" & vbCrLf
            strBody = strBody & "</body>" & vbCrLf
            strBody = strBody & "<html>"

            eMailmessage.Body = strBody
            eMailmessage.IsBodyHtml = True

            eMailmessage.From = New MailAddress("Product@aromathailandapp.com", "Product Master")


            eMailmessage.To.Add("nattapolyal@aromathailand.com,noknoilue@aromathailand.com,phachpornpha@aromathailand.com,nattapolkhu@aromathailand.com,kanitthag@aromathailand.com,kanokphanaroj@aromathailand.com,Haruthaic@aromathailand.com")
            ' eMailmessage.To.Add("Haruthaic@aromathailand.com")
            eMailmessage.CC.Add("rathawit@aromathailand.com,panittas@aromathailand.com,onumau@aromathailand.com,sudaratche@aromathailand.com") '
            'eMailmessage.To.Add(email)


            smtp.Send(eMailmessage)
            'MsgBox("ส่งอีเมล์สำเร็จ", vbInformation, "รายงานผล")
            '   End If
        Catch ex As Exception
            ' MsgBox("ข้อมูลผิดพลาด กรุณาตรวจสอบข้อมูลอีกครั้ง", MsgBoxStyle.Critical + MsgBoxStyle.ApplicationModal, "การทำงานผิดพลาด")
            sms.Msg = "ข้อมูลผิดพลาด กรุณาตรวจสอบข้อมูลอีกครั้ง"
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "window.alert('" & sms.Msg & "');", True)
        End Try
        ' End If

    End Sub


    Sub SentMail_Cancel()
        '''''http://projectsvbnet.blogspot.com/2013/09/email-vbnet.html
        'LeaveOnline@aromathailandapp.com
        'leaveonline2016
        'mail.yourdomain.com
        'smtp port 25

        Dim i As Integer = 1
        Dim Role As String = ""
        Dim sql As String
        Dim name As String
        Dim strBody As String

        sql = "SELECT fullname  "
        sql = sql & "  FROM [dbo].[TB_Employee] "
        sql = sql & " where Employee_id = '" & drpRequester.SelectedValue & "'"
        dt = db_HR.GetDataTable(sql)
        If dt.Rows.Count > 0 Then
            name = dt.Rows(0)("Fullname")
        End If


        'If Len(txtMyemail.Text) = 0 Or Len(txtMypassword.Text) = 0 Then
        '    MsgBox("ค่าอีเมล์โฮสต์หรือรหัสผ่านโฮสต์ยังไม่ได้ป้อนข้อมูล", MsgBoxStyle.Critical + MsgBoxStyle.ApplicationModal, "ล้มเหลว")
        '    txtMyemail.Focus()
        'Else
        Try
            'If Len(txtTo.Text) = 0 Then
            '    MsgBox("คุณต้องป้อนอีเมล์ที่จะส่งก่อนทำการส่ง", MsgBoxStyle.Critical + MsgBoxStyle.ApplicationModal, "ล้มเหลว")
            'Else
            'โฮสต์ และ เลขพอร์ต ที่คุณจะส่งอีเมล
            'และคุณต้องมีบัญชีของ Gmail ถ้าจะใช้ smtp.gmail.com
            Dim smtp As New SmtpClient("mail.aromathailandapp.com", 25)
            'สร้างตัวแปร eMailmessage เก็บการส่งข้อความอีเมล
            Dim eMailmessage As New System.Net.Mail.MailMessage() 'New MailMessage
            'กำหนดความปลอดภัยในการเข้ารหัสการส่งข้อความให้เป็น True
            smtp.EnableSsl = False
            'ติดตั้งโฮสต์ของ Gmail
            'smtp.Credentials = New System.Net.NetworkCredential("aroma.leave@gmail.com", "aroma1991")
            smtp.Credentials = New System.Net.NetworkCredential("Product@aromathailandapp.com", "product2018")
            'กำค่าส่วนประกอบของอีเมล์ เช่น ข้อความ, หัวข้อ, จากผู้ส่ง และ ผู้รับ
            ' eMailmessage.Subject = "แจ้งยอดการชำระเงิน"

            eMailmessage.Subject = "ยกเลิก ตั้งรหัสสินค้าใหม่กลุ่มวัตถุดิบ Doc No. " & lblRequestID.Text

            strBody = "<html>" & vbCrLf
            strBody = strBody & "<body>" & vbCrLf
            strBody = strBody & "<FONT face=Tahoma>" & vbCrLf

            strBody = strBody & "<table>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td> เรียน  หน่วยงานบัญชี" & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>" & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td height=20  valign=middle style=font-family:Georgia, 'Times New Roman', Times, serif; font-size:30px; color:#0033cc;>" & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>" & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf


            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>แจ้งตั้งรหัสสินค้าใหม่ กลุ่มวัตถุดิบ" & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>" & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "</table>" & vbCrLf

            strBody = strBody & "<table>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>Doc No. : " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>" & lblRequestID.Text & " " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>วัตถุประสงค์ที่ขอตั้งรหัสสินค้าใหม่ : " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>" & rdoObjective.SelectedValue & " " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>ประเภทสินค้า : " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>" & rdoItem_Type.SelectedValue & " " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>กลุ่มสินค้าที่ซื้อ (Product Category) : " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>" & rdoItem_ProductCat.SelectedValue & " " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>รหัสสินค้า : " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>รอบัญชีตั้งรหัส " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>ชื่อสินค้าในระบบ : " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>" & txtItemDescription.Text & " " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>Product Type : " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>" & drpProductType.SelectedValue & " " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>Product Category : " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>" & drpProductCat.SelectedValue & " " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>Product Group : " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>" & drpProductGroup.SelectedValue & " " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>สถานะ : " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>ยกเลิกการตั้งรหัสสินค้า Doc No." & lblRequestID.Text & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "</table>" & vbCrLf

            strBody = strBody & "<table>" & vbCrLf
            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td> รายละเอียดสินค้าตาม Link ด้านล่าง  " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>https://armapplication.com/Product/login.aspx?id=" & lblRequestID.Text & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td height=20  valign=middle style=font-family:Georgia, 'Times New Roman', Times, serif; font-size:30px; color:#0033cc;>" & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td> จึงเรียนมาเพื่อทราบ" & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>" & name & " " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf
            strBody = strBody & "</table>" & vbCrLf

            strBody = strBody & "</FONT >" & vbCrLf
            strBody = strBody & "</body>" & vbCrLf
            strBody = strBody & "<html>"

            eMailmessage.Body = strBody
            eMailmessage.IsBodyHtml = True

            eMailmessage.From = New MailAddress("Product@aromathailandapp.com", "Product Master")

            eMailmessage.To.Add("nattapolyal@aromathailand.com,noknoilue@aromathailand.com,phachpornpha@aromathailand.com,nattapolkhu@aromathailand.com,kanitthag@aromathailand.com")
            'eMailmessage.To.Add("Haruthaic@aromathailand.com")
            eMailmessage.CC.Add("rathawit@aromathailand.com,panittas@aromathailand.com,onumau@aromathailand.com,kanokphanaroj@aromathailand.com,sudaratche@aromathailand.com") '
            'eMailmessage.To.Add(email)


            smtp.Send(eMailmessage)
            'MsgBox("ส่งอีเมล์สำเร็จ", vbInformation, "รายงานผล")
            '   End If
        Catch ex As Exception
            ' MsgBox("ข้อมูลผิดพลาด กรุณาตรวจสอบข้อมูลอีกครั้ง", MsgBoxStyle.Critical + MsgBoxStyle.ApplicationModal, "การทำงานผิดพลาด")
            sms.Msg = "ข้อมูลผิดพลาด กรุณาตรวจสอบข้อมูลอีกครั้ง"
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "window.alert('" & sms.Msg & "');", True)
        End Try
        ' End If

    End Sub

    Protected Sub btnPrint_Click(sender As Object, e As EventArgs) Handles btnPrint.Click
        'http://armapplication.com:90/From/RequestNewProduct?RequestId

        'Server.Transfer("http://armapplication.com:90/From/RequestNewProduct?RequestId=" & lblRequestID.Text)

        Response.Redirect("http://armapplication.com:90/From/RequestNewProduct?RequestId=" & lblRequestID.Text)

    End Sub

    Protected Sub rdoObjective1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rdoObjective1.SelectedIndexChanged


    End Sub

    Protected Sub txtPurchasePrice_unit_Init(sender As Object, e As EventArgs) Handles txtPurchasePrice_unit.Init
        Dim sqlCon As New SqlConnection(DB_Product.sqlCon)
        Dim sqlCmd As New SqlCommand
        Dim Rs As SqlDataReader
        Dim sql As String

        sql = "select [Unit] from [TB_ProductUnit]"

        If sqlCon.State = ConnectionState.Closed Then
            sqlCon = New SqlConnection(DB_Product.sqlCon)
            sqlCon.Open()
        End If
        With sqlCmd
            .Connection = sqlCon
            .CommandType = CommandType.Text
            .CommandText = sql
            Rs = .ExecuteReader
        End With
        txtPurchasePrice_unit.Items.Clear()
        txtPurchasePrice_unit.Items.Add(New ListItem("", 0))
        While Rs.Read
            txtPurchasePrice_unit.Items.Add(New ListItem(Rs.GetString(0), Rs.GetString(0)))
        End While
    End Sub

    Protected Sub txtPurchasePrice_wholesale_unit_Init(sender As Object, e As EventArgs) Handles txtPurchasePrice_wholesale_unit.Init
        Dim sqlCon As New SqlConnection(DB_Product.sqlCon)
        Dim sqlCmd As New SqlCommand
        Dim Rs As SqlDataReader
        Dim sql As String

        sql = "select [Unit] from [TB_ProductUnit]"

        If sqlCon.State = ConnectionState.Closed Then
            sqlCon = New SqlConnection(DB_Product.sqlCon)
            sqlCon.Open()
        End If
        With sqlCmd
            .Connection = sqlCon
            .CommandType = CommandType.Text
            .CommandText = sql
            Rs = .ExecuteReader
        End With
        txtPurchasePrice_wholesale_unit.Items.Clear()
        txtPurchasePrice_wholesale_unit.Items.Add(New ListItem("", 0))
        While Rs.Read
            txtPurchasePrice_wholesale_unit.Items.Add(New ListItem(Rs.GetString(0), Rs.GetString(0)))
        End While
    End Sub

    Protected Sub txtPrice_unit_Init(sender As Object, e As EventArgs) Handles txtPrice_unit.Init
        Dim sqlCon As New SqlConnection(DB_Product.sqlCon)
        Dim sqlCmd As New SqlCommand
        Dim Rs As SqlDataReader
        Dim sql As String

        sql = "select [Unit] from [TB_ProductUnit]"

        If sqlCon.State = ConnectionState.Closed Then
            sqlCon = New SqlConnection(DB_Product.sqlCon)
            sqlCon.Open()
        End If
        With sqlCmd
            .Connection = sqlCon
            .CommandType = CommandType.Text
            .CommandText = sql
            Rs = .ExecuteReader
        End With
        txtPrice_unit.Items.Clear()
        txtPrice_unit.Items.Add(New ListItem("", 0))
        While Rs.Read
            txtPrice_unit.Items.Add(New ListItem(Rs.GetString(0), Rs.GetString(0)))
        End While
    End Sub

    Protected Sub txtPrice_wholesale_unit_Init(sender As Object, e As EventArgs) Handles txtPrice_wholesale_unit.Init
        Dim sqlCon As New SqlConnection(DB_Product.sqlCon)
        Dim sqlCmd As New SqlCommand
        Dim Rs As SqlDataReader
        Dim sql As String

        sql = "select [Unit] from [TB_ProductUnit]"

        If sqlCon.State = ConnectionState.Closed Then
            sqlCon = New SqlConnection(DB_Product.sqlCon)
            sqlCon.Open()
        End If
        With sqlCmd
            .Connection = sqlCon
            .CommandType = CommandType.Text
            .CommandText = sql
            Rs = .ExecuteReader
        End With
        txtPrice_wholesale_unit.Items.Clear()
        txtPrice_wholesale_unit.Items.Add(New ListItem("", 0))
        While Rs.Read
            txtPrice_wholesale_unit.Items.Add(New ListItem(Rs.GetString(0), Rs.GetString(0)))
        End While
    End Sub

    Protected Sub txtLF_unitsmall_Init(sender As Object, e As EventArgs) Handles txtLF_unitsmall.Init
        Dim sqlCon As New SqlConnection(DB_Product.sqlCon)
        Dim sqlCmd As New SqlCommand
        Dim Rs As SqlDataReader
        Dim sql As String

        sql = "select [Unit] from [TB_ProductUnit]"

        If sqlCon.State = ConnectionState.Closed Then
            sqlCon = New SqlConnection(DB_Product.sqlCon)
            sqlCon.Open()
        End If
        With sqlCmd
            .Connection = sqlCon
            .CommandType = CommandType.Text
            .CommandText = sql
            Rs = .ExecuteReader
        End With
        txtLF_unitsmall.Items.Clear()
        txtLF_unitsmall.Items.Add(New ListItem("", 0))
        While Rs.Read
            txtLF_unitsmall.Items.Add(New ListItem(Rs.GetString(0), Rs.GetString(0)))
        End While
    End Sub

    Protected Sub txtLF_unitbig_Init(sender As Object, e As EventArgs) Handles txtLF_unitbig.Init
        Dim sqlCon As New SqlConnection(DB_Product.sqlCon)
        Dim sqlCmd As New SqlCommand
        Dim Rs As SqlDataReader
        Dim sql As String

        sql = "select [Unit] from [TB_ProductUnit]"

        If sqlCon.State = ConnectionState.Closed Then
            sqlCon = New SqlConnection(DB_Product.sqlCon)
            sqlCon.Open()
        End If
        With sqlCmd
            .Connection = sqlCon
            .CommandType = CommandType.Text
            .CommandText = sql
            Rs = .ExecuteReader
        End With
        txtLF_unitbig.Items.Clear()
        txtLF_unitbig.Items.Add(New ListItem("", 0))
        While Rs.Read
            txtLF_unitbig.Items.Add(New ListItem(Rs.GetString(0), Rs.GetString(0)))
        End While
    End Sub

    Protected Sub txtStorageCat_Init(sender As Object, e As EventArgs) Handles txtStorageCat.Init
        Dim sqlCon As New SqlConnection(DB_Product.sqlCon)
        Dim sqlCmd As New SqlCommand
        Dim Rs As SqlDataReader
        Dim sql As String

        sql = "select [StorageCategory] from [TB_ProductStorage]  group by [StorageCategory]"

        If sqlCon.State = ConnectionState.Closed Then
            sqlCon = New SqlConnection(DB_Product.sqlCon)
            sqlCon.Open()
        End If
        With sqlCmd
            .Connection = sqlCon
            .CommandType = CommandType.Text
            .CommandText = sql
            Rs = .ExecuteReader
        End With
        txtStorageCat.Items.Clear()
        txtStorageCat.Items.Add(New ListItem("", 0))
        While Rs.Read
            txtStorageCat.Items.Add(New ListItem(Rs.GetString(0), Rs.GetString(0)))
        End While
    End Sub



    Protected Sub txtPrimaryGroup_Init(sender As Object, e As EventArgs) Handles txtPrimaryGroup.Init
        Dim sqlCon As New SqlConnection(DB_Product.sqlCon)
        Dim sqlCmd As New SqlCommand
        Dim Rs As SqlDataReader
        Dim sql As String

        sql = "select [PrimartGroup] from [TB_ProductStorage]  group by [PrimartGroup]"

        If sqlCon.State = ConnectionState.Closed Then
            sqlCon = New SqlConnection(DB_Product.sqlCon)
            sqlCon.Open()
        End If
        With sqlCmd
            .Connection = sqlCon
            .CommandType = CommandType.Text
            .CommandText = sql
            Rs = .ExecuteReader
        End With
        txtPrimaryGroup.Items.Clear()
        txtPrimaryGroup.Items.Add(New ListItem("", 0))
        While Rs.Read
            txtPrimaryGroup.Items.Add(New ListItem(Rs.GetString(0), Rs.GetString(0)))
        End While
    End Sub

    Protected Sub txtProduct_cat_Init(sender As Object, e As EventArgs) Handles txtProduct_cat.Init
        Dim sqlCon As New SqlConnection(DB_Product.sqlCon)
        Dim sqlCmd As New SqlCommand
        Dim Rs As SqlDataReader
        Dim sql As String

        sql = "select [ProductCategory] from [TB_ProductStorage]  group by [ProductCategory]"

        If sqlCon.State = ConnectionState.Closed Then
            sqlCon = New SqlConnection(DB_Product.sqlCon)
            sqlCon.Open()
        End If
        With sqlCmd
            .Connection = sqlCon
            .CommandType = CommandType.Text
            .CommandText = sql
            Rs = .ExecuteReader
        End With
        txtProduct_cat.Items.Clear()
        txtProduct_cat.Items.Add(New ListItem("", 0))
        While Rs.Read
            txtProduct_cat.Items.Add(New ListItem(Rs.GetString(0), Rs.GetString(0)))
        End While
    End Sub


    Protected Sub txtUnitsmall_Init(sender As Object, e As EventArgs) Handles txtUnitsmall.Init
        Dim sqlCon As New SqlConnection(DB_Product.sqlCon)
        Dim sqlCmd As New SqlCommand
        Dim Rs As SqlDataReader
        Dim sql As String

        sql = "select [Unit] from [TB_ProductUnit]"

        If sqlCon.State = ConnectionState.Closed Then
            sqlCon = New SqlConnection(DB_Product.sqlCon)
            sqlCon.Open()
        End If
        With sqlCmd
            .Connection = sqlCon
            .CommandType = CommandType.Text
            .CommandText = sql
            Rs = .ExecuteReader
        End With
        txtUnitsmall.Items.Clear()
        txtUnitsmall.Items.Add(New ListItem("", 0))
        While Rs.Read
            txtUnitsmall.Items.Add(New ListItem(Rs.GetString(0), Rs.GetString(0)))
        End While
    End Sub

    Protected Sub txtUnitbig_Init(sender As Object, e As EventArgs) Handles txtUnitbig.Init
        Dim sqlCon As New SqlConnection(DB_Product.sqlCon)
        Dim sqlCmd As New SqlCommand
        Dim Rs As SqlDataReader
        Dim sql As String

        sql = "select [Unit] from [TB_ProductUnit]"

        If sqlCon.State = ConnectionState.Closed Then
            sqlCon = New SqlConnection(DB_Product.sqlCon)
            sqlCon.Open()
        End If
        With sqlCmd
            .Connection = sqlCon
            .CommandType = CommandType.Text
            .CommandText = sql
            Rs = .ExecuteReader
        End With
        txtUnitbig.Items.Clear()
        txtUnitbig.Items.Add(New ListItem("", 0))
        While Rs.Read
            txtUnitbig.Items.Add(New ListItem(Rs.GetString(0), Rs.GetString(0)))
        End While
    End Sub

    Protected Sub drpBrand_Init(sender As Object, e As EventArgs) Handles drpBrand.Init
        Dim sqlCon As New SqlConnection(DB_Product.sqlCon)
        Dim sqlCmd As New SqlCommand
        Dim Rs As SqlDataReader
        Dim sql As String

        sql = "select Brand from [TB_ProductBrand] "

        If sqlCon.State = ConnectionState.Closed Then
            sqlCon = New SqlConnection(DB_Product.sqlCon)
            sqlCon.Open()
        End If
        With sqlCmd
            .Connection = sqlCon
            .CommandType = CommandType.Text
            .CommandText = sql
            Rs = .ExecuteReader
        End With
        drpBrand.Items.Clear()
        drpBrand.Items.Add(New ListItem("", 0))
        While Rs.Read
            drpBrand.Items.Add(New ListItem(Rs.GetString(0), Rs.GetString(0)))
        End While
    End Sub


    Protected Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        UpdateData()

        UpdateStatus_cancel()
        SentMail_Cancel()

        Server.Transfer("NewMasterAll.aspx?usr=" & usr)

    End Sub

    Sub UpdateStatus_cancel()

        Dim x As Integer
        Dim db_Mac5 As New Connect_Mac5
        Dim Cn As New SqlConnection(DB_Product.sqlCon)

        Dim sSql As String = "UPDATE [TB_ProductRequest_RawMat] SET  "
        sSql += " Stat='Cancel' ,ModifyBy=@ModifyBy ,ModifyDate=@ModifyDate "
        sSql += " ,SCMBy=@SCMBy , SCMDate=@SCMDate"
        sSql += " Where RequestId ='" & lblRequestID.Text & "' "


        Dim command As SqlCommand = New SqlCommand(sSql, Cn)
        Try
            command.Parameters.Add("@SCMBy", Data.SqlDbType.VarChar).Value = usr
            command.Parameters.Add("@SCMDate", Data.SqlDbType.VarChar).Value = DateTime.Now.ToString("yyyy-MM-dd")
            command.Parameters.Add("@ModifyBy", Data.SqlDbType.VarChar).Value = usr
            command.Parameters.Add("@ModifyDate", Data.SqlDbType.VarChar).Value = DateTime.Now.ToString("yyyy-MM-dd")

            command.CommandType = Data.CommandType.Text
            Cn.Open()
            x = command.ExecuteScalar
        Catch x1 As Exception
            Err.Raise(60002, , x1.Message)
        Finally
            Cn.Close()
        End Try

    End Sub


    Protected Sub rdoObjective_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rdoObjective.SelectedIndexChanged

        If rdoObjective.Items.FindByValue("สินค้าผลิต").Selected = True Then
            Panel_Purchase.Visible = False
        Else
            Panel_Purchase.Visible = True
        End If

    End Sub


    Protected Sub btnUpload_Click(sender As Object, e As EventArgs) Handles btnUpload.Click

        sql = " select * "
        sql += " from [VW_ProductRequest_RawMat] "
        sql += " where RequestId = '" & lblRequestID.Text & "' "
        dt = DB_Product.GetDataTable(sql)
        If dt.Rows.Count = 0 Then
            GenID()
            lblRequestID.Text = RequestId
            InsertData(RequestId)
        End If


        ''Dim path As String = "C:\Aroma_Program\Aroma_Product\Aroma_ProductMaster\Aroma_ProductMaster\Temp_Attachment\" & lblRequestID.Text
        'Dim path As String = "D:\!! Code Program Aroma !!\Aroma_ProductMaster\Aroma_ProductMaster\Temp_Attachment\" & lblRequestID.Text
        'If Not Directory.Exists(path) Then
        '    Directory.CreateDirectory(path)
        'End If

        'If UploadImage2.FileName <> "" Then

        '    Dim CurrentFileName As String
        '    Dim Pic As String
        '    CurrentFileName = UploadImage2.FileName
        '    Dim CurrentPath As String = Server.MapPath("~/Temp_Attachment/" & lblRequestID.Text & "/")
        '    UploadImage2.PostedFile.SaveAs(CurrentPath & UploadImage2.FileName)
        '    UploadImage2.PostedFile.SaveAs("" & path & "\" & UploadImage2.FileName)
        '    Pic = path & "\" & UploadImage2.FileName


        '    Dim sql0 As String = " INSERT INTO [TB_Attachment] ("
        '    sql0 &= "RequestId,Attach_name,Attach_part,[CreateBy], CreateDate) "
        '    sql0 &= "Values ( '" & lblRequestID.Text & "','" & UploadImage2.FileName & "' ,'/Aroma_ProductMaster/Temp_Attachment/" & lblRequestID.Text & "/','" & usr & "'"
        '    sql0 &= ",'" & Date.Now.ToString("yyyy-MM-dd HH:mm:ss") & "')"

        '    dtt = DB_Product.GetDataTable(sql0)
        'End If


        'BindData()

        ''บันทึก เอกสารแนบ
        'Dim dt2 As New Data.DataTable
        'Dim sSql2 As String = "select count(leaveID) as  countleaveID from [TB_Attachment]  where [Employee_id] = '" & lblEmpID.Text & "'  and [LeaveID] = '" & txtLeaveID.Text & "' "
        'dt2 = db_HR.GetDataTable(sSql2)

        'If dt2.Rows(0)("countleaveID") >= 2 Then
        '    sms.Msg = "ไม่อนุญาติให้แนบเอกสารเกิน 2 ไฟล์!"
        '    Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "window.alert('" & sms.Msg & "');", True)
        '    Exit Sub
        'Else
        Call att_Save(usr, lblRequestID.Text)
        att_Read()
        'End If

    End Sub


    ' Private Sub att_Save(ByVal BUnumber As String, ByVal supcode As String, ByVal resid As String)
    Private Sub att_Save(ByVal Employee_id As String, ByVal RequestID As String)
        Dim ImgPath As String = Server.MapPath(".") & "\Upload"
        Dim filename3 As String
        Dim fileN2 As FileStream
        Dim fileN3 As FileStream
        Dim extension As String
        Dim zip3 As ZipOutputStream
        Dim Zip4 As ZipEntry
        Dim IDE As String
        Dim isN As String = ""
        Dim msg As String = ""

        If Me.FileUpload1.HasFile Then

            filename3 = Path.GetFileName(FileUpload1.PostedFile.FileName)
            FileUpload1.PostedFile.SaveAs(ImgPath & "\" & Trim(filename3))
            Dim ss() As String = filename3.Split(".")
            Dim sFiletype As String = (".") & ss(ss.GetUpperBound(0))

            If filename3.IndexOf("Upload/") = -1 Then
                If ImgPath & "\" & lblRequestID.Text & sFiletype = ImgPath & "\" & lblRequestID.Text & sFiletype Then
                    Dim FileIn As New FileInfo(Server.MapPath("Upload/" & lblRequestID.Text & sFiletype))
                    If FileIn.Exists Then
                        FileIn.Delete()
                    End If
                End If

                Rename(ImgPath & "\" & Trim(filename3), ImgPath & "\" & lblRequestID.Text & sFiletype)

                fileN3 = New FileStream(ImgPath & "\" & lblRequestID.Text & sFiletype, FileMode.Open, FileAccess.Read)
                fileN2 = New FileStream(ImgPath & "\" & lblRequestID.Text & "-" & Left(FileUpload1.FileName, Len(FileUpload1.FileName) - 4) & ".zip", FileMode.Create, FileAccess.Write)

                zip3 = New ZipOutputStream(fileN2)
                Dim buffer((CheckSize(ImgPath & "\" & lblRequestID.Text & sFiletype))) As Byte
                Zip4 = New ZipEntry(Path.GetFileName(ImgPath & "\" & lblRequestID.Text & sFiletype))
                zip3.PutNextEntry(Zip4)

                Dim size As Int32
                Do
                    size = fileN3.Read(buffer, 0, buffer.Length)
                    zip3.Write(buffer, 0, size)
                Loop While size > 0
                zip3.Close()
                fileN3.Close()
                fileN2.Close()
            End If

            IDE = ImgPath & "\" & lblRequestID.Text & "-" & Left(FileUpload1.FileName, Len(FileUpload1.FileName) - 4) & ".zip"
            Dim filename As String
            Dim fileN As FileStream
            Dim Ln As Int32
            Dim oBinaryReader As BinaryReader
            Dim oImgByteArray As Byte()
            Dim file As String
            'Dim supcodeT As String  '----
            'supcodeT = supcode      '----
            'Dim ResidT As String
            'ResidT = resid

            filename = Path.GetFileName(lblRequestID.Text & sFiletype)
            file = Left(FileUpload1.FileName, Len(FileUpload1.FileName) - 4) & ".zip"
            fileN = New FileStream(IDE, FileMode.Open, FileAccess.Read)
            oBinaryReader = New BinaryReader(fileN)
            oImgByteArray = oBinaryReader.ReadBytes(CInt(fileN.Length))
            Ln = CInt(fileN.Length)

            Dim s As Integer = dp.InsertAttatchment(usr, lblRequestID.Text, file, oImgByteArray)
        End If

    End Sub

    Private Sub att_Read()
        Dim dt As New Data.DataTable
        Dim sSql As String = "select * from [TB_Attachment] where  [RequestId] = '" & lblRequestID.Text & "' "
        dt = DB_Product.GetDataTable(sSql)
        If dt.Rows.Count > 0 Then
            DataGrid1.DataSource = dt
            DataGrid1.DataBind()
        Else
        End If
    End Sub

    Function CheckSize(ByVal f As String) As Integer
        Dim oBinaryReader As BinaryReader
        Dim oImgByteArray As Byte()
        Dim FileN As FileStream
        Dim ln As Long
        FileN = New FileStream(f, FileMode.Open, FileAccess.Read)
        oBinaryReader = New BinaryReader(FileN)
        oImgByteArray = oBinaryReader.ReadBytes(CInt(FileN.Length))
        ln = CInt(FileN.Length)

        CheckSize = ln
    End Function

    Protected Sub DataGrid1_DeleteCommand(source As Object, e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DataGrid1.DeleteCommand
        Dim SelectID As Integer
        SelectID = Convert.ToString(DataGrid1.DataKeys(e.Item.ItemIndex))
        Dim s As Integer
        Dim ssql As String = "delete from TB_Attachment where Att_ID = " & SelectID
        s = DB_Product.Execute(ssql)
        DataGrid1.Controls.Clear()
        att_Read()
    End Sub

    Protected Sub DataGrid1_EditCommand(source As Object, e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DataGrid1.EditCommand
        If e.Item.ItemIndex <> -1 Then
            Dim SelectID As Integer
            SelectID = Convert.ToString(DataGrid1.DataKeys(e.Item.ItemIndex))
            Dim dt As New Data.DataTable
            Dim ssql As String = "select * from [TB_Attachment] with (nolock) where [RequestId] = '" & lblRequestID.Text & "' and [Att_ID] = " & SelectID
            dt = DB_Product.GetDataTable(ssql)

            Response.ClearContent()
            Response.ClearHeaders()
            If Right(dt.Rows(0)("Att_Filetype").ToString, 4) = ".pdf" Then
                Response.ContentType = "application/pdf" '"application/pdf"
                Response.BinaryWrite(dt.Rows(0)("Att"))
            ElseIf Right(dt.Rows(0)("Att_Filetype").ToString, 4) = ".xls" Then
                Response.ContentType = "application/vnd.ms-EXCEL" '"vnd.ms/"
                Response.BinaryWrite(dt.Rows(0)("Att"))
            ElseIf Right(dt.Rows(0)("Att_Filetype").ToString, 4) = ".doc" Then
                Response.ContentType = "application/msword" '"application/vnd.ms-WORD" '"vnds/"
                Response.BinaryWrite(dt.Rows(0)("Att"))
            ElseIf Right(dt.Rows(0)("Att_Filetype").ToString, 4) = ".zip" Then
                Response.ContentType = "application/x-zip-compressed"
                Response.BinaryWrite(dt.Rows(0)("Att"))

            Else
                Response.ContentType = dp.GetContentType(dt.Rows(0)("Att_Filetype"))
                Response.BinaryWrite(dt.Rows(0)("Att"))
            End If

            Response.Flush()
            Response.Close()
        Else
            Exit Sub
        End If
    End Sub

    Protected Sub btnApprove_Click(sender As Object, e As EventArgs) Handles btnApprove.Click

        'check ส่ง LF หรือไม่ 
        If rdoLFDC.Items.FindByValue("Yes").Selected = True Then
            UpdateStatus_waitSCM()
            SentMail_SCM()
        Else
            UpdateStatus_waitAccount()
            SentMail_Acct()
        End If

        Server.Transfer("NewMasterAll.aspx?usr=" & usr)
    End Sub


    'Protected Sub txtvendor_Init(sender As Object, e As EventArgs) Handles txtvendor.Init
    '    Dim sqlCon As New SqlConnection(db_Enpro.sqlCon)
    '    Dim sqlCmd As New SqlCommand
    '    Dim Rs As SqlDataReader
    '    Dim sql As String

    '    sql = "select Code,Name_Th from [SMVENDMS] "

    '    If sqlCon.State = ConnectionState.Closed Then
    '        sqlCon = New SqlConnection(db_Enpro.sqlCon)
    '        sqlCon.Open()
    '    End If

    '    With sqlCmd
    '        .Connection = sqlCon
    '        .CommandType = CommandType.Text
    '        .CommandText = sql
    '        Rs = .ExecuteReader
    '    End With
    '    txtvendor.Items.Clear()
    '    txtvendor.Items.Add(New ListItem("", 0))
    '    While Rs.Read
    '        'txtvendor.Items.Add(New ListItem(Rs.GetString(0), Rs.GetString(0)))
    '        txtvendor.Items.Add(New ListItem(Rs.GetString(0) + " : " + Rs.GetString(1), Rs.GetString(0)))
    '    End While
    'End Sub

End Class