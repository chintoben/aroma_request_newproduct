﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Maintain_Brand.aspx.vb" Inherits="Aroma_ProductMaster.Maintain_Brand" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">


<head id="Head1" runat="server">
    <title></title>
    <meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="Your description">
<meta name="keywords" content="Your keywords">
<meta name="author" content="Your name">
<link rel="icon" href="images/favicon.ico" type="image/x-icon">
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />

<link rel="stylesheet" href="css/bootstrap.css" type="text/css" media="screen">
<link rel="stylesheet" href="css/bootstrap-responsive.css" type="text/css" media="screen">    
<link rel="stylesheet" href="css/style.css" type="text/css" media="screen">

<script type="text/javascript" src="js/jquery.js"></script>  
<script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="js/superfish.js"></script>

<script type="text/javascript" src="js/jquery.ui.totop.js"></script>

<script type="text/javascript" src="js/cform.js"></script>
<script>
    $(document).ready(function () {
        //	


    }); //
    $(window).load(function () {
        //

    }); //
</script>		
    <style type="text/css">
        .style1
        {
        }
        .style4
        {
            width: 316px;
        }
        .style8
        {
        }
        .style9
        {
            height: 22px;
        }
        .style10
        {
            width: 316px;
            height: 22px;
        }
        </style>

<%--<link rel="stylesheet" href="css/bootstrap.css" type="text/css" media="screen">
<link rel="stylesheet" href="css/bootstrap-responsive.css" type="text/css" media="screen">    
<link rel="stylesheet" href="css/style.css" type="text/css" media="screen">
<script type="text/javascript" src="js/jquery.js"></script>  
<script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="js/superfish.js"></script>
<script type="text/javascript" src="js/jquery.ui.totop.js"></script>
<script type="text/javascript" src="js/cform.js"></script>
	--%>

         <script type = "text/javascript">
             function SetTarget() {
                 document.forms[0].target = "_blank";
             }
</script>

</head>
<body>
    <form id="form1" runat="server">
    <div>
    
    <div id="main">
<div class="top1_wrapper">
<div class="top1">
<div class="container">
<div class="row">
<div class="span12">
<div class="top1_inner clearfix">
	
<div class="top2 clearfix">
<header><div class="logo_wrapper"><a href="index.html" class="logo">
        <img src="images/aroma/logotrans.png" alt="" >
        <%--<img src="images/aroma/Lion.png" alt="" width="100" >--%>
        </a></div>
        
  </header>	
<div class="top3 clearfix">
<div class="phone1">
	<div class="txt1">Product Master</div>
<div class="txt2">จัดการสินค้า</div>
</div>

<div class="search-form-wrapper clearfix">

</div>
</div>

</div>

<div class="menu_wrapper">
<div class="navbar navbar_">
	<div class="navbar-inner navbar-inner_">
		<a class="btn btn-navbar btn-navbar_" data-toggle="collapse" data-target=".nav-collapse_">
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</a>
		<div class="nav-collapse nav-collapse_ collapse">
			<ul class="nav sf-menu clearfix">
               
               
                  <li><a href="Maintain_ProductType.aspx?usr=<%=Request.QueryString("usr") %>">Product Type</a></li>
            <li><a href="Maintain_ProductCat.aspx?usr=<%=Request.QueryString("usr") %>">Product Category</a></li>
            <li><a href="Maintain_ProductGroup.aspx?usr=<%=Request.QueryString("usr") %>">Product Group / Machine Type </a></li>

            <li><a href="Maintain_Brand.aspx?usr=<%=Request.QueryString("usr") %>">Brand</a></li>
            <li><a href="Maintain_Color.aspx?usr=<%=Request.QueryString("usr") %>">Color</a></li>
            <li><a href="Maintain_GroupHead.aspx?usr=<%=Request.QueryString("usr") %>">Group Head</a></li>
            					
    </ul>
		</div>
	</div>
</div>	
</div>

</div>	
</div>	
</div>	
</div>	
</div>	
</div>
<div id="inner">

<div id="content">
<div class="container">
<div class="row">
<div class="span12">


<div class="row">
<div class="span8">

<h1>Brand</h1>

</div>	

</div>



<div id="note"></div>
<div id="fields">

    <table>
   
    <tr>
        <td class="style8">
            &nbsp;</td>
        <td colspan="6">
            &nbsp;</td>
    </tr>
   
    <tr>
        <td class="style8">
            <asp:Label ID="Label33" runat="server" Text="Brand  : " 
                Font-Size="Medium" Font-Bold="False"></asp:Label>
            </td>
        <td class="style4">
            <asp:TextBox ID="txtProductType" runat="server" Width="300px" Height="30px"></asp:TextBox>
        </td>
        <td align ="rigth" > 
            &nbsp;</td>
        <td colspan="3">
            <asp:Button ID="btnAdd" runat="server" Text="ADD"  class="btn btn-primary" OnClientClick = "SetTarget();"
                BackColor="#003399" BorderColor="#0066FF" Font-Bold="True" Font-Size="Medium" 
                ForeColor="White" Height="30px" Width="100px"/>

        </td>
    </tr>
  
  
    
    </table>
    </br>

        <asp:GridView ID="GridView1" runat="server" AllowPaging="True"  
                                                            
            AutoGenerateColumns="False" DataKeyNames="id" DataSourceID="SqlDataSource1" 
                                                            Font-Bold="False" 
            Font-Names="Tahoma" Font-Size="Medium" ShowFooter="True" 
                                                            Width="90%" 
            CellPadding="4" 
                                                           
                                                            GridLines="None" 
            Height="157px" PageSize="12" 
            ForeColor="#333333">
                                                            <EditRowStyle BackColor="#999999" Width="100%" />
                                                            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                                            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                                                            <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                                                            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                                            <HeaderStyle BackColor="#5D7B9D" Font-Bold="true" ForeColor="White" />
                                                            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                                                                
                                                            <Columns>
                                                                
                                                       
                                                                <asp:BoundField DataField="id" HeaderText="id" 
                                                                    SortExpression="id" ReadOnly="True" >
                                                                    <HeaderStyle HorizontalAlign="Left" />
                                                                </asp:BoundField>

                                                                <asp:BoundField DataField="Brand" HeaderText="Brand" 
                                                                    SortExpression="Brand" >
                                                                    <ControlStyle Width="500px" />
                                                                    <HeaderStyle HorizontalAlign="Left" />
                                                                </asp:BoundField>
                                                            
                                                     
                                                                    
                                                                    <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" 
                                                                   
                                                                    DeleteImageUrl="~/Images/icon_delete.ico" 
                                                                    EditImageUrl="~/Images/icon_edit.ico" 
                                                                    ButtonType="Image" 
                                                                    CancelImageUrl="~/Images/icon_close.ico" 
                                                                    UpdateImageUrl="~/Images/icon_Save.ico" >
                                                                         <HeaderStyle HorizontalAlign="Left" />
                                                                   </asp:CommandField>
                                                                   
                                                            </Columns>
                                                            <SortedAscendingCellStyle BackColor="#E9E7E2" />
                                                            <SortedAscendingHeaderStyle BackColor="#506C8C" />
                                                            <SortedDescendingCellStyle BackColor="#FFFDF8" />
                                                            <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                                                        </asp:GridView>
          <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
        ConnectionString="<%$ ConnectionStrings:ProductConnectionString %>" 
        DeleteCommand="DELETE FROM  [TB_ProductBrand] where [id] = @id" 
        SelectCommand="SELECT id,Brand from TB_ProductBrand order by id desc"
        UpdateCommand="UPDATE [TB_ProductBrand] set  Brand=@Brand where [id] = @id"
        >
        
        <UpdateParameters>      
            <asp:Parameter Name="Brand" />       
        </UpdateParameters>
        
    </asp:SqlDataSource>

<%--
	<form id="ajax-contact-form" class="form-horizontal" action="javascript:alert('success!');">
		<div class="row">
			<div class="span4">
				<div class="control-group">
				    <label class="control-label" for="inputName">Your full name:</label>
				    <div class="controls">				      
				        <asp:CheckBoxList ID="CheckBoxList3" runat="server" RepeatColumns="4" 
                Font-Size="Medium" Width="800px">
                  <asp:ListItem Value="kvn">บจ. เค.วี.เอ็น.อิมปอร์ต เอ็กซ์ปอร์ต  (1991)</asp:ListItem>
                  <asp:ListItem Value="lion">บจ. ไลอ้อน ทรี-สตาร์</asp:ListItem>
            
                  <asp:ListItem Value="kvn">บจ. เค.วี.เอ็น.อิมปอร์ต เอ็กซ์ปอร์ต  (1991)</asp:ListItem>
                  <asp:ListItem Value="lion">บจ. ไลอ้อน ทรี-สตาร์</asp:ListItem>
                  <asp:ListItem Value="lion">บจ. ไลอ้อน ทรี-สตาร์</asp:ListItem>
                
              </asp:CheckBoxList>
                        
                    </div>
				</div>				
			</div>	
			<div class="span4">
				<div class="control-group">
				    <label class="control-label" for="inputEmail">Your email:</label>
				    <div class="controls">				      
				      <input class="span4" type="text" id="inputEmail" name="email" value="Your email:" onBlur="if(this.value=='') this.value='Your email:'" onFocus="if(this.value =='Your email:' ) this.value=''">
				    </div>
				</div>
			</div>		
			<div class="span4">
				<div class="control-group">
				    <label class="control-label" for="inputPhone">Phone number:</label>
				    <div class="controls">				      
				      <input class="span4" type="text" id="inputPhone" name="phone" value="Phone number:" onBlur="if(this.value=='') this.value='Phone number:'" onFocus="if(this.value =='Phone number:' ) this.value=''">
				    </div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="span12">
				<div class="control-group">
				    <label class="control-label" for="inputMessage">Message:</label>
				    <div class="controls">				      				      
				      <textarea class="span12" id="inputMessage" name="content" onBlur="if(this.value=='') this.value='Message:'" 
                        onFocus="if(this.value =='Message:' ) this.value=''">Message:</textarea>
				    </div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="span12">
				<div class="control-group capthca">
				    <label class="control-label" for="inputCapthca">Capthca:</label>
				    <div class="controls">				      
				      <input class="" type="text" id="inputCapthca" name="capthca" value="Capthca:" onBlur="if(this.value=='') this.value='Capthca:'" onFocus="if(this.value =='Capthca:' ) this.value=''">
				      <img src="captcha/captcha.php">
				    </div>
				</div>
			</div>
		</div>
		<button type="submit" class="submit">submit</button>
	</form>--%>


</div>	

	
	

</div>	
</div>	
</div>	
</div>


<div class="bot1">
<div class="container">
<div class="row">
<div class="span12">
<div class="bot1_inner">
<div class="row">
<div class="span4">
	
<div class="logo2_wrapper"><a href="index.html" class="logo2"><img src="images/aroma/logotrans.png" alt=""></a></div>

<footer><div class="copyright">Copyright   © 2018. All rights reserved.<br><a href="#">Aroma Group</a></div></footer>


</div>
<div class="span4">
	
<div class="bot1_title">Contact Us</div>

สำนักงานใหญ่ อโรม่า กรุ๊ป<br>
บริษัท เค.วี.เอ็น.อิมปอร์ต เอกซ์ปอร์ต (1991) จำกัด<br>
เลขที่ 43 ชั้น 2 ซอยนาคนิวาส 6 ถนนนาคนิวาส แขวงลาดพร้าว <br>
เขตลาดพร้าว กรุงเทพฯ 10230<br>
โทรศัพท์ : 02 159-8999<br>
โทรสาร : 02 538-8144, 02 539-5597

<%--E-mail: <a href="#">mail@demosite.com</a>--%>


</div>
<div class="span4">
	
<%--<div class="bot1_title">Follow Us:</div>	

<div class="social_wrapper">
		<ul class="social clearfix">
	    <li><a href="#"><img src="images/social_ic1.png"></a></li>
	    <li><a href="#"><img src="images/social_ic2.png"></a></li>
	    <li><a href="#"><img src="images/social_ic3.png"></a></li>	    
	    <li><a href="#"><img src="images/social_ic4.png"></a></li>
		</ul>
	</div>
--%>


</div>	

</div>	
</div>
</div>	
</div>	
</div>	
</div>

</div>	
</div>
<script type="text/javascript" src="js/bootstrap.js"></script>



    </div>
    </form>
</body>
</html>