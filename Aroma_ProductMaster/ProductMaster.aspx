﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ProductMaster.aspx.vb" Inherits="Aroma_ProductMaster.ProductMaster" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="Your description">
<meta name="keywords" content="Your keywords">
<meta name="author" content="Your name">
<link rel="icon" href="images/favicon.ico" type="image/x-icon">
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />

<link rel="stylesheet" href="css/bootstrap.css" type="text/css" media="screen">
<link rel="stylesheet" href="css/bootstrap-responsive.css" type="text/css" media="screen">    
<link rel="stylesheet" href="css/style.css" type="text/css" media="screen">

<script type="text/javascript" src="js/jquery.js"></script>  
<script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="js/superfish.js"></script>

<script type="text/javascript" src="js/jquery.ui.totop.js"></script>

<script type="text/javascript" src="js/cform.js"></script>
<script>
    $(document).ready(function () {
        //	


    }); //
    $(window).load(function () {
        //

    }); //
</script>		
    <style type="text/css">
        .style2
        {
            height: 49px;
        }
        .style4
        {
        }
        .style7
        {
            height: 49px;
            width: 162px;
        }
        .style8
        {
            width: 162px;
        }
        .style9
        {
            height: 35px;
            width: 162px;
        }
        .style10
        {
            height: 35px;
        }
        .style11
        {
            width: 162px;
            height: 32px;
        }
        .style12
        {
            height: 32px;
        }
        </style>

<%--<link rel="stylesheet" href="css/bootstrap.css" type="text/css" media="screen">
<link rel="stylesheet" href="css/bootstrap-responsive.css" type="text/css" media="screen">    
<link rel="stylesheet" href="css/style.css" type="text/css" media="screen">
<script type="text/javascript" src="js/jquery.js"></script>  
<script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="js/superfish.js"></script>
<script type="text/javascript" src="js/jquery.ui.totop.js"></script>
<script type="text/javascript" src="js/cform.js"></script>
	--%>

     <script type = "text/javascript">
         function SetTarget() {
             document.forms[0].target = "_blank";
         }
</script>

</head>
<body>
    <form id="form1" runat="server">
    <div>
    
    <div id="main">
<div class="top1_wrapper">
<div class="top1">
<div class="container">
<div class="row">
<div class="span12">
<div class="top1_inner clearfix">
	
<div class="top2 clearfix">
<header><div class="logo_wrapper"><a href="index.html" class="logo">
        <img src="images/aroma/logotrans.png" alt="" >
        <%--<img src="images/aroma/Lion.png" alt="" width="100" >--%>
        </a></div>
        
  </header>	
<div class="top3 clearfix">
<div class="phone1">
	<div class="txt1">Request New Master</div>
	<div class="txt2">Product</div>
</div>

<div class="search-form-wrapper clearfix">

</div>
</div>

</div>

<div class="menu_wrapper">
<div class="navbar navbar_">
	<div class="navbar-inner navbar-inner_">
		<a class="btn btn-navbar btn-navbar_" data-toggle="collapse" data-target=".nav-collapse_">
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</a>
		<div class="nav-collapse nav-collapse_ collapse">
			<ul class="nav sf-menu clearfix">
            <li class="active"><a href="Main.aspx?usr=<%=Request.QueryString("usr") %>">Home</a></li>
            <li><a href="NewMaster.aspx?usr=<%=Request.QueryString("usr") %>">Request New Master</a></li>
            <li><a href="NewMasterAll.aspx?usr=<%=Request.QueryString("usr") %>">Request New Master All</a></li>
             <li><a href="ProductMaster.aspx?usr=<%=Request.QueryString("usr") %>">Product Master</a></li>
            <li><a href="Login.aspx">Sign Out</a></li>										
     </ul>
		</div>
	</div>
</div>	
</div>

</div>	
</div>	
</div>	
</div>	
</div>	
</div>
<div id="inner">

<div id="content">
<div class="container">
<div class="row">
<div class="span12">

<div class="breadcrumbs1"><a href="Main.aspx?usr=<%=Request.QueryString("usr") %>">Home Page</a><span></span>Product Master</div>



<div class="row">
<div class="span8">

<h1>Product Master</h1>

</div>	

</div>



<div id="note"></div>
<div id="fields">

    <table>
    <tr>
        <td class="style7">
            <asp:Label ID="Label1" runat="server" Text="บริษัท : " Font-Size="Medium" 
                Width="100px" Font-Bold="True"></asp:Label>
            <br />
        </td>
        <td colspan="7" class="style2">

              <asp:CheckBoxList ID="chkCompany" runat="server" RepeatColumns="3" 
                Font-Size="Medium" Width="420px" RepeatDirection="Horizontal">
                  <asp:ListItem Value="kvn" Selected="True">บจ. เค.วี.เอ็น.อิมปอร์ต เอ็กซ์ปอร์ต  (1991)</asp:ListItem>
                  <asp:ListItem Value="lion" Selected="True">บจ. ไลอ้อน ทรี-สตาร์</asp:ListItem>
              </asp:CheckBoxList>
        </td>
    </tr>

 
    <tr>
        <td class="style8">
            <asp:Label ID="Label43" runat="server" Text="รหัสสินค้า" 
                Font-Size="Medium" Font-Bold="False"></asp:Label>
            </td>
        <td class="style4">

            <asp:TextBox ID="txtItem" runat="server" Width="250px" Height="30px"></asp:TextBox>
        </td>
        <td class="style4">
            &nbsp;</td>
        <td align ="rigth" > 
            <asp:Label ID="Label20" runat="server" Text="ชื่อสินค้า (ภาษาไทย) :" 
                Font-Size="Medium" Font-Bold="False" Width="190px"></asp:Label>
            </td>
        <td colspan="3">
            <asp:TextBox ID="txtItemDesc" runat="server" Width="250px" Height="30px"></asp:TextBox>
        </td>
    </tr>
  
    <tr>
        <td class="style8">
            <asp:Label ID="Label33" runat="server" Text="Product Type  : " 
                Font-Size="Medium" Font-Bold="False"></asp:Label>
            </td>
        <td class="style4">
            <asp:DropDownList ID="drpProductType" runat="server" Height="30px" 
                Width="250px">
            </asp:DropDownList>
        </td>
        <td class="style4">
            &nbsp;</td>
        <td align ="rigth" > 
            <asp:Label ID="Label34" runat="server" Text="Product Category  : " 
                Font-Size="Medium" Font-Bold="False"></asp:Label>
            </td>
        <td colspan="3">
            <asp:DropDownList ID="drpProductCat" runat="server" Height="30px" Width="250px">
            </asp:DropDownList>
        </td>
    </tr>
  
 
  
    <tr>
        <td class="style9">
            <asp:Label ID="Label35" runat="server" Text="Product Group : " 
                Font-Size="Medium" Font-Bold="False" Width="230px"></asp:Label>
            </td>
        <td class="style10">
            <asp:DropDownList ID="drpProductGroup" runat="server" Height="30px" 
                Width="250px">
            </asp:DropDownList>
        </td>
        <td class="style10">
            </td>
        <td align ="rigth" class="style10" colspan="2"> 
            <asp:Label ID="Label38" runat="server" Text="Group Head : " 
                Font-Size="Medium" Font-Bold="False"></asp:Label>
            </td>
        <td colspan="3" class="style10">
            <asp:DropDownList ID="drpGroup" runat="server" Height="30px" Width="250px">
            </asp:DropDownList>
        </td>
    </tr>
  
    <tr>
        <td class="style11">
            <asp:Label ID="Label39" runat="server" Text="Color : " 
                Font-Size="Medium" Font-Bold="False"></asp:Label>
            </td>
        <td class="style12">
            <asp:DropDownList ID="txtColor" runat="server" Height="30px" 
                Width="250px">
            </asp:DropDownList>
        </td>
        <td class="style12">
            </td>
        <td align ="rigth" class="style12" colspan="2"> 
            <asp:Label ID="Label36" runat="server" Text="Brand  : " 
                Font-Size="Medium" Font-Bold="False"></asp:Label>
            </td>
        <td colspan="3" class="style12">
            <asp:DropDownList ID="txtBrand" runat="server" Height="30px" 
                Width="250px">
            </asp:DropDownList>
        </td>
    </tr>
  
    
    <%--<tr>
        <td class="style9">
            <asp:Label ID="Label40" runat="server" Text="Option 1 : " 
                Font-Size="Medium" Font-Bold="False"></asp:Label>
            </td>
        <td class="style10">
            <asp:TextBox ID="txtOption1" runat="server" Width="200px"></asp:TextBox>
        </td>
        <td align =rigth class="style10" colspan="2"> 
            <asp:Label ID="Label41" runat="server" Text="Option 2 : " 
                Font-Size="Medium" Font-Bold="False"></asp:Label>
            </td>
        <td class="style10">
            <asp:TextBox ID="txtOption2" runat="server" Width="200px"></asp:TextBox>
        </td>
        <td align = rigth class="style10">
            <asp:Label ID="Label42" runat="server" Text="Option 3 : " 
                Font-Size="Medium" Font-Bold="False"></asp:Label>
            </td>
        <td class="style10">
            <asp:TextBox ID="txtOption3" runat="server" Width="200px"></asp:TextBox>
        </td>
    </tr>--%>   
<%-- <tr>
        <td class="style8">
            <asp:Label ID="Label2" runat="server" Text="สำหรับ Marketing" 
                Font-Size="Medium" Font-Bold="False"></asp:Label>
            </td>
        <td class="style4">
            <asp:CheckBox ID="chkCheckRawMaterial" runat="server" Font-Size="Medium" 
                Text="CheckRawMaterial" />
        </td>
        <td align ="rigth" class="style1" colspan="2"> 
            &nbsp;</td>
        <td class="style11" colspan="2">
            <asp:CheckBox ID="chkCheckMachine" runat="server" Font-Size="Medium" 
                Text="CheckMachine" />
        </td>
        <td>
            &nbsp;</td>
    </tr>--%>  
    <%--<tr>
        <td class="style8">
            <asp:Label ID="Label44" runat="server" Text="ประเภทสินค้า" 
                Font-Size="Medium" Font-Bold="False"></asp:Label>
            </td>
        <td class="style4">

            <asp:DropDownList ID="drpChaodoi" runat="server" Width="200px">
             <asp:ListItem Value=""></asp:ListItem>
                <asp:ListItem Value="Y">สินค้าชาวดอย</asp:ListItem>
                <asp:ListItem Value="N">ไม่ใช่สินค้าชาวดอย</asp:ListItem>
                <asp:ListItem Value="C">สินค้า Common</asp:ListItem>
            </asp:DropDownList>
        </td>
        <td align ="rigth" class="style1" colspan="2"> 
            &nbsp;</td>
        <td class="style11" colspan="2">
            &nbsp;</td>
        <td>
            &nbsp;</td>
    </tr> --%>
    
    <tr>
        <td class="style8">
            &nbsp;</td>
        <td class="style4" colspan="4">
            <asp:Button ID="btnSearch" runat="server" Text="Search"  class="btn btn-primary" 
                BackColor="#003399" BorderColor="#0066FF" Font-Bold="True" Font-Size="Medium" 
                ForeColor="White" Height="30px" Width="100px"/>
            <%--	
              &nbsp;<asp:Button ID="btnExport" runat="server" Text="Export"  class="btn btn-primary" OnClientClick = "SetTarget();"
                BackColor="#003399" BorderColor="#0066FF" Font-Bold="True" Font-Size="Medium" 
                ForeColor="White" Height="30px" Width="100px"/>
--%>&nbsp;</td>
        <td>
            &nbsp;</td>
        <td align = rigth>
            &nbsp;</td>
        <td>
            &nbsp;</td>
    </tr>
   
    
    <tr>
        <td class="style8">
            &nbsp;</td>
        <td class="style4" colspan="4">
            &nbsp;</td>
        <td>
            &nbsp;</td>
        <td align = rigth>
            &nbsp;</td>
        <td>
            &nbsp;</td>
    </tr>
   
    
    </table>

    <asp:GridView ID="GridView" runat="server"  Width = "100%"  DataKeyNames="Item" 
        AutoGenerateColumns = "False" 
        AlternatingRowStyle-BackColor = "#F0F8FF"  
        HeaderStyle-BackColor = "green" 
        AllowPaging ="True"  ShowFooter = "True"  
        OnPageIndexChanging = "OnPaging" CellPadding="3" Height="100%" 
        BorderStyle="None" BorderWidth="1px" 
                  Font-Names="Tahoma" BackColor="White" BorderColor="#CCCCCC" 
        PageSize="20" >
                            <Columns>

                               <asp:HyperLinkField DataNavigateUrlFields="Item,username,Company" 
                                            DataNavigateUrlFormatString="ProductMasterDetail.aspx?Item={0}&username={1}&Company={2}" 
                                            DataTextField="Item" HeaderText="รหัสสินค้า" 
                                            NavigateUrl="ProductMasterDetail.aspx?Item={0}&username={1}&Company={2}"  Target="_self" >
                                        <ControlStyle ForeColor="Blue"></ControlStyle>
                                            <ItemStyle Font-Bold="True" />
                                        </asp:HyperLinkField>

                           <%-- <asp:BoundField DataField="Item" HeaderText="Item" />--%>

                            <asp:BoundField DataField="ItemDescription" HeaderText="ชื่อสินค้า" />

                            <asp:BoundField DataField="Company" HeaderText="Company" />

                            <asp:BoundField DataField="ProductType" HeaderText="Product Type" />

                            <asp:BoundField DataField="ProductCategory" HeaderText="Product Category" />

                            <asp:BoundField DataField="ProductGroup" HeaderText="Product Group" />

                            <asp:BoundField DataField="Brand" HeaderText="Brand" />

                           <asp:BoundField DataField="Model" HeaderText="Model" />

                           <asp:BoundField DataField="GroupHead" HeaderText="Group Head" />
       
                           <asp:BoundField DataField="Color" HeaderText="Color" />

                        
                            </Columns>

                            <FooterStyle BackColor="White" ForeColor="#000066" />
                            <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                            <RowStyle ForeColor="#000066" />
                            <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                            <SortedAscendingCellStyle BackColor="#F1F1F1" />
                            <SortedAscendingHeaderStyle BackColor="#007DBB" />
                            <SortedDescendingCellStyle BackColor="#CAC9C9" />
                            <SortedDescendingHeaderStyle BackColor="#00547E" />
    </asp:GridView>

     

<%-- <asp:BoundField DataField="Item" HeaderText="Item" />--%>


</div>	

	
	

</div>	
</div>	
</div>	
</div>


<div class="bot1">
<div class="container">
<div class="row">
<div class="span12">
<div class="bot1_inner">
<div class="row">
<div class="span4">
	
<div class="logo2_wrapper"><a href="index.html" class="logo2"><img src="images/aroma/logotrans.png" alt=""></a></div>

<footer><div class="copyright">Copyright   © 2018. All rights reserved.<br><a href="#">Aroma Group</a></div></footer>


</div>
<div class="span4">
	
<div class="bot1_title">Contact Us</div>

สำนักงานใหญ่ อโรม่า กรุ๊ป<br>
บริษัท เค.วี.เอ็น.อิมปอร์ต เอกซ์ปอร์ต (1991) จำกัด<br>
เลขที่ 43 ชั้น 2 ซอยนาคนิวาส 6 ถนนนาคนิวาส แขวงลาดพร้าว <br>
เขตลาดพร้าว กรุงเทพฯ 10230<br>
โทรศัพท์ : 02 159-8999<br>
โทรสาร : 02 538-8144, 02 539-5597

<%-- <asp:BoundField DataField="Item" HeaderText="Item" />--%>


</div>
<div class="span4">
	
    <%-- <asp:BoundField DataField="Item" HeaderText="Item" />--%>


</div>	

</div>	
</div>
</div>	
</div>	
</div>	
</div>

</div>	
</div>


<%--   <asp:BoundField DataField="Option1" HeaderText="Option1" />

                           <asp:BoundField DataField="Option2" HeaderText="Option2" />

                           <asp:BoundField DataField="Option3" HeaderText="Option3" />--%>



    </div>
    </form>
</body>
</html>