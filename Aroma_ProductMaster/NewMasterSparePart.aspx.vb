﻿Imports System.Drawing
Imports System.IO
Imports System.Data.SqlClient
Imports System.Data.OleDb
Imports System.Net.Mail

Public Class NewMasterSparePart
    Inherits System.Web.UI.Page
    Dim DB_Product As New Connect_product
    Dim db_HR As New Connect_HR
    Dim db_mac5 As New Connect_Mac5
    Dim db_Enpro As New Connect_Enpro
    Dim dt, dt1, dt2, dt3, dt4, dtt, dtRole As New DataTable

    Dim No As String
    Dim EmpID, DocID, RequestId, Status, usr, sql3, sql4 As String
    Private sms As New PKMsg("")

    Dim sql, sql2, CallID, Type, item As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        usr = Request.QueryString("usr")
        RequestId = Request.QueryString("id")

        'LoadData(RequestId)
        If usr = "" Then
            Server.Transfer("login.aspx")
        End If


        If Not IsPostBack Then

            If RequestId <> "" Then

                Panel1.Visible = False
                Label1.Visible = False

                'check ข้อมูลเก่า
                sql3 = " select * "
                sql3 += " from  [TB_ProductRequest_spare]"
                sql3 += " where  RequestId = '" & RequestId & "'"
                dt3 = DB_Product.GetDataTable(sql3)
                If dt3.Rows.Count > 0 Then
                    LoadData(RequestId)
                    lblRequestID.Text = RequestId
                    lblStatus.Text = dt3.Rows(0)("stat")

                    If dt3.Rows(0)("stat") = "Waiting Account Approve" Then
                        'check สิทธิ์
                        sql4 = " select * "
                        sql4 += " from  [TB_UserApprove]"
                        sql4 += " where  [Stauts_approve] = 'Waiting Account Approve' and  [EmployeeID]  = '" & usr & "'"
                        dt4 = DB_Product.GetDataTable(sql4)
                        If dt4.Rows.Count > 0 Then
                            btnSubmit.Visible = False
                        Else
                            btnSubmit.Visible = False
                            btnApprove.Visible = False
                        End If

                    ElseIf dt3.Rows(0)("stat") = "Complete" Then
                        btnSubmit.Visible = False
                        btnApprove.Visible = False

                    End If

                Else

                End If

            Else

                'new
                drpRequester.SelectedValue = usr
                sql = " select * "
                sql += " from VW_Employee "
                sql += " where Employee_id = '" & usr & "' "
                dt = db_HR.GetDataTable(sql)
                If dt.Rows.Count > 0 Then
                    txtDep.Text = dt.Rows(0)("department_name")
                    drpRequester.SelectedValue = dt.Rows(0)("fullname")
                End If

                'check ข้อมูลเก่า
                sql2 = " select isnull(RequestId,'') as RequestId "
                sql2 += " from  [TB_ProductRequest_spare]"
                sql2 += " where  stat = 'Draft' and CreateBy = '" & usr & "'"
                dt2 = DB_Product.GetDataTable(sql2)
                If dt2.Rows.Count > 0 Then
                    RequestId = dt2.Rows(0)("RequestId")
                    lblRequestID.Text = RequestId
                    LoadData(RequestId)
                    lblStatus.Text = "Draft"
                End If

                btnApprove.Visible = False


            End If
        End If

    End Sub


    Private Sub LoadData(RequestId)

        Dim IntPageNumber As Int32 = 0
        Dim sqlcon As New SqlConnection(DB_Product.sqlCon)
        Dim cmd As New SqlCommand
        Dim DSet As New DataSet
        Dim sql As String
        Dim constr As String

        Try
            constr = "data source=10.0.24.20;initial catalog=DB_RequestNewProduct;persist security info=false;User ID=armth;Password=Kb5r#Ge9Z3M*mQ;Connect Timeout=0;Max Pool Size=500;Enlist=true"
            sqlcon = New SqlConnection(constr)
            sqlcon.Open()
            'Return True
            'ถ้าเกิด Error ขึ้นให้ใช้ 
        Catch
            Try
                constr = "data source=10.0.24.20;initial catalog=DB_RequestNewProduct;persist security info=false;User ID=armth;Password=Kb5r#Ge9Z3M*mQ;Connect Timeout=0;Pooling=false"
                sqlcon = New SqlConnection(constr)
                sqlcon.Open()
                'Return True
            Catch
                'Return False
            End Try

        End Try
        Try

            sql = "select * " & vbCrLf
            sql = sql & " from VW_ProductRequest_spare  "

            If RequestId = "" Then
                sql = sql & " where stat = 'Draft' "
            Else
                sql = sql & " where RequestId = '" & RequestId & "'"
            End If


            sql = sql & " Order by RequestDate desc "

            Dim dt As New DataTable
            dt = DB_Product.GetDataTable(sql)
            If dt.Rows.Count > 0 Then
                GridView.DataSource = dt
                GridView.DataBind()
            Else
                ShowNoResultFound(dt, GridView)
            End If

        Catch


        End Try
        sqlcon.Close()

        'Gridview1
        'Attribute to show the Plus Minus Button.
        GridView.HeaderRow.Cells(0).Attributes("data-class") = "expand"
        'Attribute to hide column in Phone.
        GridView.HeaderRow.Cells(2).Attributes("data-hide") = "phone"
        GridView.HeaderRow.Cells(3).Attributes("data-hide") = "phone"
        'Adds THEAD and TBODY to GridView.
        GridView.HeaderRow.TableSection = TableRowSection.TableHeader

    End Sub

    Protected Sub OnPaging(ByVal sender As Object, ByVal e As GridViewPageEventArgs)
        LoadData(RequestId)
        GridView.PageIndex = e.NewPageIndex
        GridView.DataBind()
    End Sub

    Sub ShowNoResultFound(ByVal dtt As DataTable, ByVal gv As GridView)
        dtt.Rows.Add(dtt.NewRow)
        gv.DataSource = dtt
        gv.DataBind()
        Dim columnsCount As Integer = gv.Columns.Count
        gv.Rows(0).Cells.Clear()
        gv.Rows(0).Cells.Add(New TableCell)
        gv.Rows(0).Cells(0).ColumnSpan = columnsCount
        gv.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
        gv.Rows(0).Cells(0).ForeColor = Drawing.Color.Red
        gv.Rows(0).Cells(0).Font.Bold = True
        gv.Rows(0).Cells(0).Text = "-ไม่พบข้อมูล-"
    End Sub


    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click

        sql = " select * "
        sql += " from [TB_ProductRequest_spare] "
        sql += " where [Stat] = 'Draft'  "
        dt = DB_Product.GetDataTable(sql)
        If dt.Rows.Count = 0 Then
            sms.Msg = "กรุณา Upload file "
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "window.alert('" & sms.Msg & "');", True)
            Exit Sub
        End If

        GenID()
        UpdateStatus(RequestId)

        SentMail()

        Server.Transfer("NewMasterAll.aspx?usr=" & usr)
        LoadData(RequestId)

    End Sub


    Sub GenID()

        Dim conn As New Connect_product
        Dim sql As String = "Select * from TB_ProductRequest_spare where len(RequestId) = 8 and substring(cast(year(getdate()) as varchar(50)),3,2) = substring(RequestId,2,2) "
        Dim sql2 As String = "Select Max(Right(RequestId,4)) as RequestId from TB_ProductRequest_spare where len(RequestId) = 8  and substring(cast(year(getdate()) as varchar(50)),3,2) = substring(RequestId,2,2)"

        dt = DB_Product.GetDataTable(sql)
        dt2 = DB_Product.GetDataTable(sql2)

        If dt.Rows.Count <= 0 Then
            DocID = "S" & Now.ToString("yy") & "-" & "0001"
        Else
            Dim newID As Integer = CInt(dt2.Rows(0)("RequestId"))
            newID += 1
            DocID = "S" & Now.ToString("yy") & "-" & newID.ToString("0000")
        End If
        RequestId = DocID
        lblRequestID.Text = RequestId

    End Sub


    Sub UpdateStatus(RequestId)
        Dim x As Integer
        Dim db_product As New Connect_product
        Dim Cn As New SqlConnection(db_product.sqlCon)

        Dim sSql As String = "UPDATE [TB_ProductRequest_spare] SET  "
        sSql += " Stat= 'Waiting Account Approve'  "

        sSql += " ,ModifyBy=@ModifyBy ,ModifyDate=@ModifyDate "
        sSql += " ,RequestId = '" & RequestId & "'"

        sSql += " Where  Stat='Draft' "

        Dim command As SqlCommand = New SqlCommand(sSql, Cn)
        Try
            command.Parameters.Add("@ModifyBy", Data.SqlDbType.VarChar).Value = usr
            command.Parameters.Add("@ModifyDate", Data.SqlDbType.VarChar).Value = DateTime.Now.ToString("yyyy-MM-dd")

            command.CommandType = Data.CommandType.Text
            Cn.Open()
            x = command.ExecuteScalar
        Catch x1 As Exception
            Err.Raise(60002, , x1.Message)
        Finally
            Cn.Close()
        End Try

    End Sub

    Sub InsertBI_kvn()
        Dim dt As New DataTable()
        dt.Columns.AddRange(New DataColumn(1) {New DataColumn("ItemCode"), New DataColumn("ItemDescription")})

        For Each row As GridViewRow In GridView.Rows
            If row.RowType = DataControlRowType.DataRow Then

                sql = " SELECT Item "
                sql += " from    TB_ProductMasterGroup "
                sql += " where Item = '" & row.Cells(6).Text & "' and Company= 'KVN'"
                dt = db_mac5.GetDataTable(sql)
                If dt.Rows.Count <= 0 Then   'check ซ้ำ

                    Dim Company As String = row.Cells(1).Text
                    Dim ProductType As String = row.Cells(2).Text
                    Dim ProductCategory As String = row.Cells(3).Text
                    Dim ProductGroup As String = row.Cells(4).Text
                    Dim Brand As String = row.Cells(5).Text
                    Dim item As String = row.Cells(6).Text


                    Dim Cn As New SqlConnection(db_mac5.sqlCon)
                    Dim sqlcon As New OleDbConnection(db_mac5.cnPAStr)
                    Dim sqlcom As New OleDbCommand
                    Dim sSql As String = "INSERT INTO [TB_ProductMasterGroup] "
                    sSql += " (Company,ProductType,ProductCategory,ProductGroup,Brand,item)"
                    sSql += " Values(  "
                    sSql += "'KVN'"
                    sSql += ",'" & ProductType & "'"
                    sSql += ",'" & ProductCategory & "'"
                    sSql += ",'" & ProductGroup & "'"
                    sSql += ",'" & Brand & "','" & item & "' )"
                    If sqlcon.State = ConnectionState.Closed Then
                        sqlcon = New OleDbConnection(db_mac5.cnPAStr)
                        sqlcon.Open()
                    End If
                    With sqlcom
                        .Connection = sqlcon
                        .CommandType = CommandType.Text
                        .CommandText = sSql
                        .ExecuteNonQuery()
                    End With

                End If

            End If
        Next
    End Sub

    Sub InsertBI_lion()
        Dim dt As New DataTable()
        dt.Columns.AddRange(New DataColumn(1) {New DataColumn("ItemCode"), New DataColumn("ItemDescription")})

        For Each row As GridViewRow In GridView.Rows
            If row.RowType = DataControlRowType.DataRow Then

                sql = " SELECT Item "
                sql += " from    TB_ProductMasterGroup "
                sql += " where Item = '" & row.Cells(6).Text & "'  and Company = 'Lion' "
                dt = db_mac5.GetDataTable(sql)
                If dt.Rows.Count <= 0 Then   'check ซ้ำ

                    Dim Company As String = row.Cells(1).Text
                    Dim ProductType As String = row.Cells(2).Text
                    Dim ProductCategory As String = row.Cells(3).Text
                    Dim ProductGroup As String = row.Cells(4).Text
                    Dim Brand As String = row.Cells(5).Text
                    Dim item As String = row.Cells(6).Text

                    Dim Cn As New SqlConnection(db_mac5.sqlCon)
                    Dim sqlcon As New OleDbConnection(db_mac5.cnPAStr)
                    Dim sqlcom As New OleDbCommand
                    Dim sSql As String = "INSERT INTO [TB_ProductMasterGroup] "
                    sSql += " (Company,ProductType,ProductCategory,ProductGroup,Brand,item)"
                    sSql += " Values(  "
                    sSql += "'Lion'"
                    sSql += ",'" & ProductType & "'"
                    sSql += ",'" & ProductCategory & "'"
                    sSql += ",'" & ProductGroup & "'"
                    sSql += ",'" & Brand & "','" & item & "' )"
                    If sqlcon.State = ConnectionState.Closed Then
                        sqlcon = New OleDbConnection(db_mac5.cnPAStr)
                        sqlcon.Open()
                    End If
                    With sqlcom
                        .Connection = sqlcon
                        .CommandType = CommandType.Text
                        .CommandText = sSql
                        .ExecuteNonQuery()
                    End With

                End If

            End If
        Next
    End Sub


    Sub Insert_Mac5()
        Dim dt As New DataTable()
        dt.Columns.AddRange(New DataColumn(1) {New DataColumn("ItemCode"), New DataColumn("ItemDescription")})

        For Each row As GridViewRow In GridView.Rows
            If row.RowType = DataControlRowType.DataRow Then

                sql = " SELECT STKcode "
                sql += " from    [STK] "
                sql += " where STKcode = '" & row.Cells(7).Text & "'"
                dt = db_mac5.GetDataTable(sql)
                If dt.Rows.Count <= 0 Then   'check ซ้ำ

                    Dim Company As String = row.Cells(2).Text
                    Dim code As String = row.Cells(7).Text
                    Dim part_no As String = row.Cells(13).Text
                    Dim group As String = row.Cells(8).Text
                    Dim item_desc As String = row.Cells(10).Text & " (" & row.Cells(9).Text & ")"
                    Dim brand As String = row.Cells(6).Text
                    Dim unit As String = row.Cells(12).Text
                    Dim unit_price As String = row.Cells(14).Text


                    Dim Cn As New SqlConnection(db_mac5.sqlCon)
                    Dim sqlcon As New OleDbConnection(db_mac5.cnPAStr)
                    Dim sqlcom As New OleDbCommand


                    Dim sSql As String = "INSERT INTO [STK] "

                    sSql += " ("
                    sSql += " STKcode,STKgroup,STKcode2,STKdescT1,STKdescT2,STKdescT3,STKdescE1,STKdescE2,STKdescE3"
                    sSql += " ,STKmax,STKmin,STKunit1,STKunit2,STKconv1 ,STKconv2,STKsnsv,STKuname0,STKuname1,STKuname2 "
                    sSql += " ,STKuname3,STKuname4,STKuname5,STKqU1,STKqU2,STKqU3,STKqU4,STKqU5,STKqE1,STKqE2,STKqE3 "
                    sSql += " ,STKqE4,STKqE5,STKqN1,STKqN2,STKqN3,STKqN4,STKqN5,STKvat,STKexpire,STKhide,STKuse2,STKu2name"
                    sSql += " ,STKfx ,STKacP,STKacS ,STKacC,STKlock,STKmemo ,STKacC1,STKacC2 ,STKeditLK,STKrefWE"
                    sSql += " ,STKbarC1,STKbarC2,STKbarC3,STKsortNT,STKsortNE,STKeditDT,STKstatus,STKsto,STKeditMF"
                    sSql += " )"

                    sSql += " SELECT '" & code & "','" & group & "','" & part_no & "','" & item_desc & "','" & brand & "','" & unit_price & "',STKdescE1,STKdescE2,STKdescE3"
                    sSql += " ,STKmax,STKmin,STKunit1,STKunit2,STKconv1 ,STKconv2,STKsnsv,STKuname0,'" & unit & "',STKuname2 "
                    sSql += " ,STKuname3,STKuname4,STKuname5,STKqU1,STKqU2,STKqU3,STKqU4,STKqU5,STKqE1,STKqE2,STKqE3 "
                    sSql += " ,STKqE4,STKqE5,STKqN1,STKqN2,STKqN3,STKqN4,STKqN5,STKvat,STKexpire,STKhide,STKuse2,STKu2name"
                    sSql += " ,STKfx ,'51020103','41030300' ,STKacC,STKlock,STKmemo ,STKacC1,STKacC2 ,STKeditLK,STKrefWE"
                    sSql += " ,STKbarC1,STKbarC2,STKbarC3,'" & item_desc & "',STKsortNE, getdate(),STKstatus,'KHM','MIS'"
                    sSql += " FROM [dbo].[STK] "
                    sSql += "  where  stkcode like '61120-10000122'"


                    If sqlcon.State = ConnectionState.Closed Then
                        sqlcon = New OleDbConnection(db_mac5.cnPAStr)
                        sqlcon.Open()
                    End If
                    With sqlcom
                        .Connection = sqlcon
                        .CommandType = CommandType.Text
                        .CommandText = sSql
                        .ExecuteNonQuery()
                    End With

                End If

                ' chkRow.Checked = False
                'End If
            End If
        Next
    End Sub

    Sub Insert_Enpro()
        Dim dt As New DataTable()
        dt.Columns.AddRange(New DataColumn(1) {New DataColumn("ItemCode"), New DataColumn("ItemDescription")})

        For Each row As GridViewRow In GridView.Rows
            If row.RowType = DataControlRowType.DataRow Then

                sql = " SELECT code "
                sql += " from    [SMITEMMS] "
                sql += " where code = '" & row.Cells(6).Text & "'"
                dt = db_Enpro.GetDataTable(sql)
                If dt.Rows.Count <= 0 Then   'check ซ้ำ

                    Dim Company As String = row.Cells(1).Text
                    Dim STKcode As String = row.Cells(6).Text
                    Dim STKgroup As String = row.Cells(7).Text
                    Dim STKdescT1 As String = row.Cells(8).Text
                    Dim STKdescT2 As String = row.Cells(9).Text

                    Dim Cn As New SqlConnection(db_Enpro.sqlCon)
                    Dim sqlcon As New OleDbConnection(db_Enpro.cnPAStr)
                    Dim sqlcom As New OleDbCommand

                    Dim sSql As String = " "

                    sSql += " insert into [SMITEMMS] ("
                    sSql += " code, [Code_Secondary], [SMSTORMS_ID], [Type], [F_MRP], [Name1_Th], [Name2_Th], [Name3_Th], [Name1_En], [Name2_En]"
                    sSql += " ,[Name3_En],[Descr_Th],[Descr_En],[LeadTime_Pd],[Lot_Size_Pd] ,[LeadTime_Dl_Buffer],[RateWaste_Pd],[Weight],[F_Expire],[Alert_Month]"
                    sSql += " ,[Alert_Day],[Expire_Month],[Expire_Day] ,[F_Bin] ,[F_Inspect],[F_Budget],[Inspect_SMSTORMS_ID],[Df_Ltsr],[Ltsr_Charecter],[Df_LtsrFormat]"
                    sSql += " ,[Ltsr_running],[F_Unit_MS],[Main_SMUNITMS_ID],[Second_SMUNITMS_ID],[Small_SMUNITMS_ID],[Pur_SMUNITMS_ID],[Sale_SMUNITMS_ID],[F_Parallel],[F_Parallel_Lot],[Par1_SMUNITMS_ID]"
                    sSql += " ,[Par2_SMUNITMS_ID],[SMGRP1MS_ID],[SMGRP2MS_ID],[SMGRP3MS_ID],[SMGRP4MS_ID],[SMGRP5MS_ID],[Ref1],[Ref2],[Ref3],[Ref4]"
                    sSql += " ,[Ref5],[Remark1],[Remark2],[Remark3],[Remark4] ,[Remark5],[F_Active],[F_KIT],[FileName1],[FileName2]"
                    sSql += " ,[FileName3],[FileName4],[FileName5],[FileName6],[F_Pararell_Avg],[F_Tool],[F_Control_Tool],[Tool_Expire_Day],[Tool_Expire_Month]     "
                    sSql += " ,[Tool_Alert_Day],[Tool_Alert_Month],[F_bom_cost],[min_qty] ,[max_qty],[F_Lot_FIFO],[F_Cost],[Picture1],[Picture2],[Picture3] "
                    sSql += " ,[Picture4],[Picture5],[Picture6],[Df_Serial],[Serial_Charecter],[Df_SerialFormat],[Serial_running],[F_IssuePriority],[Mfg_SMUNITMS_ID],[Seq_Group]"
                    sSql += " ,[Activity_Date],[Start_Sale_Date],[Last_Sale_Date],[F_Unit_Control],[Width],[Lenght],[Hight],[MRSTPTHD_id],[F_Auto_Convert_P1_Qty],[Status]     "
                    sSql += " ,[F_UseRateWaste],[Temp_SMSTORMS_ID],[SMACCOMS_ID],[LeadTime_Pd_Self],[F_RECMoreThanPO],[Qty_RECMoreThanPO],[Percent_RECMoreThanPO],[User_Code],[Update_Date_Time],[Daily_Capacity]   "
                    sSql += " ,[Ref6],[F_UseItemWeightAddUnitWeight],[F_RECMoreThanSTD_P],[Qty_RECMoreThanSTD_P],[Percent_RECMoreThanSTD_P],[F_RECMoreThanPR],[Qty_RECMoreThanPR],[Percent_RECMoreThanPR],[f_Integer_Qty],[F_RECMoreThanPO_P]"
                    sSql += " ,[Qty_RECMoreThanPO_P],[Percent_RECMoreThanPO_P],[Barcode_SMUNITMS_ID],[Allocate_SMUNITMS_ID],[F_QC_PO],[F_Issue_Full_Lot],[LastPurchaseDate],[f_CanOpenQtySOOverIV],[SMLOGOMS_ID],[f_lot_fifo_pd]    "
                    sSql += " ,[Create_SMUSERMS_ID],[Create_Date_Time],[F_RECMoreThanIP],[Qty_RECMoreThanIP],[Percent_RECMoreThanIP],[F_ExpireDate_Manual],[F_CalBackward_AleartDate],[F_GenDocOnlyMRPbyItem],[SMACCOTG_ID],[app_type]"
                    sSql += " ,[acc_match_code],[SMLTTPHD_ID],[SA_SMVATTMS_ID],[PA_SMVATTMS_ID],[JC_SMUNITMS_ID]"
                    sSql += " )"
                    sSql += " SELECT '" & STKcode & "',[Code_Secondary],[SMSTORMS_ID],[Type],[F_MRP],'" & STKdescT1 & "',[Name2_Th],[Name3_Th],[Name1_En],[Name2_En]"
                    sSql += " ,[Name3_En],[Descr_Th],[Descr_En],[LeadTime_Pd],[Lot_Size_Pd] ,[LeadTime_Dl_Buffer],[RateWaste_Pd],[Weight],[F_Expire],[Alert_Month]"
                    sSql += " ,[Alert_Day],[Expire_Month],[Expire_Day] ,[F_Bin] ,[F_Inspect],[F_Budget],[Inspect_SMSTORMS_ID],[Df_Ltsr],[Ltsr_Charecter],[Df_LtsrFormat]"
                    sSql += " ,[Ltsr_running],[F_Unit_MS],[Main_SMUNITMS_ID],[Second_SMUNITMS_ID],[Small_SMUNITMS_ID],[Pur_SMUNITMS_ID],[Sale_SMUNITMS_ID],[F_Parallel],[F_Parallel_Lot],[Par1_SMUNITMS_ID]"
                    sSql += " ,[Par2_SMUNITMS_ID],[SMGRP1MS_ID],[SMGRP2MS_ID],[SMGRP3MS_ID],[SMGRP4MS_ID],[SMGRP5MS_ID],[Ref1],[Ref2],[Ref3],[Ref4]"
                    sSql += " ,[Ref5],[Remark1],[Remark2],[Remark3],[Remark4] ,[Remark5],[F_Active],[F_KIT],[FileName1],[FileName2]"
                    sSql += " ,[FileName3],[FileName4],[FileName5],[FileName6],[F_Pararell_Avg],[F_Tool],[F_Control_Tool],[Tool_Expire_Day],[Tool_Expire_Month]     "
                    sSql += " ,[Tool_Alert_Day],[Tool_Alert_Month],[F_bom_cost],[min_qty] ,[max_qty],[F_Lot_FIFO],[F_Cost],[Picture1],[Picture2],[Picture3]"
                    sSql += "  ,[Picture4],[Picture5],[Picture6],[Df_Serial],[Serial_Charecter],[Df_SerialFormat],[Serial_running],[F_IssuePriority],[Mfg_SMUNITMS_ID],[Seq_Group]"
                    sSql += " ,[Activity_Date],[Start_Sale_Date],[Last_Sale_Date],[F_Unit_Control],[Width],[Lenght],[Hight],[MRSTPTHD_id],[F_Auto_Convert_P1_Qty],[Status]     "
                    sSql += " ,[F_UseRateWaste],[Temp_SMSTORMS_ID],[SMACCOMS_ID],[LeadTime_Pd_Self],[F_RECMoreThanPO],[Qty_RECMoreThanPO],[Percent_RECMoreThanPO],[User_Code],[Update_Date_Time],[Daily_Capacity]   "
                    sSql += " ,[Ref6],[F_UseItemWeightAddUnitWeight],[F_RECMoreThanSTD_P],[Qty_RECMoreThanSTD_P],[Percent_RECMoreThanSTD_P],[F_RECMoreThanPR],[Qty_RECMoreThanPR],[Percent_RECMoreThanPR],[f_Integer_Qty],[F_RECMoreThanPO_P]"
                    sSql += " ,[Qty_RECMoreThanPO_P],[Percent_RECMoreThanPO_P],[Barcode_SMUNITMS_ID],[Allocate_SMUNITMS_ID],[F_QC_PO],[F_Issue_Full_Lot],[LastPurchaseDate],[f_CanOpenQtySOOverIV],[SMLOGOMS_ID],[f_lot_fifo_pd]    "
                    sSql += " ,[Create_SMUSERMS_ID],getdate(),[F_RECMoreThanIP],[Qty_RECMoreThanIP],[Percent_RECMoreThanIP],[F_ExpireDate_Manual],[F_CalBackward_AleartDate],[F_GenDocOnlyMRPbyItem],[SMACCOTG_ID],[app_type]"
                    sSql += " ,[acc_match_code],[SMLTTPHD_ID],[SA_SMVATTMS_ID],[PA_SMVATTMS_ID],[JC_SMUNITMS_ID]"
                    sSql += " FROM [dbo].[SMITEMMS] "
                    sSql += "  where  code like 'TEST-01'"


                    If sqlcon.State = ConnectionState.Closed Then
                        sqlcon = New OleDbConnection(db_Enpro.cnPAStr)
                        sqlcon.Open()
                    End If
                    With sqlcom
                        .Connection = sqlcon
                        .CommandType = CommandType.Text
                        .CommandText = sSql
                        .ExecuteNonQuery()
                    End With

                End If

                ' chkRow.Checked = False
                'End If
            End If
        Next
    End Sub



    Sub SentMail()
        '''''http://projectsvbnet.blogspot.com/2013/09/email-vbnet.html
        'LeaveOnline@aromathailandapp.com
        'leaveonline2016
        'mail.yourdomain.com
        'smtp port 25

        Dim i As Integer = 1
        Dim Role As String = ""
        Dim sql As String
        Dim name As String
        Dim strBody As String

        sql = "SELECT fullname  "
        sql = sql & "  FROM [dbo].[TB_Employee] "
        sql = sql & " where Employee_id = '" & usr & "'"
        dt = db_HR.GetDataTable(sql)
        If dt.Rows.Count > 0 Then
            name = dt.Rows(0)("Fullname")
        End If


        'If Len(txtMyemail.Text) = 0 Or Len(txtMypassword.Text) = 0 Then
        '    MsgBox("ค่าอีเมล์โฮสต์หรือรหัสผ่านโฮสต์ยังไม่ได้ป้อนข้อมูล", MsgBoxStyle.Critical + MsgBoxStyle.ApplicationModal, "ล้มเหลว")
        '    txtMyemail.Focus()
        'Else
        Try
            'If Len(txtTo.Text) = 0 Then
            '    MsgBox("คุณต้องป้อนอีเมล์ที่จะส่งก่อนทำการส่ง", MsgBoxStyle.Critical + MsgBoxStyle.ApplicationModal, "ล้มเหลว")
            'Else
            'โฮสต์ และ เลขพอร์ต ที่คุณจะส่งอีเมล
            'และคุณต้องมีบัญชีของ Gmail ถ้าจะใช้ smtp.gmail.com
            Dim smtp As New SmtpClient("mail.aromathailandapp.com", 25)
            'สร้างตัวแปร eMailmessage เก็บการส่งข้อความอีเมล
            Dim eMailmessage As New System.Net.Mail.MailMessage() 'New MailMessage
            'กำหนดความปลอดภัยในการเข้ารหัสการส่งข้อความให้เป็น True
            smtp.EnableSsl = False
            'ติดตั้งโฮสต์ของ Gmail
            'smtp.Credentials = New System.Net.NetworkCredential("aroma.leave@gmail.com", "aroma1991")
            smtp.Credentials = New System.Net.NetworkCredential("Product@aromathailandapp.com", "product2018")
            'กำค่าส่วนประกอบของอีเมล์ เช่น ข้อความ, หัวข้อ, จากผู้ส่ง และ ผู้รับ
            ' eMailmessage.Subject = "แจ้งยอดการชำระเงิน"

            eMailmessage.Subject = "แจ้งตั้งรหัสสินค้าใหม่ กลุ่มอะไหล่ " '& txtItem.Text

            strBody = "<html>" & vbCrLf
            strBody = strBody & "<body>" & vbCrLf
            strBody = strBody & "<FONT face=Tahoma>" & vbCrLf

            strBody = strBody & "<table>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td> เรียน  หน่วยงานบัญชี" & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>" & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td height=20  valign=middle style=font-family:Georgia, 'Times New Roman', Times, serif; font-size:30px; color:#0033cc;>" & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>" & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf


            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>แจ้งตั้งรหัสสินค้าใหม่ กลุ่มอะไหล่" & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>" & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf



            strBody = strBody & "</table>" & vbCrLf

            strBody = strBody & "<table>" & vbCrLf
            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td> รายละเอียดสินค้าตาม Link ด้านล่าง  " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>https://armapplication.com/Product/Login.aspx?id=" & lblRequestID.Text & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td height=20  valign=middle style=font-family:Georgia, 'Times New Roman', Times, serif; font-size:30px; color:#0033cc;>" & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td> จึงเรียนมาเพื่อทราบ" & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>" & name & " " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf
            strBody = strBody & "</table>" & vbCrLf

            strBody = strBody & "</FONT >" & vbCrLf
            strBody = strBody & "</body>" & vbCrLf
            strBody = strBody & "<html>"

            eMailmessage.Body = strBody
            eMailmessage.IsBodyHtml = True

            eMailmessage.From = New MailAddress("Product@aromathailandapp.com", "Product Master")

            eMailmessage.To.Add("rathawit@aromathailand.com,onumau@aromathailand.com,panittas@aromathailand.com,Branchsupport@aromathailand.com") ',kongyotbiy@aromathailand.com,
            'eMailmessage.To.Add("Haruthaic@aromathailand.com,panittas@aromathailand.com")
            eMailmessage.CC.Add("callcenter@aromathailand.com,watchanuntan@aromathailand.com,chonaweej@aromathailand.com,laddawansee@aromathailand.com,sirikarnnge@aromathailand.com,supansas@aromathailand.com,somjaip@aromathailand.com,praserts@aromathailand.com,sudaratche@aromathailand.com") '
            'eMailmessage.To.Add(email)


            smtp.Send(eMailmessage)
            'MsgBox("ส่งอีเมล์สำเร็จ", vbInformation, "รายงานผล")
            '   End If
        Catch ex As Exception
            ' MsgBox("ข้อมูลผิดพลาด กรุณาตรวจสอบข้อมูลอีกครั้ง", MsgBoxStyle.Critical + MsgBoxStyle.ApplicationModal, "การทำงานผิดพลาด")
            sms.Msg = "ข้อมูลผิดพลาด กรุณาตรวจสอบข้อมูลอีกครั้ง"
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "window.alert('" & sms.Msg & "');", True)
        End Try
        ' End If
    End Sub


    Protected Sub GridView_RowDeleting(sender As Object, e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles GridView.RowDeleting

        Dim row As GridViewRow = GridView.Rows(e.RowIndex)

        Dim item As String = GridView.DataKeys(e.RowIndex).Values(0)

        Dim query As String = "DELETE FROM TB_ProductRequest_spare WHERE item=@item  "
        Dim constr As String = ConfigurationManager.ConnectionStrings("ProductConnectionString").ConnectionString
        Using con As SqlConnection = New SqlConnection(constr)
            Using cmd As SqlCommand = New SqlCommand(query)
                cmd.Parameters.AddWithValue("@item", item)
                'cmd.Parameters.AddWithValue("@PMS_ID", PMS_ID)
                cmd.Connection = con
                con.Open()
                cmd.ExecuteNonQuery()
                con.Close()
            End Using
        End Using

        LoadData(RequestId)
    End Sub



    Protected Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        Dim kvn, lion As String

        If chkKVN.Checked = False And chkLION.Checked = False Then
            sms.Msg = "กรุณาระบุบริษัท"
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "window.alert('" & sms.Msg & "');", True)
            Exit Sub
        End If

        If chkKVN.Checked = True Then
            kvn = "Yes"
        Else
            kvn = "No"
        End If

        If chkLION.Checked = True Then
            lion = "Yes"
        Else
            lion = "No"
        End If

        '****
        sql = " select * "
        sql += " from VW_ProductRequest "
        sql += " where item = '" & txtItem.Text & "'  "
        dt = DB_Product.GetDataTable(sql)
        If dt.Rows.Count > 0 Then
            sms.Msg = "Duplicate Item !"
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "window.alert('" & sms.Msg & "');", True)
            Exit Sub
        End If

        If txtItem.Text = "" Then
            sms.Msg = "กรุณาระบุรหัสสินค้า"
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "window.alert('" & sms.Msg & "');", True)
            Exit Sub
        End If

        If txtItemDescTH.Text = "" Or txtItemDescEN.Text = "" Then
            sms.Msg = "กรุณาระบุชื่อสินค้า"
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "window.alert('" & sms.Msg & "');", True)
            Exit Sub
        End If

        If drpProductType.SelectedValue = "0" Or drpProductCat.SelectedValue = "0" Or drpProductGroup.SelectedValue = "0" Then
            sms.Msg = "กรุณาระบุข้อมูลกลุ่มสินค้า ให้ครบถ้วน"
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "window.alert('" & sms.Msg & "');", True)
            Exit Sub
        End If


        sql = " select * "
        sql += " from [TB_ProductRequest_spare] "
        sql += " where Item = '" & txtItem.Text & "' "
        dt = DB_Product.GetDataTable(sql)
        If dt.Rows.Count = 0 Then
            InsertData()
        End If

        LoadData(RequestId)
        CleareData()

        'sms.Msg = "บันทึกข้อมูลเรียบร้อยแล้วค่ะ"
        'Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "window.alert('" & sms.Msg & "');", True)
        'Exit Sub
    End Sub


    Sub InsertData()

        Dim kvn, lion As String

        If chkKVN.Checked = True Then
            kvn = "Yes"
        Else
            kvn = "No"
        End If

        If chkLION.Checked = True Then
            lion = "Yes"
        Else
            lion = "No"
        End If


        Dim query As String = "INSERT INTO TB_ProductRequest_spare ("
        query &= "Company_KVN,Company_LION,ProductType,ProductCategory,ProductGroup ,Item"
        query &= ",PartNo,ItemGroup,ItemDescriptionTH,ItemDescriptionEN,Unit,UnitPrice"
        query &= ",Stat,CreateBy ,CreateDate , itemtype , brand ,RequestDate"
        query &= ")"

        query += " VALUES( "
        query &= "@Company_KVN,@Company_LION,@ProductType,@ProductCategory,@ProductGroup ,@Item"
        query &= ",@PartNo,@ItemGroup,@ItemDescriptionTH,@ItemDescriptionEN,@Unit,@UnitPrice"
        query &= ",@Stat,@CreateBy ,@CreateDate ,@itemtype , @brand ,@RequestDate"
        query &= ")"

        Dim constr As String = ConfigurationManager.ConnectionStrings("ProductConnectionString").ConnectionString

        Using con As SqlConnection = New SqlConnection(constr)
            Using cmd As SqlCommand = New SqlCommand(query)
                cmd.Parameters.AddWithValue("@Company_KVN", kvn)
                cmd.Parameters.AddWithValue("@Company_LION", lion)
                cmd.Parameters.AddWithValue("@ProductType", drpProductType.SelectedValue)
                cmd.Parameters.AddWithValue("@ProductCategory", drpProductCat.SelectedValue)
                cmd.Parameters.AddWithValue("@ProductGroup", drpProductGroup.SelectedValue)

                cmd.Parameters.AddWithValue("@Item", txtItem.Text)
                cmd.Parameters.AddWithValue("@PartNo", txtPart.Text)
                cmd.Parameters.AddWithValue("@ItemGroup", txtItemGroup.Text)
                cmd.Parameters.AddWithValue("@ItemDescriptionTH", txtItemDescTH.Text)
                cmd.Parameters.AddWithValue("@ItemDescriptionEN", txtItemDescEN.Text)
                cmd.Parameters.AddWithValue("@Unit", txtunit.Text)
                cmd.Parameters.AddWithValue("@UnitPrice", txtUnitPrice.Text)

                cmd.Parameters.AddWithValue("@Stat", "Draft")
                cmd.Parameters.AddWithValue("@CreateBy", usr)
                cmd.Parameters.AddWithValue("@CreateDate", DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss"))

                cmd.Parameters.AddWithValue("@itemtype", txtItemType.Text)
                cmd.Parameters.AddWithValue("@brand", drpBrand.SelectedValue)

                cmd.Parameters.AddWithValue("@RequestDate", DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss"))

                cmd.Connection = con
                con.Open()
                cmd.ExecuteNonQuery()
                con.Close()
            End Using
        End Using


    End Sub

    Sub CleareData()
        drpProductType.SelectedValue = 0
        drpProductCat.SelectedValue = 0
        drpProductGroup.SelectedValue = 0
        drpBrand.SelectedValue = 0
        txtItem.Text = ""
        txtPart.Text = ""
        txtItemGroup.Text = ""
        txtItemDescTH.Text = ""
        txtItemDescEN.Text = ""
        txtunit.Text = ""
        txtUnitPrice.Text = ""
        txtItemType.Text = ""
    End Sub

    Protected Sub drpProductType_Init(sender As Object, e As EventArgs) Handles drpProductType.Init
        Dim sqlCon As New SqlConnection(DB_Product.sqlCon)
        Dim sqlCmd As New SqlCommand
        Dim Rs As SqlDataReader
        Dim sql As String


        sql = "SELECT [ProductType] FROM [dbo].[TB_ProductMaster] group by [ProductType]  "

        If sqlCon.State = ConnectionState.Closed Then
            sqlCon = New SqlConnection(DB_Product.sqlCon)
            sqlCon.Open()
        End If
        With sqlCmd
            .Connection = sqlCon
            .CommandType = CommandType.Text
            .CommandText = sql
            Rs = .ExecuteReader
        End With
        drpProductType.Items.Clear()
        drpProductType.Items.Add(New ListItem("", 0))
        While Rs.Read
            drpProductType.Items.Add(New ListItem(Rs.GetString(0), Rs.GetString(0)))
        End While
    End Sub

    Protected Sub drpProductCat_Init(sender As Object, e As EventArgs) Handles drpProductCat.Init
        Dim sqlCon As New SqlConnection(DB_Product.sqlCon)
        Dim sqlCmd As New SqlCommand
        Dim Rs As SqlDataReader
        Dim sql As String


        sql = "SELECT [ProductCategory] FROM [dbo].[TB_ProductMaster] where ([ProductCategory] Is Not null)   group by [ProductCategory]  "


        If sqlCon.State = ConnectionState.Closed Then
            sqlCon = New SqlConnection(DB_Product.sqlCon)
            sqlCon.Open()
        End If
        With sqlCmd
            .Connection = sqlCon
            .CommandType = CommandType.Text
            .CommandText = sql
            Rs = .ExecuteReader
        End With
        drpProductCat.Items.Clear()
        drpProductCat.Items.Add(New ListItem("", 0))
        While Rs.Read
            drpProductCat.Items.Add(New ListItem(Rs.GetString(0), Rs.GetString(0)))
        End While
    End Sub

    Protected Sub drpProductGroup_Init(sender As Object, e As EventArgs) Handles drpProductGroup.Init
        Dim sqlCon As New SqlConnection(DB_Product.sqlCon)
        Dim sqlCmd As New SqlCommand
        Dim Rs As SqlDataReader
        Dim sql As String


        sql = "SELECT [ProductGroup] FROM [dbo].[TB_ProductMaster] where  ProductGroup is not null  group by [ProductGroup]  "


        If sqlCon.State = ConnectionState.Closed Then
            sqlCon = New SqlConnection(DB_Product.sqlCon)
            sqlCon.Open()
        End If
        With sqlCmd
            .Connection = sqlCon
            .CommandType = CommandType.Text
            .CommandText = sql
            Rs = .ExecuteReader
        End With
        drpProductGroup.Items.Clear()
        drpProductGroup.Items.Add(New ListItem("", 0))
        While Rs.Read
            drpProductGroup.Items.Add(New ListItem(Rs.GetString(0), Rs.GetString(0)))
        End While
    End Sub

    Protected Sub drpProductType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles drpProductType.SelectedIndexChanged
        Dim sqlCon As New SqlConnection(DB_Product.sqlCon)
        Dim sqlCmd As New SqlCommand
        Dim Rs As SqlDataReader
        Dim sql As String


        sql = "SELECT [ProductCategory] FROM [dbo].[TB_ProductMaster] "

        sql = sql & "where ([ProductCategory] Is Not null)  and ProductType  = '" & drpProductType.SelectedValue & "' "

        sql = sql & "group by [ProductCategory]  "


        If sqlCon.State = ConnectionState.Closed Then
            sqlCon = New SqlConnection(DB_Product.sqlCon)
            sqlCon.Open()
        End If
        With sqlCmd
            .Connection = sqlCon
            .CommandType = CommandType.Text
            .CommandText = sql
            Rs = .ExecuteReader
        End With
        drpProductCat.Items.Clear()
        drpProductCat.Items.Add(New ListItem("", 0))
        While Rs.Read
            drpProductCat.Items.Add(New ListItem(Rs.GetString(0), Rs.GetString(0)))
        End While
    End Sub

    Protected Sub drpProductCat_SelectedIndexChanged(sender As Object, e As EventArgs) Handles drpProductCat.SelectedIndexChanged
        Dim sqlCon As New SqlConnection(DB_Product.sqlCon)
        Dim sqlCmd As New SqlCommand
        Dim Rs As SqlDataReader
        Dim sql As String


        sql = " SELECT [ProductGroup] FROM [dbo].[TB_ProductMaster] "
        sql = sql & " where  ProductCategory  = '" & drpProductCat.SelectedValue & "' "
        sql = sql & " and  ProductType  = '" & drpProductType.SelectedValue & "' "


        If sqlCon.State = ConnectionState.Closed Then
            sqlCon = New SqlConnection(DB_Product.sqlCon)
            sqlCon.Open()
        End If
        With sqlCmd
            .Connection = sqlCon
            .CommandType = CommandType.Text
            .CommandText = sql
            Rs = .ExecuteReader
        End With
        drpProductGroup.Items.Clear()
        drpProductGroup.Items.Add(New ListItem("", 0))
        While Rs.Read
            drpProductGroup.Items.Add(New ListItem(Rs.GetString(0), Rs.GetString(0)))
        End While
    End Sub

    Protected Sub drpBrand_Init(sender As Object, e As EventArgs) Handles drpBrand.Init
        Dim sqlCon As New SqlConnection(DB_Product.sqlCon)
        Dim sqlCmd As New SqlCommand
        Dim Rs As SqlDataReader
        Dim sql As String


        sql = "SELECT [Brand] FROM [dbo].[TB_ProductBrand]  "


        If sqlCon.State = ConnectionState.Closed Then
            sqlCon = New SqlConnection(DB_Product.sqlCon)
            sqlCon.Open()
        End If
        With sqlCmd
            .Connection = sqlCon
            .CommandType = CommandType.Text
            .CommandText = sql
            Rs = .ExecuteReader
        End With
        drpBrand.Items.Clear()
        drpBrand.Items.Add(New ListItem("", 0))
        While Rs.Read
            drpBrand.Items.Add(New ListItem(Rs.GetString(0), Rs.GetString(0)))
        End While
    End Sub

    Protected Sub drpRequester_Init(sender As Object, e As EventArgs) Handles drpRequester.Init
        Dim sqlCon As New SqlConnection(db_HR.sqlCon)
        Dim sqlCmd As New SqlCommand
        Dim Rs As SqlDataReader
        Dim sql As String

        sql = "select [employee_id] as id,[fullname] from [TB_Employee] order by employee_id asc"

        If sqlCon.State = ConnectionState.Closed Then
            sqlCon = New SqlConnection(db_HR.sqlCon)
            sqlCon.Open()
        End If
        With sqlCmd
            .Connection = sqlCon
            .CommandType = CommandType.Text
            .CommandText = sql
            Rs = .ExecuteReader
        End With
        drpRequester.Items.Clear()
        drpRequester.Items.Add(New ListItem("", 0))
        While Rs.Read
            drpRequester.Items.Add(New ListItem(Rs.GetString(0) + " : " + Rs.GetString(1), Rs.GetString(0)))
        End While
    End Sub

    Protected Sub btnApprove_Click(sender As Object, e As EventArgs) Handles btnApprove.Click

        'sql = " select * "
        'sql += " from [VW_ProductRequest_Machine] "
        'sql += " where RequestId = '" & lblRequestID.Text & "' "
        'dt = DB_Product.GetDataTable(sql)
        'If dt.Rows.Count >= 1 Then
        '    UpdateData()
        '    'Else
        '    '    GenID()
        '    '    InsertData(RequestId)
        'End If

        If chkKVN.Checked = True Then
            InsertBI_kvn()
            Insert_Enpro()
        End If

        If chkLION.Checked = True Then
            InsertBI_lion()
            Insert_Mac5()
        End If

        UpdateStatus_complete()
        'SentMail_product()


        Server.Transfer("NewMasterAll.aspx?usr=" & usr)

    End Sub

    Sub UpdateStatus_complete()

        Dim x As Integer
        Dim db_Mac5 As New Connect_Mac5
        Dim Cn As New SqlConnection(DB_Product.sqlCon)

        Dim sSql As String = "UPDATE [TB_ProductRequest_spare] SET  "
        sSql += " Stat='Complete' ,ModifyBy=@ModifyBy ,ModifyDate=@ModifyDate "
        sSql += " ,AcctBy=@AcctBy , AcctDate=@AcctDate"

        sSql += " Where RequestId ='" & lblRequestID.Text & "' "

        Dim command As SqlCommand = New SqlCommand(sSql, Cn)
        Try
            command.Parameters.Add("@AcctBy", Data.SqlDbType.VarChar).Value = usr
            command.Parameters.Add("@AcctDate", Data.SqlDbType.VarChar).Value = DateTime.Now.ToString("yyyy-MM-dd")
            command.Parameters.Add("@ModifyBy", Data.SqlDbType.VarChar).Value = usr
            command.Parameters.Add("@ModifyDate", Data.SqlDbType.VarChar).Value = DateTime.Now.ToString("yyyy-MM-dd")

            command.CommandType = Data.CommandType.Text
            Cn.Open()
            x = command.ExecuteScalar
        Catch x1 As Exception
            Err.Raise(60002, , x1.Message)
        Finally
            Cn.Close()
        End Try

    End Sub

    Sub UpdateStatus_complete2()

        Dim x As Integer
        Dim db_Mac5 As New Connect_Mac5
        Dim Cn As New SqlConnection(DB_Product.sqlCon)

        Dim sSql As String = "UPDATE [TB_ProductRequest_spare] SET  "
        sSql += " Stat='Complete' ,ModifyBy=@ModifyBy ,ModifyDate=@ModifyDate "
        sSql += " ,AcctBy=@AcctBy , AcctDate=@AcctDate"

        sSql += " Where id ='" & lblid.Text & "' "

        Dim command As SqlCommand = New SqlCommand(sSql, Cn)
        Try
            command.Parameters.Add("@AcctBy", Data.SqlDbType.VarChar).Value = usr
            command.Parameters.Add("@AcctDate", Data.SqlDbType.VarChar).Value = DateTime.Now.ToString("yyyy-MM-dd")
            command.Parameters.Add("@ModifyBy", Data.SqlDbType.VarChar).Value = usr
            command.Parameters.Add("@ModifyDate", Data.SqlDbType.VarChar).Value = DateTime.Now.ToString("yyyy-MM-dd")

            command.CommandType = Data.CommandType.Text
            Cn.Open()
            x = command.ExecuteScalar
        Catch x1 As Exception
            Err.Raise(60002, , x1.Message)
        Finally
            Cn.Close()
        End Try

    End Sub

    Protected Sub Edit(ByVal sender As Object, ByVal e As EventArgs)
        Dim row As GridViewRow = CType(CType(sender, LinkButton).Parent.Parent, GridViewRow)

        ' txtCallNumber.ReadOnly = True

        lblid.Text = row.Cells(0).Text
        drpProductType2.Text = row.Cells(3).Text
        drpProductCat2.Text = row.Cells(4).Text
        drpProductGroup2.Text = row.Cells(5).Text
        drpBrand2.Text = row.Cells(6).Text
        txtItem2.Text = row.Cells(7).Text
        txtItemGroup2.Text = row.Cells(8).Text
        txtItemDescTH2.Text = row.Cells(9).Text
        txtItemDescEN2.Text = row.Cells(10).Text
        txtItemType2.SelectedValue = row.Cells(11).Text
        txtunit2.Text = row.Cells(12).Text
        txtPart2.Text = row.Cells(13).Text
        txtUnitPrice2.Text = row.Cells(14).Text

        'If row.Cells(7).Text = "Open" Or row.Cells(7).Text = "Assigned" Then
        '    btnAssign.Visible = True

        'ElseIf row.Cells(7).Text = "Inprogress" Then
        '    btnAssign.Visible = False

        'End If

        'sql = " select [ProductType],[ProductCategory],[ProductGroup],[brand]"
        'sql += " from [TB_ProductRequest_spare] "
        'sql += " where [item] Like '" & row.Cells(6).Text & "' "
        'dt = DB_Product.GetDataTable(sql)
        'If dt.Rows.Count > 0 Then    
        '    drpProductType2.SelectedValue = dt.Rows(0)("ProductType")
        '    drpProductCat2.SelectedValue = dt.Rows(0)("ProductCategory")
        '    drpProductGroup2.SelectedValue = dt.Rows(0)("ProductGroup")
        'End If

        popup.Show()

    End Sub

    Protected Sub Save(ByVal sender As Object, ByVal e As EventArgs) Handles btnSave2.Click

        Dim x As Integer
        Dim db_Mac5 As New Connect_Mac5
        Dim Cn As New SqlConnection(DB_Product.sqlCon)

        Dim sSql As String = "UPDATE [TB_ProductRequest_spare] SET  "
        sSql += " Item=@Item, ItemGroup=@ItemGroup,PartNo=@PartNo,ItemDescriptionTH=@ItemDescriptionTH,ItemDescriptionEN=@ItemDescriptionEN "
        sSql += " ,Unit=@Unit, UnitPrice=@UnitPrice, ItemType=@ItemType, ProductType=@ProductType, ProductCategory=@ProductCategory, ProductGroup=@ProductGroup "
        sSql += " ,ModifyBy=@ModifyBy ,ModifyDate=@ModifyDate "

        sSql += " Where id = '" & lblid.Text & "' "

        Dim command As SqlCommand = New SqlCommand(sSql, Cn)
        Try
            command.Parameters.Add("@Item", Data.SqlDbType.VarChar).Value = txtItem2.Text
            command.Parameters.Add("@ItemGroup", Data.SqlDbType.VarChar).Value = txtItemGroup2.Text
            command.Parameters.Add("@PartNo", Data.SqlDbType.VarChar).Value = txtPart2.Text
            command.Parameters.Add("@ItemDescriptionTH", Data.SqlDbType.VarChar).Value = txtItemDescTH2.Text
            command.Parameters.Add("@ItemDescriptionEN", Data.SqlDbType.VarChar).Value = txtItemDescEN2.Text
            command.Parameters.Add("@Unit", Data.SqlDbType.VarChar).Value = txtunit2.Text
            command.Parameters.Add("@UnitPrice", Data.SqlDbType.VarChar).Value = txtUnitPrice2.Text
            command.Parameters.Add("@ItemType", Data.SqlDbType.VarChar).Value = txtItemType2.SelectedValue
            command.Parameters.Add("@ProductType", Data.SqlDbType.VarChar).Value = drpProductType2.Text
            command.Parameters.Add("@ProductCategory", Data.SqlDbType.VarChar).Value = drpProductCat2.Text
            command.Parameters.Add("@ProductGroup", Data.SqlDbType.VarChar).Value = drpProductGroup2.Text

            command.Parameters.Add("@ModifyBy", Data.SqlDbType.VarChar).Value = usr
            command.Parameters.Add("@ModifyDate", Data.SqlDbType.VarChar).Value = DateTime.Now.ToString("yyyy-MM-dd")

            command.CommandType = Data.CommandType.Text
            Cn.Open()
            x = command.ExecuteScalar
        Catch x1 As Exception
            Err.Raise(60002, , x1.Message)
        Finally
            Cn.Close()
        End Try

        LoadData(RequestId)

    End Sub

    'Protected Sub Approve(ByVal sender As Object, ByVal e As EventArgs) Handles btnApprove2.Click

    '    If chkKVN2.Checked = True Then
    '        InsertBI_kvn2()
    '        Insert_Enpro2()
    '    End If

    '    If chkLION2.Checked = True Then
    '        InsertBI_lion2()
    '        Insert_Mac52()
    '    End If

    '    UpdateStatus_complete()

    'End Sub


    'Protected Sub drpProductType2_Init(sender As Object, e As EventArgs) Handles drpProductType2.Init
    '    Dim sqlCon As New SqlConnection(DB_Product.sqlCon)
    '    Dim sqlCmd As New SqlCommand
    '    Dim Rs As SqlDataReader
    '    Dim sql As String

    '    sql = "SELECT [ProductType] FROM [dbo].[TB_ProductMaster] group by [ProductType]  "

    '    If sqlCon.State = ConnectionState.Closed Then
    '        sqlCon = New SqlConnection(DB_Product.sqlCon)
    '        sqlCon.Open()
    '    End If
    '    With sqlCmd
    '        .Connection = sqlCon
    '        .CommandType = CommandType.Text
    '        .CommandText = sql
    '        Rs = .ExecuteReader
    '    End With
    '    drpProductType2.Items.Clear()
    '    drpProductType2.Items.Add(New ListItem("", 0))
    '    While Rs.Read
    '        drpProductType2.Items.Add(New ListItem(Rs.GetString(0), Rs.GetString(0)))
    '    End While
    'End Sub

    'Protected Sub drpProductGroup2_Init(sender As Object, e As EventArgs) Handles drpProductGroup2.Init
    '    Dim sqlCon As New SqlConnection(DB_Product.sqlCon)
    '    Dim sqlCmd As New SqlCommand
    '    Dim Rs As SqlDataReader
    '    Dim sql As String


    '    sql = "SELECT [ProductGroup] FROM [dbo].[TB_ProductMaster] where  ProductGroup is not null  group by [ProductGroup]  "


    '    If sqlCon.State = ConnectionState.Closed Then
    '        sqlCon = New SqlConnection(DB_Product.sqlCon)
    '        sqlCon.Open()
    '    End If
    '    With sqlCmd
    '        .Connection = sqlCon
    '        .CommandType = CommandType.Text
    '        .CommandText = sql
    '        Rs = .ExecuteReader
    '    End With
    '    drpProductGroup2.Items.Clear()
    '    drpProductGroup2.Items.Add(New ListItem("", 0))
    '    While Rs.Read
    '        drpProductGroup2.Items.Add(New ListItem(Rs.GetString(0), Rs.GetString(0)))
    '    End While
    'End Sub

    'Protected Sub drpProductCat2_Init(sender As Object, e As EventArgs) Handles drpProductCat2.Init
    '    Dim sqlCon As New SqlConnection(DB_Product.sqlCon)
    '    Dim sqlCmd As New SqlCommand
    '    Dim Rs As SqlDataReader
    '    Dim sql As String


    '    sql = "SELECT [ProductCategory] FROM [dbo].[TB_ProductMaster] where ([ProductCategory] Is Not null)   group by [ProductCategory]  "


    '    If sqlCon.State = ConnectionState.Closed Then
    '        sqlCon = New SqlConnection(DB_Product.sqlCon)
    '        sqlCon.Open()
    '    End If
    '    With sqlCmd
    '        .Connection = sqlCon
    '        .CommandType = CommandType.Text
    '        .CommandText = sql
    '        Rs = .ExecuteReader
    '    End With
    '    drpProductCat2.Items.Clear()
    '    drpProductCat2.Items.Add(New ListItem("", 0))
    '    While Rs.Read
    '        drpProductCat2.Items.Add(New ListItem(Rs.GetString(0), Rs.GetString(0)))
    '    End While
    'End Sub

    'Protected Sub drpBrand2_Init(sender As Object, e As EventArgs) Handles drpBrand2.Init
    '    Dim sqlCon As New SqlConnection(DB_Product.sqlCon)
    '    Dim sqlCmd As New SqlCommand
    '    Dim Rs As SqlDataReader
    '    Dim sql As String


    '    sql = "SELECT [Brand] FROM [dbo].[TB_ProductBrand]  "


    '    If sqlCon.State = ConnectionState.Closed Then
    '        sqlCon = New SqlConnection(DB_Product.sqlCon)
    '        sqlCon.Open()
    '    End If
    '    With sqlCmd
    '        .Connection = sqlCon
    '        .CommandType = CommandType.Text
    '        .CommandText = sql
    '        Rs = .ExecuteReader
    '    End With
    '    drpBrand2.Items.Clear()
    '    drpBrand2.Items.Add(New ListItem("", 0))
    '    While Rs.Read
    '        drpBrand2.Items.Add(New ListItem(Rs.GetString(0), Rs.GetString(0)))
    '    End While
    'End Sub

    Sub InsertBI_kvn2()

        sql = " SELECT Item "
        sql += " from    TB_ProductMasterGroup"
        sql += " where Item = '" & txtItem2.Text & "'"
        dt = db_mac5.GetDataTable(sql)
        If dt.Rows.Count <= 0 Then   'check ซ้ำ

            Dim Cn As New SqlConnection(db_mac5.sqlCon)
            Dim sqlcon As New OleDbConnection(db_mac5.cnPAStr)
            Dim sqlcom As New OleDbCommand
            Dim sSql As String = "INSERT INTO [TB_ProductMasterGroup] "
            sSql += " (Company,ProductType,ProductCategory,ProductGroup,Brand,item)"
            sSql += " Values(  "
            sSql += "'KVN'"
            sSql += ",'" & drpProductType2.Text & "'"
            sSql += ",'" & drpProductCat2.Text & "'"
            sSql += ",'" & drpProductGroup2.Text & "'"
            sSql += ",'" & drpBrand2.Text & "','" & item & "' )"
            If sqlcon.State = ConnectionState.Closed Then
                sqlcon = New OleDbConnection(db_mac5.cnPAStr)
                sqlcon.Open()
            End If
            With sqlcom
                .Connection = sqlcon
                .CommandType = CommandType.Text
                .CommandText = sSql
                .ExecuteNonQuery()
            End With

        End If
    End Sub

    Sub InsertBI_lion2()

        sql = " SELECT Item "
        sql += " from    TB_ProductMasterGroup "
        sql += " where Item = '" & txtItem2.Text & "'"
        dt = db_mac5.GetDataTable(sql)
        If dt.Rows.Count <= 0 Then   'check ซ้ำ


            Dim Cn As New SqlConnection(db_mac5.sqlCon)
            Dim sqlcon As New OleDbConnection(db_mac5.cnPAStr)
            Dim sqlcom As New OleDbCommand
            Dim sSql As String = "INSERT INTO [TB_ProductMasterGroup] "
            sSql += " (Company,ProductType,ProductCategory,ProductGroup,Brand,item)"
            sSql += " Values(  "
            sSql += "'Lion'"
            sSql += ",'" & drpProductType2.Text & "'"
            sSql += ",'" & drpProductCat2.Text & "'"
            sSql += ",'" & drpProductGroup2.Text & "'"
            sSql += ",'" & drpBrand2.Text & "','" & item & "' )"
            If sqlcon.State = ConnectionState.Closed Then
                sqlcon = New OleDbConnection(db_mac5.cnPAStr)
                sqlcon.Open()
            End If
            With sqlcom
                .Connection = sqlcon
                .CommandType = CommandType.Text
                .CommandText = sSql
                .ExecuteNonQuery()
            End With

        End If

    End Sub



    Sub Insert_Mac52()
  
        sql = " SELECT STKcode "
        sql += " from    [STK] "
        sql += " where STKcode = '" & txtItem2.Text & "'"
        dt = db_mac5.GetDataTable(sql)
        If dt.Rows.Count <= 0 Then   'check ซ้ำ


            Dim STKcode As String = txtItem2.Text
            Dim STKcode2 As String = txtPart2.Text
            Dim STKgroup As String = txtItemGroup2.Text
            Dim STKdescT1 As String = txtItemDescEN2.Text & " (" & txtItemDescTH2.Text & ")"
            Dim STKdescT2 As String = drpBrand2.Text


            Dim Cn As New SqlConnection(db_mac5.sqlCon)
            Dim sqlcon As New OleDbConnection(db_mac5.cnPAStr)
            Dim sqlcom As New OleDbCommand

            Dim sSql As String = "INSERT INTO [STK] "

            sSql += " ("
            sSql += " STKcode,STKgroup,STKcode2,STKdescT1,STKdescT2,STKdescT3,STKdescE1,STKdescE2,STKdescE3"
            sSql += " ,STKmax,STKmin,STKunit1,STKunit2,STKconv1 ,STKconv2,STKsnsv,STKuname0,STKuname1,STKuname2 "
            sSql += " ,STKuname3,STKuname4,STKuname5,STKqU1,STKqU2,STKqU3,STKqU4,STKqU5,STKqE1,STKqE2,STKqE3 "
            sSql += " ,STKqE4,STKqE5,STKqN1,STKqN2,STKqN3,STKqN4,STKqN5,STKvat,STKexpire,STKhide,STKuse2,STKu2name"
            sSql += " ,STKfx ,STKacP,STKacS ,STKacC,STKlock,STKmemo ,STKacC1,STKacC2 ,STKeditLK,STKrefWE"
            sSql += " ,STKbarC1,STKbarC2,STKbarC3,STKsortNT,STKsortNE,STKeditDT,STKstatus,STKsto,STKeditMF"
            sSql += " )"


            sSql += " SELECT '" & STKcode & "','" & STKgroup & "','" & STKcode2 & "','" & STKdescT1 & "','" & STKdescT2 & "',STKdescT3,STKdescE1,STKdescE2,STKdescE3"
            sSql += " ,STKmax,STKmin,STKunit1,STKunit2,STKconv1 ,STKconv2,STKsnsv,STKuname0,STKuname1,STKuname2 "
            sSql += " ,STKuname3,STKuname4,STKuname5,STKqU1,STKqU2,STKqU3,STKqU4,STKqU5,STKqE1,STKqE2,STKqE3 "
            sSql += " ,STKqE4,STKqE5,STKqN1,STKqN2,STKqN3,STKqN4,STKqN5,7,STKexpire,STKhide,STKuse2,STKu2name"
            sSql += " ,STKfx ,STKacP,STKacS ,STKacC,STKlock,STKmemo ,STKacC1,STKacC2 ,STKeditLK,STKrefWE"
            sSql += " ,STKbarC1,STKbarC2,STKbarC3,STKsortNT,STKsortNE,STKeditDT,STKstatus,STKsto,STKeditMF"
            sSql += " FROM [dbo].[STK] "
            sSql += "  where  stkcode like '61120-10000122'"


            If sqlcon.State = ConnectionState.Closed Then
                sqlcon = New OleDbConnection(db_mac5.cnPAStr)
                sqlcon.Open()
            End If
            With sqlcom
                .Connection = sqlcon
                .CommandType = CommandType.Text
                .CommandText = sSql
                .ExecuteNonQuery()
            End With

        End If

    End Sub

    Sub Insert_Enpro2()

        sql = " SELECT code "
        sql += " from    [SMITEMMS] "
        sql += " where code = '" & txtItem2.Text & "'"
        dt = db_Enpro.GetDataTable(sql)
        If dt.Rows.Count <= 0 Then   'check ซ้ำ

            Dim STKcode As String = txtItem2.Text
            Dim STKgroup As String = txtItemGroup2.Text
            Dim STKdescT1 As String = txtItemDescTH2.Text
            Dim STKdescT2 As String = txtItemDescEN2.Text

            Dim Cn As New SqlConnection(db_Enpro.sqlCon)
            Dim sqlcon As New OleDbConnection(db_Enpro.cnPAStr)
            Dim sqlcom As New OleDbCommand

            Dim sSql As String = " "

            sSql += " insert into [SMITEMMS] ("
            sSql += " code, [Code_Secondary], [SMSTORMS_ID], [Type], [F_MRP], [Name1_Th], [Name2_Th], [Name3_Th], [Name1_En], [Name2_En]"
            sSql += " ,[Name3_En],[Descr_Th],[Descr_En],[LeadTime_Pd],[Lot_Size_Pd] ,[LeadTime_Dl_Buffer],[RateWaste_Pd],[Weight],[F_Expire],[Alert_Month]"
            sSql += " ,[Alert_Day],[Expire_Month],[Expire_Day] ,[F_Bin] ,[F_Inspect],[F_Budget],[Inspect_SMSTORMS_ID],[Df_Ltsr],[Ltsr_Charecter],[Df_LtsrFormat]"
            sSql += " ,[Ltsr_running],[F_Unit_MS],[Main_SMUNITMS_ID],[Second_SMUNITMS_ID],[Small_SMUNITMS_ID],[Pur_SMUNITMS_ID],[Sale_SMUNITMS_ID],[F_Parallel],[F_Parallel_Lot],[Par1_SMUNITMS_ID]"
            sSql += " ,[Par2_SMUNITMS_ID],[SMGRP1MS_ID],[SMGRP2MS_ID],[SMGRP3MS_ID],[SMGRP4MS_ID],[SMGRP5MS_ID],[Ref1],[Ref2],[Ref3],[Ref4]"
            sSql += " ,[Ref5],[Remark1],[Remark2],[Remark3],[Remark4] ,[Remark5],[F_Active],[F_KIT],[FileName1],[FileName2]"
            sSql += " ,[FileName3],[FileName4],[FileName5],[FileName6],[F_Pararell_Avg],[F_Tool],[F_Control_Tool],[Tool_Expire_Day],[Tool_Expire_Month]     "
            sSql += " ,[Tool_Alert_Day],[Tool_Alert_Month],[F_bom_cost],[min_qty] ,[max_qty],[F_Lot_FIFO],[F_Cost],[Picture1],[Picture2],[Picture3] "
            sSql += " ,[Picture4],[Picture5],[Picture6],[Df_Serial],[Serial_Charecter],[Df_SerialFormat],[Serial_running],[F_IssuePriority],[Mfg_SMUNITMS_ID],[Seq_Group]"
            sSql += " ,[Activity_Date],[Start_Sale_Date],[Last_Sale_Date],[F_Unit_Control],[Width],[Lenght],[Hight],[MRSTPTHD_id],[F_Auto_Convert_P1_Qty],[Status]     "
            sSql += " ,[F_UseRateWaste],[Temp_SMSTORMS_ID],[SMACCOMS_ID],[LeadTime_Pd_Self],[F_RECMoreThanPO],[Qty_RECMoreThanPO],[Percent_RECMoreThanPO],[User_Code],[Update_Date_Time],[Daily_Capacity]   "
            sSql += " ,[Ref6],[F_UseItemWeightAddUnitWeight],[F_RECMoreThanSTD_P],[Qty_RECMoreThanSTD_P],[Percent_RECMoreThanSTD_P],[F_RECMoreThanPR],[Qty_RECMoreThanPR],[Percent_RECMoreThanPR],[f_Integer_Qty],[F_RECMoreThanPO_P]"
            sSql += " ,[Qty_RECMoreThanPO_P],[Percent_RECMoreThanPO_P],[Barcode_SMUNITMS_ID],[Allocate_SMUNITMS_ID],[F_QC_PO],[F_Issue_Full_Lot],[LastPurchaseDate],[f_CanOpenQtySOOverIV],[SMLOGOMS_ID],[f_lot_fifo_pd]    "
            sSql += " ,[Create_SMUSERMS_ID],[Create_Date_Time],[F_RECMoreThanIP],[Qty_RECMoreThanIP],[Percent_RECMoreThanIP],[F_ExpireDate_Manual],[F_CalBackward_AleartDate],[F_GenDocOnlyMRPbyItem],[SMACCOTG_ID],[app_type]"
            sSql += " ,[acc_match_code],[SMLTTPHD_ID],[SA_SMVATTMS_ID],[PA_SMVATTMS_ID],[JC_SMUNITMS_ID]"
            sSql += " )"
            sSql += " SELECT '" & STKcode & "',[Code_Secondary],[SMSTORMS_ID],[Type],[F_MRP],'" & STKdescT1 & "','" & STKdescT2 & "',[Name3_Th],[Name1_En],[Name2_En]"
            sSql += " ,[Name3_En],[Descr_Th],[Descr_En],[LeadTime_Pd],[Lot_Size_Pd] ,[LeadTime_Dl_Buffer],[RateWaste_Pd],[Weight],[F_Expire],[Alert_Month]"
            sSql += " ,[Alert_Day],[Expire_Month],[Expire_Day] ,[F_Bin] ,[F_Inspect],[F_Budget],[Inspect_SMSTORMS_ID],[Df_Ltsr],[Ltsr_Charecter],[Df_LtsrFormat]"
            sSql += " ,[Ltsr_running],[F_Unit_MS],[Main_SMUNITMS_ID],[Second_SMUNITMS_ID],[Small_SMUNITMS_ID],[Pur_SMUNITMS_ID],[Sale_SMUNITMS_ID],[F_Parallel],[F_Parallel_Lot],[Par1_SMUNITMS_ID]"
            sSql += " ,[Par2_SMUNITMS_ID],[SMGRP1MS_ID],[SMGRP2MS_ID],[SMGRP3MS_ID],[SMGRP4MS_ID],[SMGRP5MS_ID],[Ref1],[Ref2],[Ref3],[Ref4]"
            sSql += " ,[Ref5],[Remark1],[Remark2],[Remark3],[Remark4] ,[Remark5],[F_Active],[F_KIT],[FileName1],[FileName2]"
            sSql += " ,[FileName3],[FileName4],[FileName5],[FileName6],[F_Pararell_Avg],[F_Tool],[F_Control_Tool],[Tool_Expire_Day],[Tool_Expire_Month]     "
            sSql += " ,[Tool_Alert_Day],[Tool_Alert_Month],[F_bom_cost],[min_qty] ,[max_qty],[F_Lot_FIFO],[F_Cost],[Picture1],[Picture2],[Picture3]"
            sSql += "  ,[Picture4],[Picture5],[Picture6],[Df_Serial],[Serial_Charecter],[Df_SerialFormat],[Serial_running],[F_IssuePriority],[Mfg_SMUNITMS_ID],[Seq_Group]"
            sSql += " ,[Activity_Date],[Start_Sale_Date],[Last_Sale_Date],[F_Unit_Control],[Width],[Lenght],[Hight],[MRSTPTHD_id],[F_Auto_Convert_P1_Qty],[Status]     "
            sSql += " ,[F_UseRateWaste],[Temp_SMSTORMS_ID],[SMACCOMS_ID],[LeadTime_Pd_Self],[F_RECMoreThanPO],[Qty_RECMoreThanPO],[Percent_RECMoreThanPO],[User_Code],[Update_Date_Time],[Daily_Capacity]   "
            sSql += " ,[Ref6],[F_UseItemWeightAddUnitWeight],[F_RECMoreThanSTD_P],[Qty_RECMoreThanSTD_P],[Percent_RECMoreThanSTD_P],[F_RECMoreThanPR],[Qty_RECMoreThanPR],[Percent_RECMoreThanPR],[f_Integer_Qty],[F_RECMoreThanPO_P]"
            sSql += " ,[Qty_RECMoreThanPO_P],[Percent_RECMoreThanPO_P],[Barcode_SMUNITMS_ID],[Allocate_SMUNITMS_ID],[F_QC_PO],[F_Issue_Full_Lot],[LastPurchaseDate],[f_CanOpenQtySOOverIV],[SMLOGOMS_ID],[f_lot_fifo_pd]    "
            sSql += " ,[Create_SMUSERMS_ID],getdate(),[F_RECMoreThanIP],[Qty_RECMoreThanIP],[Percent_RECMoreThanIP],[F_ExpireDate_Manual],[F_CalBackward_AleartDate],[F_GenDocOnlyMRPbyItem],[SMACCOTG_ID],[app_type]"
            sSql += " ,[acc_match_code],[SMLTTPHD_ID],[SA_SMVATTMS_ID],[PA_SMVATTMS_ID],[JC_SMUNITMS_ID]"
            sSql += " FROM [dbo].[SMITEMMS] "
            sSql += "  where  code like 'TEST-01'"


            If sqlcon.State = ConnectionState.Closed Then
                sqlcon = New OleDbConnection(db_Enpro.cnPAStr)
                sqlcon.Open()
            End If
            With sqlcom
                .Connection = sqlcon
                .CommandType = CommandType.Text
                .CommandText = sSql
                .ExecuteNonQuery()
            End With

        End If

    End Sub


End Class