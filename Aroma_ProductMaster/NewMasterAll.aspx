﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="NewMasterAll.aspx.vb" Inherits="Aroma_ProductMaster.NewMasterAll" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="Your description">
<meta name="keywords" content="Your keywords">
<meta name="author" content="Your name">
<link rel="icon" href="images/favicon.ico" type="image/x-icon">
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />

<link rel="stylesheet" href="css/bootstrap.css" type="text/css" media="screen">
<link rel="stylesheet" href="css/bootstrap-responsive.css" type="text/css" media="screen">    
<link rel="stylesheet" href="css/style.css" type="text/css" media="screen">

<script type="text/javascript" src="js/jquery.js"></script>  
<script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="js/superfish.js"></script>

<script type="text/javascript" src="js/jquery.ui.totop.js"></script>

<script type="text/javascript" src="js/cform.js"></script>
<script>
    $(document).ready(function () {
        //	


    }); //
    $(window).load(function () {
        //

    }); //
</script>		
    <style type="text/css">
        .style1
        {
        }
        .style2
        {
            height: 49px;
        }
        .style4
        {
        }
        .style7
        {
            height: 49px;
            width: 162px;
        }
        .style8
        {
            width: 162px;
        }
        .style15
        {
            width: 162px;
            height: 21px;
        }
        .style16
        {
            height: 21px;
        }
        .style17
        {
            width: 162px;
            height: 44px;
        }
        .style18
        {
            height: 44px;
        }
        </style>

<%--<link rel="stylesheet" href="css/bootstrap.css" type="text/css" media="screen">
<link rel="stylesheet" href="css/bootstrap-responsive.css" type="text/css" media="screen">    
<link rel="stylesheet" href="css/style.css" type="text/css" media="screen">
<script type="text/javascript" src="js/jquery.js"></script>  
<script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="js/superfish.js"></script>
<script type="text/javascript" src="js/jquery.ui.totop.js"></script>
<script type="text/javascript" src="js/cform.js"></script>
	--%>

         <script type = "text/javascript">
             function SetTarget() {
                 document.forms[0].target = "_blank";
             }
</script>

</head>
<body>
    <form id="form1" runat="server">
    <div>
    
    <div id="main">
<div class="top1_wrapper">
<div class="top1">
<div class="container">
<div class="row">
<div class="span12">
<div class="top1_inner clearfix">
	
<div class="top2 clearfix">
<header><div class="logo_wrapper"><a href="index.html" class="logo">
        <img src="images/aroma/logotrans.png" alt="" >
        <%--<img src="images/aroma/Lion.png" alt="" width="100" >--%>
        </a></div>
        
  </header>	
<div class="top3 clearfix">
<div class="phone1">
	<div class="txt1">Request New Master</div>
	<div class="txt2">Product</div>
</div>

<div class="search-form-wrapper clearfix">

</div>
</div>

</div>

<div class="menu_wrapper">
<div class="navbar navbar_">
	<div class="navbar-inner navbar-inner_">
		<a class="btn btn-navbar btn-navbar_" data-toggle="collapse" data-target=".nav-collapse_">
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</a>
		<div class="nav-collapse nav-collapse_ collapse">
	<ul class="nav sf-menu clearfix">
            <li class="active"><a href="Main.aspx?usr=<%=Request.QueryString("usr") %>">Home</a></li>
            <li><a href="NewMaster_Type.aspx?usr=<%=Request.QueryString("usr") %>">Request New Master</a></li>
            <li><a href="NewMasterAll.aspx?usr=<%=Request.QueryString("usr") %>">Request New Master All</a></li>
            <li><a href="ProductMaster.aspx?usr=<%=Request.QueryString("usr") %>">Product Master</a></li>           
            <li><a href="Maintain_ProductType.aspx?usr=<%=Request.QueryString("usr") %>" target="_blank">จัดการสินค้า</a></li>
           
            <li><a href="Login.aspx">Sign Out</a></li>										
            </ul>
		</div>
	</div>
</div>	
</div>

</div>	
</div>	
</div>	
</div>	
</div>	
</div>
<div id="inner">

<div id="content">
<div class="container">
<div class="row">
<div class="span12">

<div class="breadcrumbs1"><a href="Main.aspx?usr=<%=Request.QueryString("usr") %>">Home Page</a><span></span>Request Master all</div>



<div class="row">
<div class="span8">

<h1>Request New Master All</h1>

</div>	

</div>



<div id="note"></div>
<div id="fields">

    <table>
    <tr>
        <td class="style7">
            <asp:Label ID="Label1" runat="server" Text="บริษัท : " Font-Size="Medium" 
                Width="100px" Font-Bold="True"></asp:Label>
            <br />
        </td>
        <td colspan="8" class="style2">

      
            <asp:CheckBox ID="chkKVN" runat="server" 
                Text="บจ. เค.วี.เอ็น.อิมปอร์ต เอ็กซ์ปอร์ต  (1991)" Width="250px" 
                Checked="True" />
              
           
            <asp:CheckBox ID="chkLION" runat="server" 
                Text="บจ. ไลอ้อน ทรี-สตาร์" Width="129px" Checked="True" />

        </td>
    </tr>


  
    <tr>
        <td class="style15">
            <asp:Label ID="Label43" runat="server" Text="รหัสสินค้า" 
                Font-Size="Medium" Font-Bold="False"></asp:Label>
            </td>
        <td class="style16">

            <asp:TextBox ID="txtItem" runat="server" Width="200px"></asp:TextBox>
        </td>
        <td class="style16">
            &nbsp;</td>
        <td  class="style16" align ="rigth">
            <asp:Label ID="Label20" runat="server" Text="ชื่อสินค้า (ภาษาไทย) : " 
                Font-Size="Medium" Font-Bold="False" Width="190px"></asp:Label>
            </td>
        <td align ="rigth" class="style16" > 
            <asp:TextBox ID="txtItemDesc" runat="server" Width="300px"></asp:TextBox>
            </td>
        <td colspan="3" class="style16">
            &nbsp;</td>
    </tr>
  
  
  
    <tr>
        <td class="style17">
            <asp:Label ID="Label33" runat="server" Text="Product Type  : " 
                Font-Size="Medium" Font-Bold="False"></asp:Label>
            </td>
        <td class="style18">
            <asp:DropDownList ID="drpProductType" runat="server" Width="220px" 
                AutoPostBack="True">
            </asp:DropDownList>
        </td>
        <td class="style18">
            </td>
        <td class="style18" align = "right">
            <asp:Label ID="Label34" runat="server" Text="Product Category  : " 
                Font-Size="Medium" Font-Bold="False"></asp:Label>
            </td>
        <td align ="rigth" class="style18" > 
            <asp:DropDownList ID="drpProductCat" runat="server" Width="220px" 
                AutoPostBack="True">
            </asp:DropDownList>
            </td>
        <td colspan="3" class="style18">
            </td>
    </tr>
  
    <tr>
        <td class="style8">
            <asp:Label ID="Label35" runat="server" Text="Product Group :" 
                Font-Size="Medium" Font-Bold="False" Width="230px"></asp:Label>
            </td>
        <td class="style4">
            <asp:DropDownList ID="drpProductGroup" runat="server" Width="220px">
            </asp:DropDownList>
        </td>
        <td class="style4">
            &nbsp;</td>
        <td class="style4">
            &nbsp;</td>
        <td align ="rigth" class="style1" colspan="2"> 
            &nbsp;</td>
        <td colspan="3">
            &nbsp;</td>
    </tr>
  
    <tr>
        <td class="style8">
            </td>
        <td>
            </td>
        <td>
            </td>
        <td>
            </td>
        <td align ="rigth" colspan="2"> 
            </td>
        <td colspan="3">
        </td>
    </tr>
  
    
   
    
    <tr>
        <td class="style8">
            &nbsp;</td>
        <td>
            &nbsp;</td>
        <td>
            &nbsp;</td>
        <td>
            &nbsp;</td>
        <td align ="rigth" colspan="2"> 
            &nbsp;</td>
        <td colspan="3">
            &nbsp;</td>
    </tr>
  
    
   
    
    <tr>
        <td class="style8">
            &nbsp;</td>
        <td class="style4">
            <asp:Button ID="btnSearch" runat="server" Text="Search" class="submit" 
                BackColor="#003399" BorderColor="#0066FF" Font-Bold="True" Font-Size="Medium" 
                ForeColor="White" Height="30px" Width="100px"/>
        	

            &nbsp; <asp:Button ID="btnExport" runat="server" Text="Export"  class="btn btn-primary" OnClientClick = "SetTarget();"
                BackColor="#003399" BorderColor="#0066FF" Font-Bold="True" Font-Size="Medium" 
                ForeColor="White" Height="30px" Width="100px"/>

            </td>
        <td class="style4">
            &nbsp;</td>
        <td class="style4">
            &nbsp;</td>
        <td align =rigth class="style1" colspan="2"> 
            &nbsp;</td>
        <td>
            &nbsp;</td>
        <td align = rigth>
            &nbsp;</td>
        <td>
            &nbsp;</td>
    </tr>
   
    
   
    
    <tr>
        <td class="style8">
            &nbsp;</td>
        <td class="style4">
            &nbsp;</td>
        <td class="style4">
            &nbsp;</td>
        <td class="style4">
            &nbsp;</td>
        <td align =rigth class="style1" colspan="2"> 
            &nbsp;</td>
        <td>
            &nbsp;</td>
        <td align = rigth>
            &nbsp;</td>
        <td>
            &nbsp;</td>
    </tr>
   
    
    </table>

    <asp:GridView ID="GridView" runat="server"  Width = "100%"  DataKeyNames="Item" 
        AutoGenerateColumns = "False" 
        AlternatingRowStyle-BackColor = "#F0F8FF"  
        HeaderStyle-BackColor = "green" 
        AllowPaging ="True"  ShowFooter = "True"  
        OnPageIndexChanging = "OnPaging" CellPadding="3" Height="100%" 
        BorderStyle="None" BorderWidth="1px" 
                  Font-Names="Tahoma" BackColor="White" BorderColor="#CCCCCC" 
        PageSize="20" >
<AlternatingRowStyle BackColor="AliceBlue"></AlternatingRowStyle>
                            <Columns>

                           
                           <asp:BoundField DataField="RequestId" HeaderText="Doc No." >

                                 <ItemStyle Width="60px" />
                                </asp:BoundField>

                            <asp:BoundField DataField="RequestDate" HeaderText="วันที่ Request" >

                                <ItemStyle Width="70px" />
                                </asp:BoundField>

                            <asp:BoundField DataField="Company_KVN" HeaderText="KVN" />

                            <asp:BoundField DataField="Company_LION" HeaderText="LION" />

                              <%--<asp:HyperLinkField DataNavigateUrlFields="Item,username" 
                                            DataNavigateUrlFormatString="NewMaster.aspx?Item={0}&username={1}" 
                                            DataTextField="Item" HeaderText="Item" 
                                            NavigateUrl="NewMaster.aspx?Item={0}&username={1}"  Target="_self" >
                                        <ControlStyle ForeColor="Blue"></ControlStyle>
                                            <ItemStyle Font-Bold="True" Width="120px" />
                                        </asp:HyperLinkField>--%>

                            <asp:BoundField DataField="ProductType" HeaderText="Product Type" />

                            <asp:BoundField DataField="ProductCategory" HeaderText="Product Category" />

                            <asp:BoundField DataField="ProductGroup" HeaderText="Product Group" />

                             <asp:BoundField DataField="Item" HeaderText="รหัสสินค้า" />

                             <asp:BoundField DataField="ItemDescriptionTH" HeaderText="ชื่อสินค้า (ภาษาไทย)" />

                              <asp:BoundField DataField="stat" HeaderText="สถานะ" />


                              <asp:HyperLinkField DataNavigateUrlFields="Item,username,RequestType,RequestId" 
                                            DataNavigateUrlFormatString="NewMasterAll_Type.aspx?Item={0}&usr={1}&RequestType={2}&id={3}" 
                                            DataTextField="detail" HeaderText="รายละเอียด" 
                                            NavigateUrl="NewMasterAll_Type.aspx?Item={0}&usr={1}&RequestType={2}&id={3}" >
                                        <ControlStyle ForeColor="Blue"></ControlStyle>
                                            <ItemStyle Font-Bold="True" Width="50px" />
                                        </asp:HyperLinkField>

                            </Columns>

                            <FooterStyle BackColor="White" ForeColor="#000066" />
                            <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                            <RowStyle ForeColor="#000066" />
                            <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                            <SortedAscendingCellStyle BackColor="#F1F1F1" />
                            <SortedAscendingHeaderStyle BackColor="#007DBB" />
                            <SortedDescendingCellStyle BackColor="#CAC9C9" />
                            <SortedDescendingHeaderStyle BackColor="#00547E" />
    </asp:GridView>

     

<%--
	<form id="ajax-contact-form" class="form-horizontal" action="javascript:alert('success!');">
		<div class="row">
			<div class="span4">
				<div class="control-group">
				    <label class="control-label" for="inputName">Your full name:</label>
				    <div class="controls">				      
				        <asp:CheckBoxList ID="CheckBoxList3" runat="server" RepeatColumns="4" 
                Font-Size="Medium" Width="800px">
                  <asp:ListItem Value="kvn">บจ. เค.วี.เอ็น.อิมปอร์ต เอ็กซ์ปอร์ต  (1991)</asp:ListItem>
                  <asp:ListItem Value="lion">บจ. ไลอ้อน ทรี-สตาร์</asp:ListItem>
            
                  <asp:ListItem Value="kvn">บจ. เค.วี.เอ็น.อิมปอร์ต เอ็กซ์ปอร์ต  (1991)</asp:ListItem>
                  <asp:ListItem Value="lion">บจ. ไลอ้อน ทรี-สตาร์</asp:ListItem>
                  <asp:ListItem Value="lion">บจ. ไลอ้อน ทรี-สตาร์</asp:ListItem>
                
              </asp:CheckBoxList>
                        
                    </div>
				</div>				
			</div>	
			<div class="span4">
				<div class="control-group">
				    <label class="control-label" for="inputEmail">Your email:</label>
				    <div class="controls">				      
				      <input class="span4" type="text" id="inputEmail" name="email" value="Your email:" onBlur="if(this.value=='') this.value='Your email:'" onFocus="if(this.value =='Your email:' ) this.value=''">
				    </div>
				</div>
			</div>		
			<div class="span4">
				<div class="control-group">
				    <label class="control-label" for="inputPhone">Phone number:</label>
				    <div class="controls">				      
				      <input class="span4" type="text" id="inputPhone" name="phone" value="Phone number:" onBlur="if(this.value=='') this.value='Phone number:'" onFocus="if(this.value =='Phone number:' ) this.value=''">
				    </div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="span12">
				<div class="control-group">
				    <label class="control-label" for="inputMessage">Message:</label>
				    <div class="controls">				      				      
				      <textarea class="span12" id="inputMessage" name="content" onBlur="if(this.value=='') this.value='Message:'" 
                        onFocus="if(this.value =='Message:' ) this.value=''">Message:</textarea>
				    </div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="span12">
				<div class="control-group capthca">
				    <label class="control-label" for="inputCapthca">Capthca:</label>
				    <div class="controls">				      
				      <input class="" type="text" id="inputCapthca" name="capthca" value="Capthca:" onBlur="if(this.value=='') this.value='Capthca:'" onFocus="if(this.value =='Capthca:' ) this.value=''">
				      <img src="captcha/captcha.php">
				    </div>
				</div>
			</div>
		</div>
		<button type="submit" class="submit">submit</button>
	</form>--%>


</div>	

	
	

</div>	
</div>	
</div>	
</div>


<div class="bot1">
<div class="container">
<div class="row">
<div class="span12">
<div class="bot1_inner">
<div class="row">
<div class="span4">
	
<div class="logo2_wrapper"><a href="index.html" class="logo2"><img src="images/aroma/logotrans.png" alt=""></a></div>

<footer><div class="copyright">Copyright   © 2018. All rights reserved.<br><a href="#">Aroma Group</a></div></footer>


</div>
<div class="span4">
	
<div class="bot1_title">Contact Us</div>

สำนักงานใหญ่ อโรม่า กรุ๊ป<br>
บริษัท เค.วี.เอ็น.อิมปอร์ต เอกซ์ปอร์ต (1991) จำกัด<br>
เลขที่ 43 ชั้น 2 ซอยนาคนิวาส 6 ถนนนาคนิวาส แขวงลาดพร้าว <br>
เขตลาดพร้าว กรุงเทพฯ 10230<br>
โทรศัพท์ : 02 159-8999<br>
โทรสาร : 02 538-8144, 02 539-5597

<%--E-mail: <a href="#">mail@demosite.com</a>--%>


</div>
<div class="span4">
	
<%--<div class="bot1_title">Follow Us:</div>	

<div class="social_wrapper">
		<ul class="social clearfix">
	    <li><a href="#"><img src="images/social_ic1.png"></a></li>
	    <li><a href="#"><img src="images/social_ic2.png"></a></li>
	    <li><a href="#"><img src="images/social_ic3.png"></a></li>	    
	    <li><a href="#"><img src="images/social_ic4.png"></a></li>
		</ul>
	</div>
--%>


</div>	

</div>	
</div>
</div>	
</div>	
</div>	
</div>

</div>	
</div>
<script type="text/javascript" src="js/bootstrap.js"></script>

  <script src="https://code.jquery.com/jquery-2.1.1.min.js" type="text/javascript"></script>
            <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2-rc.1/css/select2.min.css" rel="stylesheet" />
            <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2-rc.1/js/select2.min.js" type="text/javascript"></script>
            <script type="text/javascript">
                $(document).ready(function () {
                    $("#drpProductType").select2({
                    });
                });

                $(document).ready(function () {
                    $("#drpProductCat").select2({
                    });
                });

                $(document).ready(function () {
                    $("#drpProductGroup").select2({
                    });
                });

                $(document).ready(function () {
                    $("#drpGroup").select2({
                    });
                });

            </script>  


    </div>
    </form>
</body>
</html>