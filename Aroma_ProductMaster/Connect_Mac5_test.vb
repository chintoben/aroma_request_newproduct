﻿
Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports System.Data

Public Class Connect_Mac5_test

    'Dim db_200 As New Connect_Form

    ' Public sqlCon As String = "Data Source=dbl2\dbc2;Initial Catalog=" & ConfigurationManager.AppSettings("DBSETTING_SubcontractPortal") & ";User ID=sa;Password=edpedp;packet size=4096;Connect Timeout=4500;Min Pool Size=1;Max Pool Size=200;pooling=false"
    Public sqlCon As String = "Data Source=10.0.4.182;Initial Catalog=" & ConfigurationManager.AppSettings("DBSETTING_DB_Mac5_Test") & ";User ID=sa;Password=@Ar0magroup123;packet size=4096;Connect Timeout=4500;Min Pool Size=1;Max Pool Size=200;pooling=false"
    Public cnPAStr As String = "Provider='SQLOLEDB.1';User ID='sa';PASSWORD='@Ar0magroup123';Data Source='10.0.4.182';Initial Catalog='M5CM-AA-09';"

    'strConnString = "Server=localhost;UID=sa;PASSWORD=;Database=mydatabase;Max Pool Size=400;Connect Timeout=600;"
    '  objConn.ConnectionString = strConnString
    '  objConn.Open()

    Public Function Strcon() As String
        Dim DSx As New Data.DataSet
        Dim Strconx As String
        'Strconx = "Provider=sqloledb;Data Source=dbl2\dbc2;Initial Catalog=" & ConfigurationManager.AppSettings("DBSETTING_SubcontractPortal") & ";User ID=sa;Password=edpedp;Connect Timeout=0; pooling=true; Max Pool Size=200"

        Strconx = "Provider=sqloledb;Data Source=10.0.4.182;Initial Catalog=M5CM-AA-09 ;User ID=sa;Password=@Ar0magroup123;Connect Timeout=0; pooling=true; Max Pool Size=200"

        Return Strconx

    End Function

    Public Function Constr2() As String
        Dim sqlconX As String
        sqlconX = "Data Source=10.0.4.182" & ",1433;Initial Catalog=" & ConfigurationManager.AppSettings("DBSETTING_DB_Mac5_Test") & ";User ID=sa;Password=@Ar0magroup123;Connect Timeout=3600; pooling=true; Max Pool Size=200"
        'Public sqlCon As String = "Data Source=172.10.1.25,1433;Initial Catalog=" & ConfigurationManager.AppSettings("DBSETTING") & ";User ID=sa;Password=edpedp;Connect Timeout=0; pooling=true; Max Pool Size=200"

        Return sqlconX
    End Function

    Public Function GetDataset(ByVal Strsql As String, _
        Optional ByVal DatasetName As String = "Dataset1", _
        Optional ByVal TableName As String = "Table1") As Data.DataSet
        Dim DA As New OleDbDataAdapter(Strsql, Strcon)
        Dim DS As New Data.DataSet(DatasetName)
        Try
            DA.Fill(DS, TableName)
        Catch x1 As Exception
            Err.Raise(60002, , x1.Message)
        End Try
        Return DS
    End Function

    Public Function GetDataTable(ByVal Strsql As String, _
         Optional ByVal TableName As String = "Table1") As Data.DataTable
        Dim DA As New OleDbDataAdapter(Strsql, Strcon)
        Dim DT As New Data.DataTable(TableName)
        Try
            DA.Fill(DT)
        Catch x1 As Exception
            Err.Raise(60002, , Strsql)
        End Try
        Return DT
    End Function

    Public Function Constr() As String
        Dim sqlconX As String
        sqlconX = "Data Source=" & ConfigurationManager.AppSettings("Server") & ",1433;Initial Catalog=" & ConfigurationManager.AppSettings("DBSETTING_SubContractPortal") & ";User ID=mac5usr;Password=@Ar0magroup123;Connect Timeout=3600; pooling=true; Max Pool Size=200"
        Return sqlconX
    End Function


    'Private Function SearchData(ByVal Tb As String, ByVal Fld As String, Optional ByVal Cond As String = "")
    '    Dim sql As String
    '    Dim Sqladp As SqlDataAdapter
    '    Dim DataSearch As New DataSet
    '    Dim dtrow As DataRow
    '    Dim SCon As New SqlConnection(db_200.sqlCon)
    '    sql = "select top 1 " & Fld & " from " & Tb
    '    If Cond <> "" Then
    '        sql = sql & " where " & Cond
    '    End If
    '    Sqladp = New SqlDataAdapter(sql, SCon)
    '    Sqladp.Fill(DataSearch, "data1")
    '    If DataSearch.Tables("data1").Rows.Count <> 0 Then  'Not IsDBNull(DataSearch.Tables("data1").Rows(0)) Then
    '        dtrow = DataSearch.Tables("data1").Rows(0)
    '        SearchData = dtrow.Item(0)
    '    Else
    '        SearchData = ""
    '    End If
    '    SCon.Close()
    'End Function

    Public Function CreateCommand(ByVal Strsql As String) As OleDbCommand
        Dim cmd As New OleDbCommand(Strsql)
        Return cmd
    End Function

    Public Function Execute(ByVal Strsql As String) As Integer
        Dim Cn As New OleDbConnection(Strcon)
        Dim cmd As New OleDbCommand(Strsql, Cn)
        Dim X As Integer
        Try
            Cn.Open()
            X = cmd.ExecuteNonQuery()
        Catch ex As Exception
            Err.Raise(60002, , ex.Message)
            X = -1
        Finally
            Cn.Close()
        End Try
        Return X
    End Function

    Public Function Execute(ByVal Strsql As String, ByVal mt As String) As Integer
        Dim Cn As New OleDbConnection(Strcon)
        Dim cmd As New OleDbCommand(Strsql, Cn)
        Dim X As Integer
        Try
            Cn.Open()
            X = cmd.ExecuteScalar
        Catch ex As Exception
            Err.Raise(60002, , ex.Message)
            X = -1
        Finally
            Cn.Close()
        End Try
        Return X
    End Function
    Public Sub CreateParam(ByRef Cmd As OleDbCommand, ByVal StrType As String)
        'T:Text, M:Memo, Y:Currency, D:Datetime, I:Integer, S:Single, B:Boolean
        Dim i As Integer
        Dim j As String
        For i = 1 To Len(StrType)
            j = UCase(Mid(StrType, i, 1))
            Dim P1 As New OleDbParameter
            P1.ParameterName = "@P" & i
            Select Case j
                Case "T"
                    P1.OleDbType = OleDbType.VarChar
                Case "M"
                    P1.OleDbType = OleDbType.LongVarChar
                Case "Y"
                    P1.OleDbType = OleDbType.Currency
                Case "D"
                    P1.OleDbType = OleDbType.Date
                Case "I"
                    P1.OleDbType = OleDbType.Integer
                Case "S"
                    P1.OleDbType = OleDbType.Decimal
                Case "B"
                    P1.OleDbType = OleDbType.Boolean
            End Select
            Cmd.Parameters.Add(P1)
        Next
    End Sub

    Public Function ExecuteDataTable(ByVal storedProcedureName As String, ByVal ParamArray arrParam() As SqlParameter) As DataTable
        Dim dt As DataTable

        ' Open the connection 
        Using cnn As New SqlConnection(sqlCon)
            cnn.Open()
            ' Define the command 
            Using cmd As New SqlCommand
                cmd.Connection = cnn
                cmd.CommandType = CommandType.StoredProcedure
                cmd.CommandText = storedProcedureName
                ' Handle the parameters 
                If arrParam IsNot Nothing Then
                    For Each param As SqlParameter In arrParam
                        cmd.Parameters.Add(param)
                    Next
                End If
                ' Define the data adapter and fill the dataset 
                Using da As New SqlDataAdapter(cmd)
                    dt = New DataTable
                    da.Fill(dt)
                End Using
            End Using
        End Using

        Return dt
    End Function

    Public Function ExecuteDataTable(ByVal storedProcedureName As String) As DataTable
        Dim dt As DataTable

        ' Open the connection 
        Using cnn As New SqlConnection(sqlCon)
            cnn.Open()
            ' Define the command 
            Using cmd As New SqlCommand
                cmd.Connection = cnn
                cmd.CommandType = CommandType.StoredProcedure
                cmd.CommandText = storedProcedureName
                ' Define the data adapter and fill the dataset 
                Using da As New SqlDataAdapter(cmd)
                    dt = New DataTable
                    da.Fill(dt)
                End Using
            End Using
        End Using

        Return dt
    End Function

    Public Function ExcuteStoredProcedure(ByVal storedProcedureName As String, ByVal ParamArray arrParam() As SqlParameter) As String
        Dim x As Integer
        Dim Cn As New SqlConnection(sqlCon)
        Using cmd As New SqlCommand(storedProcedureName, Cn)
            Try
                If arrParam IsNot Nothing Then
                    For Each param As SqlParameter In arrParam
                        cmd.Parameters.Add(param)
                    Next
                End If
                cmd.CommandType = Data.CommandType.StoredProcedure
                Cn.Open()
                x = cmd.ExecuteScalar
            Catch x1 As Exception
                Err.Raise(60002, , x1.Message)
            Finally
                Cn.Close()
            End Try
        End Using

        Return x
    End Function

    Public Function ExcuteStoredProcedure(ByVal storedProcedureName As String) As String
        Dim x As Integer
        Dim Cn As New SqlConnection(sqlCon)
        Using cmd As New SqlCommand(storedProcedureName, Cn)
            Try
                cmd.CommandType = Data.CommandType.StoredProcedure
                Cn.Open()
                x = cmd.ExecuteScalar
            Catch x1 As Exception
                Err.Raise(60002, , x1.Message)
            Finally
                Cn.Close()
            End Try
        End Using
        Return x
    End Function

    Public Function CreateParam(ByVal sqlParam As Data.SqlClient.SqlParameter, ByVal paramName As String, ByVal paramType As Data.SqlDbType, ByVal paramValue As String, ByVal paramSize As Integer) As Data.SqlClient.SqlParameter
        With sqlParam
            .ParameterName = paramName
            .DbType = paramType
            .Size = paramSize
            .Value = paramValue
        End With
        Return sqlParam
    End Function

    Public Sub DropExistViews(ByVal vwName As String)
        Dim sb As New StringBuilder("")
        sb.Append("if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[" & vwName & "]') ")
        sb.Append("and OBJECTPROPERTY(id, N'IsTable') = 1) ")
        sb.Append("drop table [dbo].[" & vwName & "]")
        Execute(sb.ToString)
    End Sub
End Class
