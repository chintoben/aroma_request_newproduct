﻿Public Class Main
    Inherits System.Web.UI.Page
    Dim usr As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        usr = Request.QueryString("usr")
    End Sub

    Protected Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Server.Transfer("ProductMaster.aspx?&usr=" & usr & "")
    End Sub

    Protected Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Server.Transfer("NewMaster_Type.aspx?&usr=" & usr & "")
    End Sub

    Protected Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Server.Transfer("NewMasterAll.aspx?&usr=" & usr & "")
    End Sub


End Class