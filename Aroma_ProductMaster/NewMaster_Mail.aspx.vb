﻿Imports System.Data.SqlClient
Imports System.Net.Mail
Imports System.IO

Public Class NewMaster_Mail
    Inherits System.Web.UI.Page
    Dim db_Mac5 As New Connect_Mac5
    Dim db_HR As New Connect_HR
    Dim dt, dt2, dtt As New DataTable
    Dim sql, sql2, username, CallID, Type, item As String
    Private sms As New PKMsg("")
    'Private con As New SqlConnection("data source=10.0.24.20;initial catalog=DB_Service;persist security info=false;User ID=sa;Password=Quax_005;Connect Timeout=0;Max Pool Size=500;Enlist=true")
    Dim DocID As String


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        item = Request.QueryString("item")


        If Not IsPostBack Then

            txtRequestDate.Text = DateTime.Now.ToString("yyyy-MM-dd")
            txtItem.Text = item


            drpRequester.SelectedValue = username
            sql = " select * "
            sql += " from [VW_ProductMasterGroupRequest] "
            sql += " where Item = '" & item & "' "
            dt = db_Mac5.GetDataTable(sql)
            If dt.Rows.Count > 0 Then
                LoadData()
            End If


        End If
    End Sub


    Sub InsertData()

        Dim sql0 As String
        Dim Company, ProductType, ProductCat, ProductGroup, kvn, lion As String

        'If chkCompany.Items.FindByValue("kvn").Selected = True And chkCompany.Items.FindByValue("lion").Selected = False Then
        '    Company = "KVN"
        'ElseIf chkCompany.Items.FindByValue("kvn").Selected = False And chkCompany.Items.FindByValue("lion").Selected = True Then
        '    Company = "LION"
        'End If
        If chkKVN.Checked = True Then
            kvn = "Yes"
        Else
            kvn = "NO"
        End If

        If chkLION.Checked = True Then
            lion = "Yes"
        Else
            lion = "NO"
        End If

        If drpProductType.SelectedValue = "อื่นๆ" Then
            ProductType = txtProductType.Text
        Else
            ProductType = drpProductType.SelectedValue
        End If

        If drpProductCat.SelectedValue = "อื่นๆ" Then
            ProductCat = txtProductCat.Text
        Else
            ProductCat = drpProductCat.SelectedValue
        End If

        If drpProductGroup.SelectedValue = "อื่นๆ" Then
            ProductGroup = txtProductGroup.Text
        Else
            ProductGroup = drpProductGroup.SelectedValue
        End If

        'If rdoObjective.Items.FindByValue("สินค้าชุด/Promotion").Selected = True Then
        '    Objective = "สินค้าชุด/Promotion"
        'ElseIf rdoObjective.Items.FindByValue("Manufacture").Selected = True Then
        '    Objective = "Manufacture"
        'ElseIf rdoObjective.Items.FindByValue("NewsProduct").Selected = True Then
        '    Objective = "NewsProduct"
        'ElseIf rdoObjective.Items.FindByValue("Aroma").Selected = True Then
        '    Objective = "Aroma"
        'ElseIf rdoObjective.Items.FindByValue("Other").Selected = True Then
        '    Objective = "Other"
        'End If

        'If rdoUnitPurchase.Items.FindByValue("กิโลกรัม").Selected = True Then
        '    UnitPurchase = "กิโลกรัม"
        'ElseIf rdoUnitPurchase.Items.FindByValue("ลัง").Selected = True Then
        '    UnitPurchase = "ลัง"
        'ElseIf rdoUnitPurchase.Items.FindByValue("กล่อง").Selected = True Then
        '    UnitPurchase = "กล่อง"
        'ElseIf rdoUnitPurchase.Items.FindByValue("หีบ").Selected = True Then
        '    UnitPurchase = "หีบ"
        'ElseIf rdoUnitPurchase.Items.FindByValue("ซอง").Selected = True Then
        '    UnitPurchase = "ซอง"
        'ElseIf rdoUnitPurchase.Items.FindByValue("ถุง").Selected = True Then
        '    UnitPurchase = "ถุง"
        'ElseIf rdoUnitPurchase.Items.FindByValue("ชิ้น").Selected = True Then
        '    UnitPurchase = "ชิ้น"
        'ElseIf rdoUnitPurchase.Items.FindByValue("ขวด").Selected = True Then
        '    UnitPurchase = "ขวด"
        'ElseIf rdoUnitPurchase.Items.FindByValue("เครื่อง").Selected = True Then
        '    UnitPurchase = "เครื่อง"
        'ElseIf rdoUnitPurchase.Items.FindByValue("อื่นๆ").Selected = True Then
        '    UnitPurchase = txtUnitPurchase.Text
        'End If


        'If rdoPurchasedProductGroup.Items.FindByValue("coffee").Selected = True Then
        '    PurchasedProductGroup = "coffee"
        'ElseIf rdoPurchasedProductGroup.Items.FindByValue("Tea").Selected = True Then
        '    PurchasedProductGroup = "Tea"
        'ElseIf rdoPurchasedProductGroup.Items.FindByValue("Chocolat").Selected = True Then
        '    PurchasedProductGroup = "Chocolat"
        'ElseIf rdoPurchasedProductGroup.Items.FindByValue("Mix").Selected = True Then
        '    PurchasedProductGroup = "Mix"
        'ElseIf rdoPurchasedProductGroup.Items.FindByValue("เครื่องชง").Selected = True Then
        '    PurchasedProductGroup = "Machine"
        'ElseIf rdoPurchasedProductGroup.Items.FindByValue("อะไหล่").Selected = True Then
        '    PurchasedProductGroup = "Part"
        'ElseIf rdoPurchasedProductGroup.Items.FindByValue("Bakery").Selected = True Then
        '    PurchasedProductGroup = "Bakery"
        'ElseIf rdoPurchasedProductGroup.Items.FindByValue("อาหาร").Selected = True Then
        '    PurchasedProductGroup = "food"
        'ElseIf rdoPurchasedProductGroup.Items.FindByValue("อื่นๆ").Selected = True Then
        '    PurchasedProductGroup = "other"
        'End If


        sql0 = "INSERT INTO [TB_ProductMasterGroupRequest] "
        sql0 &= "("
        sql0 &= "Company_KVN,Company_Lion,Requester,RequestDate,objective,Seller,ItemDescSeller"
        sql0 &= ",PurchaseUnit,PriceSell1,PriceSell2,DiscountSell,PurchasedProductGroup"
        sql0 &= ",ItemDescription,ProductType,ProductCategory,ProductGroup,Item ,CheckRawMaterial ,CheckMachine"
        sql0 &= ",Check1,Check2,Check3,Brand ,Model,GroupHead,Color,Option1 ,Option2,Option3"
        sql0 &= ",UnitMain,UnitSmall,Warehouse "
        sql0 &= ",Price1,Price2,Discount,ItemPOS"
        sql0 &= ",Stat,Remark ,CreateBy ,CreateDate"
        sql0 &= ")"


        sql0 &= "VALUES ('" & kvn & "','" & lion & "','" & drpRequester.SelectedValue & "','" & txtRequestDate.Text & "','" & rdoObjective.SelectedValue & "','" & txtSeller.Text & "','" & txtItemDescSeller.Text & "'"
        sql0 &= " ,'" & rdoUnitPurchase.SelectedValue & "'," & txtPriceSell1.Text & "," & txtPriceSell2.Text & " ," & txtDiscountSell.Text & ",'" & rdoPurchasedProductGroup.SelectedValue & "'"
        sql0 &= " ,'" & txtItemDescription.Text & "','" & ProductType & "' ,'" & ProductCat & "','" & ProductGroup & "','" & txtItem.Text & "' ,'' ,'' "
        sql0 &= " ,'','','','" & txtBrand.Text & "','" & txtModel.Text & "','" & drpGroup.SelectedValue & "','" & txtColor.Text & "','" & txtOption1.Text & "','" & txtOption2.Text & "','" & txtOption3.Text & "'"
        sql0 &= " ,'" & txtUnitMain.Text & "','" & txtUnitSmall.Text & "','" & txtWarehouse.Text & "' "
        sql0 &= " ," & txtPrice1.Text & "," & txtPrice2.Text & "," & txtDiscount.Text & ",'" & txtItemPOS.Text & "' "
        sql0 &= ",'Open','" & txtRemark.Text & "','" & username & "','" & Date.Now.ToString("yyyy-MM-dd") & "')"


        dtt = db_Mac5.GetDataTable(sql0)

    End Sub

    'Sub InsertProductMaster()

    '    Dim sql0 As String
    '    Dim Company, Objective, UnitPurchase, PurchasedProductGroup As String

    '    'If chkCompany.Items.FindByValue("kvn").Selected = True And chkCompany.Items.FindByValue("lion").Selected = False Then
    '    '    Company = "KVN"
    '    'ElseIf chkCompany.Items.FindByValue("kvn").Selected = False And chkCompany.Items.FindByValue("lion").Selected = True Then
    '    '    Company = "LION"
    '    'End If

    '    sql0 = "INSERT INTO [TB_ProductMasterGroup] "
    '    sql0 &= "("
    '    sql0 &= "Company"
    '    sql0 &= ",ProductType,ProductCategory,ProductGroup,Item ,CheckRawMaterial ,CheckMachine"
    '    sql0 &= ",Check1,Check2,Check3,Brand ,Model,GroupHead,Color,Option1 ,Option2,Option3"
    '    sql0 &= ",Stat,Remark "
    '    sql0 &= ")"


    '    sql0 &= "VALUES ('" & Company & "'"
    '    sql0 &= " ,'" & drpProductType.SelectedValue & "' ,'" & drpProductCat.SelectedValue & "','" & drpProductGroup.SelectedValue & "','" & txtItem.Text & "' ,'' ,'' "
    '    sql0 &= " ,'','','','" & txtBrand.Text & "','" & txtModel.Text & "','" & drpGroup.SelectedValue & "','" & txtColor.Text & "','" & txtOption1.Text & "','" & txtOption2.Text & "','" & txtOption3.Text & "'"
    '    sql0 &= ",'Active','" & txtRemark.Text & "')"


    '    dtt = db_Mac5.GetDataTable(sql0)

    'End Sub

    Sub UpdateData()
        Dim x As Integer
        Dim db_Mac5 As New Connect_Mac5
        Dim Cn As New SqlConnection(db_Mac5.sqlCon)
        Dim Company, ProductType, ProductCat, ProductGroup, kvn, lion As String


        Dim sSql As String = "UPDATE [TB_ProductMasterGroupRequest] SET  "
        sSql += " Company_KVN=@Company_KVN,Company_LION=@Company_LION ,Requester=@Requester,RequestDate=@RequestDate,objective=@objective,Seller=@Seller,ItemDescSeller=@ItemDescSeller"
        sSql += " ,PurchaseUnit=@PurchaseUnit, PriceSell1=@PriceSell1, PriceSell2=@PriceSell2 ,DiscountSell=@DiscountSell ,PurchasedProductGroup=@PurchasedProductGroup "
        sSql += " ,ItemDescription=@ItemDescription ,ProductType=@ProductType,ProductCategory=@ProductCategory ,ProductGroup=@ProductGroup ,Item=@Item ,CheckRawMaterial=@CheckRawMaterial,CheckMachine=@CheckMachine "
        sSql += " ,Check1=@Check1 ,Check2=@Check2 ,Check3=@Check3 ,Brand=@Brand ,Model=@Model ,GroupHead=@GroupHead "
        sSql += " ,Color=@Color ,Option1=@Option1 ,Option2=@Option2,Option3=@Option3 "
        sSql += " ,UnitMain=@UnitMain,UnitSmall=@UnitSmall ,Warehouse=@Warehouse  "
        sSql += " ,Price1=@Price1 ,Price2=@Price2 ,Discount=@Discount ,ItemPOS=@ItemPOS "
        sSql += " ,Remark=@Remark ,ModifyBy=@ModifyBy ,ModifyDate=@ModifyDate "

        sSql += " Where Item ='" & txtItem.Text & "' "

        Dim command As SqlCommand = New SqlCommand(sSql, Cn)
        Try

            'If chkCompany.Items.FindByValue("kvn").Selected = True And chkCompany.Items.FindByValue("lion").Selected = False Then
            '    Company = "KVN"
            'ElseIf chkCompany.Items.FindByValue("kvn").Selected = False And chkCompany.Items.FindByValue("lion").Selected = True Then
            '    Company = "LION"
            'End If
            If chkKVN.Checked = True Then
                kvn = "Yes"
            Else
                kvn = "NO"
            End If

            If chkLION.Checked = True Then
                lion = "Yes"
            Else
                lion = "NO"
            End If


            If drpProductType.SelectedValue = "อื่นๆ" Then
                ProductType = txtProductType.Text
            Else
                ProductType = drpProductType.SelectedValue
            End If

            If drpProductCat.SelectedValue = "อื่นๆ" Then
                ProductCat = txtProductCat.Text
            Else
                ProductCat = drpProductCat.SelectedValue
            End If

            If drpProductGroup.SelectedValue = "อื่นๆ" Then
                ProductGroup = txtProductGroup.Text
            Else
                ProductGroup = drpProductGroup.SelectedValue
            End If

            'command.Parameters.Add("@Company", Data.SqlDbType.VarChar).Value = Company
            command.Parameters.Add("@Company_KVN", Data.SqlDbType.VarChar).Value = kvn
            command.Parameters.Add("@Company_LION", Data.SqlDbType.VarChar).Value = lion
            command.Parameters.Add("@Requester", Data.SqlDbType.VarChar).Value = drpRequester.SelectedValue
            command.Parameters.Add("@RequestDate", Data.SqlDbType.VarChar).Value = txtRequestDate.Text
            command.Parameters.Add("@objective", Data.SqlDbType.VarChar).Value = rdoObjective.SelectedValue
            command.Parameters.Add("@Seller", Data.SqlDbType.VarChar).Value = txtSeller.Text
            command.Parameters.Add("@ItemDescSeller", Data.SqlDbType.VarChar).Value = txtItemDescSeller.Text
            command.Parameters.Add("@PurchaseUnit", Data.SqlDbType.VarChar).Value = rdoUnitPurchase.SelectedValue
            command.Parameters.Add("@PriceSell1", Data.SqlDbType.VarChar).Value = txtPriceSell1.Text
            command.Parameters.Add("@PriceSell2", Data.SqlDbType.VarChar).Value = txtPriceSell2.Text
            command.Parameters.Add("@DiscountSell", Data.SqlDbType.VarChar).Value = txtDiscountSell.Text
            command.Parameters.Add("@PurchasedProductGroup", Data.SqlDbType.VarChar).Value = rdoPurchasedProductGroup.SelectedValue
            command.Parameters.Add("@ItemDescription", Data.SqlDbType.VarChar).Value = txtItemDescription.Text
            command.Parameters.Add("@ProductType", Data.SqlDbType.VarChar).Value = ProductType
            command.Parameters.Add("@ProductCategory", Data.SqlDbType.VarChar).Value = ProductCat
            command.Parameters.Add("@ProductGroup", Data.SqlDbType.VarChar).Value = ProductGroup
            command.Parameters.Add("@Item", Data.SqlDbType.VarChar).Value = txtItem.Text
            command.Parameters.Add("@CheckRawMaterial", Data.SqlDbType.VarChar).Value = ""
            command.Parameters.Add("@CheckMachine", Data.SqlDbType.VarChar).Value = ""
            command.Parameters.Add("@Check1", Data.SqlDbType.VarChar).Value = ""
            command.Parameters.Add("@Check2", Data.SqlDbType.VarChar).Value = ""
            command.Parameters.Add("@Check3", Data.SqlDbType.VarChar).Value = ""
            command.Parameters.Add("@Brand", Data.SqlDbType.VarChar).Value = txtBrand.Text
            command.Parameters.Add("@Model", Data.SqlDbType.VarChar).Value = txtModel.Text
            command.Parameters.Add("@GroupHead", Data.SqlDbType.VarChar).Value = drpGroup.SelectedValue
            command.Parameters.Add("@Color", Data.SqlDbType.VarChar).Value = txtColor.Text
            command.Parameters.Add("@Option1", Data.SqlDbType.VarChar).Value = txtOption1.Text
            command.Parameters.Add("@Option2", Data.SqlDbType.VarChar).Value = txtOption2.Text
            command.Parameters.Add("@Option3", Data.SqlDbType.VarChar).Value = txtOption3.Text


            command.Parameters.Add("@UnitMain", Data.SqlDbType.VarChar).Value = txtUnitMain.Text
            command.Parameters.Add("@UnitSmall", Data.SqlDbType.VarChar).Value = txtUnitSmall.Text
            command.Parameters.Add("@Warehouse", Data.SqlDbType.VarChar).Value = txtWarehouse.Text

            command.Parameters.Add("@Price1", Data.SqlDbType.VarChar).Value = txtPrice1.Text
            command.Parameters.Add("@Price2", Data.SqlDbType.VarChar).Value = txtPrice2.Text
            command.Parameters.Add("@Discount", Data.SqlDbType.VarChar).Value = txtDiscount.Text
            command.Parameters.Add("@ItemPOS", Data.SqlDbType.VarChar).Value = txtItemPOS.Text
            command.Parameters.Add("@Remark", Data.SqlDbType.VarChar).Value = txtRemark.Text

            command.Parameters.Add("@ModifyBy", Data.SqlDbType.VarChar).Value = username
            command.Parameters.Add("@ModifyDate", Data.SqlDbType.VarChar).Value = DateTime.Now.ToString("yyyy-MM-dd")

            command.CommandType = Data.CommandType.Text
            Cn.Open()
            x = command.ExecuteScalar
        Catch x1 As Exception
            Err.Raise(60002, , x1.Message)
        Finally
            Cn.Close()
        End Try

    End Sub


    Sub LoadData()
        sql = " select * "
        sql += " from [VW_ProductMasterGroupRequest] "
        sql += " where Item = '" & txtItem.Text & "' "
        dt = db_Mac5.GetDataTable(sql)
        If dt.Rows.Count > 0 Then

            'If dt.Rows(0)("Company") = "KVN" Then
            '    chkCompany.Items.FindByValue("kvn").Selected = True
            'ElseIf dt.Rows(0)("Company") = "LION" Then
            '    chkCompany.Items.FindByValue("lion").Selected = True
            'End If

            If dt.Rows(0)("Company_KVN") = "YES" Then
                chkKVN.Checked = True
            End If

            If dt.Rows(0)("Company_LION") = "YES" Then
                chkLION.Checked = True
            End If



            drpRequester.SelectedValue = dt.Rows(0)("Requester")
            txtDep.Text = dt.Rows(0)("department_name")

            txtRequestDate.Text = dt.Rows(0)("RequestDate")
            rdoObjective.SelectedValue = dt.Rows(0)("objective")
            txtSeller.Text = dt.Rows(0)("Seller")
            txtItemDescSeller.Text = dt.Rows(0)("ItemDescSeller")
            rdoUnitPurchase.SelectedValue = dt.Rows(0)("PurchaseUnit")
            txtPriceSell1.Text = dt.Rows(0)("PriceSell1")
            txtPriceSell2.Text = dt.Rows(0)("PriceSell2")
            txtDiscountSell.Text = dt.Rows(0)("DiscountSell")
            rdoPurchasedProductGroup.SelectedValue = dt.Rows(0)("PurchasedProductGroup")
            txtItemDescription.Text = dt.Rows(0)("ItemDescription")
            drpProductType.SelectedValue = dt.Rows(0)("ProductType")
            txtItemDescription.Text = dt.Rows(0)("ItemDescription")
            drpProductType.SelectedValue = dt.Rows(0)("ProductType")
            drpProductCat.SelectedValue = dt.Rows(0)("ProductCategory")
            drpProductGroup.SelectedValue = dt.Rows(0)("ProductGroup")
            txtItem.Text = dt.Rows(0)("Item")
            txtBrand.Text = dt.Rows(0)("Brand")
            txtModel.Text = dt.Rows(0)("Model")
            drpGroup.SelectedValue = dt.Rows(0)("GroupHead")
            txtColor.Text = dt.Rows(0)("Color")
            txtOption1.Text = dt.Rows(0)("Option1")
            txtOption2.Text = dt.Rows(0)("Option2")
            txtOption3.Text = dt.Rows(0)("Option3")
            txtPrice1.Text = dt.Rows(0)("Price1")
            txtPrice2.Text = dt.Rows(0)("Price2")
            txtDiscount.Text = dt.Rows(0)("Discount")
            txtItemPOS.Text = dt.Rows(0)("ItemPOS")
            txtRemark.Text = dt.Rows(0)("Remark")


            'cmd.Parameters.AddWithValue("@sales1", Sale1)
            'cmd.Parameters.AddWithValue("@sales2", Sale2)
            'cmd.Parameters.AddWithValue("@sales3", Sale3)
            'cmd.Parameters.AddWithValue("@sales4", Sale4)
            'cmd.Parameters.AddWithValue("@packing", packing)

            If dt.Rows(0)("Sales1") = "YES" Then
                chkSale1.Checked = True
            End If
            If dt.Rows(0)("Sales2") = "YES" Then
                chkSale2.Checked = True
            End If
            If dt.Rows(0)("Sales3") = "YES" Then
                chkSale3.Checked = True
            End If
            If dt.Rows(0)("Sales4") = "YES" Then
                chkSale4.Checked = True
            End If

            If dt.Rows(0)("packing") = "YES" Then
                rdoPacking.Items.FindByValue("YES").Selected = True
            ElseIf dt.Rows(0)("packing") = "NO" Then
                rdoPacking.Items.FindByValue("NO").Selected = True
            End If

            txtSale1_text.Text = dt.Rows(0)("sales1_text")
            txtSale2_text.Text = dt.Rows(0)("sales2_text")
            txtSale3_text.Text = dt.Rows(0)("sales3_text")
            txtSale4_text.Text = dt.Rows(0)("sales4_text")
            txtPacking_detail.Text = dt.Rows(0)("packing_detail")

            txtPrice1_1.Text = dt.Rows(0)("price1_1")
            txtPrice1_2.Text = dt.Rows(0)("price1_2")
            txtPrice1_3.Text = dt.Rows(0)("price1_3")
            txtPrice1_4.Text = dt.Rows(0)("price1_4")
            txtPrice2_1.Text = dt.Rows(0)("price2_1")
            txtPrice2_2.Text = dt.Rows(0)("price2_2")
            txtPrice2_3.Text = dt.Rows(0)("price2_3")
            txtPrice2_4.Text = dt.Rows(0)("price2_4")
            txtPrice3_1.Text = dt.Rows(0)("price3_1")
            txtPrice3_2.Text = dt.Rows(0)("price3_2")
            txtPrice3_3.Text = dt.Rows(0)("price3_3")
            txtPrice3_4.Text = dt.Rows(0)("price3_4")
            txtPrice4_1.Text = dt.Rows(0)("price4_1")
            txtPrice4_2.Text = dt.Rows(0)("price4_2")
            txtPrice4_3.Text = dt.Rows(0)("price4_3")
            txtPrice4_4.Text = dt.Rows(0)("price4_4")

            txtPrice1_5.Text = dt.Rows(0)("price1_5")
            txtPrice2_5.Text = dt.Rows(0)("price2_5")
            txtPrice3_5.Text = dt.Rows(0)("price3_5")
            txtPrice4_5.Text = dt.Rows(0)("price4_5")

            txtProduct_code.Text = dt.Rows(0)("product_code")

            'cmd.Parameters.AddWithValue("@sugar", sugar)
            If dt.Rows(0)("sugar") = "YES" Then
                rdoSuger.Items.FindByValue("YES").Selected = True
            ElseIf dt.Rows(0)("sugar") = "NO" Then
                rdoSuger.Items.FindByValue("NO").Selected = True
            End If

            txtProduct_age.Text = dt.Rows(0)("product_age")
            txtBarcode_pcs.Text = dt.Rows(0)("barcode_pcs")
            txtBarcode_case.Text = dt.Rows(0)("barcode_case")
            txtPrimary_group.Text = dt.Rows(0)("primary_group")
            txtProduct_category.Text = dt.Rows(0)("primary_cat")

            'cmd.Parameters.AddWithValue("@LFDC", LFDC)
            If dt.Rows(0)("LFDC") = "YES" Then
                rdoLFDC.Items.FindByValue("YES").Selected = True
            ElseIf dt.Rows(0)("LFDC") = "NO" Then
                rdoLFDC.Items.FindByValue("NO").Selected = True
            End If

            txtPcs_wide.Text = dt.Rows(0)("pcs_wide")
            txtPcs_long.Text = dt.Rows(0)("pcs_long")
            txtPcs_hight.Text = dt.Rows(0)("pcs_hight")
            txtPcs_weight.Text = dt.Rows(0)("pcs_weight")
            txtCase_wide.Text = dt.Rows(0)("case_wide")
            txtCase_long.Text = dt.Rows(0)("case_long")
            txtCase_hight.Text = dt.Rows(0)("case_hight")
            txtCase_weight.Text = dt.Rows(0)("case_weight")
            txtCount_stock.Text = dt.Rows(0)("count_stock")
            txtSales_unit.Text = dt.Rows(0)("sales_unit")
            txtPurchase_unit.Text = dt.Rows(0)("purchase_unit")


            If Not IsDBNull(dt.Rows(0)("image")) Then
                Image1.ImageUrl = ReadImage(dt.Rows(0)("Item"))
            End If

        End If
    End Sub


    Function ReadImage(ByVal strID As String)
        Return ("Readimage.aspx?ImageID=" & strID)
    End Function


    Protected Sub drpRequester_Init(sender As Object, e As EventArgs) Handles drpRequester.Init
        Dim sqlCon As New SqlConnection(db_HR.sqlCon)
        Dim sqlCmd As New SqlCommand
        Dim Rs As SqlDataReader
        Dim sql As String

        sql = "select [employee_id] as id,[fullname] from [TB_Employee] "

        If sqlCon.State = ConnectionState.Closed Then
            sqlCon = New SqlConnection(db_HR.sqlCon)
            sqlCon.Open()
        End If
        With sqlCmd
            .Connection = sqlCon
            .CommandType = CommandType.Text
            .CommandText = sql
            Rs = .ExecuteReader
        End With
        drpRequester.Items.Clear()
        drpRequester.Items.Add(New ListItem("", 0))
        While Rs.Read
            drpRequester.Items.Add(New ListItem(Rs.GetString(0) + " : " + Rs.GetString(1), Rs.GetString(0)))
        End While
    End Sub


    Protected Sub drpProductType_Init(sender As Object, e As EventArgs) Handles drpProductType.Init
        Dim sqlCon As New SqlConnection(db_Mac5.sqlCon)
        Dim sqlCmd As New SqlCommand
        Dim Rs As SqlDataReader
        Dim sql As String

        sql = "select 'อื่นๆ' union all select isnull(ProductType,'')  from [TB_ProductMasterGroupRequest]  group by  [ProductType]  union all select isnull(ProductType,'')  from [TB_ProductMasterGroup]  group by  [ProductType] "

        If sqlCon.State = ConnectionState.Closed Then
            sqlCon = New SqlConnection(db_Mac5.sqlCon)
            sqlCon.Open()
        End If
        With sqlCmd
            .Connection = sqlCon
            .CommandType = CommandType.Text
            .CommandText = sql
            Rs = .ExecuteReader
        End With
        drpProductType.Items.Clear()
        drpProductType.Items.Add(New ListItem("", 0))
        While Rs.Read
            drpProductType.Items.Add(New ListItem(Rs.GetString(0), Rs.GetString(0)))
        End While
    End Sub

    Protected Sub drpProductCat_Init(sender As Object, e As EventArgs) Handles drpProductCat.Init
        Dim sqlCon As New SqlConnection(db_Mac5.sqlCon)
        Dim sqlCmd As New SqlCommand
        Dim Rs As SqlDataReader
        Dim sql As String

        sql = "select 'อื่นๆ' union all select isnull(ProductCategory,'')  from [TB_ProductMasterGroupRequest]  group by  [ProductCategory]  union all select isnull(ProductCategory,'')  from [TB_ProductMasterGroup]  group by  [ProductCategory]"

        If sqlCon.State = ConnectionState.Closed Then
            sqlCon = New SqlConnection(db_Mac5.sqlCon)
            sqlCon.Open()
        End If
        With sqlCmd
            .Connection = sqlCon
            .CommandType = CommandType.Text
            .CommandText = sql
            Rs = .ExecuteReader
        End With
        drpProductCat.Items.Clear()
        drpProductCat.Items.Add(New ListItem("", 0))
        While Rs.Read
            drpProductCat.Items.Add(New ListItem(Rs.GetString(0), Rs.GetString(0)))
        End While
    End Sub

    Protected Sub drpProductGroup_Init(sender As Object, e As EventArgs) Handles drpProductGroup.Init
        Dim sqlCon As New SqlConnection(db_Mac5.sqlCon)
        Dim sqlCmd As New SqlCommand
        Dim Rs As SqlDataReader
        Dim sql As String

        sql = "select 'อื่นๆ' union all select isnull(ProductGroup,'')  from [TB_ProductMasterGroupRequest]  group by  [ProductGroup]  union all select isnull(ProductGroup,'')  from [TB_ProductMasterGroup]  group by  [ProductGroup] "

        If sqlCon.State = ConnectionState.Closed Then
            sqlCon = New SqlConnection(db_Mac5.sqlCon)
            sqlCon.Open()
        End If
        With sqlCmd
            .Connection = sqlCon
            .CommandType = CommandType.Text
            .CommandText = sql
            Rs = .ExecuteReader
        End With
        drpProductGroup.Items.Clear()
        drpProductGroup.Items.Add(New ListItem("", 0))
        While Rs.Read
            drpProductGroup.Items.Add(New ListItem(Rs.GetString(0), Rs.GetString(0)))
        End While
    End Sub

    Protected Sub drpGroup_Init(sender As Object, e As EventArgs) Handles drpGroup.Init
        Dim sqlCon As New SqlConnection(db_Mac5.sqlCon)
        Dim sqlCmd As New SqlCommand
        Dim Rs As SqlDataReader
        Dim sql As String

        sql = "select isnull(GroupHead,'')  from [TB_ProductMasterGroupRequest]  group by  isnull(GroupHead,'') "

        If sqlCon.State = ConnectionState.Closed Then
            sqlCon = New SqlConnection(db_Mac5.sqlCon)
            sqlCon.Open()
        End If
        With sqlCmd
            .Connection = sqlCon
            .CommandType = CommandType.Text
            .CommandText = sql
            Rs = .ExecuteReader
        End With
        drpGroup.Items.Clear()
        drpGroup.Items.Add(New ListItem("", 0))
        While Rs.Read
            drpGroup.Items.Add(New ListItem(Rs.GetString(0), Rs.GetString(0)))
        End While
    End Sub


    'Protected Sub chkCompany_SelectedIndexChanged(sender As Object, e As EventArgs) Handles chkCompany.SelectedIndexChanged
    '    If chkCompany.Items.FindByValue("kvn").Selected = True And chkCompany.Items.FindByValue("lion").Selected = False Then

    '        'ProductType
    '        Dim sqlCon As New SqlConnection(db_Mac5.sqlCon)
    '        Dim sqlCmd As New SqlCommand
    '        Dim Rs As SqlDataReader
    '        Dim sql As String

    '        sql = "select ProductType  from [TB_ProductMasterGroup]  group by  [ProductType] where Company = 'KVN' "

    '        If sqlCon.State = ConnectionState.Closed Then
    '            sqlCon = New SqlConnection(db_Mac5.sqlCon)
    '            sqlCon.Open()
    '        End If
    '        With sqlCmd
    '            .Connection = sqlCon
    '            .CommandType = CommandType.Text
    '            .CommandText = sql
    '            Rs = .ExecuteReader
    '        End With
    '        drpProductType.Items.Clear()
    '        drpProductType.Items.Add(New ListItem("", 0))
    '        While Rs.Read
    '            drpProductType.Items.Add(New ListItem(Rs.GetString(0), Rs.GetString(0)))
    '        End While

    '    ElseIf chkCompany.Items.FindByValue("kvn").Selected = False And chkCompany.Items.FindByValue("lion").Selected = True Then

    '        'ProductType
    '        Dim sqlCon As New SqlConnection(db_Mac5.sqlCon)
    '        Dim sqlCmd As New SqlCommand
    '        Dim Rs As SqlDataReader
    '        Dim sql As String

    '        sql = "select ProductType  from [TB_ProductMasterGroup]  group by  [ProductType] where Company = 'LION' "

    '        If sqlCon.State = ConnectionState.Closed Then
    '            sqlCon = New SqlConnection(db_Mac5.sqlCon)
    '            sqlCon.Open()
    '        End If
    '        With sqlCmd
    '            .Connection = sqlCon
    '            .CommandType = CommandType.Text
    '            .CommandText = sql
    '            Rs = .ExecuteReader
    '        End With
    '        drpProductType.Items.Clear()
    '        drpProductType.Items.Add(New ListItem("", 0))
    '        While Rs.Read
    '            drpProductType.Items.Add(New ListItem(Rs.GetString(0), Rs.GetString(0)))
    '        End While


    '    End If


    'End Sub


    Sub UpdateStatus()
        Dim x As Integer
        Dim db_Mac5 As New Connect_Mac5
        Dim Cn As New SqlConnection(db_Mac5.sqlCon)

        Dim sSql As String = "UPDATE [TB_ProductMasterGroupRequest] SET  "
        sSql += " Stat='Sent'"

        sSql += " ,ModifyBy=@ModifyBy ,ModifyDate=@ModifyDate "

        sSql += " Where Item ='" & txtItem.Text & "' "

        Dim command As SqlCommand = New SqlCommand(sSql, Cn)
        Try
            command.Parameters.Add("@ModifyBy", Data.SqlDbType.VarChar).Value = username
            command.Parameters.Add("@ModifyDate", Data.SqlDbType.VarChar).Value = DateTime.Now.ToString("yyyy-MM-dd")

            command.CommandType = Data.CommandType.Text
            Cn.Open()
            x = command.ExecuteScalar
        Catch x1 As Exception
            Err.Raise(60002, , x1.Message)
        Finally
            Cn.Close()
        End Try

    End Sub


    Sub SentMail()
        '''''http://projectsvbnet.blogspot.com/2013/09/email-vbnet.html
        'LeaveOnline@aromathailandapp.com
        'leaveonline2016
        'mail.yourdomain.com
        'smtp port 25

        Dim i As Integer = 1
        Dim Role As String = ""
        Dim sql As String
        Dim name As String
        Dim strBody As String

        sql = "SELECT fullname  "
        sql = sql & "  FROM [dbo].[TB_Employee] "
        sql = sql & " where Employee_id = '" & username & "'"
        dt = db_HR.GetDataTable(sql)
        If dt.Rows.Count > 0 Then
            name = dt.Rows(0)("Fullname")
        End If


        'If Len(txtMyemail.Text) = 0 Or Len(txtMypassword.Text) = 0 Then
        '    MsgBox("ค่าอีเมล์โฮสต์หรือรหัสผ่านโฮสต์ยังไม่ได้ป้อนข้อมูล", MsgBoxStyle.Critical + MsgBoxStyle.ApplicationModal, "ล้มเหลว")
        '    txtMyemail.Focus()
        'Else
        Try
            'If Len(txtTo.Text) = 0 Then
            '    MsgBox("คุณต้องป้อนอีเมล์ที่จะส่งก่อนทำการส่ง", MsgBoxStyle.Critical + MsgBoxStyle.ApplicationModal, "ล้มเหลว")
            'Else
            'โฮสต์ และ เลขพอร์ต ที่คุณจะส่งอีเมล
            'และคุณต้องมีบัญชีของ Gmail ถ้าจะใช้ smtp.gmail.com
            Dim smtp As New SmtpClient("mail.aromathailandapp.com", 25)
            'สร้างตัวแปร eMailmessage เก็บการส่งข้อความอีเมล
            Dim eMailmessage As New System.Net.Mail.MailMessage() 'New MailMessage
            'กำหนดความปลอดภัยในการเข้ารหัสการส่งข้อความให้เป็น True
            smtp.EnableSsl = False
            'ติดตั้งโฮสต์ของ Gmail
            'smtp.Credentials = New System.Net.NetworkCredential("aroma.leave@gmail.com", "aroma1991")
            smtp.Credentials = New System.Net.NetworkCredential("Product@aromathailandapp.com", "product2018")
            'กำค่าส่วนประกอบของอีเมล์ เช่น ข้อความ, หัวข้อ, จากผู้ส่ง และ ผู้รับ
            ' eMailmessage.Subject = "แจ้งยอดการชำระเงิน"
            eMailmessage.Subject = "แจ้งขอตั้งรหัสสินค้าใหม่  : " & txtItem.Text

            strBody = "<html>" & vbCrLf
            strBody = strBody & "<body>" & vbCrLf
            strBody = strBody & "<FONT face=Tahoma>" & vbCrLf

            strBody = strBody & "<table>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td> เรียน  หน่วยงานบัญชี" & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>" & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td height=20  valign=middle style=font-family:Georgia, 'Times New Roman', Times, serif; font-size:30px; color:#0033cc;>" & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>" & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf


            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>ข้อมูลการขอตั้งรหัสสินค้าใหม่" & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>" & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>รหัสสินค้าหลัก : " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>" & txtItem.Text & " " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>ชื่อสินค้า : " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>" & txtItemDescription.Text & " " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>ประเภทสินค้า : " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>" & drpProductCat.SelectedValue & " " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>ชื่อสินค้า 1 / Brand : " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>" & txtBrand.Text & " " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>ชื่อสินค้า 2 / Model : " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>" & txtModel.Text & " " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>ชื่อสินค้า 3 / หัวชง : " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>" & drpGroup.SelectedValue & " " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>ชื่อสินค้า 3 / สี : " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>" & txtColor.Text & " " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>Memo / Option 1 : " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>" & txtOption1.Text & " " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>Memo / Option 2 : " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>" & txtOption2.Text & " " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>Memo / Option 3 : " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>" & txtOption3.Text & " " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf


            strBody = strBody & "</table>" & vbCrLf

            strBody = strBody & "<table>" & vbCrLf
            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td> รายละเอียดตาม Link ด้านล้างค่ะ  " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>http://10.0.24.20/ProductMaster/NewMaster.aspx?Type=View&Item=" & txtItem.Text & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td height=20  valign=middle style=font-family:Georgia, 'Times New Roman', Times, serif; font-size:30px; color:#0033cc;>" & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td> จึงเรียนมาเพื่อทราบ" & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>" & name & " " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf
            strBody = strBody & "</table>" & vbCrLf

            strBody = strBody & "</FONT >" & vbCrLf
            strBody = strBody & "</body>" & vbCrLf
            strBody = strBody & "<html>"

            eMailmessage.Body = strBody
            eMailmessage.IsBodyHtml = True

            eMailmessage.From = New MailAddress("Product@aromathailandapp.com", "Product Master")

            eMailmessage.To.Add("Haruthaic@aromathailand.com,panittas@aromathailand.com,sudaratche@aromathailand.com")
            'eMailmessage.To.Add("Haruthaic@aromathailand.com")
            'eMailmessage.To.Add(email)


            smtp.Send(eMailmessage)
            'MsgBox("ส่งอีเมล์สำเร็จ", vbInformation, "รายงานผล")
            '   End If
        Catch ex As Exception
            ' MsgBox("ข้อมูลผิดพลาด กรุณาตรวจสอบข้อมูลอีกครั้ง", MsgBoxStyle.Critical + MsgBoxStyle.ApplicationModal, "การทำงานผิดพลาด")
            sms.Msg = "ข้อมูลผิดพลาด กรุณาตรวจสอบข้อมูลอีกครั้ง"
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "window.alert('" & sms.Msg & "');", True)
        End Try
        ' End If
    End Sub

    Protected Sub drpRequester_SelectedIndexChanged(sender As Object, e As EventArgs) Handles drpRequester.SelectedIndexChanged
        sql = " select * "
        sql += " from Tb_employee "
        sql += " where Employee_id = '" & drpRequester.SelectedValue & "' "
        dt = db_HR.GetDataTable(sql)
        If dt.Rows.Count > 0 Then
            txtDep.Text = dt.Rows(0)("Requester")
        End If
    End Sub

    Protected Sub drpProductType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles drpProductType.SelectedIndexChanged
        If drpProductType.SelectedValue = "อื่นๆ" Then
            lblProductType.Visible = True
            txtProductType.Visible = True
        Else
            lblProductType.Visible = False
            txtProductType.Visible = False
        End If
    End Sub

    Protected Sub drpProductCat_SelectedIndexChanged(sender As Object, e As EventArgs) Handles drpProductCat.SelectedIndexChanged
        If drpProductCat.SelectedValue = "อื่นๆ" Then
            lblProductCat.Visible = True
            txtProductCat.Visible = True
        Else
            lblProductCat.Visible = False
            txtProductCat.Visible = False
        End If
    End Sub

    Protected Sub drpProductGroup_SelectedIndexChanged(sender As Object, e As EventArgs) Handles drpProductGroup.SelectedIndexChanged
        If drpProductGroup.SelectedValue = "อื่นๆ" Then
            lblProductGroup.Visible = True
            txtProductGroup.Visible = True
        Else
            lblProductGroup.Visible = False
            txtProductGroup.Visible = False
        End If
    End Sub

    Protected Sub btnSaveIMG_Click(sender As Object, e As EventArgs) Handles btnSaveIMG.Click
        If FileUpload1.FileName <> "" Then

            sql = " select * "
            sql += " from [TB_ProductMasterGroupRequest] "
            sql += " where Item = '" & txtItem.Text & "' "
            dt = db_Mac5.GetDataTable(sql)
            If dt.Rows.Count >= 1 Then
                'UpdateData()

                Call insertIMG()
                insertLogo()  ' LOGO

            Else

                'If chkCompany.Items.FindByValue("kvn").Selected = False And chkCompany.Items.FindByValue("lion").Selected = False Then
                '    sms.Msg = "กรุณาระบุบริษัท"
                '    Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "window.alert('" & sms.Msg & "');", True)
                '    Exit Sub
                'End If

                If txtItem.Text = "" Then
                    sms.Msg = "กรุณาระบุรหัสสินค้าหลัก"
                    Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "window.alert('" & sms.Msg & "');", True)
                    Exit Sub
                End If

                InsertData()

                Call insertIMG()
                insertLogo()  ' LOGO

            End If

        End If

        LoadData()

    End Sub

    Sub insertIMG()
        Dim CurrentFileName As String
        CurrentFileName = FileUpload1.FileName

        'If (Path.GetExtension(CurrentFileName).ToLower <> ".jpg") Then
        '    sms.Msg = "You choose the file dishonestly !!! , Please choose be file .jpg"
        '    Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "window.alert('" & sms.Msg & "');", True)
        '    Exit Sub
        'End If

        'If FileUpload1.PostedFile.ContentLength > 131072 Then
        '    sms.Msg = "The size of big too file , the size of the file must 128 KB not exceed!!!"
        '    Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "window.alert('" & sms.Msg & "');", True)
        '    Exit Sub
        'End If

        Dim CurrentPath As String = Server.MapPath("~/Temp/")

        FileUpload1.PostedFile.SaveAs(CurrentPath & FileUpload1.FileName)
        FileUpload1.PostedFile.SaveAs("c:\Temp\" & FileUpload1.FileName)
        Image1.ImageUrl = CurrentPath & FileUpload1.FileName
        txtpic.Text = "c:\Temp\" & FileUpload1.FileName

    End Sub

    Sub insertLogo()
        Dim db As New Connect_Mac5
        Dim sql As String
        Dim tr As SqlTransaction
        Dim sqlcon As New SqlConnection(db_Mac5.sqlCon)

        If sqlcon.State = ConnectionState.Closed Then
            sqlcon = New SqlConnection(db_Mac5.sqlCon)
            sqlcon.Open()
        End If

        tr = sqlcon.BeginTransaction

        Try
            Dim cmd As SqlCommand = New SqlCommand(sql, sqlcon, tr)
            If txtpic.Text <> "" Then
                Dim FileN As FileStream
                Dim Ln As Int32
                Dim oBinaryReader As BinaryReader
                Dim oImgByteArray As Byte()

                FileN = New FileStream(txtpic.Text, FileMode.Open, FileAccess.Read)
                oBinaryReader = New BinaryReader(FileN)
                oImgByteArray = oBinaryReader.ReadBytes(CInt(FileN.Length))
                Ln = CInt(FileN.Length)
                oBinaryReader.Close()
                FileN.Close()

                sql = "update [TB_ProductMasterGroupRequest] set image_name=@FF1,image=@FF2 where Item  = '" & txtItem.Text & "'"
                Dim cmd3 As SqlCommand = New SqlCommand(sql, sqlcon, tr)
                cmd3.Parameters.Add("@FF1", SqlDbType.VarChar, 0).Value = txtItem.Text + Path.GetExtension(txtpic.Text).ToLower
                cmd3.Parameters.Add("@FF2", SqlDbType.Image, 0).Value = oImgByteArray
                cmd3.CommandTimeout = 0
                cmd3.ExecuteNonQuery()
            End If

            tr.Commit()
            sqlcon.Close()
        Catch ex As Exception
            tr.Rollback()
            sqlcon.Close()
            Exit Sub
        End Try

    End Sub


    Protected Sub btnReport_Click(sender As Object, e As EventArgs) Handles btnReport.Click

        Dim sqlReport As String = " 1=1 "

        sqlReport = sqlReport & " and {ReportData.item} = '" & item & "'"


        Server.Transfer("LoadReport_Excel.aspx?Form=NewProduct&sqlReport=" & sqlReport)

    End Sub


End Class