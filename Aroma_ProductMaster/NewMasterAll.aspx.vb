﻿Imports System.Data.SqlClient

Public Class NewMasterAll
    Inherits System.Web.UI.Page
    Dim db_Product As New Connect_product
    Dim sql, sql2, username, CallID, Type, item As String


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        username = Request.QueryString("usr")

        If Not IsPostBack Then
            BindData()
        End If

    End Sub

    Protected Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        BindData()
    End Sub

    Private Sub BindData()

        Dim IntPageNumber As Int32 = 0
        Dim sqlcon As New SqlConnection(db_Product.sqlCon)
        Dim cmd As New SqlCommand
        Dim DSet As New DataSet
        Dim sql As String
        Dim constr As String

        Try
            constr = "data source=10.0.24.20;initial catalog=DB_RequestNewProduct;persist security info=false;User ID=armth;Password=Kb5r#Ge9Z3M*mQ;Connect Timeout=0;Max Pool Size=500;Enlist=true"
            sqlcon = New SqlConnection(constr)
            sqlcon.Open()
            'Return True
            'ถ้าเกิด Error ขึ้นให้ใช้ 
        Catch
            Try
                constr = "data source=10.0.24.20;initial catalog=DB_RequestNewProduct;persist security info=false;User ID=armth;Password=Kb5r#Ge9Z3M*mQ;Connect Timeout=0;Pooling=false"
                sqlcon = New SqlConnection(constr)
                sqlcon.Open()
                'Return True
            Catch
                'Return False
            End Try

        End Try
        Try

            sql = "select '" & username & "' as username,'click' as detail,* " & vbCrLf
            sql = sql & " from VW_ProductRequest  "
            sql = sql & " where 1=1  "


            'If chkKVN.Checked = True Or chkLION.Checked = True Then
            '    sql = sql & " and Company_kvn = '/' "
            'End If

            'If chkLION.Checked = True Then
            '    sql = sql & " and Company_lion = '/' "
            'End If


            If txtItem.Text <> "" Then
                sql = sql & " and [Item] like '%" & txtItem.Text & "%' "
            End If

            If txtItemDesc.Text <> "" Then
                sql = sql & " and [ItemDescriptionTH] like '%" & txtItemDesc.Text & "%' "
            End If

            If drpProductType.SelectedValue <> "0" Then
                sql = sql & " and [ProductType] = '" & drpProductType.SelectedValue & "' "
            End If

            If drpProductCat.SelectedValue <> "0" Then
                sql = sql & " and [ProductCategory] = '" & drpProductCat.SelectedValue & "' "
            End If

            If drpProductGroup.SelectedValue <> "0" Then
                sql = sql & " and [ProductGroup] = '" & drpProductGroup.SelectedValue & "' "
            End If

            sql = sql & " Order by createdate  desc "

            Dim dt As New DataTable
            dt = db_Product.GetDataTable(sql)
            If dt.Rows.Count > 0 Then
                GridView.DataSource = dt
                GridView.DataBind()
            Else
                ShowNoResultFound(dt, GridView)
            End If

        Catch


        End Try
        sqlcon.Close()

        'Gridview1
        'Attribute to show the Plus Minus Button.
        GridView.HeaderRow.Cells(0).Attributes("data-class") = "expand"
        'Attribute to hide column in Phone.
        GridView.HeaderRow.Cells(2).Attributes("data-hide") = "phone"
        GridView.HeaderRow.Cells(3).Attributes("data-hide") = "phone"
        'Adds THEAD and TBODY to GridView.
        GridView.HeaderRow.TableSection = TableRowSection.TableHeader

    End Sub

    Protected Sub OnPaging(ByVal sender As Object, ByVal e As GridViewPageEventArgs)
        Me.BindData()
        GridView.PageIndex = e.NewPageIndex
        GridView.DataBind()
    End Sub

    Sub ShowNoResultFound(ByVal dtt As DataTable, ByVal gv As GridView)
        dtt.Rows.Add(dtt.NewRow)
        gv.DataSource = dtt
        gv.DataBind()
        Dim columnsCount As Integer = gv.Columns.Count
        gv.Rows(0).Cells.Clear()
        gv.Rows(0).Cells.Add(New TableCell)
        gv.Rows(0).Cells(0).ColumnSpan = columnsCount
        gv.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
        gv.Rows(0).Cells(0).ForeColor = Drawing.Color.Red
        gv.Rows(0).Cells(0).Font.Bold = True
        gv.Rows(0).Cells(0).Text = "-ไม่พบข้อมูล-"
    End Sub

    Protected Sub drpProductType_Init(sender As Object, e As EventArgs) Handles drpProductType.Init
         Dim sqlCon As New SqlConnection(db_product.sqlCon)
        Dim sqlCmd As New SqlCommand
        Dim Rs As SqlDataReader
        Dim sql As String


        sql = "SELECT [ProductType] FROM [dbo].[TB_ProductMaster] group by [ProductType]  "


        If sqlCon.State = ConnectionState.Closed Then
            sqlCon = New SqlConnection(db_Product.sqlCon)
            sqlCon.Open()
        End If
        With sqlCmd
            .Connection = sqlCon
            .CommandType = CommandType.Text
            .CommandText = sql
            Rs = .ExecuteReader
        End With
        drpProductType.Items.Clear()
        drpProductType.Items.Add(New ListItem("", 0))
        While Rs.Read
            drpProductType.Items.Add(New ListItem(Rs.GetString(0), Rs.GetString(0)))
        End While
    End Sub

    Protected Sub drpProductCat_Init(sender As Object, e As EventArgs) Handles drpProductCat.Init
        Dim sqlCon As New SqlConnection(db_product.sqlCon)
        Dim sqlCmd As New SqlCommand
        Dim Rs As SqlDataReader
        Dim sql As String


        sql = "SELECT [ProductCategory] FROM [dbo].[TB_ProductMaster] where ProductCategory is not null group by [ProductCategory]  "


        If sqlCon.State = ConnectionState.Closed Then
            sqlCon = New SqlConnection(db_Product.sqlCon)
            sqlCon.Open()
        End If
        With sqlCmd
            .Connection = sqlCon
            .CommandType = CommandType.Text
            .CommandText = sql
            Rs = .ExecuteReader
        End With
        drpProductCat.Items.Clear()
        drpProductCat.Items.Add(New ListItem("", 0))
        While Rs.Read
            drpProductCat.Items.Add(New ListItem(Rs.GetString(0), Rs.GetString(0)))
        End While
    End Sub

    Protected Sub drpProductGroup_Init(sender As Object, e As EventArgs) Handles drpProductGroup.Init
        Dim sqlCon As New SqlConnection(db_product.sqlCon)
        Dim sqlCmd As New SqlCommand
        Dim Rs As SqlDataReader
        Dim sql As String


        sql = "SELECT [ProductGroup] FROM [dbo].[TB_ProductMaster] where ProductGroup is not null group by [ProductGroup]  "


        If sqlCon.State = ConnectionState.Closed Then
            sqlCon = New SqlConnection(db_Product.sqlCon)
            sqlCon.Open()
        End If
        With sqlCmd
            .Connection = sqlCon
            .CommandType = CommandType.Text
            .CommandText = sql
            Rs = .ExecuteReader
        End With
        drpProductGroup.Items.Clear()
        drpProductGroup.Items.Add(New ListItem("", 0))
        While Rs.Read
            drpProductGroup.Items.Add(New ListItem(Rs.GetString(0), Rs.GetString(0)))
        End While
    End Sub

   
    Protected Sub btnExport_Click(sender As Object, e As EventArgs) Handles btnExport.Click
        Dim sqlReport As String = " 1=1 "


        If txtItem.Text <> "" Then
            sqlReport = sqlReport & " and {ReportData.Item] like '%" & txtItem.Text & "%' "
        End If

        If txtItemDesc.Text <> "" Then
            sqlReport = sqlReport & " and {ReportData.ItemDescription} like '%" & txtItemDesc.Text & "%' "
        End If

       
        If drpProductType.SelectedValue <> "0" Then
            sqlReport = sqlReport & " and {ReportData.ProductType} = '" & drpProductType.SelectedValue & "' "
        End If

        If drpProductCat.SelectedValue <> "0" Then
            sqlReport = sqlReport & " and {ReportData.ProductCategory} = '" & drpProductCat.SelectedValue & "' "
        End If

        If drpProductGroup.SelectedValue <> "0" Then
            sqlReport = sqlReport & " and {ReportData.ProductGroup} = '" & drpProductGroup.SelectedValue & "' "
        End If


        Server.Transfer("LoadReport_Excel.aspx?Form=ProductRequest&sqlReport=" & sqlReport)
    End Sub


    Protected Sub drpProductType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles drpProductType.SelectedIndexChanged
        Dim sqlCon As New SqlConnection(db_Product.sqlCon)
        Dim sqlCmd As New SqlCommand
        Dim Rs As SqlDataReader
        Dim sql As String


        sql = "SELECT [ProductCategory] FROM [dbo].[TB_ProductMaster] "

        sql = sql & "where([ProductCategory] Is Not null)  and ProductType  = '" & drpProductType.SelectedValue & "' "

        sql = sql & "group by [ProductCategory]  "


        If sqlCon.State = ConnectionState.Closed Then
            sqlCon = New SqlConnection(db_Product.sqlCon)
            sqlCon.Open()
        End If
        With sqlCmd
            .Connection = sqlCon
            .CommandType = CommandType.Text
            .CommandText = sql
            Rs = .ExecuteReader
        End With
        drpProductCat.Items.Clear()
        drpProductCat.Items.Add(New ListItem("", 0))
        While Rs.Read
            drpProductCat.Items.Add(New ListItem(Rs.GetString(0), Rs.GetString(0)))
        End While
    End Sub

    Protected Sub drpProductCat_SelectedIndexChanged(sender As Object, e As EventArgs) Handles drpProductCat.SelectedIndexChanged
        Dim sqlCon As New SqlConnection(db_Product.sqlCon)
        Dim sqlCmd As New SqlCommand
        Dim Rs As SqlDataReader
        Dim sql As String


        sql = " SELECT [ProductGroup] FROM [dbo].[TB_ProductMaster] "
        sql = sql & " where  ProductCategory  = '" & drpProductCat.SelectedValue & "' "
        sql = sql & " and  ProductType  = '" & drpProductType.SelectedValue & "' "


        If sqlCon.State = ConnectionState.Closed Then
            sqlCon = New SqlConnection(db_Product.sqlCon)
            sqlCon.Open()
        End If
        With sqlCmd
            .Connection = sqlCon
            .CommandType = CommandType.Text
            .CommandText = sql
            Rs = .ExecuteReader
        End With
        drpProductGroup.Items.Clear()
        drpProductGroup.Items.Add(New ListItem("", 0))
        While Rs.Read
            drpProductGroup.Items.Add(New ListItem(Rs.GetString(0), Rs.GetString(0)))
        End While
    End Sub
End Class