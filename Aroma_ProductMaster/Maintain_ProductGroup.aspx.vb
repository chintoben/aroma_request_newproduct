﻿Imports System.Data.SqlClient
Imports System.Data.OleDb

Public Class Maintain_ProductGroup
    Inherits System.Web.UI.Page
    Dim db_product As New Connect_product
    Dim sql, sql2, username, CallID, Type, item As String

    Dim dt, dt1, dt2, dt3, dtcall, dtopen, dtKpi, dtCom As New DataTable
    Dim id As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        username = Request.QueryString("usr")

        If Not IsPostBack Then
            'BindData()
        End If
    End Sub


    'Private Sub BindData()

    '    Dim IntPageNumber As Int32 = 0
    '    Dim sqlcon As New SqlConnection(db_product.sqlCon)
    '    Dim cmd As New SqlCommand
    '    Dim DSet As New DataSet
    '    Dim sql As String
    '    Dim constr As String

    '    Try
    '        constr = "data source=10.0.24.20;initial catalog=DB_RequestNewProduct;persist security info=false;User ID=armth;Password=Kb5r#Ge9Z3M*mQ;Connect Timeout=0;Max Pool Size=500;Enlist=true"
    '        sqlcon = New SqlConnection(constr)
    '        sqlcon.Open()
    '        'Return True
    '        'ถ้าเกิด Error ขึ้นให้ใช้ 
    '    Catch
    '        Try
    '            constr = "data source=10.0.24.20;initial catalog=DB_RequestNewProduct;persist security info=false;User ID=armth;Password=Kb5r#Ge9Z3M*mQ;Connect Timeout=0;Pooling=false"
    '            sqlcon = New SqlConnection(constr)
    '            sqlcon.Open()
    '            'Return True
    '        Catch
    '            'Return False
    '        End Try

    '    End Try
    '    Try

    '        sql = "SELECT [ProductType],[ProductCategory],[ProductGroup],[ProductDesc] FROM [dbo].[TB_ProductMaster] "
    '        sql = sql & " group by [ProductType],[ProductCategory],[ProductGroup],[ProductDesc] " & vbCrLf


    '        'sql = sql & " Order by ProductType  "

    '        Dim dt As New DataTable
    '        dt = db_product.GetDataTable(sql)
    '        If dt.Rows.Count > 0 Then
    '            GridView.DataSource = dt
    '            GridView.DataBind()
    '        Else
    '            ShowNoResultFound(dt, GridView)
    '        End If

    '    Catch


    '    End Try
    '    sqlcon.Close()

    '    'Gridview1
    '    'Attribute to show the Plus Minus Button.
    '    GridView.HeaderRow.Cells(0).Attributes("data-class") = "expand"
    '    'Attribute to hide column in Phone.
    '    GridView.HeaderRow.Cells(2).Attributes("data-hide") = "phone"
    '    ' GridView.HeaderRow.Cells(3).Attributes("data-hide") = "phone"
    '    'Adds THEAD and TBODY to GridView.
    '    GridView.HeaderRow.TableSection = TableRowSection.TableHeader

    'End Sub

    'Protected Sub OnPaging(ByVal sender As Object, ByVal e As GridViewPageEventArgs)
    '    Me.BindData()
    '    GridView.PageIndex = e.NewPageIndex
    '    GridView.DataBind()
    'End Sub

    Sub ShowNoResultFound(ByVal dtt As DataTable, ByVal gv As GridView)
        dtt.Rows.Add(dtt.NewRow)
        gv.DataSource = dtt
        gv.DataBind()
        Dim columnsCount As Integer = gv.Columns.Count
        gv.Rows(0).Cells.Clear()
        gv.Rows(0).Cells.Add(New TableCell)
        gv.Rows(0).Cells(0).ColumnSpan = columnsCount
        gv.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
        gv.Rows(0).Cells(0).ForeColor = Drawing.Color.Red
        gv.Rows(0).Cells(0).Font.Bold = True
        gv.Rows(0).Cells(0).Text = "-ไม่พบข้อมูล-"
    End Sub

    Protected Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click


        'check ข้อมูลว่าง
        sql = " select top 1 id,ProductType  from  [TB_ProductMaster] where  isnull([ProductType],'') = '" & drpProductType.SelectedValue & "' "
        sql += " and isnull([ProductCategory],'') = '" & drpProductCat.SelectedValue & "' and isnull([ProductGroup],'') = '' "
        dt = db_product.GetDataTable(sql)

        If dt.Rows.Count > 0 Then

            id = dt.Rows(0)("id")

            UpdateData(id)
        Else
            InsertData()
        End If


        'BindData()
        GridView1.DataBind()

    End Sub


    Sub UpdateData(ID)


        Dim Cn2 As New SqlConnection(db_product.sqlCon)
        Dim sqlcon2 As New OleDbConnection(db_product.cnPAStr)
        Dim sqlcom2 As New OleDbCommand

        Dim sSql2 As String = "UPDATE [TB_ProductMaster] SET  "
        sSql2 += " [ProductGroup] = '" & txtProductGroup.Text & "'"
        sql2 += " ,[ProductDesc] = '" & txtProductDesc.Text & "'"
        sSql2 += " ,[ModifyBy] = '" & username & "'"
        sSql2 += " ,[ModifyDate] = getdate() "
        sSql2 += " Where  [ID] = '" & ID & "'"


        If sqlcon2.State = ConnectionState.Closed Then
            sqlcon2 = New OleDbConnection(db_product.cnPAStr)
            sqlcon2.Open()
        End If

        With sqlcom2
            .Connection = sqlcon2
            .CommandType = CommandType.Text
            .CommandText = sSql2
            .ExecuteNonQuery()
        End With

    End Sub


    Sub InsertData()

        Dim query As String = "INSERT INTO [TB_ProductMaster] ("
        query &= "ProductType,ProductCategory,[ProductGroup],[ProductDesc],CreateBy,CreateDate"
        query &= ")"

        query += " VALUES( "
        query &= "@ProductType ,@ProductCategory,@ProductGroup,@ProductDesc,@CreateBy,@CreateDate"
        query &= ")"

        Dim constr As String = ConfigurationManager.ConnectionStrings("ProductConnectionString").ConnectionString

        Using con As SqlConnection = New SqlConnection(constr)
            Using cmd As SqlCommand = New SqlCommand(query)
                cmd.Parameters.AddWithValue("@ProductType", drpProductType.SelectedValue)
                cmd.Parameters.AddWithValue("@ProductCategory", drpProductCat.SelectedValue)
                cmd.Parameters.AddWithValue("@ProductGroup", txtProductGroup.Text)
                cmd.Parameters.AddWithValue("@ProductDesc", txtProductDesc.Text)


                cmd.Parameters.AddWithValue("@CreateBy", username)
                cmd.Parameters.AddWithValue("@CreateDate", DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss"))


                cmd.Connection = con
                con.Open()
                cmd.ExecuteNonQuery()
                con.Close()
            End Using
        End Using


    End Sub

    Protected Sub drpProductType_Init(sender As Object, e As EventArgs) Handles drpProductType.Init
        Dim sqlCon As New SqlConnection(db_product.sqlCon)
        Dim sqlCmd As New SqlCommand
        Dim Rs As SqlDataReader
        Dim sql As String


        sql = "SELECT [ProductType] FROM [dbo].[TB_ProductMaster] group by [ProductType]  "


        If sqlCon.State = ConnectionState.Closed Then
            sqlCon = New SqlConnection(db_product.sqlCon)
            sqlCon.Open()
        End If
        With sqlCmd
            .Connection = sqlCon
            .CommandType = CommandType.Text
            .CommandText = sql
            Rs = .ExecuteReader
        End With
        drpProductType.Items.Clear()
        drpProductType.Items.Add(New ListItem("", 0))
        While Rs.Read
            drpProductType.Items.Add(New ListItem(Rs.GetString(0), Rs.GetString(0)))
        End While
    End Sub

    Protected Sub drpProductCat_Init(sender As Object, e As EventArgs) Handles drpProductCat.Init
        Dim sqlCon As New SqlConnection(db_product.sqlCon)
        Dim sqlCmd As New SqlCommand
        Dim Rs As SqlDataReader
        Dim sql As String


        sql = "SELECT [ProductCategory] FROM [dbo].[TB_ProductMaster] "

        sql = sql & "where([ProductCategory] Is Not null)  and ProductType  = '" & drpProductType.SelectedValue & "' "

        sql = sql & "group by [ProductCategory]  "


        If sqlCon.State = ConnectionState.Closed Then
            sqlCon = New SqlConnection(db_product.sqlCon)
            sqlCon.Open()
        End If
        With sqlCmd
            .Connection = sqlCon
            .CommandType = CommandType.Text
            .CommandText = sql
            Rs = .ExecuteReader
        End With
        drpProductCat.Items.Clear()
        drpProductCat.Items.Add(New ListItem("", 0))
        While Rs.Read
            drpProductCat.Items.Add(New ListItem(Rs.GetString(0), Rs.GetString(0)))
        End While
    End Sub

    Protected Sub drpProductType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles drpProductType.SelectedIndexChanged
        Dim sqlCon As New SqlConnection(db_product.sqlCon)
        Dim sqlCmd As New SqlCommand
        Dim Rs As SqlDataReader
        Dim sql As String


        sql = "SELECT [ProductCategory] FROM [dbo].[TB_ProductMaster] "

        sql = sql & "where([ProductCategory] Is Not null)  and ProductType  = '" & drpProductType.SelectedValue & "' "

        sql = sql & "group by [ProductCategory]  "


        If sqlCon.State = ConnectionState.Closed Then
            sqlCon = New SqlConnection(db_product.sqlCon)
            sqlCon.Open()
        End If
        With sqlCmd
            .Connection = sqlCon
            .CommandType = CommandType.Text
            .CommandText = sql
            Rs = .ExecuteReader
        End With
        drpProductCat.Items.Clear()
        drpProductCat.Items.Add(New ListItem("", 0))
        While Rs.Read
            drpProductCat.Items.Add(New ListItem(Rs.GetString(0), Rs.GetString(0)))
        End While
    End Sub
End Class