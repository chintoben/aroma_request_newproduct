﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="NewMasterSparePart.aspx.vb" Inherits="Aroma_ProductMaster.NewMasterSparePart" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="Your description">
<meta name="keywords" content="Your keywords">
<meta name="author" content="Your name">
<link rel="icon" href="images/favicon.ico" type="image/x-icon">
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />

<link rel="stylesheet" href="css/bootstrap.css" type="text/css" media="screen">
<link rel="stylesheet" href="css/bootstrap-responsive.css" type="text/css" media="screen">    
<link rel="stylesheet" href="css/style.css" type="text/css" media="screen">

<script type="text/javascript" src="js/jquery.js"></script>  
<script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="js/superfish.js"></script>

<script type="text/javascript" src="js/jquery.ui.totop.js"></script>

<script type="text/javascript" src="js/cform.js"></script>
<script>
    $(document).ready(function () {
        //	


    }); //
    $(window).load(function () {
        //

    }); //
</script>		
    <style type="text/css">
        .style1
        {
            width: 6px;
        }
        .style2
        {
            height: 38px;
        }
        .style3
        {
            width: 6px;
            height: 38px;
        }
        .style4
        {
            height: 42px;
        }
        .style5
        {
            height: 22px;
        }
    </style>
 
 
 <link href="CSS3/CSS.css" rel="stylesheet" type="text/css" /> 
    <%--  <script src="scripts3/jquery-1.3.2.min.js" type="text/javascript"></script>--%>
    <script src="scripts3/jquery.blockUI.js" type="text/javascript"></script>
<script type = "text/javascript">
    function BlockUI(elementID) {
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(function () {
            $("#" + elementID).block({ message: '<table align = "center"><tr><td>' +
     '<img src="images/loadingAnim.gif"/></td></tr></table>',
                css: {},
                overlayCSS: { backgroundColor: '#000000', opacity: 0.6
                }
            });
        });
        prm.add_endRequest(function () {
            $("#" + elementID).unblock();
        });
    }
    $(document).ready(function () {

        BlockUI("<%=pnlAddEdit.ClientID %>");
        $.blockUI.defaults.css = {};
    });
    function Hidepopup() {
        $find("popup").hide();
        return false;
    }
</script> 



  <link rel="stylesheet" href="Tab/jquery-ui.css" />
    <script src="Tab/jquery-1.9.1.js"></script>
    <script src="Tab/jquery-ui.js"></script>



    <script type = "text/javascript">
        function SetTarget() {
            document.forms[0].target = "_blank";
        }
</script>

<script>
    $(function () {
        $("#tabs").tabs();
    });
    $(function () {
        $("#tabs2").tabs();
    });
   </script>
    <%--  <style type ="text/css" >
  .clr
  {
      color:Green;
      background-color:white ;
      width:90%;
      depth:500px;
      font-size :small ;
  }
  </style>--%>

  


</head>
<body>
    <form id="form1" runat="server">
    <div>
    
    <div id="main">
<div class="top1_wrapper">
<div class="top1">
<div class="container">
<div class="row">
<div class="span12">
<div class="top1_inner clearfix">
	
<div class="top2 clearfix">
<header><div class="logo_wrapper"><a href="index.html" class="logo">
        <img src="images/aroma/logotrans.png" alt="" >
        <%--<img src="images/aroma/Lion.png" alt="" width="100" >--%>
        </a></div>
        
  </header>	
<div class="top3 clearfix">
<div class="phone1">
	<div class="txt1">Request New Master</div>
	<div class="txt2">Product</div>
</div>

<div class="search-form-wrapper clearfix">

</div>
</div>

</div>

<div class="menu_wrapper">
<div class="navbar navbar_">
	<div class="navbar-inner navbar-inner_">
		<a class="btn btn-navbar btn-navbar_" data-toggle="collapse" data-target=".nav-collapse_">
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</a>
		<div class="nav-collapse nav-collapse_ collapse">
			<ul class="nav sf-menu clearfix">
            <li class="active"><a href="Main.aspx?usr=<%=Request.QueryString("usr") %>">Home</a></li>
            <li><a href="NewMaster_Type.aspx?usr=<%=Request.QueryString("usr") %>">Request New Master</a></li>
            <li><a href="NewMasterAll.aspx?usr=<%=Request.QueryString("usr") %>">Request New Master All</a></li>
            <li><a href="ProductMaster.aspx?usr=<%=Request.QueryString("usr") %>">Product Master</a></li>           
            <li><a href="Maintain_ProductType.aspx?usr=<%=Request.QueryString("usr") %>" target="_blank">จัดการสินค้า</a></li>
           
            <li><a href="Login.aspx">Sign Out</a></li>										
            </ul>
		</div>
	</div>
</div>	
</div>

</div>	
</div>	
</div>	
</div>	
</div>	
</div>
<div id="inner">

<div id="content">
<div class="container">
<div class="row">
<div class="span12">


<div class="breadcrumbs1">
<a href="Main.aspx?usr=<%=Request.QueryString("usr") %>">Home Page</a>
<span></span>

<a href="NewMaster_Type.aspx?usr=<%=Request.QueryString("usr") %>">Request New Master</a>
<span></span>
Spare Part
</div>

<%--
<div class="breadcrumbs1"><a href="Main.aspx?usr=<%=Request.QueryString("usr") %>">Home Page</a><span></span><a href="NewMaster_Type.aspx?usr=<%=Request.QueryString("usr") %>">Request New Master<span></span>Spart Part</div>


--%>

<%--<div class="row">
<div class="span8">
<h2>ตั้งรหัสสินค้าใหม่ : Spare Part</h2>
</div>	
</div>--%>


<h2>ตั้งรหัสสินค้าใหม่ : Spare Part</h2>

<div id="note"></div>
<div id="fields">

   <Table>

     <tr>
            <td class="style5">
                <asp:Label ID="Label51" runat="server" Font-Bold="True" Font-Size="Medium" 
                    Text="Doc No. " Width="100px"></asp:Label>
            </td>
         <td class="style5">
 
            <asp:Label ID="lblRequestID" runat="server" Font-Size="Medium" 
                Width="200px" Font-Bold="True">-</asp:Label>

            </td>
         <td class="style5"></td>
     </tr>
      <tr>
            <td>
                <asp:Label ID="Label52" runat="server" Font-Bold="True" Font-Size="Medium" 
                    Text="Staus : "></asp:Label>
            </td>
         <td>
 
      
            <asp:Label ID="lblStatus" runat="server" Font-Size="Medium" 
                Width="300px" Font-Bold="True" ForeColor="#009900">-</asp:Label>
 
            </td>
         <td></td>
     </tr>
     </Table>
    

    <asp:Panel ID="Panel1" runat="server">
    <div id="tabs" class ="clr">
  <ul>
  <li><a href="#home">ระบุสินค้าใหม่</a></li>
    <li><a href="#about">Upload File</a></li>    
  </ul>
  

  <div id="home" class="clr">
    
           <table>
   
  
              <tr>
                  <td class="style7">
                      <asp:Label ID="Label2" runat="server" Font-Bold="True" Font-Size="Medium" 
                          Text="บริษัท : " Width="100px"></asp:Label>
                      <br />
                  </td>
                  <td class="style2" colspan="8">
                      <asp:CheckBox ID="chkKVN" runat="server" 
                          Text="บจ. เค.วี.เอ็น.อิมปอร์ต เอ็กซ์ปอร์ต  (1991)" Width="260px" />
                      <asp:CheckBox ID="chkLION" runat="server" Checked="True" 
                          Text="บจ. ไลอ้อน ทรี-สตาร์" Width="129px" />
                  </td>
              </tr>

   
  
              <tr>
                  <td class="style13">
            <asp:Label ID="Label29" runat="server" Text="ชื่อผู้ขอ : " Font-Size="Medium" 
                Width="100px" Font-Bold="True"></asp:Label>
                  </td>
                  <td class="style14">

            <asp:DropDownList ID="drpRequester" runat="server" Width="250px" Height="30px">
            </asp:DropDownList>
              
                  </td>
                  <td class="style14" colspan="2">

  

            <asp:TextBox ID="txtDep" runat="server" Width="270px"></asp:TextBox>
              
                  </td>
                  <td align="rigth" class="style14">
                      &nbsp;</td>
                  <td class="style14" colspan="3">
                      &nbsp;</td>
              </tr>
  
              <tr>
                  <td class="style4">
                      <asp:Label ID="Label33" runat="server" Font-Bold="False" Font-Size="Medium" 
                          Text="Product Type  : "></asp:Label>
                  </td>
                  <td class="style4">
                      <asp:DropDownList ID="drpProductType" runat="server" AutoPostBack="True" 
                          Width="250px" Font-Size="Medium">
                      </asp:DropDownList>
                  </td>
                  <td class="style4">
                  </td>
                  <td class="style4">
                  </td>
                  <td align="rigth" class="style4">
                  </td>
                  <td class="style4" colspan="3">
                  </td>
              </tr>
  
    <tr>
        <td class="style8">
            <asp:Label ID="Label34" runat="server" Text="Product Category  : " 
                Font-Size="Medium" Font-Bold="False"></asp:Label>
            </td>
        <td class="style4">
            <asp:DropDownList ID="drpProductCat" runat="server" Width="250px" 
                AutoPostBack="True" Font-Size="Medium">
            </asp:DropDownList>
        </td>
        <td class="style4">
            &nbsp;</td>
        <td class="style4">
            &nbsp;</td>
        <td align ="rigth" class="style1" colspan="2"> 
            &nbsp;</td>
        <td colspan="3">
            &nbsp;</td>
    </tr>
  
    <tr>
        <td class="style11">
            <asp:Label ID="Label35" runat="server" Text="Product Group :" 
                Font-Size="Medium" Font-Bold="False" Width="230px"></asp:Label>
            </td>
        <td class="style12">
            <asp:DropDownList ID="drpProductGroup" runat="server" Width="250px" 
                Font-Size="Medium">
            </asp:DropDownList>
        </td>
        <td class="style12">
            &nbsp;</td>
        <td class="style12">
            &nbsp;</td>
        <td align ="rigth" class="style12" colspan="2"> 
            </td>
        <td colspan="3" class="style12">
        </td>
    </tr>
  
    
   
    
              <tr>
                  <td class="style11">
                      <asp:Label ID="Label43" runat="server" Font-Bold="False" Font-Size="Medium" 
                          Text="รหัสสินค้า"></asp:Label>
                  </td>
                  <td class="style12">
                      <asp:TextBox ID="txtItem" runat="server" Width="200px"></asp:TextBox>
                  </td>
                  <td class="style12">
                      &nbsp;</td>
                  <td class="style12">
                      &nbsp;</td>
                  <td align="rigth" class="style12" colspan="2">
                      &nbsp;</td>
                  <td class="style12" colspan="3">
                      &nbsp;</td>
              </tr>
             
              <tr>
                  <td class="style11">
                      <asp:Label ID="Label20" runat="server" Font-Bold="False" Font-Size="Medium" 
                          Text="ชื่อสินค้า (ภาษาไทย) : "></asp:Label>
                  </td>
                  <td class="style12">
                      <asp:TextBox ID="txtItemDescTH" runat="server" Width="200px"></asp:TextBox>
                  </td>
                  <td class="style12">
                      <asp:Label ID="Label44" runat="server" Font-Bold="False" Font-Size="Medium" 
                          Text="ชื่อสินค้า (ภาษาอังกฤษ) : " Width="180px"></asp:Label>
                  </td>
                  <td class="style12">
                      <asp:TextBox ID="txtItemDescEN" runat="server" Width="200px"></asp:TextBox>
                  </td>
                  <td align="rigth" class="style12" colspan="2">
                      &nbsp;</td>
                  <td class="style12" colspan="3">
                      &nbsp;</td>
              </tr>
  
     <tr>
                  <td class="style11">
                      <asp:Label ID="Label46" runat="server" Font-Bold="False" Font-Size="Medium" 
                          Text="Supplier Part No. :"></asp:Label>
                  </td>
                  <td class="style12">
                      <asp:TextBox ID="txtPart" runat="server" Width="200px"></asp:TextBox>
                  </td>
                  <td class="style12">
                      <asp:Label ID="Label45" runat="server" Font-Bold="False" Font-Size="Medium" 
                          Text="กลุ่ม :"></asp:Label>
                  </td>
                  <td class="style12">
                      <asp:TextBox ID="txtItemGroup" runat="server" Width="200px"></asp:TextBox>
                  </td>
                  <td align="rigth" class="style12" colspan="2">
                      &nbsp;</td>
                  <td class="style12" colspan="3">
                      &nbsp;</td>
              </tr>
   
    
              <tr>
                  <td class="style11">
            <asp:Label ID="Label49" runat="server" Font-Bold="False" Font-Size="Medium" 
                Text="ประเภท :"></asp:Label>
                  </td>
                  <td class="style12">
                      <asp:DropDownList ID="txtItemType" runat="server" Font-Size="Medium" 
                          Width="200px">
                          <asp:ListItem>Machine</asp:ListItem>
                          <asp:ListItem>Grider</asp:ListItem>
                          <asp:ListItem>Equipment</asp:ListItem>
                          <asp:ListItem>Accessories</asp:ListItem>
                      </asp:DropDownList>
                  </td>
                  <td class="style12">
                      <asp:Label ID="Label50" runat="server" Font-Bold="False" Font-Size="Medium" 
                          Text="Brand :"></asp:Label>
                  </td>
                  <td class="style12">
            <asp:DropDownList ID="drpBrand" runat="server" Width="200px">
            </asp:DropDownList>
                  </td>
                  <td align="rigth" class="style12" colspan="2">
                      &nbsp;</td>
                  <td class="style12" colspan="3">
                      &nbsp;</td>
              </tr>
  
    
   
    
    <tr>
        <td class="style2">
            <asp:Label ID="Label47" runat="server" Font-Bold="False" Font-Size="Medium" 
                Text="หน่วยนับ : "></asp:Label>
        </td>
        <td class="style2">
            <asp:TextBox ID="txtunit" runat="server" Width="200px"></asp:TextBox>

            </td>
        <td class="style2">
            <asp:Label ID="Label48" runat="server" Font-Bold="False" Font-Size="Medium" 
                Text="ราคาขายต่อชิ้นบาท (ex.vat) : " Width="220px"></asp:Label>
        </td>
        <td class="style2">
            <asp:TextBox ID="txtUnitPrice" runat="server" Width="200px"></asp:TextBox>
        </td>
        <td align =rigth class="style3" colspan="2"> 
            </td>
        <td class="style2">
            <asp:Button ID="btnAdd" runat="server" Text="Add" class="btn btn-primary" 
                BackColor="#003399" BorderColor="#0066FF" Font-Bold="False" Font-Size="Medium" 
                ForeColor="White" Width="60px"/>
            </td>
        <td align = rigth class="style3">
            </td>
        <td class="style2">
            </td>
    </tr>

    
    
    <tr>
        <td class="style8">
            &nbsp;</td>
        <td class="style4">
            &nbsp;</td>
        <td class="style4">
            &nbsp;</td>
        <td class="style4">
            &nbsp;</td>
        <td align =rigth class="style1" colspan="2"> 
            &nbsp;</td>
        <td>
            &nbsp;</td>
        <td align = rigth class="style1">
            &nbsp;</td>
        <td>
            &nbsp;</td>
    </tr>
   
    
    </table>

  </div>
  <div id="about" class="clr">

     <iframe name="report-iframe" id="report-iframe" src="https://armapplication.com/Product_Upload/NewMasterSparePart_Upload.aspx?usr=<%=Request.QueryString("usr") %>" 
 width="100%" height="220" frameborder="0"></iframe>

  </div>
</div>
 

    </asp:Panel>
 

 <br />

                
    &nbsp;&nbsp;<asp:Button 
        ID="btnSubmit" runat="server" Text="Submit" class="btn btn-primary" 
                BackColor="#003399" BorderColor="#0066FF" Font-Bold="False" Font-Size="Medium" 
                ForeColor="White" Width="100px" Height="30px"/>
  &nbsp;<asp:Button ID="btnApprove" runat="server" Text="Account Approve" class="btn btn-primary" 
                BackColor="#003399" BorderColor="#0066FF" Font-Bold="True" Font-Size="Medium" 
                ForeColor="White" Height="30px" Width="200px"/>
                
  
  &nbsp;<asp:Label ID="Label1" runat="server" Text="ตรวจสอบข้อมูลหลัง Upload กดปุ่ม Submit เพื่อยืนยันข้อมูลและส่งอีเมลล์แจ้งผู้เกี่ยวข้อง"></asp:Label>



 

    <br />
<div class="row">
   <br />


  <div class="col-12">
  
     
                 <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>


     <asp:GridView ID="GridView" runat="server"  Width = "100%"  DataKeyNames="Item" 
        AutoGenerateColumns = "False" 
        AlternatingRowStyle-BackColor = "#F0F8FF"  
        HeaderStyle-BackColor = "green" 
        AllowPaging ="True"  ShowFooter = "True"  
        OnPageIndexChanging = "OnPaging" CellPadding="3" Height="100%" 
        BorderStyle="None" BorderWidth="1px" 
                  Font-Names="Tahoma" BackColor="White" BorderColor="#CCCCCC" 
        PageSize="30" >
<AlternatingRowStyle BackColor="AliceBlue"></AlternatingRowStyle>
<%--   <asp:GridView AutoGenerateColumns="False" ID="Gridview" 
                CellPadding="4"  DataKeyNames="Item"  CssClass="footable"
                            EmptyDataText="There are no data records to display."
                                   runat="server" Width="100%" PageSize="15" 
          BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" 
          BorderWidth="1px" ForeColor="Black" GridLines="Horizontal">--%>

                            <Columns>

                           <asp:BoundField DataField="id" HeaderText="id" />
               
                            <asp:BoundField DataField="RequestDate" HeaderText="วันที่ Request" >

                                <ItemStyle Width="70px" />
                                </asp:BoundField>

                            <asp:BoundField DataField="Company" HeaderText="บริษัท" />

                            <asp:BoundField DataField="ProductType" HeaderText="Product Type" />
                            
                            <asp:BoundField DataField="ProductCategory" HeaderText="Product Category" />

                            <asp:BoundField DataField="ProductGroup" HeaderText="Product Group" />

                            <asp:BoundField DataField="Brand" HeaderText="Brand" />

                            <asp:BoundField DataField="Item" HeaderText="รหัสสินค้า" />

                             <asp:BoundField DataField="ItemGroup" HeaderText="กลุ่ม" />

                                <asp:BoundField DataField="ItemDescriptionTH" HeaderText="ชื่อสินค้า (ภาษาไทย)" />

                           <asp:BoundField DataField="ItemDescriptionEN" HeaderText="ชื่อสินค้า (ภาษาอังกฤษ)" />

                           <asp:BoundField DataField="ItemType" HeaderText="ประเภท" />
       
                           <asp:BoundField DataField="Unit" HeaderText="หน่วยนับ" />

                           <asp:BoundField DataField="PartNo" HeaderText="Supplier Part No." />

                            <asp:BoundField DataField="UnitPrice" HeaderText="ราคาขายต่อชิ้นบาท (ex.vat)" />

                              
                              <asp:TemplateField  HeaderText = "">
                                   <ItemTemplate>
                                       <asp:LinkButton ID="lnkEdit" runat="server" Text = "Edit" OnClick = "Edit"></asp:LinkButton>
                                   </ItemTemplate>
                                   <ItemStyle Width="40px" Font-Bold="True" ForeColor="Red" />
                               </asp:TemplateField>


                                <asp:CommandField ButtonType="Link"

                                 ShowDeleteButton="true"
                                 >
                                        <HeaderStyle Width="40px" />
               
                                        <ItemStyle Width="40px" Font-Bold="True" ForeColor="Red" />
               
                                </asp:CommandField>

                                 
                            </Columns>

                            <FooterStyle BackColor="White" ForeColor="#000066" />
                            <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                            <RowStyle ForeColor="#000066" />
                            <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                            <SortedAscendingCellStyle BackColor="#F1F1F1" />
                            <SortedAscendingHeaderStyle BackColor="#007DBB" />
                            <SortedDescendingCellStyle BackColor="#CAC9C9" />
                            <SortedDescendingHeaderStyle BackColor="#00547E" />
    </asp:GridView>

    <asp:Panel ID="pnlAddEdit" runat="server" CssClass="modalPopup" style = "display:none; width:850px; height:450px">
    
    
     <asp:Label Font-Bold = "True" ID = "Label4" runat = "server" Text = "Edit & Approve" 
            Font-Size="Medium" ></asp:Label>
     <br />
   
   <table align = "center" style="width:696px; height: 302px;">
   
  
              <tr>
                  <td class="style7">

        <asp:Label ID="lblid" runat="server" Font-Size="Small" Text="Label"></asp:Label>

                  </td>
                  <td class="style2" colspan="3">
                      &nbsp;</td>
              </tr>

  
              <tr>
                  <td class="style7">
                      <asp:Label ID="Label3" runat="server" Font-Bold="True" Font-Size="Small" 
                          Text="บริษัท : " Width="100px"></asp:Label>
                      <br />
                  </td>
                  <td class="style2" colspan="3">
                      <asp:CheckBox ID="chkKVN2" runat="server" 
                          Text="บจ. เค.วี.เอ็น.อิมปอร์ต เอ็กซ์ปอร์ต  (1991)" Width="260px" 
                          Font-Size="Small" />
                      <asp:CheckBox ID="chkLION2" runat="server" Checked="True" 
                          Text="บจ. ไลอ้อน ทรี-สตาร์" Width="150px" Font-Size="Small" />
                  </td>
              </tr>

              <tr>
                  <td class="style4">
                      <asp:Label ID="Label5" runat="server" Font-Bold="False" Font-Size="Small" 
                          Text="Product Type  : "></asp:Label>
                  </td>
                  <td class="style4">
                      <asp:TextBox ID="drpProductType2" runat="server" Width="200px"></asp:TextBox>
                  </td>
                  <td class="style4">
                      &nbsp;</td>
                  <td class="style4">
                      &nbsp;</td>
              </tr>
  
    
              <tr>
                  <td class="style4">
            <asp:Label ID="Label6" runat="server" Text="Product Category  : " 
                Font-Size="Small" Font-Bold="False" Width="130px"></asp:Label>
                  </td>
                  <td class="style4">
                      <asp:TextBox ID="drpProductCat2" runat="server" Width="200px"></asp:TextBox>
                  </td>
                  <td class="style4" align="right">
            <asp:Label ID="Label7" runat="server" Text="Product Group :" 
                Font-Size="Small" Font-Bold="False" style="margin-bottom: 0px"></asp:Label>
                  </td>
                  <td class="style4">
                      <asp:TextBox ID="drpProductGroup2" runat="server" Width="200px"></asp:TextBox>
                  </td>
              </tr>
  
    
              <tr>
                  <td class="style11">
                      <asp:Label ID="Label8" runat="server" Font-Bold="False" Font-Size="Small" 
                          Text="รหัสสินค้า"></asp:Label>
                  </td>
                  <td class="style12">
                      <asp:TextBox ID="txtItem2" runat="server" Width="200px"></asp:TextBox>
                  </td>
                  <td class="style12">
                      &nbsp;</td>
                  <td class="style12">
                      &nbsp;</td>
              </tr>
             
              <tr>
                  <td class="style11">
                      <asp:Label ID="Label9" runat="server" Font-Bold="False" Font-Size="Small" 
                          Text="ชื่อสินค้า (ภาษาไทย) : "></asp:Label>
                  </td>
                  <td class="style12">
                      <asp:TextBox ID="txtItemDescTH2" runat="server" Width="200px"></asp:TextBox>
                  </td>
                  <td class="style12" align="right">
                      <asp:Label ID="Label10" runat="server" Font-Bold="False" Font-Size="Small" 
                          Text="ชื่อสินค้า (ภาษาอังกฤษ) : " Width="170px"></asp:Label>
                  </td>
                  <td class="style12">
                      <asp:TextBox ID="txtItemDescEN2" runat="server" Width="200px"></asp:TextBox>
                  </td>
              </tr>
  
     <tr>
                  <td class="style11">
                      <asp:Label ID="Label11" runat="server" Font-Bold="False" Font-Size="Small" 
                          Text="Supplier Part No. :"></asp:Label>
                  </td>
                  <td class="style12">
                      <asp:TextBox ID="txtPart2" runat="server" Width="200px"></asp:TextBox>
                  </td>
                  <td class="style12" align="right">
                      <asp:Label ID="Label12" runat="server" Font-Bold="False" Font-Size="Small" 
                          Text="กลุ่ม :"></asp:Label>
                  </td>
                  <td class="style12">
                      <asp:TextBox ID="txtItemGroup2" runat="server" Width="200px"></asp:TextBox>
                  </td>
              </tr>
   
    
              <tr>
                  <td class="style11">
            <asp:Label ID="Label13" runat="server" Font-Bold="False" Font-Size="Small" 
                Text="ประเภท :"></asp:Label>
                  </td>
                  <td class="style12">
                      <asp:DropDownList ID="txtItemType2" runat="server" Font-Size="Small" 
                          Width="200px">
                          <asp:ListItem>Machine</asp:ListItem>
                          <asp:ListItem>Grider</asp:ListItem>
                          <asp:ListItem>Equipment</asp:ListItem>
                          <asp:ListItem>Accessories</asp:ListItem>
                      </asp:DropDownList>
                  </td>
                  <td class="style12" align="right">
                      <asp:Label ID="Label14" runat="server" Font-Bold="False" Font-Size="Small" 
                          Text="Brand :"></asp:Label>
                  </td>
                  <td class="style12">
                      <asp:TextBox ID="drpBrand2" runat="server" Width="200px"></asp:TextBox>
                  </td>
              </tr>
  
    
   
    
    <tr>
        <td class="style2">
            <asp:Label ID="Label15" runat="server" Font-Bold="False" Font-Size="Small" 
                Text="หน่วยนับ : "></asp:Label>
        </td>
        <td class="style2">
            <asp:TextBox ID="txtunit2" runat="server" Width="200px"></asp:TextBox>

            </td>
        <td class="style2" align="right">
            <asp:Label ID="Label16" runat="server" Font-Bold="False" Font-Size="Small" 
                Text="ราคาขายต่อชิ้นบาท (ex.vat) : " Width="170px"></asp:Label>
        </td>
        <td class="style2">
            <asp:TextBox ID="txtUnitPrice2" runat="server" Width="200px"></asp:TextBox>
        </td>
    </tr>

   

     <tr>
    <td  align="left" >
   <asp:Label ID="Label17" runat="server" Text="  " Width="5"></asp:Label>

    </td>
  

    <td class="style1" colspan="3">
      <asp:Label ID="Label18" runat="server" Text="   " Width="5"></asp:Label>
   <asp:Button ID="btnSave2" runat="server" Text="Save"  class="btn btn-primary" 
            OnClick = "Save" Width="70px"   ></asp:Button>
   &nbsp;&nbsp;<asp:Button ID="btnClose2" runat="server" Text="Close"  
            class="btn btn-primary"  OnClientClick = "return Hidepopup()" Width="70px" ></asp:Button>
    </td>
  

    </tr>
    </table>
     
  
    </asp:Panel>

     <asp:LinkButton ID="lnkFake" runat="server"></asp:LinkButton>
     <cc1:modalpopupextender ID="popup" runat="server" DropShadow="false"
     PopupControlID="pnlAddEdit" TargetControlID = "lnkFake"
    BackgroundCssClass="modalBackground">
    </cc1:modalpopupextender>
    </ContentTemplate> 
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID = "GridView" />
      <%--  <asp:AsyncPostBackTrigger ControlID = "btnApprove2" />--%>
        <asp:AsyncPostBackTrigger ControlID = "btnSave2" />
       
    </Triggers> 
    </asp:UpdatePanel> 


  </div>

</div>


</div>	

	
	

</div>	
</div>	
</div>	
</div>


<div class="bot1">
<div class="container">
<div class="row">
<div class="span12">
<div class="bot1_inner">
<div class="row">
<div class="span4">
	
<div class="logo2_wrapper"><a href="index.html" class="logo2"><img src="images/aroma/logotrans.png" alt=""></a></div>

<footer><div class="copyright">Copyright   © 2018. All rights reserved.<br><a href="#">Aroma Group</a></div></footer>


</div>
<div class="span4">
	
<div class="bot1_title">Contact Us</div>

สำนักงานใหญ่ อโรม่า กรุ๊ป<br>
บริษัท เค.วี.เอ็น.อิมปอร์ต เอกซ์ปอร์ต (1991) จำกัด<br>
เลขที่ 43 ชั้น 2 ซอยนาคนิวาส 6 ถนนนาคนิวาส แขวงลาดพร้าว <br>
เขตลาดพร้าว กรุงเทพฯ 10230<br>
โทรศัพท์ : 02 159-8999<br>
โทรสาร : 02 538-8144, 02 539-5597

<%--E-mail: <a href="#">mail@demosite.com</a>--%>


</div>
<div class="span4">
	
<%--<div class="bot1_title">Follow Us:</div>	

<div class="social_wrapper">
		<ul class="social clearfix">
	    <li><a href="#"><img src="images/social_ic1.png"></a></li>
	    <li><a href="#"><img src="images/social_ic2.png"></a></li>
	    <li><a href="#"><img src="images/social_ic3.png"></a></li>	    
	    <li><a href="#"><img src="images/social_ic4.png"></a></li>
		</ul>
	</div>
--%>


</div>	

</div>	
</div>
</div>	
</div>	
</div>	
</div>

</div>	
</div>
<script type="text/javascript" src="js/bootstrap.js"></script>


 


    </div>
    </form>
</body>
</html>