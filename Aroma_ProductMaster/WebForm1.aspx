﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="WebForm1.aspx.vb" Inherits="Aroma_ProductMaster.WebForm1" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="csrf-token" content="S3NJBJNhAJXQ0sE6iljFi22TgjiovQ7UFKrqBmPI">
  <title>Smart Service</title>

   <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-134603266-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag() { dataLayer.push(arguments); }
    gtag('js', new Date());

    gtag('config', 'UA-134603266-1');
</script>

  <!-- Tell the browser to be responsive to screen width -->
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
<!-- select2 -->
<link rel="stylesheet" href="plugins/select2/select2.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="dist/css/AdminLTE.min.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
     folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
<!-- Pace style -->
<link rel="stylesheet" href="plugins/pace/pace.min.css">
<!-- date picker -->
<link rel="stylesheet" href="plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css">
<link rel="stylesheet" href="plugins/timepicker/bootstrap-timepicker.min.css">
<!-- datatables -->
<link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap.css">
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.dataTables.min.css">
<!-- plugins upload -->
<link type="text/css" href="plugins/orakuploader/orakuploader.css" rel="stylesheet"/>
<!-- bootstrap3-typeahead -->
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
    <%--<link rel="stylesheet" href="dist/css/custom.css">
  <style>
.table-responsive
{
    width: 100%;
    margin-bottom: 15px;
    overflow-x: auto;
    overflow-y: hidden;
    -webkit-overflow-scrolling: touch;
    -ms-overflow-style: -ms-autohiding-scrollbar;
    border: 1px solid #000000;
}
table {
  border-collapse: collapse;
}
td {
  border: 1px solid #ccc;
}
th, td {
  padding: 3px;
  /*text-align: left;*/
}
</style>--%>


 <link href="CSS3/CSS.css" rel="stylesheet" type="text/css" /> 
    <%--  <script src="scripts3/jquery-1.3.2.min.js" type="text/javascript"></script>--%>
    <script src="scripts3/jquery.blockUI.js" type="text/javascript"></script>
<script type = "text/javascript">
    function BlockUI(elementID) {
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(function () {
            $("#" + elementID).block({ message: '<table align = "center"><tr><td>' +
     '<img src="images/loadingAnim.gif"/></td></tr></table>',
                css: {},
                overlayCSS: { backgroundColor: '#000000', opacity: 0.6
                }
            });
        });
        prm.add_endRequest(function () {
            $("#" + elementID).unblock();
        });
    }
    $(document).ready(function () {

        BlockUI("<%=pnlAddEdit.ClientID %>");
        $.blockUI.defaults.css = {};
    });
    function Hidepopup() {
        $find("popup").hide();
        return false;
    }
</script> 


    <style type="text/css">
        .style1
        {
            height: 18px;
        }
        .style2
        {
            height: 19px;
        }
        .style3
        {
            height: 20px;
        }
        .style4
        {
            height: 21px;
        }
        .style5
        {
            height: 22px;
        }
        .style6
        {
            height: 23px;
        }
        .style7
        {
            height: 24px;
        }
        .style8
        {
            height: 37px;
        }
        .style10
        {
            height: 42px;
        }
    </style>


</head>
<body class="hold-transition skin-blue layout-top-nav">
    <form id="form1" runat="server">
  <!-- Site wrapper -->
<div class="wrapper">

  <header class="main-header">
    <nav class="navbar navbar-static-top">
      <div class="container">
        <div class="navbar-header">
          <a href="https://armapplication.com/smartservice/index.aspx?username=<%=Request.QueryString("username") %>" class="navbar-brand">
            <!-- <b>SmartService</b> -->
            <!-- <img src="http://www.aromathailand.com/2015/uploads/franchise/thumbnail/1457515797.jpg" height="50" width="150" alt=""> -->
            <img src="assets/dist/img/aroma.png" 
                class="user-image" alt="User Image" height="51">
         <%--  --%>
          </a>
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
            <i class="fa fa-bars"></i>
          </button>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="https://armapplication.com/smartservice/index.aspx?username=<%=Request.QueryString("username") %>" class="navbar-brand">SmartService <span class="sr-only navbar-brand">(current)</span></a></li>
          </ul>
        </div>
        <!-- /.navbar-collapse -->
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">
            <!-- User Account Menu -->
            <li class="dropdown user user-menu">
              <!-- Menu Toggle Button -->
              <a href="https://armapplication.com/smartservice/index.aspx?username=<%=Request.QueryString("username") %>" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                <!-- The user image in the navbar-->
                <img src="assets/dist/img/Profile.png" class="user-image" alt="User Image">
                <!-- hidden-xs hides the username on small devices so only the image appears. -->
                <span class="hidden-xs"><asp:Label ID="lblUsername2" runat="server" Text="Label"></asp:Label></span>
              </a>
              <ul class="dropdown-menu">
                <!-- The user image in the menu -->
                <li class="user-header">
                  <img src="assets/dist/img/Profile.png" class="img-circle" alt="User Image">

                  <p>
                    <asp:Label ID="lblUsername1" runat="server" Text="Label"></asp:Label>
                      <%--<small>เป็นสมาชิกเมื่อ 2017-10-16 10:24:40.000</small>--%>
                  </p>
                </li>
                <!-- Menu Body -->
                <!-- Menu Footer-->
                <li class="user-footer">
                  <div class="pull-left">
                    <a href="#" class="btn btn-default btn-flat">Profile</a>
                  </div>
                  <div class="pull-right">
                    <a href="http://armapplication.com:90/callcenter-workshop/logout" class="btn btn-default btn-flat">Sign out</a>
                  </div>
                </li>
              </ul>
            </li>
          </ul>
        </div>
        <!-- /.navbar-custom-menu -->
      </div>
      <!-- /.container-fluid -->
    </nav>
  </header>

  <!-- =============================================== -->

  <!-- =============================================== -->
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <div class="container">
      <section class="content-header">
  <h1>
    &nbsp;
  </h1>
  <ol class="breadcrumb">
   <%-- <li><a href="http://armapplication.com:90/callcenter-workshop?api_token=<%=Request.QueryString("api") %>"><i class=""></i> หน้าแรก</a></li>--%>
     <li><a href="Index.aspx?username=<%=Request.QueryString("username") %>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="SE.aspx?username=<%=Request.QueryString("username") %>"><i class="fa fa-dashboard"></i> SE งานส่งสินค้า</a></li>
    <li class="active">มอบหมายงานให้ช่าง</li>
  </ol>
</section>
    <!-- Main content -->
    <section class="content">
      <!-- Default box -->


<div class="box">
  <div class="box-header with-border">

      
    <h3 class="box-title">มอบหมายงานส่งสินค้าให้ช่าง</h3>

    <hr />

    <table>

                          <tr>
                <td align="right" class="style5">
                    <asp:Label ID="Label38" runat="server" CssClass="fn_ContentTital" 
                        Text="Call Number : " Width="150px" Font-Bold="True" Font-Size="Medium"></asp:Label>
                </td>
                <td align="left" valign="middle" class="style5">
                  <asp:TextBox ID="txtCallID" runat="server" class="form-control" placeholder="..." 
                        Width="150px" ></asp:TextBox>

                </td>
                <td align="left" valign="middle" class="style5">
                           </td>
                <td align="left" valign="middle" class="style5">
                              </td>
                <td align="left" valign="middle" class="style5">
                              </td>
                <td align="left" valign="middle" class="style5">
                              </td>
                <td align="left" valign="middle" class="style5">

               

                              </td>
            </tr>

                          <tr>
                <td align="right" class="style8">
                    <asp:Label ID="Label39" runat="server" CssClass="fn_ContentTital" 
                        Text="ลูกค้า  : " Width="150px" Font-Bold="True" Font-Size="Medium"></asp:Label>
                              </td>
                <td align="left" valign="middle" class="style8">
                  <asp:TextBox ID="txtCustomerName" runat="server" class="form-control" placeholder="..." 
                        Width="250px" ></asp:TextBox>

                              </td>
                <td align="right" valign="middle" class="style8">
                    <asp:Label ID="Label40" runat="server" CssClass="fn_ContentTital" 
                        Text="สาขา  : " Width="100px" Font-Bold="True" Font-Size="Medium"></asp:Label>
                              </td>
                <td align="left" valign="middle" class="style8">
                  <asp:TextBox ID="txtBranch" runat="server" class="form-control" placeholder="..." 
                        Width="250px" ></asp:TextBox>

                              </td>
                <td align="left" valign="middle" class="style8">
                              </td>
                <td align="left" valign="middle" class="style8">
                              </td>
                <td align="left" valign="middle" class="style8">

               

                              </td>
            </tr>

                       <tr>
                <td align="right" class="style1">
                           </td>
                <td align="left" valign="middle" class="style1">

           



             <asp:Button ID="btnSearch" runat="server" Text="Search"  
                 class="btn btn-primary" ></asp:Button>
           
                           </td>
                <td align="left" valign="middle" class="style1">
                           </td>
                <td align="left" valign="middle" class="style1">
                           </td>
                <td align="left" valign="middle" class="style1">
                           &nbsp;</td>
                <td align="left" valign="middle" class="style1">
                           &nbsp;</td>
                <td align="left" valign="middle" class="style1">
                          &nbsp;</td>
            </tr>

        </table>  



   
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>


        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>

              <%--  <asp:GridView AutoGenerateColumns="False" ID="Gridview" 
                CellPadding="5"  CssClass="footable" DataKeyNames="Item" 
                                   runat="server" Width="100%">--%>

     <asp:GridView ID="GridView" runat="server"  Width = "100%"  DataKeyNames="call_number" 
        AutoGenerateColumns = "False" 
        AlternatingRowStyle-BackColor = "#F0F8FF" 
        HeaderStyle-BackColor = "green" 
        AllowPaging ="True"  ShowFooter = "True" CellPadding="3" Height="100%"
       PageSize="1500"
                  BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" 
                  BorderWidth="1px" >
                            <Columns>

                            
                                   
                            <asp:BoundField DataField="call_number" HeaderText="call number" />                    
                            <asp:BoundField DataField="customer_group_name" HeaderText="กลุ่มลูกค้า" />
                            <asp:BoundField DataField="Customer_Name" HeaderText="ลูกค้า" />   
                            <asp:BoundField DataField="Branch" HeaderText="สาขา" />            
                            <asp:BoundField DataField="item_name" HeaderText="สินค้า" />                           
                            <asp:BoundField DataField="call_date" HeaderText="วันที่แจ้งซ่อม" />
                            <asp:BoundField DataField="status_name" HeaderText="สถานะ" />
                          
                              <asp:TemplateField  HeaderText = "">
                                   <ItemTemplate>
                                       <asp:LinkButton ID="lnkEdit" runat="server" Text = "Assigned" OnClick = "Edit"></asp:LinkButton>
                                   </ItemTemplate>
                                   <ItemStyle Width="100px" />
                               </asp:TemplateField>

                            

                            </Columns>

                        <FooterStyle BackColor="White" ForeColor="#000066" />
                            <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                            <RowStyle ForeColor="#000066" />
                            <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                            <SortedAscendingCellStyle BackColor="#F1F1F1" />
                            <SortedAscendingHeaderStyle BackColor="#007DBB" />
                            <SortedDescendingCellStyle BackColor="#CAC9C9" />
                            <SortedDescendingHeaderStyle BackColor="#00547E" />
    </asp:GridView>
            <%--<asp:Button ID="btnAdd" runat="server" Text="Add" OnClick = "Add" />--%>
    
    <asp:Panel ID="pnlAddEdit" runat="server" CssClass="modalPopup" style = "display:none; width:650px; height:500px">
    
    
     <asp:Label Font-Bold = "True" ID = "Label4" runat = "server" Text = "Assign Technician" 
            Font-Size="Medium" ></asp:Label>
     <br />
   
    <table align = "center" style="width:90%">
     <tr>
    <td class="style4">
    <asp:Label ID = "Label6" runat = "server" Text = "Call Number" ></asp:Label>
    </td>
    <td class="style5">
    <asp:TextBox ID="txtCallNumber" Width = "200px" MaxLength = "5" runat="server" 
            class="form-control"></asp:TextBox>
    </td>
    <td class="style5">
        &nbsp;</td>
    <td class="style5">
        &nbsp;</td>
    </tr>
    <tr>
    <td class="style4">
    <asp:Label ID = "Label2" runat = "server" Text = "ลูกค้า" ></asp:Label>
    </td>
    <td class="style5">
    <asp:TextBox ID="txtCustomer" Width = "90%" MaxLength = "5" runat="server" 
            class="form-control"></asp:TextBox>
    </td>
    <td class="style5">
        &nbsp;</td>
    <td class="style5">
        &nbsp;</td>
    </tr>
     <tr>
    <td class="style7">
    <asp:Label ID = "Label7" runat = "server" Text = "สินค้า" ></asp:Label>
    </td>
    <td class="style7">
    <asp:TextBox ID="txtItemName" runat="server" Width="90%" class="form-control"></asp:TextBox>    
    </td>
    </tr>
   <%--  <tr>
    <td class="style6">
    <asp:Label ID = "Label16" runat = "server" Text = "อาการ" Width="90px" ></asp:Label>
    </td>
    <td class="style7" colspan="3">
    <asp:TextBox ID="txtproblemDesc" runat="server" Width="90%" class="form-control"></asp:TextBox>    
    </td>
    </tr>--%>
     <tr>
    <td class="style6">
        &nbsp;</td>
    <td class="style7">
        &nbsp;</td>
    <td class="style7">
        &nbsp;</td>
    <td class="style7">
        &nbsp;</td>
    </tr>
    
  

     <tr>
    <td class="style1">
    <asp:Label ID = "Label15" runat = "server" Text = "* ช่าง" ></asp:Label>
         </td>
    <td class="style1" colspan="3">
                    <asp:DropDownList ID="drpTechnical" runat="server"  
            class="form-control" Width="50%" ></asp:DropDownList>
         </td>
    </tr>


     <tr>
    <td class="style2" align =left >
        &nbsp;</td>
    <td class="style1" colspan="3">


          


                    <asp:DropDownList ID="drpTechnical2" runat="server"  
            class="form-control" Width="50%" ></asp:DropDownList>


          


         </td>
    </tr>


     <tr>
    <td class="style2" align ="left" >
        &nbsp;</td>
    <td class="style1" align ="left" colspan="3" >


          


                    <asp:DropDownList ID="drpTechnical3" runat="server"  
            class="form-control" Width="50%" ></asp:DropDownList>


          


        </td>
    </tr>


     


     <tr>
    <td class="style2" align ="right" > 
        &nbsp;</td>
    <td class="style1" align ="left" >


          


                    &nbsp;</td>
    <td class="style1" align =left colspan="2" >


          


                    &nbsp;</td>
    </tr>
     <tr>
    <td  align="left" colspan="4" >

            <asp:GridView ID="GridView2" runat="server" AllowPaging="True"                                                             
            AutoGenerateColumns="False" DataKeyNames="ID" 
                 DataSourceID="SqlDataSource2" Font-Bold="False" 
            Font-Names="Tahoma" Font-Size="Small" Width="90%" 
            CellPadding="4" 
                                                            GridLines="None" 
         ForeColor="#333333" Height="100%" PageSize="5">
                                                         
                                                            <EditRowStyle BackColor="#999999" />
                                                         
                                                            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                                            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                                                            <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                                                            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                                            <HeaderStyle BackColor="#5D7B9D" Font-Bold="true" ForeColor="White" />
                                                            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                                                                
                                                            <Columns>

                                                    <asp:TemplateField HeaderText="ID">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblTechnicianID" Text='<%# Eval("ID") %>' runat="server" />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>



                                                            <asp:TemplateField HeaderText="ช่าง">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblTechnicianName" Text='<%# Eval("TechnicianName") %>' runat="server" />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>


                                                                   <asp:CommandField ShowDeleteButton="True" 
                                                                   
                                                                    DeleteImageUrl="~/Images/icon_delete.ico" HeaderText="ลบ">
                                                                         <HeaderStyle HorizontalAlign="Left" />
                                                                   </asp:CommandField>

                                                            </Columns>

                                                            <SortedAscendingCellStyle BackColor="#E9E7E2" />
                                                            <SortedAscendingHeaderStyle BackColor="#506C8C" />
                                                            <SortedDescendingCellStyle BackColor="#FFFDF8" />
                                                            <SortedDescendingHeaderStyle BackColor="#6F8DAE" />

                                                        </asp:GridView>


          <asp:SqlDataSource ID="SqlDataSource2" runat="server" 
        ConnectionString="<%$ ConnectionStrings:ServiceConnectionString %>" 
        DeleteCommand="DELETE FROM  TB_AssignTechnician where ID = @ID" 
        SelectCommand="SELECT [ID],[TechnicianName],call_number from [VW_AssignTechnician] where call_number= @CallNumber "
        >
              <selectparameters>
                  <asp:controlparameter name="CallNumber" controlid="txtCallNumber" />
              </selectparameters>
                   
           <DeleteParameters>
                <asp:Parameter Name="ID" /> 
            </DeleteParameters>
            
    </asp:SqlDataSource>


     </td>
  

    </tr>
    
     <tr>
    <td  align="right" >
   <asp:Button ID="btnAssign" runat="server" Text="Assign"  class="btn btn-primary" OnClick = "Save"   ></asp:Button>
   <asp:Label ID="Label8" runat="server" Text="  " Width="5"></asp:Label>

    </td>
  

    <td class="style1">
      <asp:Label ID="Label5" runat="server" Text="   " Width="5"></asp:Label>
      <asp:Button ID="btnCancel" runat="server" Text="Close"  class="btn btn-primary"  OnClientClick = "return Hidepopup()" ></asp:Button>
    </td>
  

    <td class="style1">
        &nbsp;</td>
  

    <td class="style1">
        &nbsp;</td>
  

    </tr>
    </table>

    </asp:Panel>

            <asp:LinkButton ID="lnkFake" runat="server"></asp:LinkButton>
     <cc1:ModalPopupExtender ID="popup" runat="server" DropShadow="false"
     PopupControlID="pnlAddEdit" TargetControlID = "lnkFake"
    BackgroundCssClass="modalBackground">
    </cc1:ModalPopupExtender>
    </ContentTemplate> 
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID = "GridView" />
        <asp:AsyncPostBackTrigger ControlID = "btnAssign" />
      <%--  <asp:AsyncPostBackTrigger ControlID = "btnApprove" />--%>
    </Triggers> 
    </asp:UpdatePanel> 


   


  </div>
 
  <!-- /.box-body -->
  <!-- <div class="box-footer">
    &nbsp;
  </div> -->
  <!-- /.box-footer-->
</div>
<!-- /.box -->
    </section>
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<footer class="main-footer">
  <div class="container">
    <div class="pull-right hidden-xs">
      <!-- <b>Version</b> 2.3.5 -->
    </div>
    <strong>Copyright &copy; 2017 href="http://aromathailand.com">MIS TEAM AROMA GROUP>.</strong> All rights
    reserved.
  </div>
</footer>

</div>

<!-- ./wrapper -->
<!-- jQuery 2.2.3 -->
<script src="plugins/jQuery/jquery-2.2.3.min.js"></script>

<!-- Bootstrap 3.3.6 -->
<script src="bootstrap/js/bootstrap.min.js"></script>

<!-- PACE -->
<script src="plugins/pace/pace.min.js"></script>

<!-- SlimScroll -->
<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>

<!-- FastClick -->
<script src="plugins/fastclick/fastclick.js"></script>

<!-- select2 -->
<script src="plugins/select2/select2.full.min.js"></script>

<!-- jquery validation -->
<script src="plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="plugins/jquery-validation/custom-validation.js"></script>

<!-- plugins upload -->
<script type="text/javascript" src="plugins/orakuploader/jquery-ui.min.js"></script>
<script type="text/javascript" src="plugins/orakuploader/orakuploader.js"></script>

<!-- date picker -->
<script src="plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script src="plugins/bootstrap-datepicker/locales/bootstrap-datepicker.th.min.js"></script>
<script src="plugins/timepicker/bootstrap-timepicker.min.js"></script>

<!-- datatables -->
<script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.js"></script>

<!-- plugins bootstrap typehead -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js"></script>

<!-- AdminLTE App -->
<script src="dist/js/app.min.js"></script>

<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>

<!-- Custom js -->
<script src="dist/js/custom.js"></script>


<script>
    var _url = "http://armapplication.com:90/callcenter-workshop";
    var api_token = "NMmpbwE7cZ51pEjwfS34Ru0SLnZekkPv6jbY79yzNNZHCFEi46sIfjBQRdEy";
  </script>
<!-- page script -->
<script type="text/javascript">
    // To make Pace works on Ajax calls
    $(document).ajaxStart(function () { Pace.restart(); });
    $('.ajax').click(function () {
        $.ajax({ url: '#', success: function (result) {
            $('.ajax-content').html('<hr>Ajax Request Completed !');
        }
        });
    });
</script>
<script src="dist/js/report/report.js"></script>
      



    </form>
</body>
</html>
