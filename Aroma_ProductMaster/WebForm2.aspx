﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="WebForm2.aspx.vb" Inherits="Aroma_ProductMaster.WebForm2" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
     <table align = "center" style="width:696px; height: 302px;">
   
  
              <tr>
                  <td class="style7">

        <asp:Label ID="lblid" runat="server" Font-Size="Small" Text="Label"></asp:Label>

                  </td>
                  <td class="style2" colspan="3">
                      &nbsp;</td>
              </tr>

  
              <tr>
                  <td class="style7">
                      <asp:Label ID="Label2" runat="server" Font-Bold="True" Font-Size="Small" 
                          Text="บริษัท : " Width="100px"></asp:Label>
                      <br />
                  </td>
                  <td class="style2" colspan="3">
                      <asp:CheckBox ID="chkKVN2" runat="server" 
                          Text="บจ. เค.วี.เอ็น.อิมปอร์ต เอ็กซ์ปอร์ต  (1991)" Width="260px" 
                          Font-Size="Small" />
                      <asp:CheckBox ID="chkLION2" runat="server" Checked="True" 
                          Text="บจ. ไลอ้อน ทรี-สตาร์" Width="150px" Font-Size="Small" />
                  </td>
              </tr>

              <tr>
                  <td class="style4">
                      <asp:Label ID="Label33" runat="server" Font-Bold="False" Font-Size="Small" 
                          Text="Product Type  : "></asp:Label>
                  </td>
                  <td class="style4">
                      <asp:TextBox ID="drpProductType2" runat="server" Width="200px"></asp:TextBox>
                  </td>
                  <td class="style4">
                      &nbsp;</td>
                  <td class="style4">
                      &nbsp;</td>
              </tr>
  
    
              <tr>
                  <td class="style4">
            <asp:Label ID="Label34" runat="server" Text="Product Category  : " 
                Font-Size="Small" Font-Bold="False" Width="130px"></asp:Label>
                  </td>
                  <td class="style4">
                      <asp:TextBox ID="drpProductCat2" runat="server" Width="200px"></asp:TextBox>
                  </td>
                  <td class="style4" align="right">
            <asp:Label ID="Label35" runat="server" Text="Product Group :" 
                Font-Size="Small" Font-Bold="False" style="margin-bottom: 0px"></asp:Label>
                  </td>
                  <td class="style4">
                      <asp:TextBox ID="drpProductGroup2" runat="server" Width="200px"></asp:TextBox>
                  </td>
              </tr>
  
    
              <tr>
                  <td class="style11">
                      <asp:Label ID="Label43" runat="server" Font-Bold="False" Font-Size="Small" 
                          Text="รหัสสินค้า"></asp:Label>
                  </td>
                  <td class="style12">
                      <asp:TextBox ID="txtItem2" runat="server" Width="200px"></asp:TextBox>
                  </td>
                  <td class="style12">
                      &nbsp;</td>
                  <td class="style12">
                      &nbsp;</td>
              </tr>
             
              <tr>
                  <td class="style11">
                      <asp:Label ID="Label20" runat="server" Font-Bold="False" Font-Size="Small" 
                          Text="ชื่อสินค้า (ภาษาไทย) : "></asp:Label>
                  </td>
                  <td class="style12">
                      <asp:TextBox ID="txtItemDescTH2" runat="server" Width="200px"></asp:TextBox>
                  </td>
                  <td class="style12" align="right">
                      <asp:Label ID="Label44" runat="server" Font-Bold="False" Font-Size="Small" 
                          Text="ชื่อสินค้า (ภาษาอังกฤษ) : " Width="170px"></asp:Label>
                  </td>
                  <td class="style12">
                      <asp:TextBox ID="txtItemDescEN2" runat="server" Width="200px"></asp:TextBox>
                  </td>
              </tr>
  
     <tr>
                  <td class="style11">
                      <asp:Label ID="Label46" runat="server" Font-Bold="False" Font-Size="Small" 
                          Text="Supplier Part No. :"></asp:Label>
                  </td>
                  <td class="style12">
                      <asp:TextBox ID="txtPart2" runat="server" Width="200px"></asp:TextBox>
                  </td>
                  <td class="style12" align="right">
                      <asp:Label ID="Label45" runat="server" Font-Bold="False" Font-Size="Small" 
                          Text="กลุ่ม :"></asp:Label>
                  </td>
                  <td class="style12">
                      <asp:TextBox ID="txtItemGroup2" runat="server" Width="200px"></asp:TextBox>
                  </td>
              </tr>
   
    
              <tr>
                  <td class="style11">
            <asp:Label ID="Label49" runat="server" Font-Bold="False" Font-Size="Small" 
                Text="ประเภท :"></asp:Label>
                  </td>
                  <td class="style12">
                      <asp:DropDownList ID="txtItemType2" runat="server" Font-Size="Small" 
                          Width="200px">
                          <asp:ListItem>Machine</asp:ListItem>
                          <asp:ListItem>Grider</asp:ListItem>
                          <asp:ListItem>Equipment</asp:ListItem>
                          <asp:ListItem>Accessories</asp:ListItem>
                      </asp:DropDownList>
                  </td>
                  <td class="style12" align="right">
                      <asp:Label ID="Label50" runat="server" Font-Bold="False" Font-Size="Small" 
                          Text="Brand :"></asp:Label>
                  </td>
                  <td class="style12">
                      <asp:TextBox ID="drpBrand2" runat="server" Width="200px"></asp:TextBox>
                  </td>
              </tr>
  
    
   
    
    <tr>
        <td class="style2">
            <asp:Label ID="Label47" runat="server" Font-Bold="False" Font-Size="Small" 
                Text="หน่วยนับ : "></asp:Label>
        </td>
        <td class="style2">
            <asp:TextBox ID="txtunit2" runat="server" Width="200px"></asp:TextBox>

            </td>
        <td class="style2" align="right">
            <asp:Label ID="Label48" runat="server" Font-Bold="False" Font-Size="Small" 
                Text="ราคาขายต่อชิ้นบาท (ex.vat) : " Width="170px"></asp:Label>
        </td>
        <td class="style2">
            <asp:TextBox ID="txtUnitPrice2" runat="server" Width="200px"></asp:TextBox>
        </td>
    </tr>

   

     <tr>
    <td  align="left" >
   <asp:Label ID="Label8" runat="server" Text="  " Width="5"></asp:Label>

    </td>
  

    <td class="style1" colspan="3">
      <asp:Label ID="Label5" runat="server" Text="   " Width="5"></asp:Label>
   <asp:Button ID="btnSave2" runat="server" Text="Save"  class="btn btn-primary" 
            OnClick = "Save" Width="70px"   ></asp:Button>
   &nbsp;&nbsp;<asp:Button ID="btnClose2" runat="server" Text="Close"  
            class="btn btn-primary"  OnClientClick = "return Hidepopup()" Width="70px" ></asp:Button>
    </td>
  

    </tr>
    </table>

    </div>
    </form>
</body>
</html>
