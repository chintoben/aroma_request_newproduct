﻿Imports Microsoft.VisualBasic
Imports System.Data.OleDb
Imports System.Data.SqlClient

Public Class Attachment
    Dim db As New Connect_product

    Public Function InsertAttatchment(ByVal Employee_id As String, ByVal RequestId As String, ByVal sFilename As String, ByVal Attatch As Object) As String
        Dim x As Integer
        Dim Cn As New SqlConnection(db.sqlCon)

        Dim sSql As String = "INSERT INTO [TB_Attachment] ([RequestId],[Att_FileName],[Att_Filetype],[Att],[CreateBy],[CreateDate])"
        sSql &= " VALUES (@RequestId,@Att_FileName,@Att_Filetype,@Att,@CreateBy,@CreateDate)"


        Dim command As SqlCommand = New SqlCommand(sSql, Cn)
        Dim ss() As String = sFilename.Split(".")

        Dim sFiletype As String = (".") & ss(ss.GetUpperBound(0))
        Try

            command.Parameters.Add("@RequestId", Data.SqlDbType.VarChar).Value = RequestId
            command.Parameters.Add("@Att_FileName", Data.SqlDbType.VarChar).Value = sFilename.ToLower()
            command.Parameters.Add("@Att_Filetype", Data.SqlDbType.VarChar).Value = sFiletype.ToLower()
            command.Parameters.Add("@Att", Data.SqlDbType.Image).Value = Attatch
            command.Parameters.Add("@CreateDate", Data.SqlDbType.VarChar).Value = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")
            command.Parameters.Add("@CreateBy", Data.SqlDbType.VarChar).Value = Employee_id

            command.CommandType = Data.CommandType.Text
            Cn.Open()
            x = command.ExecuteScalar
        Catch x1 As Exception
            Err.Raise(60002, , x1.Message)
        Finally
            Cn.Close()
        End Try
        Return x
    End Function

    Public Function InsertAttatchment_OT(ByVal Employee_id As String, ByVal LeaveID As String, ByVal sFilename As String, ByVal Attatch As Object) As String
        Dim x As Integer
        Dim Cn As New SqlConnection(db.sqlCon)

        Dim sSql As String = "INSERT INTO [TB_OTAttachment] ([Employee_id],[OTID],[Att_FileName],[Att_Filetype],[Att],[CreateBy],[CreateDate])"
        sSql &= " VALUES (@Employee_id,@OTID,@Att_FileName,@Att_Filetype,@Att,@CreateBy,@CreateDate)"


        Dim command As SqlCommand = New SqlCommand(sSql, Cn)
        Dim ss() As String = sFilename.Split(".")

        Dim sFiletype As String = (".") & ss(ss.GetUpperBound(0))
        Try

            command.Parameters.Add("@Employee_id", Data.SqlDbType.VarChar).Value = Employee_id
            command.Parameters.Add("@OTID", Data.SqlDbType.VarChar).Value = LeaveID
            'command.Parameters.Add("@MType", Data.SqlDbType.VarChar).Value = BURunning
            command.Parameters.Add("@Att_FileName", Data.SqlDbType.VarChar).Value = sFilename.ToLower()
            command.Parameters.Add("@Att_Filetype", Data.SqlDbType.VarChar).Value = sFiletype.ToLower()
            command.Parameters.Add("@Att", Data.SqlDbType.Image).Value = Attatch
            command.Parameters.Add("@CreateDate", Data.SqlDbType.VarChar).Value = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")
            command.Parameters.Add("@CreateBy", Data.SqlDbType.VarChar).Value = Employee_id

            command.CommandType = Data.CommandType.Text
            Cn.Open()
            x = command.ExecuteScalar
        Catch x1 As Exception
            Err.Raise(60002, , x1.Message)
        Finally
            Cn.Close()
        End Try
        Return x
    End Function


    Public Function InsertAttatchmentIndex(ByVal IndexID As String, ByVal sFilename As String, ByVal Attatch As Object) As String
        Dim x As Integer
        Dim Cn As New SqlConnection(db.sqlCon)

        Dim sSql As String = "INSERT INTO [TB_IndexHomeAtt] ([IndexID],[Att_FileName],[Att_Filetype],[Att],[CreateBy],[CreateDate])"
        sSql &= " VALUES (@IndexID,@Att_FileName,@Att_Filetype,@Att,@CreateBy,@CreateDate)"


        Dim command As SqlCommand = New SqlCommand(sSql, Cn)
        Dim ss() As String = sFilename.Split(".")

        Dim sFiletype As String = (".") & ss(ss.GetUpperBound(0))
        Try

            command.Parameters.Add("@IndexID", Data.SqlDbType.VarChar).Value = IndexID
            command.Parameters.Add("@Att_FileName", Data.SqlDbType.VarChar).Value = sFilename.ToLower()
            command.Parameters.Add("@Att_Filetype", Data.SqlDbType.VarChar).Value = sFiletype.ToLower()
            command.Parameters.Add("@Att", Data.SqlDbType.Image).Value = Attatch
            command.Parameters.Add("@CreateDate", Data.SqlDbType.VarChar).Value = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")
            command.Parameters.Add("@CreateBy", Data.SqlDbType.VarChar).Value = "Admin" 'Employee_id

            command.CommandType = Data.CommandType.Text
            Cn.Open()
            x = command.ExecuteScalar
        Catch x1 As Exception
            Err.Raise(60002, , x1.Message)
        Finally
            Cn.Close()
        End Try
        Return x
    End Function

End Class
