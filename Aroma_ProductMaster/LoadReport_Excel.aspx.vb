﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Drawing
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
'Imports CrystalDecisions.Web.CrystalReportViewerBase
Imports System.Data.OleDb
Imports System.IO
Imports CrystalDecisions.ReportSource
Imports System.Web.UI.WebControls.Table

Public Class LoadReport_Excel
    Inherits System.Web.UI.Page
    Dim db As New Connect_Mac5
    Dim ids As String
    Dim uid As String
    Dim uapp As String
    Dim usr() As String
    Dim sql As String
    Dim dt As New DataTable
    Dim sess As String
    Dim sqlPara1 As SqlParameter
    Dim sqlPara2 As SqlParameter
    Dim fname As String
    Dim Formula As FormulaFieldDefinition
    Dim sqlReport, Form As String
    Private sms As New PKMsg("")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim crtableLogoninfos As New TableLogOnInfos
        Dim crtableLogoninfo As New TableLogOnInfo
        Dim crConnectionInfo As New ConnectionInfo
        Dim CrTables As Tables
        Dim CrTable As Table
        Dim cryRpt As New ReportDocument

        sqlReport = Request.QueryString("sqlReport")
        Form = Request.QueryString("Form")


        If Form = "ProductRequest" Then
            'cryRpt.Load(Server.MapPath("~" + HttpRuntime.AppDomainAppVirtualPath + "RPT_ProductMaster.rpt"))
            cryRpt.Load(Server.MapPath("RPT_ProductMaster.rpt"))

        ElseIf Form = "NewProduct" Then

            cryRpt.Load(Server.MapPath("From_Product_Request.rpt"))
        Else

            'cryRpt.Load(Server.MapPath("~" + HttpRuntime.AppDomainAppVirtualPath + "RPT_ProductMasterRequest.rpt"))
            cryRpt.Load(Server.MapPath("RPT_ProductMaster.rpt"))
        End If



        Call CreateCondition()
        If sql <> "" Then
            cryRpt.RecordSelectionFormula = sql
        End If


        With crConnectionInfo
            .ServerName = "10.0.4.211"
            .DatabaseName = "M5CM-AA-09"
            .UserID = "mac5usr"
            .Password = "mac5usr_!@#"
        End With


        CrTables = cryRpt.Database.Tables
        For Each CrTable In CrTables
            crtableLogoninfo = CrTable.LogOnInfo
            crtableLogoninfo.ConnectionInfo = crConnectionInfo
            CrTable.ApplyLogOnInfo(crtableLogoninfo)
        Next


        If Form = "Product" Then
            CrystalReportViewer1.ID = "ProductMaster_" & Date.Now.ToString("dd/MM/yyyy")
        ElseIf Form = "NewProduct" Then
            CrystalReportViewer1.ID = "FormNewProduct_" & Date.Now.ToString("dd/MM/yyyy")
        Else
            CrystalReportViewer1.ID = "ProductMasterRequest_" & Date.Now.ToString("dd/MM/yyyy")
        End If



        CrystalReportViewer1.ReportSource = cryRpt
        CrystalReportViewer1.RefreshReport()

    End Sub

    Sub CreateCondition()
        If sqlReport <> "" Then
            sql = sqlReport '" {VW_FormMaintenanceCoffeeMaker.Form_id} = '" & id & "' "
        End If
    End Sub

End Class