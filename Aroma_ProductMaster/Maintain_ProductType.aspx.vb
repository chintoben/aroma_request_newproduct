﻿Imports System.Data.SqlClient

Public Class Maintain_ProductType
    Inherits System.Web.UI.Page
    Dim db_product As New Connect_product
    Dim sql, sql2, username, CallID, Type, item As String



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        username = Request.QueryString("usr")

        If Not IsPostBack Then
            BindData()
        End If


    End Sub

    Private Sub BindData()

        Dim IntPageNumber As Int32 = 0
        Dim sqlcon As New SqlConnection(db_product.sqlCon)
        Dim cmd As New SqlCommand
        Dim DSet As New DataSet
        Dim sql As String
        Dim constr As String

        Try
            constr = "data source=10.0.24.20;initial catalog=DB_RequestNewProduct;persist security info=false;User ID=armth;Password=Kb5r#Ge9Z3M*mQ;Connect Timeout=0;Max Pool Size=500;Enlist=true"
            sqlcon = New SqlConnection(constr)
            sqlcon.Open()
            'Return True
            'ถ้าเกิด Error ขึ้นให้ใช้ 
        Catch
            Try
                constr = "data source=10.0.24.20;initial catalog=DB_RequestNewProduct;persist security info=false;User ID=armth;Password=Kb5r#Ge9Z3M*mQ;Connect Timeout=0;Pooling=false"
                sqlcon = New SqlConnection(constr)
                sqlcon.Open()
                'Return True
            Catch
                'Return False
            End Try

        End Try
        Try

            sql = "SELECT [ProductType] FROM [dbo].[TB_ProductMaster] group by [ProductType] " & vbCrLf
           

            'sql = sql & " Order by ProductType  "

            Dim dt As New DataTable
            dt = db_product.GetDataTable(sql)
            If dt.Rows.Count > 0 Then
                GridView.DataSource = dt
                GridView.DataBind()
            Else
                ShowNoResultFound(dt, GridView)
            End If

        Catch


        End Try
        sqlcon.Close()

        'Gridview1
        'Attribute to show the Plus Minus Button.
        GridView.HeaderRow.Cells(0).Attributes("data-class") = "expand"
        'Attribute to hide column in Phone.
        ' GridView.HeaderRow.Cells(2).Attributes("data-hide") = "phone"
        ' GridView.HeaderRow.Cells(3).Attributes("data-hide") = "phone"
        'Adds THEAD and TBODY to GridView.
        GridView.HeaderRow.TableSection = TableRowSection.TableHeader

    End Sub

    Protected Sub OnPaging(ByVal sender As Object, ByVal e As GridViewPageEventArgs)
        Me.BindData()
        GridView.PageIndex = e.NewPageIndex
        GridView.DataBind()
    End Sub

    Sub ShowNoResultFound(ByVal dtt As DataTable, ByVal gv As GridView)
        dtt.Rows.Add(dtt.NewRow)
        gv.DataSource = dtt
        gv.DataBind()
        Dim columnsCount As Integer = gv.Columns.Count
        gv.Rows(0).Cells.Clear()
        gv.Rows(0).Cells.Add(New TableCell)
        gv.Rows(0).Cells(0).ColumnSpan = columnsCount
        gv.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
        gv.Rows(0).Cells(0).ForeColor = Drawing.Color.Red
        gv.Rows(0).Cells(0).Font.Bold = True
        gv.Rows(0).Cells(0).Text = "-ไม่พบข้อมูล-"
    End Sub


 

    Protected Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        InsertData()
        Cleardata()
        BindData()
    End Sub

    Sub Cleardata()
        txtProductType.Text = ""

    End Sub

    Sub InsertData()

        Dim query As String = "INSERT INTO [TB_ProductMaster] ("
        query &= "ProductType,CreateBy,CreateDate"
        query &= ")"

        query += " VALUES( "
        query &= "@ProductType,@CreateBy,@CreateDate"
        query &= ")"

        Dim constr As String = ConfigurationManager.ConnectionStrings("ProductConnectionString").ConnectionString

        Using con As SqlConnection = New SqlConnection(constr)
            Using cmd As SqlCommand = New SqlCommand(query)
                cmd.Parameters.AddWithValue("@ProductType", txtProductType.Text)
                cmd.Parameters.AddWithValue("@CreateBy", username)
                cmd.Parameters.AddWithValue("@CreateDate", DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss"))

                cmd.Connection = con
                con.Open()
                cmd.ExecuteNonQuery()
                con.Close()
            End Using
        End Using


    End Sub

End Class