﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ProductMasterDetail.aspx.vb" Inherits="Aroma_ProductMaster.ProductMasterDetail" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="Your description">
<meta name="keywords" content="Your keywords">
<meta name="author" content="Your name">
<link rel="icon" href="images/favicon.ico" type="image/x-icon">
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />

<link rel="stylesheet" href="css/bootstrap.css" type="text/css" media="screen">
<link rel="stylesheet" href="css/bootstrap-responsive.css" type="text/css" media="screen">    
<link rel="stylesheet" href="css/style.css" type="text/css" media="screen">

<script type="text/javascript" src="js/jquery.js"></script>  
<script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="js/superfish.js"></script>

<script type="text/javascript" src="js/jquery.ui.totop.js"></script>

<script type="text/javascript" src="js/cform.js"></script>
<script>
    $(document).ready(function () {
        //	


    }); //
    $(window).load(function () {
        //

    }); //
</script>		
    <style type="text/css">
        .style1
        {
        }
        .style2
        {
            height: 49px;
        }
        .style4
        {
        }
        .style7
        {
            height: 49px;
            width: 162px;
        }
        .style8
        {
            width: 162px;
        }
        .style11
        {
            width: 368px;
        }
        .style12
        {
            width: 162px;
            height: 62px;
        }
        .style13
        {
            height: 62px;
        }
        .style14
        {
            width: 162px;
            height: 27px;
        }
        .style15
        {
            height: 27px;
        }
        .style16
        {
            width: 162px;
            height: 13px;
        }
        .style17
        {
            height: 13px;
        }
        </style>
<!--[if lt IE 8]>
		<div style='text-align:center'><a href="http://www.microsoft.com/windows/internet-explorer/default.aspx?ocid=ie6_countdown_bannercode"><img src="http://www.theie6countdown.com/images/upgrade.jpg"border="0"alt=""/></a></div>  
	<![endif]-->    

<!--[if lt IE 9]>
  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>      
  <link rel="stylesheet" href="css/ie.css" type="text/css" media="screen">
<![endif]-->
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
    <div id="main">
<div class="top1_wrapper">
<div class="top1">
<div class="container">
<div class="row">
<div class="span12">
<div class="top1_inner clearfix">
	
<div class="top2 clearfix">
<header><div class="logo_wrapper"><a href="index.html" class="logo">
        <img src="images/aroma/logotrans.png" alt="" >
        <%--<img src="images/aroma/Lion.png" alt="" width="100" >--%>
        </a></div>
        
  </header>	
<div class="top3 clearfix">
<div class="phone1">
	<div class="txt1">Request New Master</div>
	<div class="txt2">Product</div>
</div>

<div class="search-form-wrapper clearfix">

</div>
</div>

</div>

<div class="menu_wrapper">
<div class="navbar navbar_">
	<div class="navbar-inner navbar-inner_">
		<a class="btn btn-navbar btn-navbar_" data-toggle="collapse" data-target=".nav-collapse_">
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</a>
		<div class="nav-collapse nav-collapse_ collapse">
			<ul class="nav sf-menu clearfix">
            <li class="active"><a href="Main.aspx?usr=<%=Request.QueryString("usr") %>">Home</a></li>
           
            <li><a href="ProductMaster.aspx?usr=<%=Request.QueryString("usr") %>">Product Master</a></li>
            <li><a href="NewMaster.aspx?usr=<%=Request.QueryString("usr") %>">Request New Master</a></li>
            <li><a href="NewMasterAll.aspx?usr=<%=Request.QueryString("usr") %>">Request New Master All</a></li>
           
            <li><a href="Login.aspx">Sign Out</a></li>										
    </ul>
		</div>
	</div>
</div>	
</div>

</div>	
</div>	
</div>	
</div>	
</div>	
</div>
<div id="inner">

<div id="content">
<div class="container">
<div class="row">
<div class="span12">

<div class="breadcrumbs1"><a href="Main.aspx?username=<%=Request.QueryString("username") %>">Home Page</a><span></span><a href="ProductMaster.aspx?username=<%=Request.QueryString("username") %>">Product Master</a> <span></span> ข้อมูล Product Master</div>



<div class="row">
<div class="span8">

<h1>ข้อมูล Product Master</h1>

</div>	

</div>



<div id="note"></div>
<div id="fields">

    <table>
    <tr>
        <td class="style7">
            <asp:Label ID="Label1" runat="server" Text="บริษัท : " Font-Size="Medium" 
                Width="100px" Font-Bold="True"></asp:Label>
            <br />
        </td>
        <td colspan="6" class="style2">

            <asp:DropDownList ID="drpCompany" runat="server" Width="200px" Enabled="False" 
                Height="30px">
                <asp:ListItem>KVN</asp:ListItem>
                <asp:ListItem>LION</asp:ListItem>
            </asp:DropDownList>
        </td>
    </tr>

   
    <tr>
        <td class="style8">
            <asp:Label ID="Label30" runat="server" Text="รหัสสินค้าหลัก :" 
                Font-Size="Medium" Font-Bold="False"></asp:Label>
            </td>
        <td colspan="6">
            <asp:TextBox ID="txtItem" runat="server" Width="200px" Enabled="False" 
                Height="30px"></asp:TextBox>
        </td>
    </tr>
   
   
  
    <tr>
        <td class="style8">
            <asp:Label ID="Label20" runat="server" Text="ชื่อสินค้า  : " 
                Font-Size="Medium" Font-Bold="False"></asp:Label>
            </td>
        <td colspan="6">
            <asp:TextBox ID="txtItemDescription" runat="server" Width="80%" Enabled="False" 
                Height="30px"></asp:TextBox>
        </td>
    </tr>
   
   
  
    <tr>
        <td class="style8">
            <asp:Label ID="Label33" runat="server" Text="Product Type  : " 
                Font-Size="Medium" Font-Bold="False"></asp:Label>
            </td>
        <td class="style4" colspan="2">
            <asp:DropDownList ID="drpProductType" runat="server" Width="400px" 
                Font-Size="Medium">
            </asp:DropDownList>
        </td>
        <td colspan="2">
            &nbsp;</td>
        <td colspan="2">
            &nbsp;</td>
    </tr>
  
    <tr>
        <td class="style8">
            <asp:Label ID="Label34" runat="server" Text="Product Category  : " 
                Font-Size="Medium" Font-Bold="False"></asp:Label>
            </td>
        <td class="style4" colspan="2">
            <asp:DropDownList ID="drpProductCat" runat="server" Width="400px" 
                Font-Size="Medium">
            </asp:DropDownList>
        </td>
        <td colspan="2">
            &nbsp;</td>
        <td colspan="2">
            &nbsp;</td>
    </tr>
  
    <tr>
        <td class="style8">
            <asp:Label ID="Label35" runat="server" Text="Product Group / Machine Type  : " 
                Font-Size="Medium" Font-Bold="False" Width="250px"></asp:Label>
            </td>
        <td class="style4" colspan="2">
            <asp:DropDownList ID="drpProductGroup" runat="server" Width="400px" 
                Font-Size="Medium">
            </asp:DropDownList>
        </td>
        <td colspan="2">
            &nbsp;</td>
        <td colspan="2">
            &nbsp;</td>
    </tr>
  
    <tr>
        <td class="style14">
            <asp:Label ID="Label36" runat="server" Text="Brand  : " 
                Font-Size="Medium" Font-Bold="False"></asp:Label>
            </td>
        <td class="style15" colspan="6">
            <asp:TextBox ID="txtBrand" runat="server" Width="350px" Height="30px"></asp:TextBox>
        </td>
    </tr>
  
    
    <tr>
        <td class="style16">
            <asp:Label ID="Label37" runat="server" Text="Model  : " 
                Font-Size="Medium" Font-Bold="False"></asp:Label>
            </td>
        <td class="style17" colspan="6">
            <asp:TextBox ID="txtModel" runat="server" Width="350px" Height="30px"></asp:TextBox>
        </td>
    </tr>
  
    
    <tr>
        <td class="style8">
            <asp:Label ID="Label38" runat="server" Text="Group Head : " 
                Font-Size="Medium" Font-Bold="False"></asp:Label>
            </td>
        <td class="style4">
            <asp:DropDownList ID="drpGroup" runat="server">
            </asp:DropDownList>
        </td>
        <td align ="rigth" class="style1"> 
            &nbsp;</td>
        <td colspan="4">
            &nbsp;</td>
    </tr>
  
    
    <tr>
        <td class="style8">
            <asp:Label ID="Label39" runat="server" Text="Color : " 
                Font-Size="Medium" Font-Bold="False"></asp:Label>
            </td>
        <td class="style4">
            <asp:TextBox ID="txtColor" runat="server" Width="200px" Height="30px"></asp:TextBox>
        </td>
        <td align ="rigth" class="style1"> 
            &nbsp;</td>
        <td colspan="4">
            &nbsp;</td>
    </tr>
  
    
    <tr>
        <td class="style8">
            <asp:Label ID="Label40" runat="server" Text="Option 1 : " 
                Font-Size="Medium" Font-Bold="False"></asp:Label>
            </td>
        <td class="style4">
            <asp:TextBox ID="txtOption1" runat="server" Width="200px" Height="30px"></asp:TextBox>
        </td>
        <td align =rigth class="style1"> 
            <asp:Label ID="Label41" runat="server" Text="Option 2 : " 
                Font-Size="Medium" Font-Bold="False"></asp:Label>
            </td>
        <td>
            <asp:TextBox ID="txtOption2" runat="server" Width="150px" Height="30px"></asp:TextBox>
        </td>
        <td align = rigth class="style11" colspan="2">
            <asp:Label ID="Label42" runat="server" Text="Option 3 : " 
                Font-Size="Medium" Font-Bold="False" Width="100px"></asp:Label>
            </td>
        <td>
            <asp:TextBox ID="txtOption3" runat="server" Width="150px" Height="30px"></asp:TextBox>
        </td>
    </tr>
  
    <tr>
        <td class="style8">
            <asp:Label ID="Label43" runat="server" Text="สำหรับ Marketing" 
                Font-Size="Medium" Font-Bold="False"></asp:Label>
            </td>
        <td class="style4">
            <asp:CheckBox ID="chkCheckRawMaterial" runat="server" Font-Size="Medium" 
                Text="CheckRawMaterial" />
        </td>
        <td align ="rigth" class="style1"> 
            &nbsp;</td>
        <td>
            <asp:CheckBox ID="chkCheckMachine" runat="server" Font-Size="Medium" 
                Text="CheckMachine" />
        </td>
        <td class="style11" colspan="2">
            &nbsp;</td>
        <td>
            &nbsp;</td>
    </tr>
  
    <tr>
        <td class="style8">
            <asp:Label ID="Label44" runat="server" Text="ประเภทสินค้า" 
                Font-Size="Medium" Font-Bold="False"></asp:Label>
            </td>
        <td class="style4">

            <asp:DropDownList ID="drpChaodoi" runat="server" Width="200px" Height="30px">
                <asp:ListItem Value="Y">สินค้าชาวดอย</asp:ListItem>
                <asp:ListItem Value="N">ไม่ใช่สินค้าชาวดอย</asp:ListItem>
                <asp:ListItem Value="C">สินค้า Common</asp:ListItem>
            </asp:DropDownList>
        </td>
        <td align ="rigth" class="style1"> 
            &nbsp;</td>
        <td>
            &nbsp;</td>
        <td class="style11" colspan="2">
            &nbsp;</td>
        <td>
            &nbsp;</td>
    </tr>
    <tr>
        <td class="style12">
            <asp:Label ID="Label32" runat="server" Text="หมายเหตุ/เพิ่มเติม  : " 
                Font-Size="Medium" Font-Bold="True"></asp:Label>
            </td>
        <td class="style13" colspan="6">
            <asp:TextBox ID="txtRemark" runat="server" Width="80%" Height="40px" 
                TextMode="MultiLine"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td class="style8">
            &nbsp;</td>
        <td class="style4" colspan="6">
            &nbsp;</td>
    </tr>
    <tr>
        <td class="style8">
            &nbsp;</td>
        <td class="style4" colspan="6">

        <asp:Button ID="btnSave" runat="server" Text="Save" class="submit" 
                BackColor="#003399" BorderColor="#0066FF" Font-Bold="True" Font-Size="Medium" 
                ForeColor="White" Height="35px" Width="100px"/>

            &nbsp;
            
                </td>
    </tr>
    </table>


     

<%--
	<form id="ajax-contact-form" class="form-horizontal" action="javascript:alert('success!');">
		<div class="row">
			<div class="span4">
				<div class="control-group">
				    <label class="control-label" for="inputName">Your full name:</label>
				    <div class="controls">				      
				        <asp:CheckBoxList ID="CheckBoxList3" runat="server" RepeatColumns="4" 
                Font-Size="Medium" Width="800px">
                  <asp:ListItem Value="kvn">บจ. เค.วี.เอ็น.อิมปอร์ต เอ็กซ์ปอร์ต  (1991)</asp:ListItem>
                  <asp:ListItem Value="lion">บจ. ไลอ้อน ทรี-สตาร์</asp:ListItem>
            
                  <asp:ListItem Value="kvn">บจ. เค.วี.เอ็น.อิมปอร์ต เอ็กซ์ปอร์ต  (1991)</asp:ListItem>
                  <asp:ListItem Value="lion">บจ. ไลอ้อน ทรี-สตาร์</asp:ListItem>
                  <asp:ListItem Value="lion">บจ. ไลอ้อน ทรี-สตาร์</asp:ListItem>
                
              </asp:CheckBoxList>
                        
                    </div>
				</div>				
			</div>	
			<div class="span4">
				<div class="control-group">
				    <label class="control-label" for="inputEmail">Your email:</label>
				    <div class="controls">				      
				      <input class="span4" type="text" id="inputEmail" name="email" value="Your email:" onBlur="if(this.value=='') this.value='Your email:'" onFocus="if(this.value =='Your email:' ) this.value=''">
				    </div>
				</div>
			</div>		
			<div class="span4">
				<div class="control-group">
				    <label class="control-label" for="inputPhone">Phone number:</label>
				    <div class="controls">				      
				      <input class="span4" type="text" id="inputPhone" name="phone" value="Phone number:" onBlur="if(this.value=='') this.value='Phone number:'" onFocus="if(this.value =='Phone number:' ) this.value=''">
				    </div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="span12">
				<div class="control-group">
				    <label class="control-label" for="inputMessage">Message:</label>
				    <div class="controls">				      				      
				      <textarea class="span12" id="inputMessage" name="content" onBlur="if(this.value=='') this.value='Message:'" 
                        onFocus="if(this.value =='Message:' ) this.value=''">Message:</textarea>
				    </div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="span12">
				<div class="control-group capthca">
				    <label class="control-label" for="inputCapthca">Capthca:</label>
				    <div class="controls">				      
				      <input class="" type="text" id="inputCapthca" name="capthca" value="Capthca:" onBlur="if(this.value=='') this.value='Capthca:'" onFocus="if(this.value =='Capthca:' ) this.value=''">
				      <img src="captcha/captcha.php">
				    </div>
				</div>
			</div>
		</div>
		<button type="submit" class="submit">submit</button>
	</form>--%>


</div>	

	
	

</div>	
</div>	
</div>	
</div>


<div class="bot1">
<div class="container">
<div class="row">
<div class="span12">
<div class="bot1_inner">
<div class="row">
<div class="span4">
	
<div class="logo2_wrapper"><a href="index.html" class="logo2"><img src="images/aroma/logotrans.png" alt=""></a></div>

<footer><div class="copyright">Copyright   © 2018. All rights reserved.<br><a href="#">Aroma Group</a></div></footer>


</div>
<div class="span4">
	
<div class="bot1_title">Contact Us</div>

สำนักงานใหญ่ อโรม่า กรุ๊ป<br>
บริษัท เค.วี.เอ็น.อิมปอร์ต เอกซ์ปอร์ต (1991) จำกัด<br>
เลขที่ 43 ชั้น 2 ซอยนาคนิวาส 6 ถนนนาคนิวาส แขวงลาดพร้าว <br>
เขตลาดพร้าว กรุงเทพฯ 10230<br>
โทรศัพท์ : 02 159-8999<br>
โทรสาร : 02 538-8144, 02 539-5597

<%--E-mail: <a href="#">mail@demosite.com</a>--%>


</div>
<div class="span4">
	
<%--<div class="bot1_title">Follow Us:</div>	

<div class="social_wrapper">
		<ul class="social clearfix">
	    <li><a href="#"><img src="images/social_ic1.png"></a></li>
	    <li><a href="#"><img src="images/social_ic2.png"></a></li>
	    <li><a href="#"><img src="images/social_ic3.png"></a></li>	    
	    <li><a href="#"><img src="images/social_ic4.png"></a></li>
		</ul>
	</div>
--%>


</div>	

</div>	
</div>
</div>	
</div>	
</div>	
</div>

</div>	
</div>
<script type="text/javascript" src="js/bootstrap.js"></script>



              <script src="https://code.jquery.com/jquery-2.1.1.min.js" type="text/javascript"></script>
            <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2-rc.1/css/select2.min.css" rel="stylesheet" />
            <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2-rc.1/js/select2.min.js" type="text/javascript"></script>
            <script type="text/javascript">
                $(document).ready(function () {
                    $("#drpProductType").select2({
                    });
                });

                $(document).ready(function () {
                    $("#drpProductCat").select2({
                    });
                });

                $(document).ready(function () {
                    $("#drpProductGroup").select2({
                    });
                });

                $(document).ready(function () {
                    $("#drpGroup").select2({
                    });
                });

            </script>  


    </div>
    </form>
</body>
</html>