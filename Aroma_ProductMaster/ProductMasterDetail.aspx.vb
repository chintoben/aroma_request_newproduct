﻿Imports System.Data.SqlClient
Imports System.Net.Mail

Public Class ProductMasterDetail
    Inherits System.Web.UI.Page
    Dim db_Mac5 As New Connect_Mac5
    Dim db_HR As New Connect_HR
    Dim dt, dt2, dtt As New DataTable
    Dim sql, sql2, username, CallID, type, item, Company As String
    Private sms As New PKMsg("")
    Dim DocID As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        username = Request.QueryString("usr")
        item = Request.QueryString("item")
        Company = Request.QueryString("Company")

        If Not IsPostBack Then

            txtItem.Text = item

            sql = " select * "
            sql += " from [VW_ProductMasterGroup] "
            sql += " where Item = '" & item & "' and Company = '" & Company & "' "
            dt = db_Mac5.GetDataTable(sql)
            If dt.Rows.Count > 0 Then
                LoadData()
            End If

        End If
    End Sub

    Sub LoadData()
        sql = " select * "
        sql += " from [VW_ProductMasterGroup] "
        sql += " where Item = '" & txtItem.Text & "' and Company = '" & Company & "' "
        dt = db_Mac5.GetDataTable(sql)
        If dt.Rows.Count > 0 Then

            drpCompany.SelectedValue = dt.Rows(0)("company")
            txtItemDescription.Text = dt.Rows(0)("ItemDescription")
            drpProductType.SelectedValue = dt.Rows(0)("ProductType")
            txtItemDescription.Text = dt.Rows(0)("ItemDescription")
            drpProductType.SelectedValue = dt.Rows(0)("ProductType")
            drpProductCat.SelectedValue = dt.Rows(0)("ProductCategory")
            drpProductGroup.SelectedValue = dt.Rows(0)("ProductGroup")
            txtItem.Text = dt.Rows(0)("Item")
            txtBrand.Text = dt.Rows(0)("Brand")
            txtModel.Text = dt.Rows(0)("Model")
            drpGroup.SelectedValue = dt.Rows(0)("GroupHead")
            txtColor.Text = dt.Rows(0)("Color")
            txtOption1.Text = dt.Rows(0)("Option1")
            txtOption2.Text = dt.Rows(0)("Option2")
            txtOption3.Text = dt.Rows(0)("Option3")

            drpChaodoi.SelectedValue = dt.Rows(0)("Check1")

            If dt.Rows(0)("CheckRawMaterial") = "Y" Then
                chkCheckRawMaterial.Checked = True
            Else
                chkCheckRawMaterial.Checked = False
            End If

            If dt.Rows(0)("CheckMachine") = "Y" Then
                chkCheckMachine.Checked = True
            Else
                chkCheckMachine.Checked = False
            End If


            txtRemark.Text = dt.Rows(0)("Remark")

        End If
    End Sub

    Protected Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click

        sql = " select * "
        sql += " from [TB_ProductMasterGroup] "
        sql += " where Item = '" & txtItem.Text & "' and company = '" & Company & "'  "
        dt = db_Mac5.GetDataTable(sql)
        If dt.Rows.Count >= 1 Then
            UpdateData()
        Else
            InsertProductMaster()
        End If

        LoadData()

        sms.Msg = "บันทึกข้อมูลเรียบร้อยแล้วค่ะ"
        Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "window.alert('" & sms.Msg & "');", True)
        Exit Sub

    End Sub

    Sub InsertProductMaster()

        Dim sql0 As String

        sql0 = "INSERT INTO [TB_ProductMasterGroup] "
        sql0 &= "("
        sql0 &= "Company"
        sql0 &= ",ProductType,ProductCategory,ProductGroup,Item ,CheckRawMaterial ,CheckMachine"
        sql0 &= ",Check1,Check2,Check3,Brand ,Model,GroupHead,Color,Option1 ,Option2,Option3"
        sql0 &= ",Stat,Remark "
        sql0 &= ")"

        sql0 &= "VALUES ('" & drpCompany.SelectedValue & "'"
        sql0 &= " ,'" & drpProductType.SelectedValue & "' ,'" & drpProductCat.SelectedValue & "','" & drpProductGroup.SelectedValue & "','" & txtItem.Text & "' ,'' ,'' "
        sql0 &= " ,'','','','" & txtBrand.Text & "','" & txtModel.Text & "','" & drpGroup.SelectedValue & "','" & txtColor.Text & "','" & txtOption1.Text & "','" & txtOption2.Text & "','" & txtOption3.Text & "'"
        sql0 &= ",'Active','" & txtRemark.Text & "')"


        dtt = db_Mac5.GetDataTable(sql0)

    End Sub

    Sub UpdateData()
        Dim x As Integer
        Dim db_Mac5 As New Connect_Mac5
        Dim Cn As New SqlConnection(db_Mac5.sqlCon)
        Dim Company, CheckRawMaterial, CheckMachine As String


        Dim sSql As String = "UPDATE [TB_ProductMasterGroup] SET  "
        sSql += " Company=@Company"
        sSql += " ,ProductType=@ProductType,ProductCategory=@ProductCategory ,ProductGroup=@ProductGroup ,Item=@Item ,CheckRawMaterial=@CheckRawMaterial,CheckMachine=@CheckMachine "
        sSql += " ,Check1=@Check1 ,Check2=@Check2 ,Check3=@Check3 ,Brand=@Brand ,Model=@Model ,GroupHead=@GroupHead "
        sSql += " ,Color=@Color ,Option1=@Option1 ,Option2=@Option2,Option3=@Option3 "
        sSql += " ,Remark=@Remark"

        sSql += " Where Item ='" & txtItem.Text & "' and Company ='" & drpCompany.SelectedValue & "' "

        Dim command As SqlCommand = New SqlCommand(sSql, Cn)
        Try

            If chkCheckRawMaterial.Checked = True Then
                CheckRawMaterial = "Y"
            Else
                CheckRawMaterial = "N"
            End If

            If chkCheckMachine.Checked = True Then
                CheckMachine = "Y"
            Else
                CheckMachine = "N"
            End If


            command.Parameters.Add("@Company", Data.SqlDbType.VarChar).Value = drpCompany.SelectedValue
            command.Parameters.Add("@ItemDescription", Data.SqlDbType.VarChar).Value = txtItemDescription.Text
            command.Parameters.Add("@ProductType", Data.SqlDbType.VarChar).Value = drpProductType.SelectedValue
            command.Parameters.Add("@ProductCategory", Data.SqlDbType.VarChar).Value = drpProductCat.SelectedValue
            command.Parameters.Add("@ProductGroup", Data.SqlDbType.VarChar).Value = drpProductGroup.SelectedValue
            command.Parameters.Add("@Item", Data.SqlDbType.VarChar).Value = txtItem.Text
            command.Parameters.Add("@CheckRawMaterial", Data.SqlDbType.VarChar).Value = CheckRawMaterial
            command.Parameters.Add("@CheckMachine", Data.SqlDbType.VarChar).Value = CheckMachine
            command.Parameters.Add("@Check1", Data.SqlDbType.VarChar).Value = drpChaodoi.SelectedValue
            command.Parameters.Add("@Check2", Data.SqlDbType.VarChar).Value = ""
            command.Parameters.Add("@Check3", Data.SqlDbType.VarChar).Value = ""
            command.Parameters.Add("@Brand", Data.SqlDbType.VarChar).Value = txtBrand.Text
            command.Parameters.Add("@Model", Data.SqlDbType.VarChar).Value = txtModel.Text
            command.Parameters.Add("@GroupHead", Data.SqlDbType.VarChar).Value = drpGroup.SelectedValue
            command.Parameters.Add("@Color", Data.SqlDbType.VarChar).Value = txtColor.Text
            command.Parameters.Add("@Option1", Data.SqlDbType.VarChar).Value = txtOption1.Text
            command.Parameters.Add("@Option2", Data.SqlDbType.VarChar).Value = txtOption2.Text
            command.Parameters.Add("@Option3", Data.SqlDbType.VarChar).Value = txtOption3.Text
            command.Parameters.Add("@Remark", Data.SqlDbType.VarChar).Value = txtRemark.Text

            command.CommandType = Data.CommandType.Text
            Cn.Open()
            x = command.ExecuteScalar
        Catch x1 As Exception
            Err.Raise(60002, , x1.Message)
        Finally
            Cn.Close()
        End Try

    End Sub

    Protected Sub drpProductType_Init(sender As Object, e As EventArgs) Handles drpProductType.Init
        Dim sqlCon As New SqlConnection(db_Mac5.sqlCon)
        Dim sqlCmd As New SqlCommand
        Dim Rs As SqlDataReader
        Dim sql As String

        sql = "select isnull(ProductType,'')  from [TB_ProductMasterGroup]  group by  [ProductType] "

        If sqlCon.State = ConnectionState.Closed Then
            sqlCon = New SqlConnection(db_Mac5.sqlCon)
            sqlCon.Open()
        End If
        With sqlCmd
            .Connection = sqlCon
            .CommandType = CommandType.Text
            .CommandText = sql
            Rs = .ExecuteReader
        End With
        drpProductType.Items.Clear()
        drpProductType.Items.Add(New ListItem("", 0))
        While Rs.Read
            drpProductType.Items.Add(New ListItem(Rs.GetString(0), Rs.GetString(0)))
        End While
    End Sub

    Protected Sub drpProductCat_Init(sender As Object, e As EventArgs) Handles drpProductCat.Init
        Dim sqlCon As New SqlConnection(db_Mac5.sqlCon)
        Dim sqlCmd As New SqlCommand
        Dim Rs As SqlDataReader
        Dim sql As String

        sql = "select isnull(ProductCategory,'')  from [TB_ProductMasterGroup]  group by  [ProductCategory] "

        If sqlCon.State = ConnectionState.Closed Then
            sqlCon = New SqlConnection(db_Mac5.sqlCon)
            sqlCon.Open()
        End If
        With sqlCmd
            .Connection = sqlCon
            .CommandType = CommandType.Text
            .CommandText = sql
            Rs = .ExecuteReader
        End With
        drpProductCat.Items.Clear()
        drpProductCat.Items.Add(New ListItem("", 0))
        While Rs.Read
            drpProductCat.Items.Add(New ListItem(Rs.GetString(0), Rs.GetString(0)))
        End While
    End Sub

    Protected Sub drpProductGroup_Init(sender As Object, e As EventArgs) Handles drpProductGroup.Init
        Dim sqlCon As New SqlConnection(db_Mac5.sqlCon)
        Dim sqlCmd As New SqlCommand
        Dim Rs As SqlDataReader
        Dim sql As String

        sql = "select isnull(ProductGroup,'')  from [TB_ProductMasterGroup]  group by  [ProductGroup] "

        If sqlCon.State = ConnectionState.Closed Then
            sqlCon = New SqlConnection(db_Mac5.sqlCon)
            sqlCon.Open()
        End If
        With sqlCmd
            .Connection = sqlCon
            .CommandType = CommandType.Text
            .CommandText = sql
            Rs = .ExecuteReader
        End With
        drpProductGroup.Items.Clear()
        drpProductGroup.Items.Add(New ListItem("", 0))
        While Rs.Read
            drpProductGroup.Items.Add(New ListItem(Rs.GetString(0), Rs.GetString(0)))
        End While
    End Sub

    Protected Sub drpGroup_Init(sender As Object, e As EventArgs) Handles drpGroup.Init
        Dim sqlCon As New SqlConnection(db_Mac5.sqlCon)
        Dim sqlCmd As New SqlCommand
        Dim Rs As SqlDataReader
        Dim sql As String

        sql = "select isnull(GroupHead,'')  from [TB_ProductMasterGroup]  group by  isnull(GroupHead,'') "

        If sqlCon.State = ConnectionState.Closed Then
            sqlCon = New SqlConnection(db_Mac5.sqlCon)
            sqlCon.Open()
        End If
        With sqlCmd
            .Connection = sqlCon
            .CommandType = CommandType.Text
            .CommandText = sql
            Rs = .ExecuteReader
        End With
        drpGroup.Items.Clear()
        drpGroup.Items.Add(New ListItem("", 0))
        While Rs.Read
            drpGroup.Items.Add(New ListItem(Rs.GetString(0), Rs.GetString(0)))
        End While
    End Sub

End Class