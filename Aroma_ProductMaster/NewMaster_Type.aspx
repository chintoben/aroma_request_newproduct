﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="NewMaster_Type.aspx.vb" Inherits="Aroma_ProductMaster.NewMaster_Type" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
 <title>Product Master</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="Your description">
<meta name="keywords" content="Your keywords">
<meta name="author" content="Your name">
<link rel="icon" href="images/favicon.ico" type="image/x-icon">
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />

<link rel="stylesheet" href="css/bootstrap.css" type="text/css" media="screen">
<link rel="stylesheet" href="css/bootstrap-responsive.css" type="text/css" media="screen">    
<link rel="stylesheet" href="css/camera.css" type="text/css" media="screen">
<link rel="stylesheet" href="css/style.css" type="text/css" media="screen">

<script type="text/javascript" src="js/jquery.js"></script>  
<script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="js/superfish.js"></script>

<script type="text/javascript" src="js/jquery.ui.totop.js"></script>

<script type="text/javascript" src="js/camera.js"></script>
<script type="text/javascript" src="js/jquery.mobile.customized.min.js"></script>
<script>
    $(document).ready(function () {
        // camera
        $('#camera_wrap').camera({
            //thumbnails: true
            autoAdvance: true,
            mobileAutoAdvance: true,
            //fx					: 'simpleFade',
            height: '45%',
            hover: false,
            loader: 'none',
            navigation: false,
            navigationHover: true,
            mobileNavHover: true,
            playPause: false,
            pauseOnClick: false,
            pagination: true,
            time: 7000,
            transPeriod: 1000,
            minHeight: '200px'
        });

    }); //
    $(window).load(function () {
        //

    }); //
</script>		
<!--[if lt IE 8]>
		<div style='text-align:center'><a href="http://www.microsoft.com/windows/internet-explorer/default.aspx?ocid=ie6_countdown_bannercode"><img src="http://www.theie6countdown.com/images/upgrade.jpg"border="0"alt=""/></a></div>  
	<![endif]-->    

<!--[if lt IE 9]>
  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>      
  <link rel="stylesheet" href="css/ie.css" type="text/css" media="screen">
<![endif]-->
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <div id="main">
<div class="top1_wrapper">
<div class="top1">
<div class="container">
<div class="row">
<div class="span12">
<div class="top1_inner clearfix">
	
<div class="top2 clearfix">
<header><div class="logo_wrapper"><a href="index.html" class="logo">
        <img src="images/aroma/logotrans.png" alt="" >
        <%--<img src="images/aroma/Lion.png" alt="" >--%>
        </a></div>
        
  </header>	
<div class="top3 clearfix">
<div class="phone1">
	<div class="txt1">Request New Master</div>
	<div class="txt2">Product</div>
</div>

<div class="search-form-wrapper clearfix">
<%--<form id="search-form" action="search.php" method="GET" accept-charset="utf-8" class="navbar-form" >
	<input type="text" name="s" value='Search' onBlur="if(this.value=='') this.value='Search'" onFocus="if(this.value =='Search' ) this.value=''">
	<a href="#" onClick="document.getElementById('search-form').submit()"></a>
</form>	--%>
</div>
</div>

</div>

<div class="menu_wrapper">
<div class="navbar navbar_">
	<div class="navbar-inner navbar-inner_">
		<a class="btn btn-navbar btn-navbar_" data-toggle="collapse" data-target=".nav-collapse_">
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</a>
		<div class="nav-collapse nav-collapse_ collapse">
		<ul class="nav sf-menu clearfix">
            <li class="active"><a href="Main.aspx?usr=<%=Request.QueryString("usr") %>">Home</a></li>
            <li><a href="NewMaster_Type.aspx?usr=<%=Request.QueryString("usr") %>">Request New Master</a></li>
            <li><a href="NewMasterAll.aspx?usr=<%=Request.QueryString("usr") %>">Request New Master All</a></li>
            <li><a href="ProductMaster.aspx?usr=<%=Request.QueryString("usr") %>">Product Master</a></li>           
            <li><a href="Maintain_ProductType.aspx?usr=<%=Request.QueryString("usr") %>" target="_blank">จัดการสินค้า</a></li>
           
            <li><a href="Login.aspx">Sign Out</a></li>										
            </ul>
		</div>
	</div>
</div>	
</div>

<%--<div id="slider_wrapper">
<div id="slider" class="clearfix">
<div id="camera_wrap">
	<div data-src="images/slide01.jpg">
		<div class="camera_caption fadeIn">
			<div class="txt1">keep your home clean</div>								
		</div>     
	</div>
	<div data-src="images/slide02.jpg">
		<div class="camera_caption fadeIn">
			<div class="txt1">keep your home clean</div>								
		</div>     
	</div>
	<div data-src="images/slide03.jpg">
		<div class="camera_caption fadeIn">
			<div class="txt1">keep your home clean</div>								
		</div>     
	</div>
	<div data-src="images/slide04.jpg">
		<div class="camera_caption fadeIn">
			<div class="txt1">keep your home clean</div>								
		</div>     
	</div>
	<div data-src="images/slide05.jpg">
		<div class="camera_caption fadeIn">
			<div class="txt1">keep your home clean</div>								
		</div>     
	</div>
</div>	
</div>	
</div>--%>


</div>	
</div>	
</div>	
</div>	
</div>	
</div>
<div id="inner">

<div id="content">
<div class="container">
<div class="row">
<div class="span12">

<%--<div class="phone2">
	<div class="txt1">Product Master</div>
</div>--%>

<div class="breadcrumbs1"><a href="Main.aspx?usr=<%=Request.QueryString("usr") %>">Home Page</a><span></span>Request New Master</div>


<div class="banners">
<div class="row">
<div class="span4 banner banner1">
<div class="banner_inner">
<a href="NewMasterMachine.aspx?usr=<%=Request.QueryString("usr") %>">
	<div class="txt1">Machine</div>
	<div class="txt2">สินค้ากลุ่มเครื่องชงกาแฟ</div>
    <asp:Button ID="Button1" runat="server" Text="Click" class="txt3" />

</a>	
</div>	
</div>
<div class="span4 banner banner1">
<div class="banner_inner">
<a href="NewMasterRawMat.aspx?usr=<%=Request.QueryString("usr") %>">
	<div class="txt1">Raw Materail & Accessory</div>
	<div class="txt2">สินค้ากลุ่มวัตถุดิบ</div>
	 <asp:Button ID="Button2" runat="server" Text="Click" class="txt3" />

</a>	
</div>	
</div>
<div class="span4 banner banner1">
<div class="banner_inner">
<a href="NewMasterSparePart.aspx?usr=<%=Request.QueryString("usr") %>">
	<div class="txt1">Spare part (Import file excel)</div>
	<div class="txt2">สินค้ากลุ่มอะไหล่</div>
	 <asp:Button ID="Button3" runat="server" Text="Click" class="txt3" />

</a>	
</div>	
</div>	



</div>	
</div>

<%--<div class="row">
<div class="span8">

<h1>Welcome to Our Company</h1>

<h3>Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna. </h3>

<p>
	Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna. Ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat consectetuer adipiscing elit. Nunc suscipit. Suspendisse enim arcu, convallis non, cursus sed, dignissim et, est. 
</p>

<a href="#" class="button1">read more</a>

<div class="line1"></div>

<h2>Latest News</h2>

<div class="thumb1">
	<div class="thumbnail clearfix">
		<figure class="img-polaroid"><img src="images/home01.jpg" alt=""></figure>
		<div class="caption">
			<h3>Lorem ipsum dolor sit amet conse</h3>
			<p>
				Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore.
			</p>
			<a href="#" class="button2">details</a>
		</div>
	</div>
</div>

<div class="thumb1">
	<div class="thumbnail clearfix">
		<figure class="img-polaroid"><img src="images/home02.jpg" alt=""></figure>
		<div class="caption">
			<h3>Lorem ipsum dolor sit amet conse</h3>
			<p>
				Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore.
			</p>
			<a href="#" class="button2">details</a>
		</div>
	</div>
</div>

<a href="#" class="button1">news archive</a>






</div>	
<div class="span4">
	
<div class="pad_top1"></div>

<div class="date1">
	<div class="txt1">Exploit Your Ideas</div>
	<div class="txt2">Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do.</div>
</div>

<div class="date1">
	<div class="txt1">Satisfaction Guaranteed</div>
	<div class="txt2">Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do.</div>
</div>

<div class="date1">
	<div class="txt1">Cleaning Since 1985</div>
	<div class="txt2">Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do.</div>
</div>

<div class="box1">
	
<h2>Our Services</h2>

<ul class="ul1">
  <li><a href="#">Lorem ipsum dolor</a></li>
  <li><a href="#">Sit amet consectetue</a></li>
  <li><a href="#">Adipiscing elit</a></li>
  <li><a href="#">Nunc suscipit</a></li>
  <li><a href="#">Suspendisse enim arcu</a></li>  
  <li><a href="#">Convallis non cursus sed</a></li>
  <li><a href="#">Dignissim et est</a></li>
  <li><a href="#">Aenean semper aliquet libero</a></li>  	            		      	      			      
</ul>









</div>





</div>
</div>--%>

	
	

</div>	
</div>	
</div>	
</div>

<div class="bot1">
<div class="container">
<div class="row">
<div class="span12">
<div class="bot1_inner">
<div class="row">
<div class="span4">
	
<div class="logo2_wrapper"><a href="index.html" class="logo2"><img src="images/aroma/logotrans.png" alt=""></a></div>

<footer><div class="copyright">Copyright   © 2018. All rights reserved.<br><a href="#">Aroma Group</a></div></footer>


</div>
<div class="span4">
	
<div class="bot1_title">Contact Us</div>

สำนักงานใหญ่ อโรม่า กรุ๊ป<br>
บริษัท เค.วี.เอ็น.อิมปอร์ต เอกซ์ปอร์ต (1991) จำกัด<br>
เลขที่ 43 ชั้น 2 ซอยนาคนิวาส 6 ถนนนาคนิวาส แขวงลาดพร้าว <br>
เขตลาดพร้าว กรุงเทพฯ 10230<br>
โทรศัพท์ : 02 159-8999<br>
โทรสาร : 02 538-8144, 02 539-5597

<%--E-mail: <a href="#">mail@demosite.com</a>--%>


</div>
<div class="span4">
	
<%--<div class="bot1_title">Follow Us:</div>	

<div class="social_wrapper">
		<ul class="social clearfix">
	    <li><a href="#"><img src="images/social_ic1.png"></a></li>
	    <li><a href="#"><img src="images/social_ic2.png"></a></li>
	    <li><a href="#"><img src="images/social_ic3.png"></a></li>	    
	    <li><a href="#"><img src="images/social_ic4.png"></a></li>
		</ul>
	</div>
--%>

</div>	

</div>	
</div>
</div>	
</div>	
</div>	
</div>

</div>	
</div>
<script type="text/javascript" src="js/bootstrap.js"></script>
    </div>
    </form>
</body>
</html>
