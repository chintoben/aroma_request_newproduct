﻿Imports System.Data.SqlClient
Imports System.Net.Mail
Imports System.IO
Imports System.Data.OleDb


Public Class NewMasterMachine
    Inherits System.Web.UI.Page

    Dim db_Mac5 As New Connect_Mac5
    Dim db_Enpro As New Connect_Enpro

    Dim db_HR As New Connect_HR
    Dim DB_Product As New Connect_product

    Dim dt, dt2, dt3, dt4, dtt As New DataTable
    Dim sql, sql2, sql3, sql4, usr, CallID, Type, item, RequestId, RequestId2 As String
    Private sms As New PKMsg("")
    'Private con As New SqlConnection("data source=10.0.24.20;initial catalog=DB_Service;persist security info=false;User ID=sa;Password=Quax_005;Connect Timeout=0;Max Pool Size=500;Enlist=true")
    Dim DocID As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        usr = Request.QueryString("usr")
        RequestId = Request.QueryString("id")

        'LoadData(RequestId)
        If usr = "" Then
            Server.Transfer("login.aspx")
        End If


        If Not IsPostBack Then

            If RequestId <> "" Then

                'check ข้อมูลเก่า
                sql3 = " select * "
                sql3 += " from  [VW_ProductRequest_Machine]"
                sql3 += " where  RequestId = '" & RequestId & "'"
                dt3 = DB_Product.GetDataTable(sql3)
                If dt3.Rows.Count > 0 Then
                    LoadData(RequestId)
                    lblRequestID.Text = RequestId
                    lblStatus.Text = "" & dt3.Rows(0)("stat")

                    If dt3.Rows(0)("stat") = "Waiting Account Approve" Then

                        'check สิทธิ์
                        sql4 = " select * "
                        sql4 += " from  [TB_UserApprove]"
                        sql4 += " where   [Stauts_approve] = 'Waiting Account Approve' and  [EmployeeID]  = '" & usr & "'"
                        dt4 = DB_Product.GetDataTable(sql4)
                        If dt4.Rows.Count > 0 Then
                            btnSubmit.Visible = False
                        Else
                            btnSave.Visible = False
                            btnSubmit.Visible = False
                            btnApprove.Visible = False
                        End If

                    ElseIf dt3.Rows(0)("stat") = "Draft" Then
                        btnApprove.Visible = False

                    ElseIf dt3.Rows(0)("stat") = "Complete" Then
                        btnSave.Visible = False
                        btnSubmit.Visible = False
                        btnApprove.Visible = False
                    End If
                End If


            Else
                'new
                drpRequester.SelectedValue = usr
                sql = " select * "
                sql += " from VW_Employee "
                sql += " where Employee_id = '" & usr & "' "
                dt = db_HR.GetDataTable(sql)
                If dt.Rows.Count > 0 Then
                    txtDep.Text = dt.Rows(0)("department_name")
                    drpRequester.SelectedValue = dt.Rows(0)("fullname")
                End If
                txtRequestDate.Text = DateTime.Now.ToString("yyyy-MM-dd")

                'check ข้อมูลเก่า
                sql2 = " select * "
                sql2 += " from  [VW_ProductRequest_Machine]"
                sql2 += " where  stat = 'draft' and CreateBy = '" & usr & "'"
                dt2 = DB_Product.GetDataTable(sql2)
                If dt2.Rows.Count > 0 Then
                    RequestId = dt2.Rows(0)("RequestId")
                    lblRequestID.Text = RequestId
                    LoadData(RequestId)
                    lblStatus.Text = "Draf"
                End If

                btnApprove.Visible = False
            End If




        End If
    End Sub

    Protected Sub drpRequester_Init(sender As Object, e As EventArgs) Handles drpRequester.Init
        Dim sqlCon As New SqlConnection(db_HR.sqlCon)
        Dim sqlCmd As New SqlCommand
        Dim Rs As SqlDataReader
        Dim sql As String

        sql = "select [employee_id] as id,[fullname] from [TB_Employee] order by employee_id asc"

        If sqlCon.State = ConnectionState.Closed Then
            sqlCon = New SqlConnection(db_HR.sqlCon)
            sqlCon.Open()
        End If
        With sqlCmd
            .Connection = sqlCon
            .CommandType = CommandType.Text
            .CommandText = sql
            Rs = .ExecuteReader
        End With
        drpRequester.Items.Clear()
        drpRequester.Items.Add(New ListItem("", 0))
        While Rs.Read
            drpRequester.Items.Add(New ListItem(Rs.GetString(0) + " : " + Rs.GetString(1), Rs.GetString(0)))
        End While
    End Sub

    Protected Sub drpRequester_SelectedIndexChanged(sender As Object, e As EventArgs) Handles drpRequester.SelectedIndexChanged
        sql = " select * "
        sql += " from VW_Employee "
        sql += " where Employee_id = '" & drpRequester.SelectedValue & "' "
        dt = db_HR.GetDataTable(sql)
        If dt.Rows.Count > 0 Then
            txtDep.Text = dt.Rows(0)("department_name")
        End If
    End Sub

    Protected Sub drpProductType_Init(sender As Object, e As EventArgs) Handles drpProductType.Init
        Dim sqlCon As New SqlConnection(DB_Product.sqlCon)
        Dim sqlCmd As New SqlCommand
        Dim Rs As SqlDataReader
        Dim sql As String


        sql = "SELECT [ProductType] FROM [dbo].[TB_ProductMaster] where ProductType is not null and  ProductType <> '' group by [ProductType]  "

        If sqlCon.State = ConnectionState.Closed Then
            sqlCon = New SqlConnection(DB_Product.sqlCon)
            sqlCon.Open()
        End If
        With sqlCmd
            .Connection = sqlCon
            .CommandType = CommandType.Text
            .CommandText = sql
            Rs = .ExecuteReader
        End With
        drpProductType.Items.Clear()
        drpProductType.Items.Add(New ListItem("", 0))
        While Rs.Read
            drpProductType.Items.Add(New ListItem(Rs.GetString(0), Rs.GetString(0)))
        End While
    End Sub

    Protected Sub drpProductType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles drpProductType.SelectedIndexChanged
        Dim sqlCon As New SqlConnection(DB_Product.sqlCon)
        Dim sqlCmd As New SqlCommand
        Dim Rs As SqlDataReader
        Dim sql As String


        sql = "SELECT [ProductCategory] FROM [dbo].[TB_ProductMaster] "

        sql = sql & "where ([ProductCategory] Is Not null)  and ProductType  = '" & drpProductType.SelectedValue & "' "

        sql = sql & "group by [ProductCategory]  "


        If sqlCon.State = ConnectionState.Closed Then
            sqlCon = New SqlConnection(DB_Product.sqlCon)
            sqlCon.Open()
        End If
        With sqlCmd
            .Connection = sqlCon
            .CommandType = CommandType.Text
            .CommandText = sql
            Rs = .ExecuteReader
        End With
        drpProductCat.Items.Clear()
        drpProductCat.Items.Add(New ListItem("", 0))
        While Rs.Read
            drpProductCat.Items.Add(New ListItem(Rs.GetString(0), Rs.GetString(0)))
        End While
    End Sub

    Protected Sub drpProductCat_Init(sender As Object, e As EventArgs) Handles drpProductCat.Init
        Dim sqlCon As New SqlConnection(DB_Product.sqlCon)
        Dim sqlCmd As New SqlCommand
        Dim Rs As SqlDataReader
        Dim sql As String


        sql = "SELECT [ProductCategory] FROM [dbo].[TB_ProductMaster] where ProductCategory is not null  group by [ProductCategory]  "


        If sqlCon.State = ConnectionState.Closed Then
            sqlCon = New SqlConnection(DB_Product.sqlCon)
            sqlCon.Open()
        End If
        With sqlCmd
            .Connection = sqlCon
            .CommandType = CommandType.Text
            .CommandText = sql
            Rs = .ExecuteReader
        End With
        drpProductCat.Items.Clear()
        drpProductCat.Items.Add(New ListItem("", 0))
        While Rs.Read
            drpProductCat.Items.Add(New ListItem(Rs.GetString(0), Rs.GetString(0)))
        End While
    End Sub

    Protected Sub drpProductCat_SelectedIndexChanged(sender As Object, e As EventArgs) Handles drpProductCat.SelectedIndexChanged
        Dim sqlCon As New SqlConnection(DB_Product.sqlCon)
        Dim sqlCmd As New SqlCommand
        Dim Rs As SqlDataReader
        Dim sql As String


        sql = " SELECT [ProductGroup] FROM [dbo].[TB_ProductMaster] "
        sql = sql & " where  ProductCategory  = '" & drpProductCat.SelectedValue & "' "
        sql = sql & " and  ProductType  = '" & drpProductType.SelectedValue & "' "


        If sqlCon.State = ConnectionState.Closed Then
            sqlCon = New SqlConnection(DB_Product.sqlCon)
            sqlCon.Open()
        End If
        With sqlCmd
            .Connection = sqlCon
            .CommandType = CommandType.Text
            .CommandText = sql
            Rs = .ExecuteReader
        End With
        drpProductGroup.Items.Clear()
        drpProductGroup.Items.Add(New ListItem("", 0))
        While Rs.Read
            drpProductGroup.Items.Add(New ListItem(Rs.GetString(0), Rs.GetString(0)))
        End While
    End Sub


    Protected Sub drpProductGroup_Init(sender As Object, e As EventArgs) Handles drpProductGroup.Init
        Dim sqlCon As New SqlConnection(DB_Product.sqlCon)
        Dim sqlCmd As New SqlCommand
        Dim Rs As SqlDataReader
        Dim sql As String


        sql = "SELECT [ProductGroup] FROM [dbo].[TB_ProductMaster] where ProductGroup is not null group by [ProductGroup]  "


        If sqlCon.State = ConnectionState.Closed Then
            sqlCon = New SqlConnection(DB_Product.sqlCon)
            sqlCon.Open()
        End If
        With sqlCmd
            .Connection = sqlCon
            .CommandType = CommandType.Text
            .CommandText = sql
            Rs = .ExecuteReader
        End With
        drpProductGroup.Items.Clear()
        drpProductGroup.Items.Add(New ListItem("", 0))
        While Rs.Read
            drpProductGroup.Items.Add(New ListItem(Rs.GetString(0), Rs.GetString(0)))
        End While
    End Sub

    Protected Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        sql = " select * "
        sql += " from [VW_ProductRequest_Machine] "
        sql += " where RequestId = '" & lblRequestID.Text & "' "
        dt = DB_Product.GetDataTable(sql)
        If dt.Rows.Count >= 1 Then
            UpdateData()

            If FileUpload1.FileName <> "" Then
                Call insertIMG()
                insertLogo()  ' LOGO
            End If

        Else
            GenID()
            InsertData(RequestId)
            lblRequestID.Text = RequestId

            If FileUpload1.FileName <> "" Then
                Call insertIMG()
                insertLogo()  ' LOGO
            End If

        End If

        'LoadData()
        LoadData(RequestId)

        'sms.Msg = "บันทึกข้อมูลเรียบร้อยแล้วค่ะ"
        'Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "window.alert('" & sms.Msg & "');", True)
        'Exit Sub

    End Sub


    Sub insertIMG()
        Dim CurrentFileName As String
        CurrentFileName = FileUpload1.FileName

        'If (Path.GetExtension(CurrentFileName).ToLower <> ".jpg") Then
        '    sms.Msg = "You choose the file dishonestly !!! , Please choose be file .jpg"
        '    Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "window.alert('" & sms.Msg & "');", True)
        '    Exit Sub
        'End If

        'If FileUpload1.PostedFile.ContentLength > 131072 Then
        '    sms.Msg = "The size of big too file , the size of the file must 128 KB not exceed!!!"
        '    Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "window.alert('" & sms.Msg & "');", True)
        '    Exit Sub
        'End If

        Dim CurrentPath As String = Server.MapPath("~/Temp/")

        FileUpload1.PostedFile.SaveAs(CurrentPath & FileUpload1.FileName)
        FileUpload1.PostedFile.SaveAs("c:\Temp\" & FileUpload1.FileName)
        Image1.ImageUrl = CurrentPath & FileUpload1.FileName
        txtPic.Text = "c:\Temp\" & FileUpload1.FileName

    End Sub

    Sub insertLogo()
        Dim db As New Connect_HR
        Dim sql As String
        Dim tr As SqlTransaction
        Dim sqlcon As New SqlConnection(DB_Product.sqlCon)

        If sqlcon.State = ConnectionState.Closed Then
            sqlcon = New SqlConnection(DB_Product.sqlCon)
            sqlcon.Open()
        End If

        tr = sqlcon.BeginTransaction

        Try
            Dim cmd As SqlCommand = New SqlCommand(sql, sqlcon, tr)
            If txtPic.Text <> "" Then
                Dim FileN As FileStream
                Dim Ln As Int32
                Dim oBinaryReader As BinaryReader
                Dim oImgByteArray As Byte()

                FileN = New FileStream(txtPic.Text, FileMode.Open, FileAccess.Read)
                oBinaryReader = New BinaryReader(FileN)
                oImgByteArray = oBinaryReader.ReadBytes(CInt(FileN.Length))
                Ln = CInt(FileN.Length)
                oBinaryReader.Close()
                FileN.Close()

                sql = "update [TB_ProductRequest_Machine] set image_name=@FF1,image=@FF2 where RequestId  = '" & lblRequestID.Text & "'"
                Dim cmd3 As SqlCommand = New SqlCommand(sql, sqlcon, tr)
                cmd3.Parameters.Add("@FF1", SqlDbType.VarChar, 0).Value = txtitem.Text + Path.GetExtension(txtpic.Text).ToLower
                cmd3.Parameters.Add("@FF2", SqlDbType.Image, 0).Value = oImgByteArray
                cmd3.CommandTimeout = 0
                cmd3.ExecuteNonQuery()
            End If

            tr.Commit()
            sqlcon.Close()
        Catch ex As Exception
            tr.Rollback()
            sqlcon.Close()
            Exit Sub
        End Try

    End Sub


    Sub InsertData(RequestId)

        Dim kvn, lion As String

        If chkKVN.Checked = True Then
            kvn = "Yes"
        Else
            kvn = "No"
        End If

        If chkLION.Checked = True Then
            lion = "Yes"
        Else
            lion = "No"
        End If


        Dim query As String = "INSERT INTO TB_ProductRequest_Machine ("
        query &= "RequestId,Company_KVN,Company_LION,Company_other,Requester,RequestDate,RequestFor,objective"
        query &= ",manufacturer,productName,unitMeasure,Product_Group,unitMeasure_other,Product_Group_other,Item,ItemDescription"
        query &= ",ProductType,ProductCategory,ProductGroup"
        query &= ",Brand,Model,GroupHead,Color"
        query &= ",Remark,Stat,CreateBy,CreateDate"
        query &= ")"

        query &= " VALUES( "
        query &= "@RequestId,@Company_KVN,@Company_LION,@Company_other,@Requester,@RequestDate,@RequestFor,@objective"
        query &= ",@manufacturer,@productName,@unitMeasure,@Product_Group,@unitMeasure_other,@Product_Group_other,@Item,@ItemDescription"
        query &= ",@ProductType,@ProductCategory,@ProductGroup"
        query &= ",@Brand,@Model,@GroupHead,@Color"
        query &= ",@Remark,@Stat,@CreateBy,@CreateDate"
        query &= ")"

        Dim constr As String = ConfigurationManager.ConnectionStrings("ProductConnectionString").ConnectionString

        Using con As SqlConnection = New SqlConnection(constr)
            Using cmd As SqlCommand = New SqlCommand(query)
                cmd.Parameters.AddWithValue("@RequestId", RequestId)
                cmd.Parameters.AddWithValue("@Company_KVN", kvn)
                cmd.Parameters.AddWithValue("@Company_LION", lion)
                cmd.Parameters.AddWithValue("@Company_other", txtcompany.Text)
                cmd.Parameters.AddWithValue("@Requester", drpRequester.SelectedValue)
                cmd.Parameters.AddWithValue("@RequestDate", DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss"))
                cmd.Parameters.AddWithValue("@RequestFor", rdoFor.SelectedValue)
                cmd.Parameters.AddWithValue("@objective", rdoObjective.SelectedValue)

                cmd.Parameters.AddWithValue("@manufacturer", rdoUnit.SelectedValue)
                cmd.Parameters.AddWithValue("@productName", txtProductname.Text)
                cmd.Parameters.AddWithValue("@Product_Group", rdoproductgroup.SelectedValue)
                cmd.Parameters.AddWithValue("@unitMeasure", rdoUnit.SelectedValue)
                cmd.Parameters.AddWithValue("@unitMeasure_other", txtunit.Text)
                cmd.Parameters.AddWithValue("@Product_Group_other", txtProductgroup.Text)

                cmd.Parameters.AddWithValue("@Item", txtitem.Text)
                cmd.Parameters.AddWithValue("@ItemDescription", txtItemDescription.Text)
                cmd.Parameters.AddWithValue("@ProductType", drpProductType.SelectedValue)
                cmd.Parameters.AddWithValue("@ProductCategory", drpProductCat.SelectedValue)
                cmd.Parameters.AddWithValue("@ProductGroup", drpProductGroup.SelectedValue)

                cmd.Parameters.AddWithValue("@Brand", drpBrand.SelectedValue)
                cmd.Parameters.AddWithValue("@Model", drpModel.SelectedValue)
                cmd.Parameters.AddWithValue("@GroupHead", "")
                cmd.Parameters.AddWithValue("@Color", drpColor.SelectedValue)

                cmd.Parameters.AddWithValue("@Remark", txtremark.Text)
                cmd.Parameters.AddWithValue("@Stat", "Draft")

                cmd.Parameters.AddWithValue("@CreateBy", usr)
                cmd.Parameters.AddWithValue("@CreateDate", DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss"))

                cmd.Connection = con
                con.Open()
                cmd.ExecuteNonQuery()
                con.Close()
            End Using
        End Using


    End Sub

    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        Dim kvn, lion, company As String

        If chkKVN.Checked = False And chkLION.Checked = False And txtcompany.Text = "" Then
            sms.Msg = "กรุณาระบุบริษัท"
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "window.alert('" & sms.Msg & "');", True)
            Exit Sub
        End If


        ''****
        If txtitem.Text = "" Then
            sms.Msg = "กรุณาระบุ Product code"
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "window.alert('" & sms.Msg & "');", True)
            Exit Sub
        End If

        If drpProductType.SelectedValue = "0" Or drpProductCat.SelectedValue = "0" Or drpProductGroup.SelectedValue = "0" Then
            sms.Msg = "กรุณาระบุข้อมูลกลุ่มสินค้า ให้ครบ"
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "window.alert('" & sms.Msg & "');", True)
            Exit Sub
        End If


        sql = " select * "
        sql += " from [VW_ProductRequest_Machine] "
        sql += " where RequestId = '" & lblRequestID.Text & "' "
        dt = DB_Product.GetDataTable(sql)
        If dt.Rows.Count >= 1 Then
            UpdateData()
        Else
            GenID()
            InsertData(RequestId)
        End If


        SentMail_Acct()
        UpdateStatus()

        'sms.Msg = "บันทึกข้อมูลเรียบร้อยแล้วค่ะ"
        'Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "window.alert('" & sms.Msg & "');", True)
        'Exit Sub

        Server.Transfer("NewMasterAll.aspx?usr=" & usr)

    End Sub

    Sub UpdateStatus()

        Dim x As Integer
        Dim db_Mac5 As New Connect_Mac5
        Dim Cn As New SqlConnection(DB_Product.sqlCon)

        Dim sSql As String = "UPDATE [TB_ProductRequest_Machine] SET  "
        sSql += " Stat='Waiting Account Approve' ,ModifyBy=@ModifyBy ,ModifyDate=@ModifyDate "

        sSql += " Where RequestId ='" & lblRequestID.Text & "' "

        Dim command As SqlCommand = New SqlCommand(sSql, Cn)
        Try

            command.Parameters.Add("@ModifyBy", Data.SqlDbType.VarChar).Value = usr
            command.Parameters.Add("@ModifyDate", Data.SqlDbType.VarChar).Value = DateTime.Now.ToString("yyyy-MM-dd")

            command.CommandType = Data.CommandType.Text
            Cn.Open()
            x = command.ExecuteScalar
        Catch x1 As Exception
            Err.Raise(60002, , x1.Message)
        Finally
            Cn.Close()
        End Try

    End Sub

    Sub GenID()

        Dim conn As New Connect_product
        Dim sql As String = "Select * from TB_ProductRequest_Machine where len(RequestId) = 8 and substring(cast(year(getdate()) as varchar(50)),3,2) = substring(RequestId,2,2) "
        Dim sql2 As String = "Select Max(Right(RequestId,4)) as RequestId from TB_ProductRequest_Machine where len(RequestId) = 8  and substring(cast(year(getdate()) as varchar(50)),3,2) = substring(RequestId,2,2)"

        dt = DB_Product.GetDataTable(sql)
        dt2 = DB_Product.GetDataTable(sql2)

        If dt.Rows.Count <= 0 Then
            DocID = "M" & Now.ToString("yy") & "-" & "0001"
        Else
            Dim newID As Integer = CInt(dt2.Rows(0)("RequestId"))
            newID += 1
            DocID = "M" & Now.ToString("yy") & "-" & newID.ToString("0000")
        End If
        RequestId = DocID

    End Sub


    Sub UpdateData()

        Dim x As Integer
        Dim db_Mac5 As New Connect_Mac5
        Dim Cn As New SqlConnection(DB_Product.sqlCon)
        Dim Company, kvn, lion, ProductType, ProductCat, ProductGroup As String


        Dim sSql As String = "UPDATE [TB_ProductRequest_Machine] SET  "
        sSql += " Company_KVN=@Company_KVN,Company_LION=@Company_LION,Company_other=@Company_other,Requester=@Requester,RequestDate=@RequestDate,RequestFor=@RequestFor,objective=@objective"
        sSql += " ,manufacturer=@manufacturer,productName=@productName,unitMeasure=@unitMeasure,Product_Group=@Product_Group,unitMeasure_other=@unitMeasure_other,Product_Group_other=@Product_Group_other"
        sSql += " ,Item=@Item,ItemDescription=@ItemDescription"
        sSql += " ,ProductType=@ProductType,ProductCategory=@ProductCategory,ProductGroup=@ProductGroup"
        sSql += " ,Brand=@Brand,Model=@Model,GroupHead=@GroupHead,Color=@Color"
        sSql += " ,Remark=@Remark ,ModifyBy=@ModifyBy ,ModifyDate=@ModifyDate "

        sSql += " Where RequestId ='" & lblRequestID.Text & "' "

        Dim command As SqlCommand = New SqlCommand(sSql, Cn)
        Try

            If chkKVN.Checked = True Then
                kvn = "Yes"
            Else
                kvn = "No"
            End If

            If chkLION.Checked = True Then
                lion = "Yes"
            Else
                lion = "No"
            End If



            command.Parameters.Add("@Company_KVN", Data.SqlDbType.VarChar).Value = kvn
            command.Parameters.Add("@Company_LION", Data.SqlDbType.VarChar).Value = lion
            command.Parameters.Add("@Company_other", Data.SqlDbType.VarChar).Value = txtcompany.Text

            command.Parameters.Add("@Requester", Data.SqlDbType.VarChar).Value = drpRequester.SelectedValue
            command.Parameters.Add("@RequestDate", Data.SqlDbType.VarChar).Value = txtRequestDate.Text
            command.Parameters.Add("@RequestFor", Data.SqlDbType.VarChar).Value = rdoFor.SelectedValue
            command.Parameters.Add("@objective", Data.SqlDbType.VarChar).Value = rdoObjective.SelectedValue
            command.Parameters.Add("@manufacturer", Data.SqlDbType.VarChar).Value = txtmanu.Text
            command.Parameters.Add("@productName", Data.SqlDbType.VarChar).Value = txtProductname.Text
            command.Parameters.Add("@unitMeasure", Data.SqlDbType.VarChar).Value = rdoUnit.SelectedValue
            command.Parameters.Add("@Product_Group", Data.SqlDbType.VarChar).Value = rdoproductgroup.SelectedValue
            command.Parameters.Add("@unitMeasure_other", Data.SqlDbType.VarChar).Value = txtunit.Text
            command.Parameters.Add("@Product_Group_other", Data.SqlDbType.VarChar).Value = txtProductgroup.Text

            command.Parameters.Add("@Item", Data.SqlDbType.VarChar).Value = txtitem.Text
            command.Parameters.Add("@ItemDescription", Data.SqlDbType.VarChar).Value = txtItemDescription.Text
            command.Parameters.Add("@ProductType", Data.SqlDbType.VarChar).Value = drpProductType.SelectedValue
            command.Parameters.Add("@ProductCategory", Data.SqlDbType.VarChar).Value = drpProductCat.SelectedValue
            command.Parameters.Add("@ProductGroup", Data.SqlDbType.VarChar).Value = drpProductGroup.SelectedValue
            command.Parameters.Add("@Brand", Data.SqlDbType.VarChar).Value = drpBrand.SelectedValue
            command.Parameters.Add("@Model", Data.SqlDbType.VarChar).Value = drpModel.SelectedValue
            command.Parameters.Add("@GroupHead", Data.SqlDbType.VarChar).Value = ""
            command.Parameters.Add("@Color", Data.SqlDbType.VarChar).Value = drpColor.SelectedValue


            command.Parameters.Add("@Remark", Data.SqlDbType.VarChar).Value = txtremark.Text
            command.Parameters.Add("@ModifyBy", Data.SqlDbType.VarChar).Value = usr
            command.Parameters.Add("@ModifyDate", Data.SqlDbType.VarChar).Value = DateTime.Now.ToString("yyyy-MM-dd")


            command.CommandType = Data.CommandType.Text
            Cn.Open()
            x = command.ExecuteScalar
        Catch x1 As Exception
            Err.Raise(60002, , x1.Message)
        Finally
            Cn.Close()
        End Try

    End Sub

    Sub SentMail_Acct()
        '''''http://projectsvbnet.blogspot.com/2013/09/email-vbnet.html
        'LeaveOnline@aromathailandapp.com
        'leaveonline2016
        'mail.yourdomain.com
        'smtp port 25

        Dim i As Integer = 1
        Dim Role As String = ""
        Dim sql As String
        Dim name As String
        Dim strBody As String

        sql = "SELECT fullname  "
        sql = sql & "  FROM VW_Employee "
        sql = sql & " where Employee_id = '" & usr & "'"
        dt = db_HR.GetDataTable(sql)
        If dt.Rows.Count > 0 Then
            name = dt.Rows(0)("Fullname")
        End If


        'If Len(txtMyemail.Text) = 0 Or Len(txtMypassword.Text) = 0 Then
        '    MsgBox("ค่าอีเมล์โฮสต์หรือรหัสผ่านโฮสต์ยังไม่ได้ป้อนข้อมูล", MsgBoxStyle.Critical + MsgBoxStyle.ApplicationModal, "ล้มเหลว")
        '    txtMyemail.Focus()
        'Else
        Try
            'If Len(txtTo.Text) = 0 Then
            '    MsgBox("คุณต้องป้อนอีเมล์ที่จะส่งก่อนทำการส่ง", MsgBoxStyle.Critical + MsgBoxStyle.ApplicationModal, "ล้มเหลว")
            'Else
            'โฮสต์ และ เลขพอร์ต ที่คุณจะส่งอีเมล
            'และคุณต้องมีบัญชีของ Gmail ถ้าจะใช้ smtp.gmail.com
            Dim smtp As New SmtpClient("mail.aromathailandapp.com", 25)
            'สร้างตัวแปร eMailmessage เก็บการส่งข้อความอีเมล
            Dim eMailmessage As New System.Net.Mail.MailMessage() 'New MailMessage
            'กำหนดความปลอดภัยในการเข้ารหัสการส่งข้อความให้เป็น True
            smtp.EnableSsl = False
            'ติดตั้งโฮสต์ของ Gmail
            'smtp.Credentials = New System.Net.NetworkCredential("aroma.leave@gmail.com", "aroma1991")
            smtp.Credentials = New System.Net.NetworkCredential("Product@aromathailandapp.com", "product2018")
            'กำค่าส่วนประกอบของอีเมล์ เช่น ข้อความ, หัวข้อ, จากผู้ส่ง และ ผู้รับ
            ' eMailmessage.Subject = "แจ้งยอดการชำระเงิน"

            eMailmessage.Subject = "แจ้งตั้งรหัสสินค้าใหม่ กลุ่ม Machine Doc No. " & lblRequestID.Text

            strBody = "<html>" & vbCrLf
            strBody = strBody & "<body>" & vbCrLf
            strBody = strBody & "<FONT face=Tahoma>" & vbCrLf

            strBody = strBody & "<table>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td> เรียน  หน่วยงานบัญชี" & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>" & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td height=20  valign=middle style=font-family:Georgia, 'Times New Roman', Times, serif; font-size:30px; color:#0033cc;>" & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>" & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf


            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>แจ้งตั้งรหัสสินค้าใหม่ กลุ่ม Machine" & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>" & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "</table>" & vbCrLf

            strBody = strBody & "<table>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>Doc No. : " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>" & lblRequestID.Text & " " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>Requested For : " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>" & rdoFor.SelectedValue & " " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>Requested Objective : " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>" & rdoObjective.SelectedValue & " " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>Product Code : " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>" & txtitem.Text & " " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>Product Name : " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>" & txtItemDescription.Text & " " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>Product Type : " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>" & drpProductType.SelectedValue & " " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>Product Category : " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>" & drpProductCat.SelectedValue & " " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>Product Group : " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>" & drpProductGroup.SelectedValue & " " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>Remark : " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>" & txtremark.Text & " " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>" & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>" & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf
            strBody = strBody & "</table>" & vbCrLf

            strBody = strBody & "<table>" & vbCrLf
            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td> รายละเอียดสินค้าตาม Link ด้านล่าง  " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>https://armapplication.com/Product/Login.aspx?id=" & lblRequestID.Text & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td height=20  valign=middle style=font-family:Georgia, 'Times New Roman', Times, serif; font-size:30px; color:#0033cc;>" & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td> จึงเรียนมาเพื่อทราบ" & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>" & name & " " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf
            strBody = strBody & "</table>" & vbCrLf

            strBody = strBody & "</FONT >" & vbCrLf
            strBody = strBody & "</body>" & vbCrLf
            strBody = strBody & "<html>"

            eMailmessage.Body = strBody
            eMailmessage.IsBodyHtml = True

            eMailmessage.From = New MailAddress("Product@aromathailandapp.com", "Product Master")


            eMailmessage.To.Add("rathawit@aromathailand.com,onumau@aromathailand.com,panittas@aromathailand.com")
            'eMailmessage.To.Add("Haruthaic@aromathailand.com,panittas@aromathailand.com")
            eMailmessage.CC.Add("callcenter@aromathailand.com,chonaweej@aromathailand.com,laddawansee@aromathailand.com,sirikarnnge@aromathailand.com,supansas@aromathailand.com,somjaip@aromathailand.com,praserts@aromathailand.com,sudaratche@aromathailand.com") '
            'eMailmessage.To.Add(email)


            smtp.Send(eMailmessage)
            'MsgBox("ส่งอีเมล์สำเร็จ", vbInformation, "รายงานผล")
            '   End If
        Catch ex As Exception
            ' MsgBox("ข้อมูลผิดพลาด กรุณาตรวจสอบข้อมูลอีกครั้ง", MsgBoxStyle.Critical + MsgBoxStyle.ApplicationModal, "การทำงานผิดพลาด")
            sms.Msg = "ข้อมูลผิดพลาด กรุณาตรวจสอบข้อมูลอีกครั้ง"
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "window.alert('" & sms.Msg & "');", True)
        End Try
        ' End If

    End Sub

    Protected Sub drpBrand_Init(sender As Object, e As EventArgs) Handles drpBrand.Init
        Dim sqlCon As New SqlConnection(DB_Product.sqlCon)
        Dim sqlCmd As New SqlCommand
        Dim Rs As SqlDataReader
        Dim sql As String


        sql = "SELECT [Brand] FROM [dbo].[TB_ProductBrand] order by Brand "


        If sqlCon.State = ConnectionState.Closed Then
            sqlCon = New SqlConnection(DB_Product.sqlCon)
            sqlCon.Open()
        End If
        With sqlCmd
            .Connection = sqlCon
            .CommandType = CommandType.Text
            .CommandText = sql
            Rs = .ExecuteReader
        End With
        drpBrand.Items.Clear()
        drpBrand.Items.Add(New ListItem("", 0))
        While Rs.Read
            drpBrand.Items.Add(New ListItem(Rs.GetString(0), Rs.GetString(0)))
        End While
    End Sub

    Protected Sub drpModel_Init(sender As Object, e As EventArgs) Handles drpModel.Init
        Dim sqlCon As New SqlConnection(DB_Product.sqlCon)
        Dim sqlCmd As New SqlCommand
        Dim Rs As SqlDataReader
        Dim sql As String


        sql = "SELECT [model] FROM [dbo].[TB_ProductModel] order by model "


        If sqlCon.State = ConnectionState.Closed Then
            sqlCon = New SqlConnection(DB_Product.sqlCon)
            sqlCon.Open()
        End If
        With sqlCmd
            .Connection = sqlCon
            .CommandType = CommandType.Text
            .CommandText = sql
            Rs = .ExecuteReader
        End With
        drpModel.Items.Clear()
        drpModel.Items.Add(New ListItem("", 0))
        While Rs.Read
            drpModel.Items.Add(New ListItem(Rs.GetString(0), Rs.GetString(0)))
        End While
    End Sub

    Protected Sub drpColor_Init(sender As Object, e As EventArgs) Handles drpColor.Init
        Dim sqlCon As New SqlConnection(DB_Product.sqlCon)
        Dim sqlCmd As New SqlCommand
        Dim Rs As SqlDataReader
        Dim sql As String


        sql = "SELECT [color] FROM [dbo].[TB_ProductColor] order by color "


        If sqlCon.State = ConnectionState.Closed Then
            sqlCon = New SqlConnection(DB_Product.sqlCon)
            sqlCon.Open()
        End If
        With sqlCmd
            .Connection = sqlCon
            .CommandType = CommandType.Text
            .CommandText = sql
            Rs = .ExecuteReader
        End With
        drpColor.Items.Clear()
        drpColor.Items.Add(New ListItem("", 0))
        While Rs.Read
            drpColor.Items.Add(New ListItem(Rs.GetString(0), Rs.GetString(0)))
        End While
    End Sub

    Sub LoadData(RequestId)

        sql = " select * "
        sql += " from  [VW_ProductRequest_Machine]"
        sql += " where RequestId = '" & RequestId & "' "
        dt = DB_Product.GetDataTable(sql)
        If dt.Rows.Count > 0 Then

            If dt.Rows(0)("Company_KVN") = "Yes" Then
                chkKVN.Checked = True
            End If

            If dt.Rows(0)("Company_LION") = "Yes" Then
                chkLION.Checked = True
            End If

            txtcompany.Text = dt.Rows(0)("company_other")

            drpRequester.SelectedValue = dt.Rows(0)("Requester")
            txtDep.Text = dt.Rows(0)("RequesterDepartment")
            txtRequestDate.Text = dt.Rows(0)("RequestDate")

            ' txtDep.Text = dt.Rows(0)("RequesterDepartment")

            ',[RequestFor]
            '<asp:ListItem Selected="True">Set Promotion</asp:ListItem>
            '    <asp:ListItem>Buy New Product</asp:ListItem>           
            '    <asp:ListItem>Interco Purchase</asp:ListItem>
            '    <asp:ListItem Value="Other">, please spcify :</asp:ListItem>
            If dt.Rows(0)("RequestFor") = "Set Promotion" Then
                rdoFor.Items.FindByValue("Set Promotion").Selected = True
            ElseIf dt.Rows(0)("objective") = "Buy New Produc" Then
                rdoFor.Items.FindByValue("Buy New Produc").Selected = True
            ElseIf dt.Rows(0)("objective") = "Interco Purchase" Then
                rdoFor.Items.FindByValue("Interco Purchase").Selected = True
            ElseIf dt.Rows(0)("objective") = "Other" Then
                rdoFor.Items.FindByValue("Other").Selected = True
            End If

            ',[objective]
            If dt.Rows(0)("objective") = "For Sell" Then
                rdoObjective.Items.FindByValue("For Sell").Selected = True
            Else
                rdoObjective.Items.FindByValue("For Demo").Selected = True
            End If

            ',[manufacturer]
            ',[productName]
            txtmanu.Text = dt.Rows(0)("manufacturer")
            txtProductname.Text = dt.Rows(0)("productName")


            ',[unitMeasure]
            ',[Product_Group]
            ',[unitMeasure_other]
            ',[Product_Group_other]
            If dt.Rows(0)("unitMeasure") = "carton/case" Then
                rdoUnit.Items.FindByValue("carton/case").Selected = True
            ElseIf dt.Rows(0)("unitMeasure") = "box" Then
                rdoUnit.Items.FindByValue("box").Selected = True
            ElseIf dt.Rows(0)("unitMeasure") = "bottle" Then
                rdoUnit.Items.FindByValue("bottle").Selected = True
            ElseIf dt.Rows(0)("unitMeasure") = "piece" Then
                rdoUnit.Items.FindByValue("piece").Selected = True
            ElseIf dt.Rows(0)("unitMeasure") = "unit" Then
                rdoUnit.Items.FindByValue("unit").Selected = True
            Else
                rdoUnit.Items.FindByValue("Other").Selected = True
            End If

            If dt.Rows(0)("Product_Group") = "Machine" Then
                rdoproductgroup.Items.FindByValue("Machine").Selected = True
            ElseIf dt.Rows(0)("Product_Group") = "Part/Spare part" Then
                rdoproductgroup.Items.FindByValue("Part/Spare part").Selected = True
            Else
                rdoproductgroup.Items.FindByValue("Other").Selected = True
            End If

            txtunit.Text = dt.Rows(0)("unitMeasure_other")
            txtProductgroup.Text = dt.Rows(0)("Product_Group_other")

            ',[Item]
            ',[ItemDescription]
            ',[ProductType]
            ',[ProductCategory]
            ',[ProductGroup]

            txtitem.Text = dt.Rows(0)("Item")
            txtItemDescription.Text = dt.Rows(0)("ItemDescription")

            drpProductType.SelectedValue = dt.Rows(0)("ProductType")
            drpProductCat.SelectedValue = dt.Rows(0)("ProductCategory")
            drpProductGroup.SelectedValue = dt.Rows(0)("ProductGroup")

            ',[Brand]
            ',[Model]
            ',[GroupHead]
            ',[Color]
            drpBrand.SelectedValue = dt.Rows(0)("Brand")
            drpModel.SelectedValue = dt.Rows(0)("Model")
            drpColor.SelectedValue = dt.Rows(0)("Color")

            ',[Remark]
            txtremark.Text = dt.Rows(0)("Remark")

            If Not IsDBNull(dt.Rows(0)("image")) Then
                Image1.ImageUrl = ReadImage(dt.Rows(0)("RequestId"))
            End If

        End If
    End Sub

    Function ReadImage(ByVal strID As String)
        Return ("Readimage.aspx?ImageID=" & strID)
    End Function

    Protected Sub btnApprove_Click(sender As Object, e As EventArgs) Handles btnApprove.Click
        sql = " select * "
        sql += " from [VW_ProductRequest_Machine] "
        sql += " where RequestId = '" & lblRequestID.Text & "' "
        dt = DB_Product.GetDataTable(sql)
        If dt.Rows.Count >= 1 Then
            UpdateData()
            'Else
            '    GenID()
            '    InsertData(RequestId)
        End If

        If chkKVN.Checked = True Then
            InsertBI_kvn()
            insert_enpro()
        End If

        If chkLION.Checked = True Then
            InsertBI_lion()
            insert_mac5()
        End If

        UpdateStatus_complete()
        'SentMail_product()


        Server.Transfer("NewMasterAll.aspx?usr=" & usr)


    End Sub

    Sub UpdateStatus_complete()

        Dim x As Integer
        Dim db_Mac5 As New Connect_Mac5
        Dim Cn As New SqlConnection(DB_Product.sqlCon)

        Dim sSql As String = "UPDATE [TB_ProductRequest_Machine] SET  "
        sSql += " Stat='Complete' ,ModifyBy=@ModifyBy ,ModifyDate=@ModifyDate "
        sSql += " ,AcctBy=@AcctBy , AcctDate=@AcctDate"

        sSql += " Where RequestId ='" & lblRequestID.Text & "' "

        Dim command As SqlCommand = New SqlCommand(sSql, Cn)
        Try
            command.Parameters.Add("@AcctBy", Data.SqlDbType.VarChar).Value = usr
            command.Parameters.Add("@AcctDate", Data.SqlDbType.VarChar).Value = DateTime.Now.ToString("yyyy-MM-dd")
            command.Parameters.Add("@ModifyBy", Data.SqlDbType.VarChar).Value = usr
            command.Parameters.Add("@ModifyDate", Data.SqlDbType.VarChar).Value = DateTime.Now.ToString("yyyy-MM-dd")

            command.CommandType = Data.CommandType.Text
            Cn.Open()
            x = command.ExecuteScalar
        Catch x1 As Exception
            Err.Raise(60002, , x1.Message)
        Finally
            Cn.Close()
        End Try

    End Sub


    Sub SentMail_product()
        '''''http://projectsvbnet.blogspot.com/2013/09/email-vbnet.html
        'LeaveOnline@aromathailandapp.com
        'leaveonline2016
        'mail.yourdomain.com
        'smtp port 25

        Dim i As Integer = 1
        Dim Role As String = ""
        Dim sql As String
        Dim name As String
        Dim strBody As String

        sql = "SELECT fullname  "
        sql = sql & "  FROM [dbo].[TB_Employee] "
        sql = sql & " where Employee_id = '" & usr & "'"
        dt = db_HR.GetDataTable(sql)
        If dt.Rows.Count > 0 Then
            name = dt.Rows(0)("Fullname")
        End If


        'If Len(txtMyemail.Text) = 0 Or Len(txtMypassword.Text) = 0 Then
        '    MsgBox("ค่าอีเมล์โฮสต์หรือรหัสผ่านโฮสต์ยังไม่ได้ป้อนข้อมูล", MsgBoxStyle.Critical + MsgBoxStyle.ApplicationModal, "ล้มเหลว")
        '    txtMyemail.Focus()
        'Else
        Try
            'If Len(txtTo.Text) = 0 Then
            '    MsgBox("คุณต้องป้อนอีเมล์ที่จะส่งก่อนทำการส่ง", MsgBoxStyle.Critical + MsgBoxStyle.ApplicationModal, "ล้มเหลว")
            'Else
            'โฮสต์ และ เลขพอร์ต ที่คุณจะส่งอีเมล
            'และคุณต้องมีบัญชีของ Gmail ถ้าจะใช้ smtp.gmail.com
            Dim smtp As New SmtpClient("mail.aromathailandapp.com", 25)
            'สร้างตัวแปร eMailmessage เก็บการส่งข้อความอีเมล
            Dim eMailmessage As New System.Net.Mail.MailMessage() 'New MailMessage
            'กำหนดความปลอดภัยในการเข้ารหัสการส่งข้อความให้เป็น True
            smtp.EnableSsl = False
            'ติดตั้งโฮสต์ของ Gmail
            'smtp.Credentials = New System.Net.NetworkCredential("aroma.leave@gmail.com", "aroma1991")
            smtp.Credentials = New System.Net.NetworkCredential("Product@aromathailandapp.com", "product2018")
            'กำค่าส่วนประกอบของอีเมล์ เช่น ข้อความ, หัวข้อ, จากผู้ส่ง และ ผู้รับ
            ' eMailmessage.Subject = "แจ้งยอดการชำระเงิน"

            eMailmessage.Subject = "แจ้งตั้งรหัสสินค้าใหม่ กลุ่ม Machine" '& txtItem.Text

            strBody = "<html>" & vbCrLf
            strBody = strBody & "<body>" & vbCrLf
            strBody = strBody & "<FONT face=Tahoma>" & vbCrLf

            strBody = strBody & "<table>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td> เรียน  ผู้เกี่ยวข้อง" & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>" & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td height=20  valign=middle style=font-family:Georgia, 'Times New Roman', Times, serif; font-size:30px; color:#0033cc;>" & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>" & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td> ดำเนินการตั้งข้อมูลสินค้าตามรายละเอียดด้านล่าง เรียบร้อยค่ะ" & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>" & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "</table>" & vbCrLf

            strBody = strBody & "<table>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>Requested For : " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>" & rdoFor.SelectedValue & " " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>Requested Objective : " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>" & rdoObjective.SelectedValue & " " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>Product Code : " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>" & txtitem.Text & " " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>Product Name : " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>" & txtItemDescription.Text & " " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>Product Type : " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>" & drpProductType.SelectedValue & " " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>Product Category : " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>" & drpProductCat.SelectedValue & " " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>Product Group : " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>" & drpProductGroup.SelectedValue & " " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>Remark : " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>" & txtremark.Text & " " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf


            strBody = strBody & "</table>" & vbCrLf

            strBody = strBody & "<table>" & vbCrLf
            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td> รายละเอียดสินค้าตาม Link ด้านล่าง  " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>https://armapplication.com/Product/Login.aspx?id=" & lblRequestID.Text & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td height=20  valign=middle style=font-family:Georgia, 'Times New Roman', Times, serif; font-size:30px; color:#0033cc;>" & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td> จึงเรียนมาเพื่อทราบ" & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>" & name & " " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf
            strBody = strBody & "</table>" & vbCrLf

            strBody = strBody & "</FONT >" & vbCrLf
            strBody = strBody & "</body>" & vbCrLf
            strBody = strBody & "<html>"

            eMailmessage.Body = strBody
            eMailmessage.IsBodyHtml = True

            eMailmessage.From = New MailAddress("Product@aromathailandapp.com", "Product Master")


            eMailmessage.To.Add("rathawit@aromathailand.com,onumau@aromathailand.com,panittas@aromathailand.com")
            'eMailmessage.To.Add("Haruthaic@aromathailand.com,panittas@aromathailand.com")
            eMailmessage.CC.Add("callcenter@aromathailand.com,chonaweej@aromathailand.com,laddawansee@aromathailand.com,sirikarnnge@aromathailand.com,supansas@aromathailand.com,somjaip@aromathailand.com,praserts@aromathailand.com,sudaratche@aromathailand.com") '
            'eMailmessage.To.Add(email)


            smtp.Send(eMailmessage)
            'MsgBox("ส่งอีเมล์สำเร็จ", vbInformation, "รายงานผล")
            '   End If
        Catch ex As Exception
            ' MsgBox("ข้อมูลผิดพลาด กรุณาตรวจสอบข้อมูลอีกครั้ง", MsgBoxStyle.Critical + MsgBoxStyle.ApplicationModal, "การทำงานผิดพลาด")
            sms.Msg = "ข้อมูลผิดพลาด กรุณาตรวจสอบข้อมูลอีกครั้ง"
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "window.alert('" & sms.Msg & "');", True)
        End Try
        ' End If

    End Sub



    Sub insert_mac5()
        Dim Cn As New SqlConnection(db_Mac5.sqlCon)
        Dim sqlcon As New OleDbConnection(db_Mac5.cnPAStr)
        Dim sqlcom As New OleDbCommand


        Dim code As String = txtitem.Text
        Dim part_no As String = ""
        Dim group As String = "MC"
        Dim item_desc As String = txtItemDescription.Text
        Dim brand As String = drpBrand.SelectedValue
        Dim unit As String = "เครื่อง"
        Dim unit_price As String = ""

        sql = " SELECT STKcode "
        sql += " from    [STK] "
        sql += " where STKcode = '" & code & "'"
        dt = db_Mac5.GetDataTable(sql)
        If dt.Rows.Count <= 0 Then   'check ซ้ำ

            Dim sSql As String = "INSERT INTO [STK] "
            sSql += " ("
            sSql += " STKcode,STKgroup,STKcode2,STKdescT1,STKdescT2,STKdescT3,STKdescE1,STKdescE2,STKdescE3"
            sSql += " ,STKmax,STKmin,STKunit1,STKunit2,STKconv1 ,STKconv2,STKsnsv,STKuname0,STKuname1,STKuname2 "
            sSql += " ,STKuname3,STKuname4,STKuname5,STKqU1,STKqU2,STKqU3,STKqU4,STKqU5,STKqE1,STKqE2,STKqE3 "
            sSql += " ,STKqE4,STKqE5,STKqN1,STKqN2,STKqN3,STKqN4,STKqN5,STKvat,STKexpire,STKhide,STKuse2,STKu2name"
            sSql += " ,STKfx ,STKacP,STKacS ,STKacC,STKlock,STKmemo ,STKacC1,STKacC2 ,STKeditLK,STKrefWE"
            sSql += " ,STKbarC1,STKbarC2,STKbarC3,STKsortNT,STKsortNE,STKeditDT,STKstatus,STKsto,STKeditMF"
            sSql += " )"

            sSql += " SELECT '" & code & "','" & group & "','" & part_no & "','" & item_desc & "','" & brand & "','" & unit_price & "',STKdescE1,STKdescE2,STKdescE3"
            sSql += " ,STKmax,STKmin,STKunit1,STKunit2,STKconv1 ,STKconv2,STKsnsv,STKuname0,'" & unit & "',STKuname2 "
            sSql += " ,STKuname3,STKuname4,STKuname5,STKqU1,STKqU2,STKqU3,STKqU4,STKqU5,STKqE1,STKqE2,STKqE3 "
            sSql += " ,STKqE4,STKqE5,STKqN1,STKqN2,STKqN3,STKqN4,STKqN5,STKvat,STKexpire,STKhide,STKuse2,STKu2name"
            sSql += " ,STKfx ,'51020102','41030100' ,STKacC,STKlock,STKmemo ,STKacC1,STKacC2 ,STKeditLK,STKrefWE"
            sSql += " ,STKbarC1,STKbarC2,STKbarC3,'" & item_desc & "',STKsortNE, getdate(),STKstatus,'KHM-01','MIS'"
            sSql += " FROM [dbo].[STK] "
            sSql += "  where  stkcode like 'M-VT-0044-N'"

            If sqlcon.State = ConnectionState.Closed Then
                sqlcon = New OleDbConnection(db_Mac5.cnPAStr)
                sqlcon.Open()
            End If
            With sqlcom
                .Connection = sqlcon
                .CommandType = CommandType.Text
                .CommandText = sSql
                .ExecuteNonQuery()
            End With

        End If


    End Sub


    Sub insert_enpro()

        Dim code As String = txtitem.Text
        Dim part_no As String = ""
        Dim group As String = "MC"
        Dim item_desc As String = txtItemDescription.Text
        Dim brand As String = drpBrand.SelectedValue
        Dim unit As String = "เครื่อง"
        Dim unit_price As String = ""

        sql = " SELECT code "
        sql += " from    [SMITEMMS] "
        sql += " where code = '" & code & "'"
        dt = db_Enpro.GetDataTable(sql)
        If dt.Rows.Count <= 0 Then   'check ซ้ำ

            Dim Cn As New SqlConnection(db_Enpro.sqlCon)
            Dim sqlcon As New OleDbConnection(db_Enpro.cnPAStr)
            Dim sqlcom As New OleDbCommand
            Dim sSql As String = " "
            sSql += " insert into [SMITEMMS] ("
            sSql += " code, [Code_Secondary], [SMSTORMS_ID], [Type], [F_MRP], [Name1_Th], [Name2_Th], [Name3_Th], [Name1_En], [Name2_En]"
            sSql += " ,[Name3_En],[Descr_Th],[Descr_En],[LeadTime_Pd],[Lot_Size_Pd] ,[LeadTime_Dl_Buffer],[RateWaste_Pd],[Weight],[F_Expire],[Alert_Month]"
            sSql += " ,[Alert_Day],[Expire_Month],[Expire_Day] ,[F_Bin] ,[F_Inspect],[F_Budget],[Inspect_SMSTORMS_ID],[Df_Ltsr],[Ltsr_Charecter],[Df_LtsrFormat]"
            sSql += " ,[Ltsr_running],[F_Unit_MS],[Main_SMUNITMS_ID],[Second_SMUNITMS_ID],[Small_SMUNITMS_ID],[Pur_SMUNITMS_ID],[Sale_SMUNITMS_ID],[F_Parallel],[F_Parallel_Lot],[Par1_SMUNITMS_ID]"
            sSql += " ,[Par2_SMUNITMS_ID],[SMGRP1MS_ID],[SMGRP2MS_ID],[SMGRP3MS_ID],[SMGRP4MS_ID],[SMGRP5MS_ID],[Ref1],[Ref2],[Ref3],[Ref4]"
            sSql += " ,[Ref5],[Remark1],[Remark2],[Remark3],[Remark4] ,[Remark5],[F_Active],[F_KIT],[FileName1],[FileName2]"
            sSql += " ,[FileName3],[FileName4],[FileName5],[FileName6],[F_Pararell_Avg],[F_Tool],[F_Control_Tool],[Tool_Expire_Day],[Tool_Expire_Month]     "
            sSql += " ,[Tool_Alert_Day],[Tool_Alert_Month],[F_bom_cost],[min_qty] ,[max_qty],[F_Lot_FIFO],[F_Cost],[Picture1],[Picture2],[Picture3] "
            sSql += " ,[Picture4],[Picture5],[Picture6],[Df_Serial],[Serial_Charecter],[Df_SerialFormat],[Serial_running],[F_IssuePriority],[Mfg_SMUNITMS_ID],[Seq_Group]"
            sSql += " ,[Activity_Date],[Start_Sale_Date],[Last_Sale_Date],[F_Unit_Control],[Width],[Lenght],[Hight],[MRSTPTHD_id],[F_Auto_Convert_P1_Qty],[Status]     "
            sSql += " ,[F_UseRateWaste],[Temp_SMSTORMS_ID],[SMACCOMS_ID],[LeadTime_Pd_Self],[F_RECMoreThanPO],[Qty_RECMoreThanPO],[Percent_RECMoreThanPO],[User_Code],[Update_Date_Time],[Daily_Capacity]   "
            sSql += " ,[Ref6],[F_UseItemWeightAddUnitWeight],[F_RECMoreThanSTD_P],[Qty_RECMoreThanSTD_P],[Percent_RECMoreThanSTD_P],[F_RECMoreThanPR],[Qty_RECMoreThanPR],[Percent_RECMoreThanPR],[f_Integer_Qty],[F_RECMoreThanPO_P]"
            sSql += " ,[Qty_RECMoreThanPO_P],[Percent_RECMoreThanPO_P],[Barcode_SMUNITMS_ID],[Allocate_SMUNITMS_ID],[F_QC_PO],[F_Issue_Full_Lot],[LastPurchaseDate],[f_CanOpenQtySOOverIV],[SMLOGOMS_ID],[f_lot_fifo_pd]    "
            sSql += " ,[Create_SMUSERMS_ID],[Create_Date_Time],[F_RECMoreThanIP],[Qty_RECMoreThanIP],[Percent_RECMoreThanIP],[F_ExpireDate_Manual],[F_CalBackward_AleartDate],[F_GenDocOnlyMRPbyItem],[SMACCOTG_ID],[app_type]"
            sSql += " ,[acc_match_code],[SMLTTPHD_ID],[SA_SMVATTMS_ID],[PA_SMVATTMS_ID],[JC_SMUNITMS_ID]"
            sSql += " )"
            sSql += " SELECT '" & txtitem.Text & "',[Code_Secondary],[SMSTORMS_ID],[Type],[F_MRP],'" & txtItemDescription.Text & "',[Name2_Th],[Name3_Th],[Name1_En],[Name2_En]"
            sSql += " ,[Name3_En],[Descr_Th],[Descr_En],[LeadTime_Pd],[Lot_Size_Pd] ,[LeadTime_Dl_Buffer],[RateWaste_Pd],[Weight],[F_Expire],[Alert_Month]"
            sSql += " ,[Alert_Day],[Expire_Month],[Expire_Day] ,[F_Bin] ,[F_Inspect],[F_Budget],[Inspect_SMSTORMS_ID],[Df_Ltsr],[Ltsr_Charecter],[Df_LtsrFormat]"
            sSql += " ,[Ltsr_running],[F_Unit_MS],[Main_SMUNITMS_ID],[Second_SMUNITMS_ID],[Small_SMUNITMS_ID],[Pur_SMUNITMS_ID],[Sale_SMUNITMS_ID],[F_Parallel],[F_Parallel_Lot],[Par1_SMUNITMS_ID]"
            sSql += " ,[Par2_SMUNITMS_ID],[SMGRP1MS_ID],[SMGRP2MS_ID],[SMGRP3MS_ID],[SMGRP4MS_ID],[SMGRP5MS_ID],[Ref1],[Ref2],[Ref3],[Ref4]"
            sSql += " ,[Ref5],[Remark1],[Remark2],[Remark3],[Remark4] ,[Remark5],[F_Active],[F_KIT],[FileName1],[FileName2]"
            sSql += " ,[FileName3],[FileName4],[FileName5],[FileName6],[F_Pararell_Avg],[F_Tool],[F_Control_Tool],[Tool_Expire_Day],[Tool_Expire_Month]     "
            sSql += " ,[Tool_Alert_Day],[Tool_Alert_Month],[F_bom_cost],[min_qty] ,[max_qty],[F_Lot_FIFO],[F_Cost],[Picture1],[Picture2],[Picture3]"
            sSql += "  ,[Picture4],[Picture5],[Picture6],[Df_Serial],[Serial_Charecter],[Df_SerialFormat],[Serial_running],[F_IssuePriority],[Mfg_SMUNITMS_ID],[Seq_Group]"
            sSql += " ,[Activity_Date],[Start_Sale_Date],[Last_Sale_Date],[F_Unit_Control],[Width],[Lenght],[Hight],[MRSTPTHD_id],[F_Auto_Convert_P1_Qty],[Status]     "
            sSql += " ,[F_UseRateWaste],[Temp_SMSTORMS_ID],[SMACCOMS_ID],[LeadTime_Pd_Self],[F_RECMoreThanPO],[Qty_RECMoreThanPO],[Percent_RECMoreThanPO],[User_Code],[Update_Date_Time],[Daily_Capacity]   "
            sSql += " ,[Ref6],[F_UseItemWeightAddUnitWeight],[F_RECMoreThanSTD_P],[Qty_RECMoreThanSTD_P],[Percent_RECMoreThanSTD_P],[F_RECMoreThanPR],[Qty_RECMoreThanPR],[Percent_RECMoreThanPR],[f_Integer_Qty],[F_RECMoreThanPO_P]"
            sSql += " ,[Qty_RECMoreThanPO_P],[Percent_RECMoreThanPO_P],[Barcode_SMUNITMS_ID],[Allocate_SMUNITMS_ID],[F_QC_PO],[F_Issue_Full_Lot],[LastPurchaseDate],[f_CanOpenQtySOOverIV],[SMLOGOMS_ID],[f_lot_fifo_pd]    "
            sSql += " ,[Create_SMUSERMS_ID],getdate(),[F_RECMoreThanIP],[Qty_RECMoreThanIP],[Percent_RECMoreThanIP],[F_ExpireDate_Manual],[F_CalBackward_AleartDate],[F_GenDocOnlyMRPbyItem],[SMACCOTG_ID],[app_type]"
            sSql += " ,[acc_match_code],[SMLTTPHD_ID],[SA_SMVATTMS_ID],[PA_SMVATTMS_ID],[JC_SMUNITMS_ID]"
            sSql += " FROM [dbo].[SMITEMMS] "
            sSql += "  where  code like 'TEST-01'"

            If sqlcon.State = ConnectionState.Closed Then
                sqlcon = New OleDbConnection(db_Enpro.cnPAStr)
                sqlcon.Open()
            End If
            With sqlcom
                .Connection = sqlcon
                .CommandType = CommandType.Text
                .CommandText = sSql
                .ExecuteNonQuery()
            End With

        End If


    End Sub

    Sub InsertBI_kvn()

        Dim Cn As New SqlConnection(db_Mac5.sqlCon)
        Dim sqlcon As New OleDbConnection(db_Mac5.cnPAStr)
        Dim sqlcom As New OleDbCommand

        sql = " SELECT Item "
        sql += " from    TB_ProductMasterGroup"
        sql += " where Item = '" & txtitem.Text & "' and company = 'KVN' "
        dt = db_Mac5.GetDataTable(sql)
        If dt.Rows.Count <= 0 Then   'check ซ้ำ

            Dim sSql As String = "INSERT INTO [TB_ProductMasterGroup] "
            sSql += " (Company,ProductType,ProductCategory,ProductGroup,item)"
            sSql += " Values("
            sSql += "'KVN'"
            sSql += ",'" & drpProductType.SelectedValue & "'"
            sSql += ",'" & drpProductCat.SelectedValue & "'"
            sSql += ",'" & drpProductGroup.SelectedValue & "'"
            sSql += ",'" & txtitem.Text & "'  )"
            If sqlcon.State = ConnectionState.Closed Then
                sqlcon = New OleDbConnection(db_Mac5.cnPAStr)
                sqlcon.Open()
            End If
            With sqlcom
                .Connection = sqlcon
                .CommandType = CommandType.Text
                .CommandText = sSql
                .ExecuteNonQuery()
            End With

        End If



    End Sub

    Sub InsertBI_lion()

        Dim Cn As New SqlConnection(db_Mac5.sqlCon)
        Dim sqlcon As New OleDbConnection(db_Mac5.cnPAStr)
        Dim sqlcom As New OleDbCommand

        sql = " SELECT Item "
        sql += " from    TB_ProductMasterGroup"
        sql += " where Item = '" & txtitem.Text & "' and company = 'Lion' "
        dt = db_Mac5.GetDataTable(sql)
        If dt.Rows.Count <= 0 Then   'check ซ้ำ

            Dim sSql As String = "INSERT INTO [TB_ProductMasterGroup] "
            sSql += " (Company,ProductType,ProductCategory,ProductGroup,item)"
            sSql += " Values(  "
            sSql += "'Lion'"
            sSql += ",'" & drpProductType.SelectedValue & "'"
            sSql += ",'" & drpProductCat.SelectedValue & "'"
            sSql += ",'" & drpProductGroup.SelectedValue & "'"
            sSql += ",'" & txtitem.Text & "'  )"
            If sqlcon.State = ConnectionState.Closed Then
                sqlcon = New OleDbConnection(db_Mac5.cnPAStr)
                sqlcon.Open()
            End If
            With sqlcom
                .Connection = sqlcon
                .CommandType = CommandType.Text
                .CommandText = sSql
                .ExecuteNonQuery()
            End With

        End If


    End Sub

    Protected Sub btnReport_Click(sender As Object, e As EventArgs) Handles btnReport.Click
        Response.Redirect("http://armapplication.com:90/From/RequestNewMaster?RequestId=" & lblRequestID.Text)


    End Sub

    Protected Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        UpdateData()

        UpdateStatus_cancel()
        SentMail_Cancel()

        Server.Transfer("NewMasterAll.aspx?usr=" & usr)

    End Sub

    Sub UpdateStatus_cancel()

        Dim x As Integer
        Dim db_Mac5 As New Connect_Mac5
        Dim Cn As New SqlConnection(DB_Product.sqlCon)

        Dim sSql As String = "UPDATE [TB_ProductRequest_Machine] SET  "
        sSql += " Stat='Cancel' ,ModifyBy=@ModifyBy ,ModifyDate=@ModifyDate "
        sSql += " ,AcctBy=@AcctBy , AcctDate=@AcctDate"
        sSql += " Where RequestId ='" & lblRequestID.Text & "' "


        Dim command As SqlCommand = New SqlCommand(sSql, Cn)
        Try
            command.Parameters.Add("@AcctBy", Data.SqlDbType.VarChar).Value = usr
            command.Parameters.Add("@AcctDate", Data.SqlDbType.VarChar).Value = DateTime.Now.ToString("yyyy-MM-dd")
            command.Parameters.Add("@ModifyBy", Data.SqlDbType.VarChar).Value = usr
            command.Parameters.Add("@ModifyDate", Data.SqlDbType.VarChar).Value = DateTime.Now.ToString("yyyy-MM-dd")

            command.CommandType = Data.CommandType.Text
            Cn.Open()
            x = command.ExecuteScalar
        Catch x1 As Exception
            Err.Raise(60002, , x1.Message)
        Finally
            Cn.Close()
        End Try

    End Sub

    Sub SentMail_Cancel()
        '''''http://projectsvbnet.blogspot.com/2013/09/email-vbnet.html
        'LeaveOnline@aromathailandapp.com
        'leaveonline2016
        'mail.yourdomain.com
        'smtp port 25

        Dim i As Integer = 1
        Dim Role As String = ""
        Dim sql As String
        Dim name As String
        Dim strBody As String

        sql = "SELECT fullname  "
        sql = sql & "  FROM [dbo].[TB_Employee] "
        sql = sql & " where Employee_id = '" & drpRequester.SelectedValue & "'"
        dt = db_HR.GetDataTable(sql)
        If dt.Rows.Count > 0 Then
            name = dt.Rows(0)("Fullname")
        End If


        'If Len(txtMyemail.Text) = 0 Or Len(txtMypassword.Text) = 0 Then
        '    MsgBox("ค่าอีเมล์โฮสต์หรือรหัสผ่านโฮสต์ยังไม่ได้ป้อนข้อมูล", MsgBoxStyle.Critical + MsgBoxStyle.ApplicationModal, "ล้มเหลว")
        '    txtMyemail.Focus()
        'Else
        Try
            'If Len(txtTo.Text) = 0 Then
            '    MsgBox("คุณต้องป้อนอีเมล์ที่จะส่งก่อนทำการส่ง", MsgBoxStyle.Critical + MsgBoxStyle.ApplicationModal, "ล้มเหลว")
            'Else
            'โฮสต์ และ เลขพอร์ต ที่คุณจะส่งอีเมล
            'และคุณต้องมีบัญชีของ Gmail ถ้าจะใช้ smtp.gmail.com
            Dim smtp As New SmtpClient("mail.aromathailandapp.com", 25)
            'สร้างตัวแปร eMailmessage เก็บการส่งข้อความอีเมล
            Dim eMailmessage As New System.Net.Mail.MailMessage() 'New MailMessage
            'กำหนดความปลอดภัยในการเข้ารหัสการส่งข้อความให้เป็น True
            smtp.EnableSsl = False
            'ติดตั้งโฮสต์ของ Gmail
            'smtp.Credentials = New System.Net.NetworkCredential("aroma.leave@gmail.com", "aroma1991")
            smtp.Credentials = New System.Net.NetworkCredential("Product@aromathailandapp.com", "product2018")
            'กำค่าส่วนประกอบของอีเมล์ เช่น ข้อความ, หัวข้อ, จากผู้ส่ง และ ผู้รับ
            ' eMailmessage.Subject = "แจ้งยอดการชำระเงิน"

            eMailmessage.Subject = "ยกเลิก ตั้งรหัสสินค้าใหม่กลุ่ม Machine Doc No. " & lblRequestID.Text

            strBody = "<html>" & vbCrLf
            strBody = strBody & "<body>" & vbCrLf
            strBody = strBody & "<FONT face=Tahoma>" & vbCrLf

            strBody = strBody & "<table>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td> เรียน  ทุกท่าน" & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>" & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td height=20  valign=middle style=font-family:Georgia, 'Times New Roman', Times, serif; font-size:30px; color:#0033cc;>" & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>" & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf


            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>แจ้งตั้งรหัสสินค้าใหม่ กลุ่ม Machine" & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>" & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "</table>" & vbCrLf

            strBody = strBody & "<table>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>Doc No. : " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>" & lblRequestID.Text & " " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>วัตถุประสงค์ที่ขอตั้งรหัสสินค้าใหม่ : " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>" & rdoObjective.SelectedValue & " " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>รหัสสินค้า : " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>รอบัญชีตั้งรหัส " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>ชื่อสินค้าในระบบ : " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>" & txtItemDescription.Text & " " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>Product Type : " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>" & drpProductType.SelectedValue & " " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>Product Category : " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>" & drpProductCat.SelectedValue & " " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>Product Group : " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>" & drpProductGroup.SelectedValue & " " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>สถานะ : " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "<td>ยกเลิกการตั้งรหัสสินค้า Doc No." & lblRequestID.Text & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "</table>" & vbCrLf

            strBody = strBody & "<table>" & vbCrLf
            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td> รายละเอียดสินค้าตาม Link ด้านล่าง  " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>https://armapplication.com/Product/login.aspx?id=" & lblRequestID.Text & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td height=20  valign=middle style=font-family:Georgia, 'Times New Roman', Times, serif; font-size:30px; color:#0033cc;>" & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td> จึงเรียนมาเพื่อทราบ" & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf

            strBody = strBody & "<tr>" & vbCrLf
            strBody = strBody & "<td>" & name & " " & vbCrLf
            strBody = strBody & "</td>" & vbCrLf
            strBody = strBody & "</tr>" & vbCrLf
            strBody = strBody & "</table>" & vbCrLf

            strBody = strBody & "</FONT >" & vbCrLf
            strBody = strBody & "</body>" & vbCrLf
            strBody = strBody & "<html>"

            eMailmessage.Body = strBody
            eMailmessage.IsBodyHtml = True

            eMailmessage.From = New MailAddress("Product@aromathailandapp.com", "Product Master")


            eMailmessage.To.Add("nattapolyal@aromathailand.com,noknoilue@aromathailand.com,phachpornpha@aromathailand.com,nattapolkhu@aromathailand.com,kanitthag@aromathailand.com,kanokphanaroj@aromathailand.com")
            'eMailmessage.To.Add("Haruthaic@aromathailand.com,panittas@aromathailand.com")
            eMailmessage.CC.Add("rathawit@aromathailand.com,panittas@aromathailand.com,onumau@aromathailand.com,sudaratche@aromathailand.com") '
            'eMailmessage.To.Add(email)


            smtp.Send(eMailmessage)
            'MsgBox("ส่งอีเมล์สำเร็จ", vbInformation, "รายงานผล")
            '   End If
        Catch ex As Exception
            ' MsgBox("ข้อมูลผิดพลาด กรุณาตรวจสอบข้อมูลอีกครั้ง", MsgBoxStyle.Critical + MsgBoxStyle.ApplicationModal, "การทำงานผิดพลาด")
            sms.Msg = "ข้อมูลผิดพลาด กรุณาตรวจสอบข้อมูลอีกครั้ง"
            Page.ClientScript.RegisterStartupScript(Page.GetType(), "", "window.alert('" & sms.Msg & "');", True)
        End Try
        ' End If

    End Sub



End Class