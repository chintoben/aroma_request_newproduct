﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="NewMasterMachine.aspx.vb" Inherits="Aroma_ProductMaster.NewMasterMachine" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
 <title>Product Master</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="Your description">
<meta name="keywords" content="Your keywords">
<meta name="author" content="Your name">
<link rel="icon" href="images/favicon.ico" type="image/x-icon">
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />

<link rel="stylesheet" href="css/bootstrap.css" type="text/css" media="screen">
<link rel="stylesheet" href="css/bootstrap-responsive.css" type="text/css" media="screen">    
<link rel="stylesheet" href="css/camera.css" type="text/css" media="screen">
<link rel="stylesheet" href="css/style.css" type="text/css" media="screen">

<script type="text/javascript" src="js/jquery.js"></script>  
<script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="js/superfish.js"></script>

<script type="text/javascript" src="js/jquery.ui.totop.js"></script>

<script type="text/javascript" src="js/camera.js"></script>
<script type="text/javascript" src="js/jquery.mobile.customized.min.js"></script>
<script>
    $(document).ready(function () {
        // camera
        $('#camera_wrap').camera({
            //thumbnails: true
            autoAdvance: true,
            mobileAutoAdvance: true,
            //fx					: 'simpleFade',
            height: '45%',
            hover: false,
            loader: 'none',
            navigation: false,
            navigationHover: true,
            mobileNavHover: true,
            playPause: false,
            pauseOnClick: false,
            pagination: true,
            time: 7000,
            transPeriod: 1000,
            minHeight: '200px'
        });

    }); //
    $(window).load(function () {
        //

    }); //
</script>		
    <style type="text/css">
        .style1
        {
            height: 49px;
        }
        .style3
        {
            height: 22px;
        }
        .style4
        {
        }
        .style6
        {
            width: 7px;
        }
        .style7
        {
            width: 6px;
            height: 22px;
        }
    </style>
<!--[if lt IE 8]>
		<div style='text-align:center'><a href="http://www.microsoft.com/windows/internet-explorer/default.aspx?ocid=ie6_countdown_bannercode"><img src="http://www.theie6countdown.com/images/upgrade.jpg"border="0"alt=""/></a></div>  
	<![endif]-->    

<!--[if lt IE 9]>
  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>      
  <link rel="stylesheet" href="css/ie.css" type="text/css" media="screen">
<![endif]-->
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <div id="main">
<div class="top1_wrapper">
<div class="top1">
<div class="container">
<div class="row">
<div class="span12">
<div class="top1_inner clearfix">
	
<div class="top2 clearfix">
<header><div class="logo_wrapper"><a href="index.html" class="logo">
        <img src="images/aroma/logotrans.png" alt="" >
        <%--<img src="images/aroma/Lion.png" alt="" >--%>
        </a></div>
        
  </header>	
<div class="top3 clearfix">
<div class="phone1">
	<div class="txt1">Request New Master</div>
	<div class="txt2">Product</div>
</div>

<div class="search-form-wrapper clearfix">
    <%--<form id="search-form" action="search.php" method="GET" accept-charset="utf-8" class="navbar-form" >
	<input type="text" name="s" value='Search' onBlur="if(this.value=='') this.value='Search'" onFocus="if(this.value =='Search' ) this.value=''">
	<a href="#" onClick="document.getElementById('search-form').submit()"></a>
</form>	--%>
</div>
</div>

</div>

<div class="menu_wrapper">
<div class="navbar navbar_">
	<div class="navbar-inner navbar-inner_">
		<a class="btn btn-navbar btn-navbar_" data-toggle="collapse" data-target=".nav-collapse_">
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</a>
		<div class="nav-collapse nav-collapse_ collapse">
		<ul class="nav sf-menu clearfix">
            <li class="active"><a href="Main.aspx?usr=<%=Request.QueryString("usr") %>">Home</a></li>
            <li><a href="NewMaster_Type.aspx?usr=<%=Request.QueryString("usr") %>">Request New Master</a></li>
            <li><a href="NewMasterAll.aspx?usr=<%=Request.QueryString("usr") %>">Request New Master All</a></li>
            <li><a href="ProductMaster.aspx?usr=<%=Request.QueryString("usr") %>">Product Master</a></li>           
            <li><a href="Maintain_ProductType.aspx?usr=<%=Request.QueryString("usr") %>" target="_blank">จัดการสินค้า</a></li>
           
            <li><a href="Login.aspx">Sign Out</a></li>										
            </ul>
		</div>
	</div>
</div>	
</div>

<%--<div id="slider_wrapper">
<div id="slider" class="clearfix">
<div id="camera_wrap">
	<div data-src="images/slide01.jpg">
		<div class="camera_caption fadeIn">
			<div class="txt1">keep your home clean</div>								
		</div>     
	</div>
	<div data-src="images/slide02.jpg">
		<div class="camera_caption fadeIn">
			<div class="txt1">keep your home clean</div>								
		</div>     
	</div>
	<div data-src="images/slide03.jpg">
		<div class="camera_caption fadeIn">
			<div class="txt1">keep your home clean</div>								
		</div>     
	</div>
	<div data-src="images/slide04.jpg">
		<div class="camera_caption fadeIn">
			<div class="txt1">keep your home clean</div>								
		</div>     
	</div>
	<div data-src="images/slide05.jpg">
		<div class="camera_caption fadeIn">
			<div class="txt1">keep your home clean</div>								
		</div>     
	</div>
</div>	
</div>	
</div>--%>


</div>	
</div>	
</div>	
</div>	
</div>	
</div>
<div id="inner">

<div id="content">
<div class="container">
<div class="row">
<div class="span12">

<%--<div class="phone2">
	<div class="txt1">Product Master</div>
</div>--%><%--
<div class="breadcrumbs1"><a href="Main.aspx?usr=<%=Request.QueryString("usr") %>">Home Page</a><span></span>Request New Master</div>
--%>

<div class="breadcrumbs1">
<a href="Main.aspx?usr=<%=Request.QueryString("usr") %>">Home Page</a>
<span></span>

<a href="NewMaster_Type.aspx?usr=<%=Request.QueryString("usr") %>">Request New Master</a>
<span></span>
Machine
</div>

<div class="row">
<div class="span8">

<h2>ตั้งรหัสสินค้าใหม่ : Machine</h2>

</div>	

</div>


            <div class="banners">
            <div class="row">

   <table bgcolor="#CCCCCC">
   
   
 

   
    <tr>
        <td class="style13">

            <asp:Label ID="Label135" runat="server" Text="Doc No." Font-Size="Medium" 
                Width="200px" Font-Bold="True"></asp:Label>
            </td>
        <td class="style13" colspan="3">

      
            <asp:Label ID="lblRequestID" runat="server" Text="ID" Font-Size="Medium" 
                Width="200px" Font-Bold="True" ForeColor="#0000CC"></asp:Label>
            </td>
        <td class="style14" rowspan="5">

           
            &nbsp;</td>
        <td colspan="4" class="style14" rowspan="7">

            <asp:Image ID="Image1" runat="server" Height="180px" Width="300px" 
                ForeColor="#999999" />
        </td>
    </tr>

   

   
   
    <tr>
        <td class="style13">

            <asp:Label ID="Label136" runat="server" Text="สถานะ : " Font-Size="Medium" 
                Width="200px" Font-Bold="True"></asp:Label>
            </td>
        <td class="style13" colspan="3">

      
            <asp:Label ID="lblStatus" runat="server" Text="status" Font-Size="Medium" 
                Width="300px" Font-Bold="True" ForeColor="#009900"></asp:Label>
            </td>
    </tr>

   

   
   
    <tr>
        <td class="style13">

            <asp:Label ID="Label1" runat="server" Text="บริษัท : " Font-Size="Medium" 
                Width="200px" Font-Bold="True"></asp:Label>
            </td>
        <td class="style13" colspan="3">

      
            <asp:CheckBox ID="chkKVN" runat="server" 
                Text="บจ. เค.วี.เอ็น.อิมปอร์ต เอ็กซ์ปอร์ต  (1991)" Width="250px" />
              
            </td>
    </tr>

   

   
   
    <tr>
        <td class="style13">

            &nbsp;</td>
        <td class="style13" colspan="3">

      
            <asp:CheckBox ID="chkLION" runat="server" 
                Text="บจ. ไลอ้อน ทรี-สตาร์" Width="250px" Checked="True" />

        </td>
    </tr>

   

   
   
    <tr>
        <td class="style13">

            &nbsp;</td>
        <td class="style13">

      
            <asp:Label ID="Label54" runat="server" Text="อื่นๆ ระบุ :" Font-Size="Medium" 
                Width="100px" Height="20px" style="margin-bottom: 0px"></asp:Label>

            </td>
        <td class="style14">

      
            <asp:TextBox ID="txtcompany" runat="server" Width="250px" Height="30px"></asp:TextBox>
              
        </td>
        <td class="style14">

      
            &nbsp;</td>
    </tr>

   

   
   
    <tr>
        <td class="style13">

            <asp:Label ID="Label2" runat="server" Text="Requested by : " Font-Size="Medium" 
                Font-Bold="True"></asp:Label>
            </td>
        <td class="style13" colspan="3">

      
            <asp:DropDownList ID="drpRequester" runat="server" Width="350px" 
                AutoPostBack="True">
            </asp:DropDownList>
              
            </td>
        <td class="style14">

           
            &nbsp;</td>
    </tr>

   

   
    <tr>
        <td class="style13">

  

            <asp:Label ID="Label3" runat="server" Text="Requested by Department : " 
                Font-Size="Medium" Font-Bold="True"></asp:Label>
              
            </td>
        <td class="style13" colspan="3">

  

            <asp:TextBox ID="txtDep" runat="server" Width="340px"></asp:TextBox>
              
            </td>
        <td class="style14">

           
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</td>
    </tr>

   

   
    <tr>
        <td class="style13">

  

            <asp:Label ID="Label5" runat="server" Text="Requested Date :" 
                Font-Size="Medium" Font-Bold="True" Height="20px"></asp:Label>
              
            </td>
        <td class="style13" colspan="3">

  

             <asp:TextBox ID="txtRequestDate" runat="server" class="form-control" 
                 placeholder="yyyy-MM-DD" ClientIDMode="Static" 
                            Width="150px" Height="30px" ></asp:TextBox>
              
            </td>
        <td class="style14">

           
            &nbsp;</td>
        <td class="style14">

           

            <asp:FileUpload ID="FileUpload1" runat="server" CssClass="sample" 
                style="font-family: Tahoma" />

             </td>
        <td class="style14">

  

             &nbsp;</td>
        <td>



             <asp:TextBox ID="txtpic" runat="server" 
                 BorderWidth="0px" Width="16px" BackColor="#DADADA" BorderColor="#DADADA"></asp:TextBox>
        </td>
        <td>


             &nbsp;</td>
    </tr>



   
    <tr>
        <td class="style13">

  

            &nbsp;</td>
        <td class="style13" colspan="3">

  

             &nbsp;</td>
        <td class="style14">

           
            &nbsp;</td>
        <td class="style14">

           
             &nbsp;</td>
        <td class="style14">

  

            &nbsp;</td>
        <td>



            &nbsp;</td>
        <td>


             &nbsp;</td>
    </tr>


   </table>
   <table>
   

    <tr>
        <td class="style8">
            <br />
            <asp:Label ID="Label6" runat="server" 
                Text="Requested For :" Font-Size="Medium" 
                Width="200px" Font-Bold="True"></asp:Label>
        </td>
        <td>

           

            <asp:RadioButtonList ID="rdoFor" runat="server" RepeatColumns="4" 
                Width="450px" Font-Names="tahoma">
                  <asp:ListItem Selected="True">Set Promotion</asp:ListItem>
                  <asp:ListItem>Buy New Product</asp:ListItem>           
                  <asp:ListItem>Interco Purchase</asp:ListItem>
                  <asp:ListItem Value="Other">, please spcify :</asp:ListItem>

            </asp:RadioButtonList>

        </td>
        <td>

           

            <asp:Label ID="Label9" runat="server" Text=" please spcify :" Width="100px"></asp:Label>

        </td>
        <td>

           

            <asp:TextBox ID="txtObject2" runat="server" Width="200px" Height="30px"></asp:TextBox>


        </td>
    </tr>

  
   
   </table>

   <table>

    <tr>
        <td class="style1">
            <asp:Label ID="Label43" runat="server" Text="Requested Objective :" 
                Font-Size="Medium" Font-Bold="True" Width="210px"></asp:Label></td>
        <td align ="rigth" width="100%" class="style1">


            <asp:RadioButtonList ID="rdoObjective" runat="server" RepeatColumns="3" 
                Width="350px" Font-Names="tahoma">
                 <asp:ListItem Selected="True">For Sell</asp:ListItem>
                  <asp:ListItem>For Demo</asp:ListItem>
            </asp:RadioButtonList>


        </td>
    </tr>

    <tr>
        <td class="style8">
           
            &nbsp;</td>
        <td align ="rigth">
          
            &nbsp;</td>
    </tr>
       </table>


    <table>
    
 
    <tr>
        <td class="style8">
           
            <asp:Label ID="Label102" runat="server" Text="Manufacturer Name :" 
                Font-Size="Medium" Font-Bold="True" Width="200px"></asp:Label></td>
        <td align ="rigth" class="style6">
          
            <asp:TextBox ID="txtmanu" runat="server" Width="800px" Height="30px"></asp:TextBox>
            </td>
    </tr>

 
    <tr>
        <td class="style8">
           
            <asp:Label ID="Label122" runat="server" Text="Product Name :" 
                Font-Size="Medium" Font-Bold="True" Width="200px"></asp:Label></td>
        <td align ="rigth" class="style6">
          
            <asp:TextBox ID="txtProductname" runat="server" Width="800px" Height="30px"></asp:TextBox>
            </td>
    </tr>

 

    </table>
    
    <table>
   
    
   
    
    <tr>
        <td class="style3">
           
            <asp:Label ID="Label123" runat="server" Text="Unit Measure :" 
                Font-Size="Medium" Font-Bold="True" Width="200px"></asp:Label>
            </td>
        <td class="style3">


            <asp:RadioButtonList ID="rdoUnit" runat="server" RepeatColumns="6" 
                Width="450px" Font-Names="tahoma">
                 <asp:ListItem Selected="True">carton/case</asp:ListItem>
                  <asp:ListItem>box</asp:ListItem>
                  <asp:ListItem>bottle</asp:ListItem>
                   <asp:ListItem>piece</asp:ListItem>
                  <asp:ListItem>unit</asp:ListItem>
                  <asp:ListItem>Other</asp:ListItem>
            </asp:RadioButtonList>


        </td>
        <td class="style3">


            <asp:Label ID="Label124" runat="server" Text="please spcify :"></asp:Label>


        </td>
        <td class="style3">


            <asp:TextBox ID="txtunit" runat="server" Width="200px" Height="30px"></asp:TextBox>


        </td>
    </tr>
  
    
  
    
    <tr>
        <td class="style3">
            &nbsp;</td>
        <td class="style3" colspan="3">


            &nbsp;</td>
    </tr>
  
    
  
    
    <tr>
        <td class="style3">
           
            <asp:Label ID="Label125" runat="server" Text="Product Group :" 
                Font-Size="Medium" Font-Bold="True" Width="200px"></asp:Label>
            </td>
        <td class="style3">


            <asp:RadioButtonList ID="rdoproductgroup" runat="server" RepeatColumns="3" 
                Width="250px" Font-Names="tahoma">
                 <asp:ListItem Selected="True">Machine</asp:ListItem>
                  <asp:ListItem>Part/Spare part</asp:ListItem>
                  <asp:ListItem>Other</asp:ListItem>
            </asp:RadioButtonList>


        </td>
        <td class="style7">


            <asp:Label ID="Label126" runat="server" Text="please spcify :" 
                Width="90px"></asp:Label>


        </td>
        <td class="style3">


            <asp:TextBox ID="txtProductgroup" runat="server" Width="200px" Height="30px"></asp:TextBox>


        </td>
    </tr>
  
    
  
    
  
   
    <tr>
        <td class="style7">
            &nbsp;</td>
        <td class="style17">

            &nbsp;</td>
        <td class="style4">

            &nbsp;</td>
        <td class="style17">

            &nbsp;</td>
    </tr>
      </table>
  <table>

    
    <tr>
        <td class="style8">
           
            <asp:Label ID="Label133" runat="server" Text="Information in Mac5" 
                Font-Size="Medium" Font-Bold="True" Width="200px" Font-Underline="True"></asp:Label>
            </td>
        <td class="style18">
            &nbsp;</td>
        <td class="style4">
            &nbsp;</td>
        <td align =rigth> 
            &nbsp;</td>
    </tr>
  
    
  
    
    <tr>
        <td class="style8">
           
            <asp:Label ID="Label132" runat="server" Text="Product Code :" 
                Font-Size="Medium" Font-Bold="True" Width="200px"></asp:Label>
            </td>
        <td class="style18">


            <asp:TextBox ID="txtitem" runat="server" Width="200px" Height="30px"></asp:TextBox>


        </td>
        <td class="style4">
            &nbsp;</td>
        <td align =rigth> 
            &nbsp;</td>
    </tr>
  
    
  
    
    <tr>
        <td class="style8">
           
            <asp:Label ID="Label131" runat="server" Text="Product Name :" 
                Font-Size="Medium" Font-Bold="True" Width="200px"></asp:Label>
            </td>
        <td class="style18" colspan="3">


            <asp:TextBox ID="txtItemDescription" runat="server" Width="800px" Height="30px"></asp:TextBox>


        </td>
    </tr>
  
    
  
    
    <tr>
        <td class="style8">
            <asp:Label ID="Label33" runat="server" Text="Product Type  : " 
                Font-Bold="True" Height="40px" Font-Size="Medium"></asp:Label>
            </td>
        <td class="style18">
            <asp:DropDownList ID="drpProductType" runat="server" Width="300px" 
                AutoPostBack="True" Height="35px" Font-Size="Medium">
            </asp:DropDownList>
        </td>
        <td class="style4" align ="rigth">
            <asp:Label ID="Label34" runat="server" Text="Product Category  : " 
                Font-Bold="True" Height="40px" Font-Italic="False" Font-Size="Medium" 
                Width="180px"></asp:Label>
            </td>
        <td align ="rigth"> 
            <asp:DropDownList ID="drpProductCat" runat="server" Width="300px" 
                AutoPostBack="True" Height="35px" Font-Size="Medium">
            </asp:DropDownList>
        </td>
    </tr>
  

    <tr>
        <td class="style8">
            <asp:Label ID="Label35" runat="server" 
                Text="Product Group :" Font-Bold="True" Width="200px" 
                Height="40px" Font-Size="Medium"></asp:Label>
            </td>
        <td class="style18">
            <asp:DropDownList ID="drpProductGroup" runat="server" Width="300px" 
                Height="35px" Font-Size="Medium">
            </asp:DropDownList>
        </td>
        <td class="style4" align ="rigth">
            <asp:Label ID="Label127" runat="server" 
                Text="Brand :" Font-Bold="True" Width="200px" 
                Height="40px" Font-Size="Medium"></asp:Label>
            </td>
        <td align =rigth> 
            <asp:DropDownList ID="drpBrand" runat="server" Width="300px" Height="35px" 
                Font-Size="Medium">
            </asp:DropDownList>
        </td>
    </tr>
  
    
  
   
 <%--   <tr>
        <td class="style8">
            <asp:Label ID="Label128" runat="server" 
                Text="Product Detail :" Font-Bold="True" Width="200px" 
                Height="40px" Font-Size="Medium"></asp:Label>
            </td>
        <td class="style18">
          
            <asp:TextBox ID="txtProductDetail" runat="server" Width="400px" Height="30px"></asp:TextBox>
        </td>
        <td class="style4">
            &nbsp;</td>
        <td align =rigth> 
            &nbsp;</td>
    </tr>--%>
 
    <tr>
        <td class="style8">
            <asp:Label ID="Label129" runat="server" 
                Text="Model :" Font-Bold="True" Width="200px" 
                Height="40px" Font-Size="Medium"></asp:Label>
            </td>
        <td class="style18">
            <asp:DropDownList ID="drpModel" runat="server" Width="300px" Height="35px" 
                Font-Size="Medium">
            </asp:DropDownList>
        </td>
        <td class="style4" align ="rigth"> 
            <asp:Label ID="Label130" runat="server" 
                Text="Color :" Font-Bold="True" Width="100px" 
                Height="40px" Font-Size="Medium"></asp:Label>
            </td>
        <td align =rigth> 
            <asp:DropDownList ID="drpColor" runat="server" Width="300px" Height="35px" 
                Font-Size="Medium">
            </asp:DropDownList>
        </td>
    </tr>
  

  
   
    <tr>
        <td class="style8">
            <asp:Label ID="Label134" runat="server" 
                Text="Remark :" Font-Bold="True" Width="200px" 
                Height="40px" Font-Size="Medium"></asp:Label>
            </td>
        <td class="style18" colspan="3">


            <asp:TextBox ID="txtremark" runat="server" Width="800px" Height="50px" 
                TextMode="MultiLine"></asp:TextBox>


        </td>
    </tr>
  
    
  
   
    <tr>
        <td class="style8">
            &nbsp;</td>
        <td class="style18">
            &nbsp;</td>
        <td class="style4">
            &nbsp;</td>
        <td align =rigth> 
            &nbsp;</td>
    </tr>
  
    
  
   
    </table>

    <table>
    
    <tr>
        <td class="style8">
            &nbsp;</td>
        <td class="style4">

        <asp:Button ID="btnSave" runat="server" Text="Save" class="btn btn-primary" 
                BackColor="#003399" BorderColor="#0066FF" Font-Bold="True" Font-Size="Medium" 
                ForeColor="White" Height="35px" Width="100px"/>


  
                &nbsp;<asp:Button ID="btnSubmit" runat="server" Text="Submit" class="btn btn-primary" 
                BackColor="#003399" BorderColor="#0066FF" Font-Bold="True" Font-Size="Medium" 
                ForeColor="White" Height="35px" Width="100px"/>
                
  
                &nbsp;<asp:Button ID="btnCancel" runat="server" Text="Cancel" class="btn btn-primary" 
                BackColor="#003399" BorderColor="#0066FF" Font-Bold="True" Font-Size="Medium" 
                ForeColor="White" Width="100px" Height="35px"/>
        &nbsp;<asp:Button ID="btnApprove" runat="server" Text="Account Approve" class="btn btn-primary" 
                BackColor="#003399" BorderColor="#0066FF" Font-Bold="True" Font-Size="Medium" 
                ForeColor="White" Height="35px" Width="200px"/>
                
  
                &nbsp;<asp:Button ID="btnReport" runat="server" Text="Print Form" 
                class="btn btn-primary"  OnClientClick = "SetTarget();"
                BackColor="#003399" BorderColor="#0066FF" Font-Bold="True" Font-Size="Medium" 
                ForeColor="White" Height="35px"/>

  
  
                </td>
       
    </tr>
    </table>
            </div>	
            </div>


</div>	
</div>	
</div>	
</div>

<div class="bot1">
<div class="container">
<div class="row">
<div class="span12">
<div class="bot1_inner">
<div class="row">
<div class="span4">
	
<div class="logo2_wrapper"><a href="index.html" class="logo2"><img src="images/aroma/logotrans.png" alt=""></a></div>

<footer><div class="copyright">Copyright   © 2018. All rights reserved.<br><a href="#">Aroma Group</a></div></footer>


</div>
<div class="span4">
	
<div class="bot1_title">Contact Us</div>

สำนักงานใหญ่ อโรม่า กรุ๊ป<br>
บริษัท เค.วี.เอ็น.อิมปอร์ต เอกซ์ปอร์ต (1991) จำกัด<br>
เลขที่ 43 ชั้น 2 ซอยนาคนิวาส 6 ถนนนาคนิวาส แขวงลาดพร้าว <br>
เขตลาดพร้าว กรุงเทพฯ 10230<br>
โทรศัพท์ : 02 159-8999<br>
โทรสาร : 02 538-8144, 02 539-5597

<%--E-mail: <a href="#">mail@demosite.com</a>--%>


</div>
<div class="span4">
	
<%--<div class="bot1_title">Follow Us:</div>	

<div class="social_wrapper">
		<ul class="social clearfix">
	    <li><a href="#"><img src="images/social_ic1.png"></a></li>
	    <li><a href="#"><img src="images/social_ic2.png"></a></li>
	    <li><a href="#"><img src="images/social_ic3.png"></a></li>	    
	    <li><a href="#"><img src="images/social_ic4.png"></a></li>
		</ul>
	</div>
--%>

</div>	

</div>	
</div>
</div>	
</div>	
</div>	
</div>

</div>	
</div>
<script type="text/javascript" src="js/bootstrap.js"></script>



 <%--date--%>
               <script src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.7.min.js"></script>
                    <script src="http://ajax.aspnetcdn.com/ajax/jquery.ui/1.8.16/jquery-ui.min.js"></script>
                    <link type="text/css" rel="Stylesheet" href="http://ajax.microsoft.com/ajax/jquery.ui/1.8.16/themes/redmond/jquery-ui.css" />

                    <script type="text/javascript">
                        //ประกาศตัวแปรเพื่อรับใหม่
                        $jq = $.noConflict();
                        $jq().ready(

                 function () {
                     $jq('#<%=txtRequestDate.ClientID%>').datepicker(
                     {

                         dateFormat: 'yy-mm-dd',

                     }
                     );

                  
                 });     
            </script>
              <script src="https://code.jquery.com/jquery-2.1.1.min.js" type="text/javascript"></script>
            <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2-rc.1/css/select2.min.css" rel="stylesheet" />
            <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2-rc.1/js/select2.min.js" type="text/javascript"></script>
            <script type="text/javascript">
                //                $(document).ready(function () {
                //                    $("#drpProductType").select2({
                //                    });
                //                });

                //                $(document).ready(function () {
                //                    $("#drpProductCat").select2({
                //                    });
                //                });

                //                $(document).ready(function () {
                //                    $("#drpProductGroup").select2({
                //                    });
                //                });

                //                $(document).ready(function () {
                //                    $("#drpGroup").select2({
                //                    });
                //                });

                $(document).ready(function () {
                    $("#drpRequester").select2({
                    });
                });
                
            </script>  

    </div>
    </form>
</body>
</html>
