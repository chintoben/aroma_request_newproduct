﻿Imports System.Drawing
Imports System.IO
Imports System.Data.SqlClient
Imports System.Data.OleDb


Public Class NewMasterSparePart_Upload
    Inherits System.Web.UI.Page
    Dim DB_Product As New Connect_product
    Dim dt, dt1, dt2, dt3, dtt, dtRole As New DataTable
    Dim No As String
    Dim EmpID, DocID, PMS_ID, Status, usr, sql As String
    Dim db_HR As New Connect_HR

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        usr = Request.QueryString("usr")

        If Not IsPostBack Then
            'Sql = " select * "
            'Sql += " from VW_Employee "
            'Sql += " where Employee_id = '" & usr & "' "
            'dt = db_HR.GetDataTable(Sql)
            'If dt.Rows.Count > 0 Then
            drpRequester.SelectedValue = usr
            'End If

        End If

    End Sub

    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        If Attach1.Value = "" Then
            Me.Label3.Text = "Please choose file to import..!!"
            Me.Label3.ForeColor = Color.Red
            Me.Label3.Font.Bold = True
            Me.lblMissItem.Visible = False
            Exit Sub
        Else
            Me.lblMissItem.Visible = False
            ' Me.lstMissItem.Items.Clear()

            UploadXLS()

        End If
    End Sub

    '---- 2
    Private Sub UploadXLS()
        Dim sFile As HttpPostedFile = Me.Attach1.PostedFile

        'Dim sSavePath As String = Server.MapPath("~" + HttpRuntime.AppDomainAppVirtualPath + "importData")
        ' Dim sSavePath As String = "D:\!! Code Program Aroma !!\Aroma_ProductMaster\Aroma_ProductMaster\ImportFile"
        Dim sSavePath As String = "C:\Aroma_Program\Aroma_Product\Aroma_ProductMaster\Aroma_ProductMaster\ImportFile" 'C:\Import Item"

        Dim sSaveFilePath As String = ""
        If Not Directory.Exists(sSavePath) Then
            Directory.CreateDirectory(sSavePath)
        End If
        Dim strFileName = System.IO.Path.GetFileName(sFile.FileName)

        No = strFileName.Substring(0, strFileName.IndexOf("."))  'strFileName

        sSaveFilePath = sSavePath & "\" & strFileName
        If strFileName <> "" Then
            Me.Label3.Text = strFileName
            Me.Label3.ForeColor = Color.Teal
            sFile.SaveAs(sSaveFilePath)
            sSaveFilePath = sSaveFilePath.Replace("/", "\")
            GetXLS(sSaveFilePath, No)
        End If

    End Sub

    '----3
    Private Sub GetXLS(ByVal sFilename As String, ByVal DeliveryNo As String)
        Dim xls03 As String = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & sFilename & " ;Extended Properties=""Excel 8.0;HDR=YES;"""
        Dim xls07 As String = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & sFilename & " ;Extended Properties=""Excel 8.0;HDR=YES;"""
        Dim strConn As String = ""
        Dim x As Boolean = False
        Dim s As Boolean = False
        Dim cItem As Boolean = True
        Dim sSQLCheckItem As String = ""
        If Right(Me.Attach1.Value, 1) = "x" Then
            strConn = xls07
        ElseIf Right(Me.Attach1.Value, 1) = "s" Then
            strConn = xls03
        End If

        Dim connExcel As New OleDbConnection(strConn)
        Dim cmdExcel As New OleDbCommand
        Dim oda As New OleDbDataAdapter
        Dim dt1 As New DataTable
        cmdExcel.Connection = connExcel

        'string connString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=C:\\source\\SiteCore65\\Individual-Data.xls;Extended Properties=Excel 8.0;";

        'Dim constring As String = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & sFilename & ";Extended Properties=""Excel 8.0;HDR=YES;"""
        'Dim con As New OleDbConnection(constring & "")
        'con.Open()

        Dim constring As String = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & sFilename & ";Extended Properties=""Excel 8.0;HDR=YES;"""
        Dim con As New OleDbConnection(constring & "")
        con.Open()


        REM: Get the name of First Sheet
        connExcel.Open()
        Dim dtExcelSchema As DataTable
        ' dtExcelSchema = connExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, Nothing)
        dtExcelSchema = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, Nothing)


        Dim SheetName As String = ""
        Me.Label3.Text = ""
        For i As Integer = 0 To dtExcelSchema.Rows.Count - 1
            SheetName = UCase(Trim(dtExcelSchema.Rows(i)("TABLE_NAME").ToString()))
            Me.Label3.Text = Me.Label3.Text & SheetName & "^"
            If SheetName = "SHEET1$" Then s = True
        Next
        Me.Label3.Text = Me.Label3.Text & ":: s = " & s.ToString
        If s = False Then
            Me.Label3.Text = "Not found 'Sheet1'"
            Me.Label3.ForeColor = Color.Red
            Exit Sub
        End If

        REM: Read Data from First Sheet
        If connExcel.State = ConnectionState.Closed Then connExcel.Open()
        cmdExcel.CommandText = "SELECT * From [sheet1$]"
        oda.SelectCommand = cmdExcel
        oda.Fill(dt1)
        connExcel.Close()
        For i As Integer = 0 To dt1.Rows.Count - 1
            sSQLCheckItem = sSQLCheckItem & "'" & dt1.Rows(i)(4) & "'"

            If i < dt1.Rows.Count - 1 Then
                sSQLCheckItem = sSQLCheckItem & ", "
            End If
        Next

        REM: Get data from [Items]
        Dim SQLADP As SqlDataAdapter
        Dim dt3 As New DataTable
        Dim sCon As New SqlConnection(DB_Product.sqlCon)
        Dim sSQL As String = ""
        Dim sqlDT2 As String
        sSQL = "select  rtrim(item) item from  TB_ProductRequest_spare  where 1=1"
        sSQL = sSQL & "AND item IN ("
        sSQL = sSQL & sSQLCheckItem & ") "
        sSQL = sSQL & "order by item"
        SQLADP = New SqlDataAdapter(sSQL, sCon)
        'SQLADP.Fill(dt3)
        dt3 = DB_Product.GetDataTable(sSQL)


        'Me.Table2.Rows.Clear()
        'cItem = CheckItemCodes(dt1, dt3)
        'If cItem = False Then
        '    Me.Label3.Text = "No Employee ID, Please check list below"
        '    Me.Label3.ForeColor = Color.Red
        '    'Me.lblMissItem.Text = "No items, Please check list below"
        '    'btnAddItem.Visible = True
        '    Exit Sub
        'End If


        REM: Get data from [TB_ItemWH]
        Dim dt2 As New DataTable


        'sqlDT2 = "SELECT [DeliveryNo]+[ItemCode]+[Pallet]+[ItemTypeImport]+CAST( Qty AS nvarchar(20) )  as itemImport, Qty  FROM [TB_ItemImportFile] with (nolock)"
        sqlDT2 = "SELECT item  FROM [TB_ProductRequest_spare] with (nolock)"
        dt2 = DB_Product.GetDataTable(sqlDT2)
        ImportDT(dt1, dt2, DeliveryNo)


    End Sub

    '---4
    Private Sub ImportDT(ByVal dt1 As DataTable, ByVal dt2 As DataTable, ByVal DeliveryNo As String)


        Dim Company_KVN, Company_LION, Requester, RequestDate, Item, ItemGroup, PartNo, ItemDescriptionTH, ItemDescriptionEN As String
        Dim Part_No, Unit, UnitPrice, ItemType, ProductType, ProductCategory, ProductGroup, Brand, Stat As String
        Dim CreateBy As String
        Dim CreateDate As Date


        Dim u As Integer = 0
        Dim n As Integer = 0
        Dim foundRows() As DataRow
        Dim bUpdate As Boolean = False
        Dim sExpression As String = ""
        Me.Label3.Text = "ImportData"
        Me.Label3.ForeColor = Color.Teal

        For i As Integer = 0 To dt1.Rows.Count - 1

            If chkKVN.Checked = True Then
                Company_KVN = "Yes"
            Else
                Company_KVN = "No"
            End If

            If chkLION.Checked = True Then
                Company_LION = "Yes"
            Else
                Company_LION = "No"
            End If

            Requester = drpRequester.SelectedValue
            RequestDate = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss")

            ProductType = checkIsDBNull(UCase(Trim(dt1.Rows(i)(0))))
            ProductCategory = checkIsDBNull(UCase(Trim(dt1.Rows(i)(1))))
            ProductGroup = checkIsDBNull(UCase(Trim(dt1.Rows(i)(2))))
            Brand = checkIsDBNull(UCase(Trim(dt1.Rows(i)(3))))
            Item = checkIsDBNull(UCase(Trim(dt1.Rows(i)(4))))
            ItemGroup = checkIsDBNull(UCase(Trim(dt1.Rows(i)(5))))
            ItemDescriptionTH = checkIsDBNull(UCase(Trim(dt1.Rows(i)(6))))
            ItemDescriptionEN = checkIsDBNull(UCase(Trim(dt1.Rows(i)(7))))
            ItemType = checkIsDBNull(UCase(Trim(dt1.Rows(i)(8))))
            Unit = checkIsDBNull(UCase(Trim(dt1.Rows(i)(9))))
            PartNo = checkIsDBNull(UCase(Trim(dt1.Rows(i)(10))))
            UnitPrice = checkIsDBNull(UCase(Trim(dt1.Rows(i)(11))))


            CreateBy = EmpID
            CreateDate = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss")

            Dim sData As String = Company_KVN & "^" & Company_LION & "^" & Requester & "^" & RequestDate & "^" & ProductType & "^" & ProductCategory & "^" & ProductGroup & "^" & Brand & "^" & Item & "^" & ItemGroup & "^" & ItemDescriptionTH & "^" & ItemDescriptionEN & "^" & ItemType & "^" & Unit & "^" & PartNo & "^" & UnitPrice & "^"
            sExpression = "1=1 and Item = '" & Item & "'"
            foundRows = dt2.Select(sExpression)

            If foundRows.Length <= 0 Then
                InsertData(sData)

                n = n + 1
            Else
                'UpdateData(sData)  'ตัด Stock
      
                'InsertData(sData)
                u = u + 1
            End If

        Next
        Me.Label3.Text = "COMPLETE..!!! dupplicate: " & u & ",, Insert: " & n
        ' Me.Label3.Text = "COMPLETE..!!! Insert: " & n + u
        Me.Label3.ForeColor = Color.RoyalBlue

    End Sub

    Private Function checkIsDBNull(ByVal FieldValue As Object, Optional ByVal sDefaultValue As String = "")
        If IsDBNull(FieldValue) Then
            FieldValue = ""
            If sDefaultValue <> "" Then
                FieldValue = sDefaultValue
            End If
        Else
            FieldValue = Trim(FieldValue)
        End If
        Return FieldValue
    End Function

    Private Sub InsertData(ByVal sData As String)

        Dim sID As Integer = 0
        Dim sqlCMD As New SqlCommand
        Dim arrData() As String = sData.Split("^")
        Dim sSQL As String = ""
        Dim sCon As New SqlConnection(DB_Product.sqlCon)


        sSQL = "INSERT INTO TB_ProductRequest_spare("
        sSQL = sSQL & " Company_KVN,Company_LION,Requester,RequestDate,ProductType,ProductCategory,ProductGroup,Brand,Item,ItemGroup"
        sSQL = sSQL & " ,ItemDescriptionTH,ItemDescriptionEN,ItemType,Unit,PartNo,UnitPrice,Stat,CreateBy, CreateDate"
        sSQL = sSQL & ")"

        '        Dim sData As String = Company_KVN1 & "^" & Company_LION2 & "^" & 3Requester & "^" & 4RequestDate & "^" & 5ProductType & "^" & ProductCategory
        '& "^" & ProductGroup & "^" & Brand & "^" & Item & "^" & ItemGroup & "^" & ItemDescriptionTH & "^" & "^" & ItemDescriptionEN & "^" & "^" & ItemType & "^" 
        '& "^" & Unit & "^" & "^" & PartNo & "^" & "^" & UnitPrice & "^"

        sSQL = sSQL & "VALUES("
        sSQL = sSQL & "'" & arrData(0) & "','" & arrData(1) & "','" & arrData(2) & "','" & arrData(3) & "','" & arrData(4) & "','" & arrData(5) & "'"
        sSQL = sSQL & ",'" & arrData(6) & "','" & arrData(7) & "','" & arrData(8) & "','" & arrData(9) & "','" & arrData(10) & "','" & arrData(11) & "','" & arrData(12) & "','" & arrData(13) & "'"
        sSQL = sSQL & ",'" & arrData(14) & "','" & arrData(15) & "'"
        sSQL = sSQL & " ,'Draft' ,'" & usr & "','" & DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") & "'"
        sSQL = sSQL & ")"

        dtt = DB_Product.GetDataTable(sSQL)

    End Sub


    Protected Sub drpRequester_Init(sender As Object, e As EventArgs) Handles drpRequester.Init
        Dim sqlCon As New SqlConnection(db_HR.sqlCon)
        Dim sqlCmd As New SqlCommand
        Dim Rs As SqlDataReader
        Dim sql As String

        sql = "select [employee_id] as id,[fullname] from [TB_Employee] order by employee_id asc"

        If sqlCon.State = ConnectionState.Closed Then
            sqlCon = New SqlConnection(db_HR.sqlCon)
            sqlCon.Open()
        End If
        With sqlCmd
            .Connection = sqlCon
            .CommandType = CommandType.Text
            .CommandText = sql
            Rs = .ExecuteReader
        End With
        drpRequester.Items.Clear()
        drpRequester.Items.Add(New ListItem("", 0))
        While Rs.Read
            drpRequester.Items.Add(New ListItem(Rs.GetString(0) + " : " + Rs.GetString(1), Rs.GetString(0)))
        End While
    End Sub


End Class