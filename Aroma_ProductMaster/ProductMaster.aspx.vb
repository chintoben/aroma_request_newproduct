﻿Imports System.Data.SqlClient

Public Class ProductMaster
    Inherits System.Web.UI.Page
    Dim db_Mac5 As New Connect_Mac5
    Dim username As String
    Dim DB_Product As New Connect_product

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        username = Request.QueryString("usr")

        If Not IsPostBack Then

            BindData()

        End If


    End Sub

    Protected Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        BindData()
    End Sub

    Private Sub BindData()

        Dim IntPageNumber As Int32 = 0
        Dim sqlcon As New SqlConnection(db_Mac5.sqlCon)
        Dim cmd As New SqlCommand
        Dim DSet As New DataSet
        Dim sql As String
        Dim constr As String

        Try
            constr = "data source=10.0.4.211;initial catalog=M5CM-AA-09;persist security info=false;User ID=mac5usr;Password=mac5usr_!@#;Connect Timeout=0;Max Pool Size=500;Enlist=true"
            sqlcon = New SqlConnection(constr)
            sqlcon.Open()
            'Return True
            'ถ้าเกิด Error ขึ้นให้ใช้ 
        Catch
            Try
                constr = "data source=10.0.4.211;initial catalog=M5CM-AA-09;persist security info=false;User ID=mac5usr;Password=mac5usr_!@#;Connect Timeout=0;Pooling=false"
                sqlcon = New SqlConnection(constr)
                sqlcon.Open()
                'Return True
            Catch
                'Return False
            End Try

        End Try
        Try

            sql = "select '" & username & "' as username,* " & vbCrLf
            sql = sql & " from VW_ProductMasterGroup  "
            sql = sql & " where 1=1  "

            If chkCompany.Items.FindByValue("kvn").Selected = True And chkCompany.Items.FindByValue("lion").Selected = False Then
                sql = sql & " and Company = 'KVN' "
            ElseIf chkCompany.Items.FindByValue("kvn").Selected = False And chkCompany.Items.FindByValue("lion").Selected = True Then
                sql = sql & " and Company = 'LION' "
            ElseIf chkCompany.Items.FindByValue("kvn").Selected = False And chkCompany.Items.FindByValue("lion").Selected = False Then
                sql = sql & " and Company = '' "
            End If

            If txtItem.Text <> "" Then
                sql = sql & " and [Item] like '%" & txtItem.Text & "%' "
            End If

            If txtItemDesc.Text <> "" Then
                sql = sql & " and [ItemDescription] like '%" & txtItemDesc.Text & "%' "
            End If

            If txtBrand.SelectedValue <> "0" Then
                sql = sql & " and [Brand] like '%" & txtBrand.SelectedValue & "%' "
            End If


            If drpProductType.SelectedValue <> "0" Then
                sql = sql & " and [ProductType] = '" & drpProductType.SelectedValue & "' "
            End If

            If drpProductCat.SelectedValue <> "0" Then
                sql = sql & " and [ProductCategory] = '" & drpProductCat.SelectedValue & "' "
            End If

            If drpProductGroup.SelectedValue <> "0" Then
                sql = sql & " and [ProductGroup] = '" & drpProductGroup.SelectedValue & "' "
            End If

            If drpGroup.SelectedValue <> "0" Then
                sql = sql & " and [GroupHead] = '" & drpGroup.SelectedValue & "' "
            End If

            If txtColor.SelectedValue <> "0" Then
                sql = sql & " and [Color] like '%" & txtColor.SelectedValue & "%' "
            End If

            'If txtOption1.Text <> "" Then
            '    sql = sql & " and [Option1] like '%" & txtOption1.Text & "%' "
            'End If

            'If txtOption2.Text <> "" Then
            '    sql = sql & " and [Option2] like '%" & txtOption2.Text & "%' "
            'End If

            'If txtOption3.Text <> "" Then
            '    sql = sql & " and [Option3] like '%" & txtOption3.Text & "%' "
            'End If

            'If drpChaodoi.SelectedValue <> "" Then
            '    sql = sql & " and [Check1] = '" & drpChaodoi.SelectedValue & "' "
            'End If

            'If chkCheckRawMaterial.Checked = True Then
            '    sql = sql & " and CheckRawMaterial = 'Y' "
            'End If

            'If chkCheckMachine.Checked = True Then
            '    sql = sql & " and CheckMachine = 'Y' "
            'End If


            Dim dt As New DataTable
            dt = db_Mac5.GetDataTable(sql)
            If dt.Rows.Count > 0 Then
                GridView.DataSource = dt
                GridView.DataBind()
            Else
                ShowNoResultFound(dt, GridView)
            End If

        Catch


        End Try
        sqlcon.Close()

        'Gridview1
        'Attribute to show the Plus Minus Button.
        GridView.HeaderRow.Cells(0).Attributes("data-class") = "expand"
        'Attribute to hide column in Phone.
        GridView.HeaderRow.Cells(2).Attributes("data-hide") = "phone"
        GridView.HeaderRow.Cells(3).Attributes("data-hide") = "phone"
        'Adds THEAD and TBODY to GridView.
        GridView.HeaderRow.TableSection = TableRowSection.TableHeader

    End Sub

    Protected Sub OnPaging(ByVal sender As Object, ByVal e As GridViewPageEventArgs)
        Me.BindData()
        GridView.PageIndex = e.NewPageIndex
        GridView.DataBind()
    End Sub

    Sub ShowNoResultFound(ByVal dtt As DataTable, ByVal gv As GridView)
        dtt.Rows.Add(dtt.NewRow)
        gv.DataSource = dtt
        gv.DataBind()
        Dim columnsCount As Integer = gv.Columns.Count
        gv.Rows(0).Cells.Clear()
        gv.Rows(0).Cells.Add(New TableCell)
        gv.Rows(0).Cells(0).ColumnSpan = columnsCount
        gv.Rows(0).Cells(0).HorizontalAlign = HorizontalAlign.Center
        gv.Rows(0).Cells(0).ForeColor = Drawing.Color.Red
        gv.Rows(0).Cells(0).Font.Bold = True
        gv.Rows(0).Cells(0).Text = "-ไม่พบข้อมูล-"
    End Sub

    Protected Sub drpProductType_Init(sender As Object, e As EventArgs) Handles drpProductType.Init
        Dim sqlCon As New SqlConnection(db_Mac5.sqlCon)
        Dim sqlCmd As New SqlCommand
        Dim Rs As SqlDataReader
        Dim sql As String

        'sql = "select isnull(ProductType,'')  from [TB_ProductMasterGroup]  group by  [ProductType] "
        sql = "select x.* from(select isnull(ProductType,'') as  ProductType   from [TB_ProductMasterGroupRequest]  group by  [ProductType]  "
        sql += " union all select isnull(ProductType,'') as  ProductType   from [TB_ProductMasterGroup]  group by  [ProductType])x"
        sql += " group by x.ProductType order by x.ProductType"

        If sqlCon.State = ConnectionState.Closed Then
            sqlCon = New SqlConnection(db_Mac5.sqlCon)
            sqlCon.Open()
        End If
        With sqlCmd
            .Connection = sqlCon
            .CommandType = CommandType.Text
            .CommandText = sql
            Rs = .ExecuteReader
        End With
        drpProductType.Items.Clear()
        drpProductType.Items.Add(New ListItem("", 0))
        While Rs.Read
            drpProductType.Items.Add(New ListItem(Rs.GetString(0), Rs.GetString(0)))
        End While
    End Sub

    Protected Sub drpProductCat_Init(sender As Object, e As EventArgs) Handles drpProductCat.Init
        Dim sqlCon As New SqlConnection(db_Mac5.sqlCon)
        Dim sqlCmd As New SqlCommand
        Dim Rs As SqlDataReader
        Dim sql As String

        'sql = "select isnull(ProductCategory,'')  from [TB_ProductMasterGroup]  group by  [ProductCategory] "

        sql = "select x.* from(select isnull(ProductCategory,'') as ProductCategory from [TB_ProductMasterGroupRequest]  group by  [ProductCategory]  "
        sql += " union all select isnull(ProductCategory,'')  from [TB_ProductMasterGroup]  group by  [ProductCategory])x"
        sql += " group by x.ProductCategory order by x.ProductCategory"

        If sqlCon.State = ConnectionState.Closed Then
            sqlCon = New SqlConnection(db_Mac5.sqlCon)
            sqlCon.Open()
        End If
        With sqlCmd
            .Connection = sqlCon
            .CommandType = CommandType.Text
            .CommandText = sql
            Rs = .ExecuteReader
        End With
        drpProductCat.Items.Clear()
        drpProductCat.Items.Add(New ListItem("", 0))
        While Rs.Read
            drpProductCat.Items.Add(New ListItem(Rs.GetString(0), Rs.GetString(0)))
        End While
    End Sub

    Protected Sub drpProductGroup_Init(sender As Object, e As EventArgs) Handles drpProductGroup.Init
        Dim sqlCon As New SqlConnection(db_Mac5.sqlCon)
        Dim sqlCmd As New SqlCommand
        Dim Rs As SqlDataReader
        Dim sql As String

        'sql = "select isnull(ProductGroup,'')  from [TB_ProductMasterGroup]  group by  [ProductGroup] "
        sql = " select x.* from(select isnull(ProductGroup,'') as ProductGroup   from [TB_ProductMasterGroupRequest]  group by  [ProductGroup]  "
        sql += " union all select isnull(ProductGroup,'')  from [TB_ProductMasterGroup]  group by  [ProductGroup] )x"
        sql += " group by x.ProductGroup order by x.ProductGroup"

        If sqlCon.State = ConnectionState.Closed Then
            sqlCon = New SqlConnection(db_Mac5.sqlCon)
            sqlCon.Open()
        End If
        With sqlCmd
            .Connection = sqlCon
            .CommandType = CommandType.Text
            .CommandText = sql
            Rs = .ExecuteReader
        End With
        drpProductGroup.Items.Clear()
        drpProductGroup.Items.Add(New ListItem("", 0))
        While Rs.Read
            drpProductGroup.Items.Add(New ListItem(Rs.GetString(0), Rs.GetString(0)))
        End While
    End Sub

    Protected Sub drpGroup_Init(sender As Object, e As EventArgs) Handles drpGroup.Init
        Dim sqlCon As New SqlConnection(db_Mac5.sqlCon)
        Dim sqlCmd As New SqlCommand
        Dim Rs As SqlDataReader
        Dim sql As String

        sql = "select isnull(GroupHead,'') as GroupHead from [TB_ProductMasterGroup]  group by  isnull(GroupHead,'') "

        If sqlCon.State = ConnectionState.Closed Then
            sqlCon = New SqlConnection(db_Mac5.sqlCon)
            sqlCon.Open()
        End If
        With sqlCmd
            .Connection = sqlCon
            .CommandType = CommandType.Text
            .CommandText = sql
            Rs = .ExecuteReader
        End With
        drpGroup.Items.Clear()
        drpGroup.Items.Add(New ListItem("", 0))
        While Rs.Read
            drpGroup.Items.Add(New ListItem(Rs.GetString(0), Rs.GetString(0)))
        End While
    End Sub

    'Protected Sub btnExport_Click(sender As Object, e As EventArgs) Handles btnExport.Click
    '    Dim sqlReport As String = " 1=1 "

    '    'If drpProvince.Text <> "1" Then
    '    '    sqlReport += " and {ReportData.BranchProvince} = '" & Trim(drpProvince.Text) & "'"
    '    'End If

    '    If chkCompany.Items.FindByValue("kvn").Selected = True And chkCompany.Items.FindByValue("lion").Selected = False Then
    '        sqlReport = sqlReport & " and {ReportData.Company} = 'KVN' "
    '    ElseIf chkCompany.Items.FindByValue("kvn").Selected = False And chkCompany.Items.FindByValue("lion").Selected = True Then
    '        sqlReport = sqlReport & " and {ReportData.Company} = 'LION' "
    '    ElseIf chkCompany.Items.FindByValue("kvn").Selected = False And chkCompany.Items.FindByValue("lion").Selected = False Then
    '        sqlReport = sqlReport & " and {ReportData.Company} = '' "
    '    End If

    '    If txtItem.Text <> "" Then
    '        sqlReport = sqlReport & " and {ReportData.Item] like '%" & txtItem.Text & "%' "
    '    End If

    '    If txtItemDesc.Text <> "" Then
    '        sqlReport = sqlReport & " and {ReportData.ItemDescription} like '%" & txtItemDesc.Text & "%' "
    '    End If

    '    If txtBrand.Text <> "" Then
    '        sqlReport = sqlReport & " and {ReportData.Brand} like '%" & txtBrand.Text & "%' "
    '    End If

    '    If txtModel.Text <> "" Then
    '        sqlReport = sqlReport & " and {ReportData.Model} like '%" & txtModel.Text & "%' "
    '    End If

    '    If drpProductType.SelectedValue <> "0" Then
    '        sqlReport = sqlReport & " and {ReportData.ProductType} = '" & drpProductType.SelectedValue & "' "
    '    End If

    '    If drpProductCat.SelectedValue <> "0" Then
    '        sqlReport = sqlReport & " and {ReportData.ProductCategory} = '" & drpProductCat.SelectedValue & "' "
    '    End If

    '    If drpProductGroup.SelectedValue <> "0" Then
    '        sqlReport = sqlReport & " and {ReportData.ProductGroup} = '" & drpProductGroup.SelectedValue & "' "
    '    End If

    '    If drpGroup.SelectedValue <> "0" Then
    '        sqlReport = sqlReport & " and {ReportData.GroupHead} = '" & drpGroup.SelectedValue & "' "
    '    End If

    '    If txtColor.Text <> "" Then
    '        sqlReport = sqlReport & " and {ReportData.Color} like '%" & txtColor.Text & "%' "
    '    End If

    '    'If txtOption1.Text <> "" Then
    '    '    sqlReport = sqlReport & " and [Option1} like '%" & txtOption1.Text & "%' "
    '    'End If

    '    'If txtOption2.Text <> "" Then
    '    '    sqlReport = sqlReport & " and [Option2} like '%" & txtOption2.Text & "%' "
    '    'End If

    '    'If txtOption3.Text <> "" Then
    '    '    sqlReport = sqlReport & " and [Option3} like '%" & txtOption3.Text & "%' "
    '    'End If

    '    'If drpChaodoi.SelectedValue <> "" Then
    '    '    sqlReport = sqlReport & " and {ReportData.Check1} = '" & drpChaodoi.SelectedValue & "' "
    '    'End If

    '    'If chkCheckRawMaterial.Checked = True Then
    '    '    sqlReport = sqlReport & " and {ReportData.CheckRawMaterial = 'Y' "
    '    'End If

    '    'If chkCheckMachine.Checked = True Then
    '    '    sqlReport = sqlReport & " and {ReportData.CheckMachine = 'Y' "
    '    'End If

    '    Server.Transfer("LoadReport_Excel.aspx?Form=Product&sqlReport=" & sqlReport)

    'End Sub

    Protected Sub txtBrand_Init(sender As Object, e As EventArgs) Handles txtBrand.Init
        Dim sqlCon As New SqlConnection(DB_Product.sqlCon)
        Dim sqlCmd As New SqlCommand
        Dim Rs As SqlDataReader
        Dim sql As String


        sql = "SELECT [Brand] FROM [dbo].[TB_ProductBrand] order by Brand "


        If sqlCon.State = ConnectionState.Closed Then
            sqlCon = New SqlConnection(DB_Product.sqlCon)
            sqlCon.Open()
        End If
        With sqlCmd
            .Connection = sqlCon
            .CommandType = CommandType.Text
            .CommandText = sql
            Rs = .ExecuteReader
        End With
        txtBrand.Items.Clear()
        txtBrand.Items.Add(New ListItem("", 0))
        While Rs.Read
            txtBrand.Items.Add(New ListItem(Rs.GetString(0), Rs.GetString(0)))
        End While
    End Sub

    Protected Sub txtColor_Init(sender As Object, e As EventArgs) Handles txtColor.Init
        Dim sqlCon As New SqlConnection(DB_Product.sqlCon)
        Dim sqlCmd As New SqlCommand
        Dim Rs As SqlDataReader
        Dim sql As String


        sql = "SELECT [color] FROM [dbo].[TB_ProductColor] order by color "


        If sqlCon.State = ConnectionState.Closed Then
            sqlCon = New SqlConnection(DB_Product.sqlCon)
            sqlCon.Open()
        End If
        With sqlCmd
            .Connection = sqlCon
            .CommandType = CommandType.Text
            .CommandText = sql
            Rs = .ExecuteReader
        End With
        txtColor.Items.Clear()
        txtColor.Items.Add(New ListItem("", 0))
        While Rs.Read
            txtColor.Items.Add(New ListItem(Rs.GetString(0), Rs.GetString(0)))
        End While
    End Sub

End Class